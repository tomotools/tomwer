try:
    from scipy.datasets import ascent
except ImportError:
    from scipy.misc import ascent
from silx.gui import qt
from tomwer.gui.utils.vignettes import VignettesQDialog
from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore


app = qt.QApplication([])

widget = VignettesQDialog("cor", "score")

data = ascent()
scores = {i: (data, ComputedScore(tv=1, std=1 / (i + 1))) for i in range(20)}
widget.setScores(
    scores=scores,
    score_method="standard deviation",
)

widget.show()
app.exec()
