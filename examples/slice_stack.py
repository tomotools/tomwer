import os
import numpy
import tempfile
from silx.gui import qt

from tomwer.core.utils.scanutils import MockNXtomo

from tomwer.core.volume.hdf5volume import HDF5Volume
from tomwer.core.reconstruction.output import (
    PROCESS_FOLDER_RECONSTRUCTED_SLICES,
)
from tomwer.gui.stacks import SliceStack


def create_vol_data(n_slice=1):
    return numpy.random.random(100 * 100 * n_slice).reshape(n_slice, 100, 100)


def create_vol_metadata():
    return {
        "nabu_config": {
            "reconstruction": {
                "method": "FBP",
            },
            "phase": {
                "method": numpy.random.choice(("", "paganin")),
                "delta_beta": numpy.random.uniform(10.0, 100.0),
            },
        },
        "processing_options": {
            "reconstruction": {
                "voxel_size_cm": (0.2, 0.2, 0.2),
                "rotation_axis_position": 104,
                "enable_halftomo": True,
                "fbp_filter_type": numpy.random.choice(("hilbert", "ramlak", "cosine")),
                "sample_detector_dist": 0.4,
            },
            "take_log": {
                "log_min_clip": numpy.random.uniform(1.0, 5.0),
                "log_max_clip": numpy.random.uniform(50.0, 200.0),
            },
        },
    }


app = qt.QApplication([])
widget = SliceStack()

with tempfile.TemporaryDirectory() as tmp_folder:
    # step 1: test adding some scans with reconstructions
    for i_scan in range(3):
        scan = MockNXtomo(
            scan_path=os.path.join(tmp_folder, f"scan{i_scan}"),
            n_proj=10,
            n_ini_proj=10,
            scan_range=180,
            dim=10,
        ).scan
        volume = HDF5Volume(
            file_path=os.path.join(
                scan.path,
                PROCESS_FOLDER_RECONSTRUCTED_SLICES,
                f"volume_slice_{i_scan}.hdf5",
            ),
            data_path="my_volume",
            data=create_vol_data(),
            metadata=create_vol_metadata(),
        )
        volume.save()
        scan.set_latest_reconstructions((volume,))
        widget.addTomoObj(scan)

    # step 2: add independent volumes
    n_volume = 4
    for i_vol in range(n_volume):
        volume = HDF5Volume(
            file_path=os.path.join(
                tmp_folder,
                "raw_volumes",
                f"third_slice_{i_vol}.hdf5",
            ),
            data_path=f"volume_{i_vol}",
            data=create_vol_data(),
            metadata=create_vol_metadata(),
        )
        volume.save()
        widget.addTomoObj(volume)

    widget.show()
    app.exec()
