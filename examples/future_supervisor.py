import os
import tempfile
from concurrent.futures import ThreadPoolExecutor
from time import sleep

from silx.gui import qt
from tomoscan.esrf.scan.mock import MockNXtomo

from orangecontrib.tomwer.widgets.cluster.FutureSupervisorOW import FutureSupervisorOW
from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.scan.nxtomoscan import NXtomoScan


class CreationThread(qt.QThread):
    sigNewFutureScan = qt.Signal(object)

    n_scans = 10
    future_sleep_duration = 10
    create_each = 1.0

    def __init__(self) -> None:
        super().__init__()
        self._folder = tempfile.mkdtemp()

    def run(self):
        executor = ThreadPoolExecutor(max_workers=2)
        for i in range(self.n_scans):
            scan = MockNXtomo(
                scan_path=os.path.join(self._folder, f"scan_{i}.nx"), n_proj=10
            ).scan
            tomwer_scan = NXtomoScan(scan=scan.master_file, entry=scan.entry)
            future_scan = FutureTomwerObject(
                tomo_obj=tomwer_scan,
                futures=[
                    executor.submit(sleep, self.future_sleep_duration),
                ],
            )
            self.sigNewFutureScan.emit(future_scan)
            sleep(self.create_each)


def main(*args, **kwargs):
    # warning: as we will not submit any job to slurm the visible status will be 'finished' even if this is not the case
    app = qt.QApplication([])

    supervisor = FutureSupervisorOW()
    supervisor.show()

    creation_thread = CreationThread()
    creation_thread.sigNewFutureScan.connect(supervisor.add)
    creation_thread.start()
    app.exec()


main()
