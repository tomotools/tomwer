import os
import tempfile
from silx.gui import qt
from orangecontrib.tomwer.widgets.control.EDF2NXTomomillOW import EDF2NXOW
from tomwer.core.utils.scanutils import MockEDF


class TmpObj(qt.QObject):
    def print_selection(self, sel: tuple):
        sender = self.sender()
        widget = sender.parent().parent().parent()
        sel = [
            bliss_url.data(qt.Qt.UserRole).get_identifier().to_str()
            for bliss_url in widget.datalist.selectedItems()
        ]
        print(
            "selected elements are:",
            "\n - ".join(
                [
                    "",
                ]
                + list(sel)
            ),
        )


with tempfile.TemporaryDirectory() as tmp_folder:
    app = qt.QApplication([])

    widget = EDF2NXOW()
    # remove connection with the conversion
    widget.widget._sendSelectedButton.clicked.disconnect(widget._sendSelected)
    tmp_obj = TmpObj()
    widget.widget._sendSelectedButton.clicked.connect(tmp_obj.print_selection)

    for i_bliss_scan in range(4):
        bliss_scan_path = os.path.join(tmp_folder, f"spec_scan_{i_bliss_scan}")
        bliss_proposal = MockEDF.mockScan(
            scanID=bliss_scan_path,
            nRadio=10,
            nRecons=1,
            nPagRecons=4,
            dim=10,
        )
        widget.add(bliss_scan_path)

    widget.show()
    app.exec()
