import os
import tempfile
from silx.gui import qt
from orangecontrib.tomwer.widgets.control.DataSelectorOW import DataSelectorOW
from tomwer.core.utils.scanutils import MockNXtomo


def print_selection(sel: tuple):
    print(
        "selected elements are:",
        "\n - ".join(
            [
                "",
            ]
            + list(sel)
        ),
    )


with tempfile.TemporaryDirectory() as tmp_folder:
    app = qt.QApplication([])

    widget = DataSelectorOW()
    widget.widget.sigSelectionChanged.connect(print_selection)

    for i_scan in range(4):
        nxtomo = MockNXtomo(
            scan_path=os.path.join(tmp_folder, f"my_nxtomo_000{i_scan}"),
            n_proj=10,
        ).scan
        widget.addScan(nxtomo)

    widget.show()
    app.exec()
