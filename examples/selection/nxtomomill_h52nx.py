import os
import tempfile
from silx.gui import qt
from orangecontrib.tomwer.widgets.control.NXTomomillOW import NXTomomillOW
from tomwer.core.utils.scanutils import MockBlissAcquisition


class TmpObj(qt.QObject):
    def print_selection(self, sel: tuple):
        sender = self.sender()
        widget = sender.parent().parent().parent()
        sel = [
            bliss_url.data(qt.Qt.UserRole).get_identifier().to_str()
            for bliss_url in widget.datalist.selectedItems()
        ]
        print(
            "selected elements are:",
            "\n - ".join(
                [
                    "",
                ]
                + list(sel)
            ),
        )


with tempfile.TemporaryDirectory() as tmp_folder:
    app = qt.QApplication([])

    widget = NXTomomillOW()
    # remove call to conversion
    widget.widget._sendSelectedButton.clicked.disconnect(widget._sendSelected)

    tmp_obj = TmpObj()
    widget.widget._sendSelectedButton.clicked.connect(tmp_obj.print_selection)

    n_sample = 4
    bliss_proposal = MockBlissAcquisition(
        n_sample=n_sample,
        n_sequence=1,
        n_scan_per_sequence=1,
        n_darks=0,
        n_flats=0,
        output_dir=os.path.join(tmp_folder, "bliss_proposal"),
    )
    for i_sample_file in range(n_sample):
        bliss_scan = bliss_proposal.samples[i_sample_file].sample_file
        widget.add(bliss_scan)

    widget.show()
    app.exec()
