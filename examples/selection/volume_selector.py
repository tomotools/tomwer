import os
import tempfile
from silx.gui import qt
from orangecontrib.tomwer.widgets.control.VolumeSelector import VolumeSelectorOW
from tomwer.core.volume.hdf5volume import HDF5Volume


def print_selection(sel: tuple):
    print(
        "selected elements are:",
        "\n - ".join(
            [
                "",
            ]
            + list(sel)
        ),
    )


with tempfile.TemporaryDirectory() as tmp_folder:
    app = qt.QApplication([])

    widget = VolumeSelectorOW()
    widget.widget.sigSelectionChanged.connect(print_selection)

    for i_volume in range(4):
        volume = HDF5Volume(
            file_path=os.path.join(tmp_folder, f"my_nxtomo_000{i_volume}"),
            data_path="my_vol",
        )
        widget.addVolume(volume)

    widget.show()
    app.exec()
