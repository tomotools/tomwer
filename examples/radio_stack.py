import os
import tempfile
from silx.gui import qt

from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.gui.stacks import RadioStack


app = qt.QApplication([])
widget = RadioStack()

with tempfile.TemporaryDirectory() as tmp_folder:
    for i_scan in range(4):
        scan = MockNXtomo(
            scan_path=os.path.join(tmp_folder, f"scan{i_scan}"),
            n_proj=10,
            n_ini_proj=10,
            scan_range=180,
            dim=10,
        ).scan
        widget.addTomoObj(scan)

    widget.show()
    app.exec()
