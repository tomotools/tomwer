from __future__ import annotations

import os
import tempfile
import threading
import time
import uuid

import numpy
from silx.gui import qt
from silx.gui.utils import concurrent

from orangecontrib.tomwer.widgets.visualization.VolumeViewerOW import VolumeViewerOW
from tomwer.core.volume.edfvolume import EDFVolume
from tomwer.core.volume.hdf5volume import HDF5Volume
from tomwer.core.volume.tiffvolume import TIFFVolume


class CreateVolumeThread(threading.Thread):
    """
    Thread updating the volume viewer with a new volume each n seconds
    """

    def __init__(
        self,
        volume_viewer: VolumeViewerOW,
        output_dir: str,
        n_time: int | None = None,
        empty: bool = False,
    ):
        """
        :param volume_viewer: viewer to update once the volume is created
        :param output_dir: temporary directory to save the volume created
        :param n_time: how much time do we want to do the volume creation. If None will loop infinitly.
        :param empty: if True will not save the volume data and should raise error at the viewer side
        """
        self.volume_viewer = volume_viewer
        self.running = False
        self._output_dir = output_dir
        self._n_time = n_time
        self._volume_types = [HDF5Volume, EDFVolume, TIFFVolume]
        self._empty = empty
        super(CreateVolumeThread, self).__init__()

    def start(self):
        """Start the update thread"""
        self.running = True
        super(CreateVolumeThread, self).start()

    def create_volume(self):
        volume_type = self._volume_types.pop()
        self._volume_types.append(volume_type)
        if self._n_time is not None:
            self._n_time -= 1

        random_str = str(uuid.uuid4())

        if volume_type is HDF5Volume:
            volume = HDF5Volume(
                file_path=os.path.join(self._output_dir, f"{random_str}.hdf5"),
                data_path="volume",
                data=numpy.random.random((10, 10, 10)),
            )
        elif volume_type in (TIFFVolume, EDFVolume):
            volume = TIFFVolume(
                folder=os.path.join(self._output_dir, random_str),
                data=numpy.random.random((10, 10, 10)),
            )
        else:
            raise NotImplementedError

        if not self._empty:
            volume.save()
        volume.clear_cache()
        return volume

    def run(self):
        """Method implementing thread loop that updates the plot"""
        while self.running:
            time.sleep(2)
            # Run plot update asynchronously
            concurrent.submitToQtMainThread(
                self.volume_viewer.addVolume,
                self.create_volume(),
            )
            if self._n_time == 0:
                break

    def stop(self):
        """Stop the update thread"""
        self.running = False
        self.join(4)


def main():
    global app
    app = qt.QApplication([])

    # Create a VolumeViewerOW
    volume_viewer = VolumeViewerOW()
    volume_viewer.show()

    # Create the thread that calls submitToQtMainThread
    volCreatorThread = CreateVolumeThread(volume_viewer, output_dir=tempfile.mkdtemp())
    volCreatorThread.start()  # Start updating the plot

    app.exec()

    volCreatorThread.stop()  # Stop updating the plot


if __name__ == "__main__":
    main()
