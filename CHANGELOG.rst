Changelog
=========

2.0.0: unreleased
-----------------

* **split tomwer.core.process** into tomwer.tasks and tomwer.core
* rename several 'darkref' to 'darkflat'

1.4.1: 2024/12/12
-----------------

* remove 'tomwer_processes' and dependency on flufl (PR 869).

1.4.0: 2024/12/11
-----------------

* **cluster**
    * **future supervisor** ewoksify the FutureSupervisorOW (PR 883, 906)
    * **slurm**
        * add walltime to the GUI (PR 904)
        * add warning for op[i]dXX users (PR 881)

* **control**
    * **dark/flat**: add an option to clear cache (PR 791)
    * **data listener**: add time stamp to the 'observation tab' (PR 863)
    * **data list**
        * replace 'send all' button by 'select all' (PR 864, 911)
        * bliss data list: filter by default the .nx file (PR 920)
    * **nxtomomill** improve settings loading (PR 892)

* **edit**
    * ewoksify 'ImageKeyEditor' (PR 777)
    * **NXtomo editor**
        * better handling of 'unknown' field (PR 795)
        * ewoksify the widget (PR 844)

* **reconstruction**
    * **gpu**: add an option to select the gpu to be used (if local processing) (PR 841)
    * **axis**
        * Refactor GUI and remove internal layer to call directly nabu 'estimate_cor' function (PR 899).
        * Reworked 'read' to read metadata from the scan and removed reads from a third-party .txt file. Was useful (PR 789).
    * **output** nabu slice will now reconstruct in 'reconstructed_slices' and nabu volume in 'reconstructed_volumes'. Better for publication to drac (PR 787)
    * **cast volume**
        * avoid advancement on the given task task input instead of only on volume (PR 1294)
        * fix setting loading (PR 889)
    * **nabu slice**
        * 'phase' section is no more checkable (PR 915)
        * expose 'exclude_projections' field (PR 879)
    * **axis**
        * handle 'instrument.detector.y_rotation_axis_pixel_position' instead of 'estimated_cor'

* **tomwer canvas**
    * Forced the process supervisor to always be visible when the 'canvas' application is launched.
    * Fully removed volume symbolic link.
    * Update software stack version (PR 918)

* **visualization**
    * **slice stack**: rework inputs. Now user can provide path to a reconstructed slice directly (PR 805, 806 886)
    * **image stack** (widget used in slice stack or data viewer): add an option to filter urls (PR 885).
    * **data viewer** fix volume metadata setting (PR 907)

* **drac**
    * rework drac publication (PR 811, 902, 890)

* **application**
    * add a standalone application for **y-stitching** (pre-processing only)
    * improve slice stack CLI inputs (PR 806)

* **Documentation**
    * Improve documentation readability (PR 853).
    * Update documentation to publish reconstructed volumes to drac

* **misc**
    * remove code related to 'add symbolic link' task (PR 804)
    * now relies on ewoksnotify (PR 753)
    * move to __future__.annotations (PR 824)
    * move to tqdm (PR 838)
    * move to pyunitsystem >= 2.0
    * add an example on how to process volume cast on slurm (PR 913)
    * remove drepreacted 'value_ref_nabu' and 'value_ref_tomwer'

1.3.21: 2024/10/08
------------------

* **slurm settings**
    * fix updating of the '--export' option (PR 871)

1.3.13: 2024/06/06
------------------

* **dark-flat copy**
    * Improved copy behavior: removed any existing reduced dark/flat if they exist before copying (PR 799).
    * **GUI**: Added a 'clear cache' button (PR 791).

1.3.0: 2024/02/28
-----------------

* **core**
    * Added Task to create screenshots in the gallery.

* **scan**
    * Renamed `hdf5tomoscan.HDF5TomoScan` to `nxtomoscan.NXtomoScan` (PR 609).

* **gui**
    * Default colormap histogram proposed (PR 642).
    * Improved handling of existing Qt paths (PR 704).
    * Improved vignettes: users can now request a specific number of columns (PRs 622, 636).

* **orangecanvas**
    * Renamed 'datas' link to 'series' (PR 654).

* **axis** (aka axis of rotation finder)
    * Allowed user input from the bottom QLineEdit (PR 655).
    * Notified with background color if a line is waiting for validation (PR 655).
    * Provided access to the latest Nabu algorithm (Fourier-angles and Octave-accurate) (PR 689).
    * Extended access to the 'near' parameter.
    * Added alias 'near' to the 'composite-coarse-to-fine' algorithm.
    * Improved +180 angle detection: now checks both -180 and +180 angles (PR 621).

* **cast-volume**
    * Changed default output to uint16.

* **data-list**
    * Allowed user to select between two display modes (short or full URL) (PR 648).

* **data-viewer**
    * Set 'histogram' display mode to colormap dialog by default.

* **future-supervisor**
    * Fixed job conversion (PR 696).

* **nabu slice**
    * Allowed reconstruction over different planes (PR 683).
    * Added Vo and mean-subtraction/division deringer options.
    * Supported slice reconstruction over different planes (PR 683).

* **data portal**
    * Added first prototype for publishing processed data to the data portal. Not yet accessible from Orangecanvas (PR 647).

* **saaxis** (aka multi-cor)
    * Used Nabu-multicor instead of Nabu (PR 593).
    * Removed 'tomo-consistency' score to speed up processing.
    * Renamed 'multi-cor' in Orange (PR 838).
    * Added processing wheel when computation is ongoing (PRs 531, 838).
    * Renamed default output folder to `multi_cor_results` (PR 703).

* **sadeltabeta** (aka multi-delta-beta)
    * Renamed default output folder to `multi_delta_beta_results` (PR 703).

* **slurm**
    * Added proposed configuration for stitching (PR 640).
    * Allowed user to specify the value for the sbatch `--export` parameter (PR 638).
    * Allowed user to load modules instead of sourcing a virtual environment (PR 638).
    * Allowed user to request a specific GPU (PR 645).

* **stitching**
    * First official prototype for stitching, currently accessible only via the standalone application (PRs 618, 630, 656, 690).

* **volume-viewer**
    * Loaded non-HDF5 volumes in a dedicated thread to avoid GUI freezing (PR 676).
    * Added processing wheel when a volume is loading (PR 676).

* **misc**
    * Moved to `silx 2.0` (PR 531).
    * Replaced `HDF5TomoScan` with `NXtomoScan`.
    * Transitioned to `nxtomomill 1.0` and `tomoscan 2.0`.
    * Fixed colored logs.
    * Reworked documentation and migrated to `sphinx-pydata-theme` (PRs 673, 682, 684, 686, 705).
    * Provided generic output (e.g., `RAW_DATA`, `PROCESSED_DATA`) to several widgets (PR 867).
    * Added webhooks to trigger ReadTheDocs documentation updates on tag `vx.y.z`.

1.2.0: 2023/09/29
-----------------

* **app**
    * Deprecated `dark-ref` application in favor of `reduce-dark-flat` (PR 587).
    * `reduce-dark-flat`: Moved processing to a thread.
    * Deprecated `dark-ref-patch` application in favor of `patch-raw-dark-flat` (PR 587).
    * Added `tomwer z-stitching` application (early prototype).
    * **axis**: Removed `radio1` and `radio2` options, added optional 'entry' input (PR 530).

* **core**
    * Multiple class deprecations:
        * `tomwer.core.process.reconstruction.sadeltabeta.SAdeltaBetaProcess` -> `tomwer.core.process.reconstruction.sadeltabeta.SADeltaBetaTask`.
        * `tomwer.core.process.reconstruction.saaxis.SAAxisProcess` -> `tomwer.core.process.reconstruction.saaxis.SAAxisTask`.
        * `tomwer.core.process.reconstruction.NabuSlices` -> `tomwer.core.process.reconstruction.NabuSlicesTask`.
        * `tomwer.core.process.reconstruction.NabuVolume` -> `tomwer.core.process.reconstruction.NabuVolumeTask`.
        * `tomwer.core.process.reconstruction.darkref.darkrefs.DarkRefs` -> `tomwer.core.process.reconstruction.darkref.darkrefs.DarkRefsTask`.
        * `tomwer.core.process.reconstruction.lamino.tofu.LaminoReconstruction` -> `tomwer.core.process.reconstruction.lamino.tofu.LaminoReconstructionTask`.
        * `tomwer.core.process.reconstruction.axis.AxisProcess` -> `tomwer.core.process.reconstruction.axis.AxisTask`.
        * `tomwer.core.process.control.volumesymlink.VolumeSymbolicLinkProcess` -> `tomwer.core.process.control.volumesymlink.VolumeSymbolicLinkTask`.
        * `tomwer.core.process.control.scantransfer.ScanTransfer` -> `tomwer.core.process.control.scantransfer.ScanTransferTask`.
        * `tomwer.core.process.conditions.filters.FileNameFilter` -> `tomwer.core.process.conditions.filters.FileNameFilterTask`.
        * `tomwer.core.process.edit.darkflatpatch.DarkFlatPatch` -> `tomwer.core.process.edit.darkflatpatch.DarkFlatPatchTask`.
        * `tomwer.core.process.edit.imagekeyeditor.ImageKeyUpgrader` -> `tomwer.core.process.edit.imagekeyeditor.ImageKeyUpgraderTask`.
        * `tomwer.core.process.edit.imagekeyeditor.ImageKeyEditor` -> `tomwer.core.process.edit.imagekeyeditor.ImageKeyEditorTask`.

* **canvas**
    * Added 'helpdesk' button to trigger ESRF Jira ticket page (PR 580).
    * Added more examples directly on the canvas (PR 564, 510).

* **control**
    * Added `scan discovery` widget: Search for scans (NXtomo, EDF, raw-bliss) in a root directory (recursive, one-time search) (PR 511).
    * Added NXtomo concatenate widget (PR 548).
    * Added 'tomoobjshub' widget (PR 590).
    * **data listener**: Activated by default when added to the canvas (PR 562).
    * Added email notification widget (PR 581).
    * **data lists**: Now supports copy and paste of a list of tomo objects (previously only single items) (PR 596).

* **edition**
    * **NXtomo editor**
        * Added an option to automate edition (PR 524).
        * Added a warning when editing a link (external or soft) (PR 545).

* **slurm**
    * Added slurm job log collection (PR 509).
    * Added found partition to 'slurm cluster configuration' GUI (PR 523).
    * Provided a set of pre-defined slurm configurations (PR 582).

* **reconstruction**
    * **axis**
        * Reworked projection deduction (PR 537).
        * Automatically propose coherent angles when 'other' angles are selected (PR 572).
    * **cast-volume**: Added input for compression ratios (PR 544).
    * Added `reduce-dark-flat-selector` widget (PR 597).

* **visualization**
    * **metadata**
        * Leveraged tomoscan volumes to display slice (data viewer) and volume (volume viewer) metadata for all file formats (not just .hdf5).
        * Extended metadata support to non-hdf5 volumes (PR 543) – raw volumes still don't display metadata.
    * **diffviewer**: Minor improvements (PR 525).
    * **scan overview**: Appended 'scan range' to metadata (PR 526).
    * Added action to display a frame in full-screen mode (PR 529).
    * **volume viewer**: Fixed issue to avoid loading all data in memory when possible (PR 541).

* **helical**
    * Preparing for helical:
        * Added `nabu-helical-prepare-weights-double` widget (PR 547).

* **doc**
    * Added reference beamline tutorials.
    * Added examples to the user corner (PR 574, 594).
    * Added more examples directly on the canvas (PR 564, 510).

* **stitching**
    * Added first prototype for stitching (PR 463, 551, 595).
    * Added NXtomo concatenate widget (PR 548).

* **misc**
    * Added the ability to cancel jobs (PR 486).
    * Replaced multiprocessing lock with `flufl.lock` for file locking (PR 555).
    * Reworked future supervisor column management (PR 579).
    * Improved opening with ImageJ (PR 567).

1.1.0: 2023/03/24
-----------------

* **NXtomo editor and viewer**
    * Added NXtomo editor and viewer (PR 474).
    
* **series management**
    * Added management of series and the definition of a series (PR 375).

* **slurm**
    * Improved slurm job submission (using sluurp and updating GUI/future handling) (PR 469).

* **nabu**
    * Integrated CTF (PR 410).
    * Added management of `centered_axis` (PR 475).

* **axis**
    * Added an option to avoid cropping images (PR 478).

* **cast volume**
    * Improvements:
        * Allowed users to provide `data_min` and `data_max` (PR 464).

* **data viewer**
    * Added display of reduced darks and flats.

* **darkref**
    * Used `FrameReduced` from tomoscan (PR 465).

* **semi-automated widgets**
    * Improved connection with other widgets: can now process Nabu directly (PR 479).
    * Added an option to save a screenshot for all reconstructed slices (PR 479).

* **notifier**
    * Added a signal tone (PR 490).

* **miscellaneous**
    * Added pylint to CI (PR 462).
    * Used ewoks docker images for CI.
    * Added a tutorial for using `ewoks convert` and `ewoks execute`.
    * Removed distutils for packaging (PR 480).
    * Improved management of the output directory (PR 488).
    * Removed `tomwer.web` module (PR 501).
    * Updated the default file to source for slurm-cluster (now `/scisoft/tomotools/activate.sh stable`).

1.0.4: 2023/02/08
-----------------

* **data transfer**
    * Avoided updating the created scan object (was freezing the GUI with EDF and GPFS - 5a7a8548).
    
* **fixes**
    * Fixed layout and Qt parenting issues (e0b65c63 & 589c8d1f).

1.0.3: 2023/01/12
-----------------

* **volume integration**
    * Integrated `.vol` volume to allow existing post-processing to work (ID16B).

1.0.2: 2022/12/20
-----------------

* **notification widget**
    * Added a 'notification' widget.

* **data transfer**
    * Moved processing to threads.
    * Rebased Nabu configuration file paths that are expected to be moved.

* **fixes**
    * **data watcher**:
        * Added a try-catch on `TomoScanBase.update`, which could fail when encountering an empty file (due to Fabio's new behavior).
        * Copied `.xml` and `.info` files to the scan directory if in `/data` instead of `/lbsram/data`.
    * **darkref**:
        * Fixed notification when a new ref (dark or flat) is set.

1.0.0: 2022/11/08
-----------------

* **integration**
    * Integrated volumes from tomoscan (PR 394). Users can now handle a volume and process it.

* **visualization**
    * Removed old data validator and data viewer (deprecated since 0.5).

* **control**
    * Added `edf2nx` widget (PR 393).
    * Added `image_key_upgrader` (PR 396).
    * Added `VolumeSelector` GUI and orange widget (PR 396).
    * Upgraded datawatcher to allow searching for bliss scan and NXtomo (PR 406).
    * Added 'single tomo obj' widget (PR 409).
    * **data listener**: Added option to filter file names (PR 426).

* **reconstruction**
    * **dark and ref**
        * Updated copy to fit new 'reduced frames' policy using tomoscan (PR 396).
        * Added 'channel' for the user to provide reduced dark and flat (PR 409).
    * Reworked volume casting to call Nabu volume casting and used the new `VolumeBase` class (PR 394).
    * **output file format**: Can now be redefined from NabuVolumeOW (PR 403).
    * **nabu**
        * **slice**: Users can now provide `angles_file`.
        * **volume**: Output location and file format can be redefined from advanced settings.
        * Added Shepp-Logan, Cosine, Hamming, Hann, Tukey, Lanczos, Hilbert filter kernels (PR 421).
        * Added management of `sino_rings_options` (PR 420).
        * Added management of `tilt correction` (PR 437).
    * **axis**
        * Fixed loading of cor value when saved in the `.ows` and bug with manual mode being locked in some cases (PR 450).
        * Added a `+180` button to automatically find the associated angle when radio angles are defined manually (PR 455).
        * Fixed relative position casting to absolute (PR 457).
    * **sadelta-beta**
        * Fixed bug when slice computation fails (PR 447).

* **slurm**
    * Replaced `dask-jobqueue` with the `tomwer.slurm` module (PR 405).

* **other**
    * Updated Python widget to handle a volume.
    * Allowed several inputs for some widgets (PR 446).

* **miscellaneous**
    * Removed `pyhst` binding.
    * First version with some widgets fully handled by `ewoksorange`.

0.10.0: 2022/06/27
------------------

* **core**
    * Used the latest tomoscan dark and flat definitions (`[scan]_flats.hdf5:flats` and `[scan]_darks.hdf5:darks`) (!674).
    * Added normalization process to apply user-defined normalization to frames and provide them to Nabu.

* **gui**
    * Added normalization GUI.
    * **nabu volume**: Activated histogram request to Nabu by default.
    * **axis**:
        * Reworked possible algorithms. If an algorithm can be done with sinogram or radios, proposed two values (instead of one with a sinogram vs. radios option previously).
        * Moved parameters to a QScrollArea (!688).
        * Allowed users to define the angle value manually (!702).
        * Moved input side-by-side with calculation to improve readability (!705).
        * Fixed issue with scrolling (!706).
    * **data list**:
        * Fixed item removal (!707).

* **orangecontrib**
    * Fixed slice stack (!708).

0.9.0: 2022/01/17
-----------------

* **app**
    * **canvas**: Removed dependency on the `orange3` project and replaced it with dependency on `orange-canvas-core` and `orange-widget-base`.
    * A fork was created to access the 'processing wheel' as an optional feature (https://github.com/payno/orange-widget-base, https://github.com/payno/orange-canvas-core) (!350).

* **core**
    * Adapted all 'core process' to `ewoks Task`. Ensured low-level compatibility with ewoks (!340).
    * **reconstructions**
        * Added option to trigger reconstruction on the slurm cluster (!349).
        * Added a cast process to convert Nabu volume to other file formats.
    * **slurm cluster**: Reworked to provide ports dynamically.
    * **darkref**: Now saves darks/flats in `darks.hdf5` and `flats.hdf5` for HDF5 acquisition.

* **gui**
    * Added future supervisor interface.
    * Added slurm cluster interface.
    * **nabu**:
        * Added interface to set the rings removal method.
        * Connected "composite-to-fine" algorithm.
        * Added the "None" method to the phase method to allow users to use other options.
    * **diffviewer**:
        * Added widget to shift the second image.

* **orangecontrib**
    * Added cluster group with `SlurmclusterOW` and `FutureSupervisorOW`.
    * Added a "cast volume" widget to cast Nabu volume.

* **doc**
    * Provided descriptions of canvas objects.
    * Added streamline videos.


0.8.0: 2021/08/31
-----------------

* **app**
    * Added scan viewer: allows the user to display the contents of a scan and browse through it.

* **gui**
    * **dataviewer**:
        * Integrated an instance of the 'scanoverview' widget to display information regarding the scan (e.g., number of darks/flats, energy).
        * Provided access to darks and flats.

    * **axis**: Fixed several minor issues with the color map and reset zoom.
    * Improved diff frame.

* **orangecontrib**
    * **widgets**:
        * Added `SADeltaBetaOW` to compute a score from several delta/beta values.
        * **patch-nx**: Added a `configuration` entry (!346).

* **miscellaneous**
    * Moved doc to autosummary.
    * Migrated to `setup.cfg`.
    * Moved to pytest.

0.7.0: 2021/05/20
-----------------

* **orangecontrib**
    * **widgets**:
        * Added `AdvancementOW`: Displays the advancement of all processes.
        * Added `SAAxisOW`: Semi-automatic axis widget.
    * Integrated processview.

* **core**
    * **process**:
        * Added `manager` module to observe `SuperviseProcess` and display processes' advancement.
    * **Axis**:
        * **AxisProcess**: Integrated the latest Nabu algorithm based on sinogram.
        * **AxisRP**: Deprecated `value_ref_tomwer` and `value_ref_nabu` in favor of `absolute_value` and `relative_value`.
    * **SAAxis**:
        * Added process to compute several centers of rotation for the same slice and Nabu reconstruction parameters, providing a score for each slice (scores include standard deviation and total variation).
    * Integrated processview.
    * **Nabu**:
        * Fixed file retrieval when `.tif` was requested.
    * **scan**:
        * Benefited from tomoscan HDF5 sinogram speed-up.

* **gui**
    * **DataList**:
        * Allowed copying of text (e.g., scan directory, master file).
    * **Axis**:
        * Improved speed (removed the `reset_scan`).
        * Added Nabu algorithm using sinogram.
            * Updated the 'Input' tab: Users can now select from radio or sinogram and provide the sinogram line and subsampling.
        * When manual mode is activated, pressing 'validate' forces validation and ensures the displayed value is set to the scan.
        * Changed `QLabel` for displaying cor values to `QLineEdit` (read-only) to allow copying/pasting the cor value.
    * **nabu**:
        * Saved processing log to the processview.
    * **volume viewer**:
        * Integrated reconstruction parameters.

* **synctools**
    * FIFO is now a `SuperviseProcess`.

0.6.0: 2020/12/15
-----------------

* **app**
    * **canvas**:
        * Added `edit` submodule:
            * Added `ImageKeyEditorOW`: Allows invalidating some frames or changing their 'frame type'.
            * Added `DarkFlatPatchOW`: Allows adding dark and/or flat to a NXtomo entry.
        * Added `debug` submodule (!289):
            * Added `random data generator`.
            * Added `tomwer object viewer`.
        * **control**:
            * Added `volume symbolic link` widget.
            * **data listener**: Added file conversion on a thread to avoid GUI freeze.
    * **darkref**:
        * Added a `--no-gui` option and options to define the calculation method for dark and ref from the command line (!299).
    * Added `image-key-editor`.
    * Added `dark-ref-patch`.

* **control**
    * **nxtomomill**:
        * Added an option to retain the last parameter provided by the user (e.g., energy, overwrite files). This will be cleared between two calls to the converter (!296).
        * Attempted to filter non-bliss files from user input (!296).
    * **datalistener**:
        * Ensured that only one instance is active on the PC. Otherwise, displayed a dialog to notify the user.
    * **utils**:
        * Reworked `isOnLbsRam` to handle either a `TomwerScanBase` or a `str`.

* **reconstruction**
    * **axis**:
        * Added 'global' algorithm from Nabu. Works for both half-acquisition and standard acquisition (!295).
        * **GUI update**:
            * Avoided reset zoom (!295).
            * Avoided validation when pressing 'enter' after manual cor editing (!295).
            * Sped up radio shift (!295).
            * Displayed both absolute and relative cor (!295).
    * **nabu** (!303):
        * **slices**:
            * Added subsampling.
            * Removed margins.
        * **volume**:
            * Added option to compute histograms.
        * Slices to be reconstructed are now always set to 'middle'. The value is no longer loaded, as it confused users (commit 947d475b31d2).
    * **dark-ref**:
        * Added first and last options to select the first or last frame from a series of dark/ref frames (!299).
        * Automated settings for half-field/standard tomography (!294).

* **visualization**
    * **viewer**:
        * Added an 'open with ImageJ' button (!298).
        * Reduced memory usage by limiting the number of images in cache (!289).
    * Added diff viewer prototype (!286).

* **miscellaneous**
    * Adopted black coding style.
    * **tomwer.core.process**:
        * Moved `timer`, `datalistener`, `datawatcher`, `scanvalidator`, `datalist`, and `datatransfert` to `control` submodule.

0.5.3: 2020/10/19
-----------------

* **core**
    * **axis**:
        * Renamed 'accurate' to 'centered'.
        * Added global algorithm.

* **orangecanvas**
    * Fixed issues (commit 8a368fd7).

* **miscellaneous**
    * Reworked logs (!292).

0.5.2: 2020/09/02
-----------------

* **core**
    * Added volume reconstruction from Nabu.
    * **nabu**:
        * Renamed Nabu to NabuSlices.
        * Caught Nabu logs (!273).
        * Managed `.tiff` and `.jp2` file formats.

* **gui**
    * Added a new data viewer [prototype].
    * Added a volume viewer for Nabu volume reconstruction (!275).
    * **axis**: Fixed display of shifted images (#424).
    * **viewers**: Added timestamp (!279).
    * **darkref**: Fixed issue when adding a widget to Orange canvas, removing it, and adding it again (!265).

* **orangecontrib**
    * Deprecated old data viewer.
    * Added new `DataViewerOW` [prototype].
    * Added `VolumeViewerOW`.
    * Added `NabuVolumeOW`.

* **app**
    * **nabu**:
        * Added interface to give cor (!280).
        * Added display button to browse reconstructed slices and projections (!280).
    * Added process application: run a `.ows` within `pypushflow` in a GUI-free environment [prototype] (!277).
    * Added ows-to-script application: translates a `.ows` file into a Python script [prototype] (!278).

* **web**
    * Fixed Graypy new API (!270).

* **miscellaneous**
    * Added dependency on `pypushflow`.
    * Managed `HDF5_USE_FILE_LOCKING` when reading or writing files using `tomoscan.io.HDF5File`.
    * Fixed some utilities using `BaseProcess._get_process_nodes`.

0.5.0: 2020/06/30
-----------------

* **core**
    * Completed compatibility with HDF5.
    * Added `datalistener` process: listens to a Tango device to retrieve the acquisition status. Also starts synchronization between `lbsram` and `nice` if on the `lbsram` system.
    * Added `nabu` process: calls Nabu to reconstruct slices and create `.conf` files for full-volume reconstruction.
    * **settings**:
        * Added `BEAMLINE_NAME`, `TANGO_PORT`, `BLISS_SESSION_NAMES`, `DEFAULT_BLISS_SESSION_NAME`, and `MAKE_OAR_PYST2_PATH`.
        * Added management of `PyHST2_*` environment variables.
    * **scan**:
        * Added dependency on `tomoscan`.
    * **utils**:
        * Added `HDF5Mock`.
    * **data transfer**: Deactivated for HDF5.

* **gui**
    * Added interfaces for Nabu, data listener, and sinogram viewer.
    * **axis**: Updated interface.
    * **viewer**: Added prototype of the next data viewer.

* **orangecontrib**
    * Added sinogram widget: used to compute a sinogram from an EDF or HDF5 (NXtomo) acquisition.
    * Added data listener widget: connects to a Tango device to retrieve information.
    * Added Nabu widget: used to reconstruct using the Nabu library.
    * Added `nxtomomill` widget: converts a Bliss HDF5 acquisition to a (NXTomo) `.nx` acquisition.
    * Added prototype of the `DataViewer`.
    * Added prototype of the new `DataValidator` (moved previous one to `OldDataValidatorOW`).

* **app**
    * Added Nabu application: GUI for calling Nabu or creating a Nabu configuration file.
    * Added sinogram viewer utility: computes a sinogram from an EDF or HDF5 (NXtomo) acquisition.
    * Added canvas application.

* **examples**
    * Added `nabu_flow.py` example, presenting a default workflow with Nabu.

* **miscellaneous**
    * Dropped Python 3.4 support.
    * Alpha version released on 2020/04/30.

0.4.0: 2020/03/06
-----------------

* **core**
    * Started compatibility with HDF5.
    * **scan folder**:
        * Added `TomoScan`, `EDFScan`, and `HDF5Scan`. These objects are all serializable (using `from_dict`, `to_dict` functions).
    * **reconstruction**:
        * **process**:
            * All processes now return a serializable object (`TomoScan`).
            * **ftseries**:
                * Removed dependency on Fastomo, using `pyhstcaller` instead.
            * Added `pyhstcaller`: generates `.par` and `.rec` files.
            * **lamino**:
                * Reworked and checked calls to Tofu with Lukas.
                * Added pre-processing and stitching for half-acquisition.
            * **darkref**: Adapted to `TomoScan` object.
            * **axis**: New process to compute the center of rotation.
        * **reconsparam**: Replaced the previous `ReconsParamRegistry` with a set of classes (`FTReconsParams`, `AxisReconsParams`, etc.).
    * **utils**:
        * Added `scanrange` file.

* **gui**
    * **reconstruction**:
        * Reworked lamino widget.
        * Added `AxisWidget` (stack requested axis).
    * Updated datawatcher:
        * Removed pop-up windows and embedded them in the same widget.
        * Checked acquisition type (HDF5 or EDF).

* **orangecontrib**
    * Links now use `TomoScan` objects instead of `str`.
    * **widgets**:
        * Added axis widget.

* **app**
    * Added Axis application: computes the center of rotation from a scan directory or some files.
    * Added Rsync application: synchronizes a folder containing a scan with another folder. The user can specify a 'point of advancement' in the scan's lifecycle, so only files created at that stage will be synchronized.

* **examples**
    * Added `orangeworkflow` example: common orange workflow with datawatcher, axis, darkref, ftseries, datavalidator, and datatransfer widgets.
    * Added datawatcher example: simple instantiation of datawatcher with automatic acquisition generation.

* **luigi**
    * Removed prototype.

0.3.0: 2019/01/08
-----------------

* **core**
    * Added simple Python binding for launching Tofu (currently only available for Python 2).
    * **conditions**: Introduced a new package to include conditions (e.g., folder names) on a workflow.
    * **ftseries**:
        * Duplicated octave outputs to `octave.log`.
        * Enabled running multiple Paganin reconstructions with different parameters.
    * Created different nodes for loading image stacks.
    * **data transfer**: Added pre-transfer operations.
    * Created `tomwer.core.scan` package to include different types of scans (e.g., `edfscan`, `hdf5scan`) with specific treatment and interfaces for each.

* **gui**
    * **lamino**: Added `TofuWidget` to call lamino reconstruction from Tofu.
    * **compare images**: Used the comparison tool from `silx` if `silx >= 0.9`.
    * **data list**:
        * Added a 'remove all' button.
        * Enabled handling of mouse drag-and-drop.
    * Added widgets `radio stack` and `slice stack` to record all radios or slices passing through the widget, allowing users to browse through them.
    * **live slice**: Embedded live slice into an Orange widget (if installed).
    * Added GUI for the new conditions process.
    * Separated tomwer's Orange add-on widgets into three parts: control, reconstruction, and visualization.
    * **data validator**: Added three different options for loading images (just-in-time, on-demand, and load-as-soon-as-possible).

* **doc**
    * Added automatic screenshots for documentation generation.

* **luigi**
    * Added the first prototype (not functional yet).

0.2.0: 2018/07/16
-----------------

* **dark & flat fields**:
    * Added a process to manage dark and flat field creation (creates the median of the mean).
    * Managed copying of dark and flat fields if they don't already exist in the repository.
* **refactoring**:
    * Moved tomwer core functions and Qt base widgets to the tomwer package.
    * The `orangecontrib` subpackage now only contains the Orange add-on.
* **ftseries**:
    * Removed the octave version previously read and loaded.
* **data watcher**:
    * Enabled parallel observations.
    * Skipped acquisition folders containing a `.abo` file (treated as if no data was available).
    * GUI refactoring.
* **scan validator**:
    * Added a GUI as a stack, allowing users to browse through it.
* Added **group slice** to display all reconstructed slices received.
* Added **data selector**: a simple GUI to select a scan from a list.
* **sample moved**: Interface to display 'key radios' and check if the sample moved during acquisition.
* Added applications:
    * **darkref**
    * **ftseries**
    * **samplemoved**
    * **slicestack**

0.1.0: 2017/09/28
-----------------

* **core features**:
    * **tomodir**: (aka data watcher) Detects when an acquisition is finished and synchronizes data with the destination directory as soon as possible if the system is on `lbsram`.
    * **ftseries**: Interface for `fastomo3` (Octave).

* **orange add-on**:
    * **ftseries widget**.
    * **scan validator**: Validates a scan or requests reconstruction with changed parameters.
    * **data watcher**.
    * **data viewer**.
    * **data list**.
    * **data transfer**.