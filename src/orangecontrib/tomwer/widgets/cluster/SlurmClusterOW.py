from __future__ import annotations

import logging

from silx.gui import qt

from orangewidget import gui
from orangewidget.settings import Setting
from orangewidget.widget import Output, OWBaseWidget

from tomwer.core.settings import ParametersKeys
from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.gui.cluster.slurm import SlurmSettingsWindow

_logger = logging.getLogger(__name__)


class SlurmClusterOW(OWBaseWidget, openclass=True):
    """
    Orange widget to define a slurm cluster as input of other
    widgets (based on nabu for now)
    """

    name = "slurm cluster"
    id = "orange.widgets.tomwer.cluster.SlurmClusterOW.SlurmClusterOW"
    description = "Let the user configure the cluster to be used."
    icon = "icons/slurm.svg"
    priority = 20
    keywords = ["tomography", "tomwer", "slurm", "cluster", "configuration"]

    want_main_area = True
    want_control_area = False
    resizing_enabled = True

    _ewoks_default_inputs = Setting(dict())

    class Outputs:
        config_out = Output(
            name=ParametersKeys.SLURM_CLUSTER_KEY, type=SlurmClusterConfiguration
        )

    def __init__(self, parent=None) -> None:
        super().__init__(parent)
        layout = gui.vBox(self.mainArea, self.name).layout()
        self._widget = SlurmSettingsWindow(parent=self)
        self._widget.setWindowFlags(qt.Qt.Widget)
        layout.addWidget(self._widget)

        if self._ewoks_default_inputs != {}:
            self._widget.setConfiguration(self._ewoks_default_inputs)

        # trigger the signal to avoid any user request
        self.Outputs.config_out.send(self.getConfiguration())

        # connect signal / slot
        self._widget.sigConfigChanged.connect(self._configurationChanged)

    def _configurationChanged(self):
        slurmClusterconfiguration = self.getConfiguration()
        self.Outputs.config_out.send(slurmClusterconfiguration)
        self._ewoks_default_inputs = slurmClusterconfiguration.to_dict()

    def getConfiguration(self):
        return self._widget.getSlurmClusterConfiguration()
