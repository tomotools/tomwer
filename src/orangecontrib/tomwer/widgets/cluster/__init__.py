"""
(Ewoks)Orange Widgets for remote processing through slurm
"""

NAME = "compute cluster"

ID = "orangecontrib.tomwer.widgets.cluster"

DESCRIPTION = "widgets to submit jobs to cluster"

LONG_DESCRIPTION = (
    "widgets to submit slurm job, observe job and convert them back to tomwer object"
)

ICON = "../../widgets/icons/tomwer_cluster.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 5
