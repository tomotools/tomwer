# coding: utf-8
from __future__ import annotations

import functools
import logging

from processview.core.manager import DatasetState, ProcessManager
from processview.core import helpers as pv_helpers

from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils.dictutils import concatenate_dict

from tomwer.gui.reconstruction.multipag.MultiPagWindowBase import MultiPagWindowBase
from tomwer.synctools.multipag import QMultiPagParams
from tomwer.synctools.stacks.reconstruction.multipag import MultiPagStack
from orangecontrib.tomwer.widgets.reconstruction.BaseNabuRecOW import BaseNabuRecOW

_logger = logging.getLogger(__name__)


class MultiPagWindow(MultiPagWindowBase, BaseNabuRecOW):
    def __init__(self, Outputs, parent=None, process_id=None):
        super().__init__(parent=parent)

        self.Outputs = Outputs
        self._multipag_params = QMultiPagParams()
        self._processing_stack = MultiPagStack(
            multipag_params=self._multipag_params, process_id=process_id
        )

    # START -- API to be implemented by child class

    def _launchReconstructions(self):
        """callback when we want to launch the reconstruction of the
        slice for n cor value"""
        scan = self.getScan()
        if scan is None:
            return
        # step1: if isAutoFocus: validate automatically the scan
        # step2: update the interface if the current scan is the one displayed
        # else skip it
        callback = functools.partial(
            self._mightUpdateResult, scan, self.isAutoFocusLock()
        )
        self._processing_stack.add(
            data=scan, configuration=self.getConfiguration(), callback=callback
        )

    def _validate(self):
        self.validateCurrentScan()

    # --- END API to be implemented by child class

    def _mightUpdateResult(self, scan: TomwerScanBase, validate: bool):
        if not isinstance(scan, TomwerScanBase):
            raise TypeError("scan is expected to be an instance of TomwerScanBase")
        if not isinstance(validate, bool):
            raise TypeError("validate is expected to be a boolean")
        if scan == self.getScan():
            self.setDBScores(
                scan.multipag_params.scores,
                score_method=scan.multipag_params.score_method,
            )
            if scan.multipag_params.autofocus is not None:
                self.setCurrentDeltaBetaValue(scan.multipag_params.autofocus)

            pm = ProcessManager()
            details = pm.get_dataset_details(
                dataset_id=scan.get_identifier(), process=self._processing_stack
            )
            current_state = ProcessManager().get_dataset_state(
                dataset_id=scan.get_identifier(), process=self._processing_stack
            )
            if current_state not in (
                DatasetState.CANCELLED,
                DatasetState.FAILED,
                DatasetState.SKIPPED,
            ):
                ProcessManager().notify_dataset_state(
                    dataset=scan,
                    process=self._processing_stack,
                    details=details,
                    state=DatasetState.WAIT_USER_VALIDATION,
                )
        if validate:
            self.validateScan(scan)

    def wait_processing(self, wait_time):
        self._processing_stack._computationThread.wait(wait_time)

    def validateCurrentScan(self):
        return self.validateScan(self.getScan())

    def validateScan(self, scan):
        if scan is None:
            return
        assert isinstance(scan, TomwerScanBase)
        selected_db_value = (
            self.getCurrentDeltaBetaValue() or scan.multipag_params.autofocus
        )
        if selected_db_value is None:
            infos = (
                f"no selected delta / beta value. {scan} skip setting multipag_params"
            )
            _logger.warning(infos)
            scan.multipag_params.set_db_selected_value(None)
            pv_helpers.notify_skip(
                process=self._processing_stack, dataset=scan, details=infos
            )
        else:
            scan.multipag_params.set_db_selected_value(selected_db_value)
            if scan.nabu_recons_params is not None:
                if "phase" not in scan.nabu_recons_params:
                    scan.nabu_recons_params["phase"] = {}
                scan.nabu_recons_params["phase"]["method"] = "Paganin"
                scan.nabu_recons_params["phase"]["delta_beta"] = (selected_db_value,)
            _db_value = scan.multipag_params.value
            infos = f"delta / beta selected for {scan}: {_db_value}"
            pv_helpers.notify_succeed(
                process=self._processing_stack, dataset=scan, details=infos
            )
        self.Outputs.nabu_rec_config.send(
            scan.nabu_recons_params,
        )
        self.Outputs.data.send(scan)

    def getConfiguration(self, allow_no_nabu_config_defined: bool = False) -> dict:
        config = BaseNabuRecOW.getConfiguration(self)
        multi_db_params = MultiPagWindowBase.getConfiguration(self)

        nabu_params = config.get(ParametersKeys.NABU_REC_PARAMS_KEY, {})
        if (not allow_no_nabu_config_defined) and (not nabu_params):
            raise ValueError(
                "No reconstruction parameters provided. You should have a 'nabu settings' widget connected upstream"
            )
        config = concatenate_dict(
            config,
            multi_db_params,
        )
        config["workflow"] = {
            "autofocus_lock": self.isAutoFocusLock(),
        }

        return config

    def setConfiguration(self, config: dict):
        MultiPagWindowBase.setConfiguration(self, config)
        BaseNabuRecOW.setConfiguration(self, config)
