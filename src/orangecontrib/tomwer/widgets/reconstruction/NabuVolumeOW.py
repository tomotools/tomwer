# coding: utf-8
from __future__ import annotations

import copy
import logging

from orangewidget import gui
from orangewidget.settings import Setting
from orangewidget.widget import Input, Output
from silx.gui import qt
from tomoscan.identifier import VolumeIdentifier

import tomwer.tasks.reconstruction.nabu.nabuvolume
from orangecontrib.tomwer.widgets.reconstruction.BaseNabuRecOW import BaseNabuRecOW
from orangecontrib.tomwer.widgets.utils import WidgetLongProcessing
from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.drac.processeddataset import DracReconstructedVolumeDataset
from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.reconstruction.params_cache import (
    load_reconstruction_parameters_from_cache,
)
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.utils.dictutils import concatenate_dict
from tomwer.core.settings import ParametersKeys
from tomwer.core.volume.volumebase import TomwerVolumeBase
from tomwer.core.volume.volumefactory import VolumeFactory
from tomwer.gui.reconstruction.nabu.volume.NabuVolumeWindow import NabuVolumeWindow
from tomwer.synctools.stacks.reconstruction.nabu import NabuVolumeProcessStack
from tomwer.utils import docstring

from ...orange.managedprocess import SuperviseOW

_logger = logging.getLogger(__name__)


class NabuVolumeOW(WidgetLongProcessing, SuperviseOW, BaseNabuRecOW):
    """
    A simple widget managing the copy of an incoming folder to an other one

    :param parent: the parent widget
    """

    # note of this widget should be the one registered on the documentation
    name = "nabu volume reconstruction"
    id = "orange.widgets.tomwer.reconstruction.NabuVolumeOW.NabuVolumeOW"
    description = (
        "This widget will call nabu for running a reconstruction " "on a volume"
    )
    icon = "icons/nabu_volume.svg"
    priority = 15
    keywords = ["tomography", "nabu", "reconstruction", "volume"]

    ewokstaskclass = tomwer.tasks.reconstruction.nabu.nabuvolume.NabuVolumeTask

    want_main_area = True
    resizing_enabled = True

    _ewoks_default_inputs = Setting(
        {"data": None, ParametersKeys.NABU_REC_PARAMS_KEY: {}, "slurm_settings": {}}
    )

    sigScanReady = qt.Signal(TomwerScanBase)
    "Signal emitted when a scan is ended"

    TIMEOUT = 30

    class Inputs:
        data = Input(
            name="data",
            type=TomwerScanBase,
            doc="one scan to be process",
            default=True,
            multiple=False,
        )
        cluster_in = Input(
            name=ParametersKeys.SLURM_CLUSTER_KEY,
            type=SlurmClusterConfiguration,
            doc="slurm cluster to be used",
            multiple=False,
        )
        nabu_params = Input(
            name="rec config",
            type=dict,
            doc="nabu reconstruction parameters",
            multiple=False,
        )

    class Outputs:
        data = Output(name="data", type=TomwerScanBase, doc="one scan to be process")

        future_out = Output(
            name="future_tomo_obj",
            type=FutureTomwerObject,
            doc="data with some remote processing",
        )
        volume = Output(
            name="volume",
            type=TomwerVolumeBase,
            doc="volume(s) created",
        )
        volume_urls = Output(
            name="volume urls", type=tuple, doc="url of the volume(s) reconstructed"
        )
        data_portal_processed_datasets = Output(
            name="data_portal_processed_datasets",
            type=tuple,
            doc="data portal processed data to be saved",
        )

    def __init__(self, parent=None, *args, **kwargs):
        SuperviseOW.__init__(self, parent)
        WidgetLongProcessing.__init__(self)
        BaseNabuRecOW.__init__(self)

        # processing tool
        self._processingStack = NabuVolumeProcessStack(self, process_id=self.process_id)

        _layout = gui.vBox(self.mainArea, self.name).layout()
        # main widget
        self._nabuWidget = NabuVolumeWindow(parent=self)
        _layout.addWidget(self._nabuWidget)
        # add button to validate when change reconstruction parameters is called
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(self)
        self._buttons.setStandardButtons(types)
        _layout.addWidget(self._buttons)

        # set up
        self._buttons.hide()

        # load settings
        self.setConfiguration(self._ewoks_default_inputs)

        # connect signal / slot
        self._processingStack.sigComputationStarted.connect(self._startProcessing)
        self._processingStack.sigComputationEnded.connect(self._endProcessing)
        self._buttons.button(qt.QDialogButtonBox.Ok).clicked.connect(self.accept)
        self._nabuWidget.sigConfigChanged.connect(self._updateSettingsVals)

    @Inputs.data
    def process(self, scan: TomwerScanBase):
        assert isinstance(scan, (TomwerScanBase, type(None)))
        if scan is None:
            return
        scan_ = copy.copy(scan)
        scan_.clear_latest_vol_reconstructions()
        self._processingStack.add(scan_, self.getConfiguration())

    @docstring(SuperviseOW)
    def reprocess(self, dataset):
        if dataset.cor_params is None or dataset.cor_params.relative_cor_value is None:
            # try to retrieve last computed cor value from nabu process
            if dataset.cor_params is None:
                from tomwer.synctools.axis import QCoRParams

                dataset.cor_params = QCoRParams()
            load_reconstruction_parameters_from_cache(scan=dataset)

        self.process(dataset)

    @Inputs.nabu_params
    def setNabuRecConfig(self, config: dict | None):
        self.setNabuParams(config=config)

    @Inputs.cluster_in
    def setCluster(self, cluster):
        self.setClusterConfiguration(config=cluster)

    def cancel(self, scan):
        if scan is None:
            return
        if scan in self._processingStack:
            self._processingStack.remove(scan)
        if (
            self._processingStack._data_currently_computed.get_identifier().to_str()
            == scan.get_identifier().to_str()
        ):
            # stop current processing
            self._processingStack.cancel()
            # if possible process next
            if self._processingStack.can_process_next():
                self._processingStack._process_next()

    def _updateDB(self, scan, dialog):
        db = dialog.getSelectedValue()
        if db is not None:
            try:
                scan.nabu_recons_params["phase"]["delta_beta"] = (db,)
            except Exception as e:
                logging.error(e)
            else:
                self.process(scan=scan)

    def _endProcessing(self, scan, future_tomo_obj):
        WidgetLongProcessing._endProcessing(self, scan)
        if scan is not None:
            # send scan
            self.Outputs.data.send(scan)
            self.sigScanReady.emit(scan)

            # send volume urls
            volume_urls = []
            for volume_id in scan.latest_vol_reconstructions:
                assert isinstance(volume_id, VolumeIdentifier)
                volume_urls.extend(VolumeFactory.from_identifier_to_vol_urls(volume_id))
            if len(volume_urls) > 0:
                self.Outputs.volume_urls.send(tuple(volume_urls))

            # send volume identifier(s) and associated IcatDataBase objects
            n_rec_volumes = len(scan.latest_vol_reconstructions)
            drac_processed_datasets = []
            if n_rec_volumes > 0:
                if n_rec_volumes > 1:
                    _logger.warning(
                        f"{n_rec_volumes} volume reconstructed when at most one expected"
                    )
                try:
                    volume = VolumeFactory.create_tomo_object_from_identifier(
                        scan.latest_vol_reconstructions[0]
                    )
                except Exception as e:
                    _logger.error(
                        f"Failed to retrieve volume from {volume_id}. Error is {e}"
                    )
                else:
                    self.Outputs.volume.send(volume)

                    icatReconstructedDataset = DracReconstructedVolumeDataset(
                        tomo_obj=volume,
                        source_scan=scan,
                    )
                    drac_processed_datasets.append(icatReconstructedDataset)

            if len(drac_processed_datasets) > 0:
                self.Outputs.data_portal_processed_datasets.send(
                    drac_processed_datasets
                )

        if future_tomo_obj is not None:
            self.Outputs.future_out.send(future_tomo_obj)

    def setDryRun(self, dry_run):
        self._processingStack.setDryRun(dry_run)

    def _ciExec(self):
        self.activateWindow()
        self.raise_()
        self.show()

    def _updateSettingsVals(self):
        self._ewoks_default_inputs = self.getConfiguration(
            allow_no_nabu_config_defined=True
        )

    def _skipProcessing(self, scan):
        self.Outputs.data.send(scan)
        self.sigScanReady.emit(scan)

    def getConfiguration(self, allow_no_nabu_config_defined: bool = False):
        """
        :param allow_no_nabu_config_defined: when we save the settings we don't want to raise an error when saving the configuration.
        But when getting it for reconstructing we want to
        """
        nabu_params_and_slurm_config = super().getConfiguration()
        assert ParametersKeys.SLURM_CLUSTER_KEY in nabu_params_and_slurm_config
        assert ParametersKeys.NABU_REC_PARAMS_KEY in nabu_params_and_slurm_config
        if (not allow_no_nabu_config_defined) and (
            not nabu_params_and_slurm_config[ParametersKeys.NABU_REC_PARAMS_KEY]
        ):
            raise ValueError(
                "No reconstruction parameters provided. You should have a 'nabu settings' widget connected upstream"
            )
        nabu_volume_params = self._nabuWidget.getConfiguration()

        # update received nabu params by the overwritten parameters defined by the widget
        nabu_params_and_slurm_config[ParametersKeys.NABU_REC_PARAMS_KEY] = (
            concatenate_dict(
                nabu_params_and_slurm_config[ParametersKeys.NABU_REC_PARAMS_KEY],
                nabu_volume_params,
            )
        )
        return nabu_params_and_slurm_config

    def setConfiguration(self, config: dict):
        if config is None:
            return
        if not isinstance(config, dict):
            raise TypeError(f"config is expected to be a config. Got {type(config)}")
        super().setConfiguration(config)
        nabu_config = config.get(ParametersKeys.NABU_REC_PARAMS_KEY, {})
        if nabu_config is None:
            # backward compatibility
            nabu_config = {}
        self._nabuWidget.setConfiguration(config=nabu_config)
