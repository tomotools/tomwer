from __future__ import annotations

from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.settings import ParametersKeys


class BaseNabuRecOW:
    """
    Base class of all widgets launching nabu reconstructions
    """

    def __init__(self):
        self._nabu_params = {}
        self._slurm_cluster = {}

    def getConfiguration(self) -> dict:
        return {
            ParametersKeys.NABU_REC_PARAMS_KEY: self._nabu_params,
            ParametersKeys.SLURM_CLUSTER_KEY: self._slurm_cluster,
        }

    def setConfiguration(self, config: dict) -> None:
        if ParametersKeys.NABU_REC_PARAMS_KEY in config:
            self._nabu_params = config[ParametersKeys.NABU_REC_PARAMS_KEY]
        if ParametersKeys.SLURM_CLUSTER_KEY in config:
            self._slurm_cluster = config[ParametersKeys.SLURM_CLUSTER_KEY]

    def setClusterConfiguration(self, config: SlurmClusterConfiguration | dict):
        if isinstance(config, SlurmClusterConfiguration):
            config = config.to_dict()
        if not isinstance(config, (type(None), dict)):
            raise TypeError(
                f"Expect None, a dict or an instance of SlurmClusterConfiguration. Not {type(config)}"
            )
        self._slurm_cluster = config

    def setNabuParams(self, config: dict) -> None:
        assert config is None or isinstance(config, dict)
        self._nabu_params = config
