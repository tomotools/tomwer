"""
(Ewoks)Orange Widgets for tomography core processing
"""

NAME = "reconstruction"

ID = "orangecontrib.tomwer.widgets.reconstruction"

DESCRIPTION = "widgets for data reconstruction"

LONG_DESCRIPTION = (
    "Contains widgets for scan reconstruction (mabu slice, nabu volume...)"
)

ICON = "../../widgets/icons/tomwer_build.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 2
