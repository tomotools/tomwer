from __future__ import annotations

import logging

from orangewidget import gui
from silx.gui import qt

from orangecontrib.tomwer.orange.managedprocess import TomwerWithStackStack
from orangecontrib.tomwer.orange.settings import CallbackSettingsHandler

import tomwer.tasks.reconstruction.nabu.multicor
from tomwer.tasks.reconstruction.cor import CoRTask
from tomwer.core import settings
from tomwer.core.utils.lbsram import is_low_on_memory
from tomwer.core.reconstruction.params_cache import (
    save_reconstruction_parameters_to_cache,
)
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.settings import ParametersKeys
from tomwer.synctools.axis import QCoRParams
from tomwer.synctools.multicor import QMultiCoRParams

from processview.core.manager import ProcessManager, DatasetState
from processview.core import helpers as pv_helpers
from processview.core.superviseprocess import SuperviseProcess

from tomwer.gui.reconstruction.multicor.MultiCoRWindow import MultiCoRWindow


_logger = logging.getLogger(__name__)


class SAAxisOW(
    TomwerWithStackStack,
    SuperviseProcess,
    ewokstaskclass=tomwer.tasks.reconstruction.nabu.multicor.MultiCoRTask,
):
    """
    Widget for multi-CoR calculation.
    It could be call 'MultiCoROW'. But in order to keep the backward compatibility as much as possible we kept the original name

    behavior within a workflow:

    * when a scan arrived:

        * if he already has a center of rotation defined (if the axis widget
          has been defined for example) it will be used as
          'estimated center of rotation'
        * if no cor has been computed yet and if the .nx entry contains
          information regarding an "x_rotation_axis_pixel_position" this value will
          be set

    * if autofocus option is lock:
        * launch the series of reconstruction (with research width defined)
          and the estimated center of rotation if defined. Once the
          reconstruction is ended and if the autofocus button is still lock
          it will select the cor with the highest
          value and mode to workflow downstream.

    * hint: you can define a "multi-step" half-automatic center of rotation
      research by creating several "saaxis" widget and reducing the research
      width.

    Details about :ref:`multi-cor score calculation`

    .. note:: in the absolute it should be named MultiCoROW. But in order to keep as much as possible backward compatibility we keep the name.
    """

    name = "multi-cor (sa-axis)"
    id = "orange.widgets.tomwer.sa_axis"
    description = "Reconstruct a slice with different center of rotation (cor) values"
    icon = "icons/saaxis.png"
    priority = 21
    keywords = [
        "multi",
        "multi-cor",
        "tomography",
        "semi automatic",
        "half automatic",
        "axis",
        "tomwer",
        "reconstruction",
        "rotation",
        "position",
        "center of rotation",
        "saaxis",
    ]

    want_main_area = True
    want_control_area = False
    resizing_enabled = True

    settingsHandler = CallbackSettingsHandler()

    sigScanReady = qt.Signal(TomwerScanBase)
    """Signal emitted when a scan is ready"""

    _ewoks_inputs_to_hide_from_orange = (
        "multicor_params",
        "dry_run",
        "dump_roi",
        "dump_process",
        "serialize_output_data",
        "pool_size",
        "platform_resources",
        "process_id",
    )

    def __init__(self, parent=None):
        """

        :param parent: QWidget parent or None
        """
        super().__init__(parent)

        self._layout = gui.vBox(self.mainArea, self.name).layout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._widget = MultiCoRWindow(parent=self)
        self._layout.addWidget(self._widget)

        self.loadSettings()

        # for GUI / orange we want to avoid serialization
        self.set_dynamic_input("serialize_output_data", False)

        # connect signal / slot
        self._widget.sigConfigurationChanged.connect(self._updateSettings)

        self._widget.sigValidated.connect(self.validate)
        self._widget.sigValidated.connect(self.accept)
        self._widget._multicorControl.sigComputationRequest.connect(
            self.execute_ewoks_task_without_propagation  # propagation is handle once processing is done to handle interactive of auto modes
        )
        self._widget._multicorControl.sigComputationRequest.connect(
            self._widget.showResults
        )
        self.task_executor_queue.sigComputationStarted.connect(self._startProcessing)
        self.task_executor_queue.sigComputationEnded.connect(self._endProcessing)

    def loadSettings(self):
        self.set_dynamic_input("process_id", self.process_id)
        self.setConfiguration(
            {
                ParametersKeys.MULTICOR_PARAMS_KEY: self.get_default_input_value(
                    ParametersKeys.MULTICOR_PARAMS_KEY, {}
                ),
                ParametersKeys.PLATFORM_RESOURCES: self.get_default_input_value(
                    ParametersKeys.PLATFORM_RESOURCES, {}
                ),
                ParametersKeys.SLURM_CLUSTER_KEY: self.get_default_input_value(
                    ParametersKeys.SLURM_CLUSTER_KEY, {}
                ),
            }
        )

    def _updateSettings(self):
        self.set_default_input(
            ParametersKeys.MULTICOR_PARAMS_KEY,
            self._widget.getConfiguration()[ParametersKeys.MULTICOR_PARAMS_KEY],
        )
        self.set_default_input(
            ParametersKeys.PLATFORM_RESOURCES,
            self._widget.getConfiguration()[ParametersKeys.PLATFORM_RESOURCES],
        )

    def getWaitingOverlay(self):
        return self._widget._tabWidget._resultsViewer._plot.getWaiterOverlay()

    def _startProcessing(self, *args, **kwargs):
        self.getWaitingOverlay().show()
        super()._startProcessing()

    def _endProcessing(self, *args, **kwargs):
        self.getWaitingOverlay().hide()
        super()._endProcessing()

    def setConfiguration(self, configuration):
        self._widget.setConfiguration(configuration)

    def process(self, scan):
        """
        call when a new scan is received
        """
        if scan is None:
            return
        if scan.cor_params is None:
            scan.cor_params = QCoRParams()
        if scan.multicor_params is None:
            scan.multicor_params = QMultiCoRParams()
        # clear interface
        self._widget.clearResults()
        self.set_dynamic_input("data", scan)

        if settings.isOnLbsram(scan) and is_low_on_memory(settings.get_lbsram_path()):
            self.notify_skip(
                scan=scan,
                details=f"multi-cor has been skipped for {scan} because of low space in lbsram",
            )
            self.propagate_downstream()
        else:
            self._widget.setScan(scan=scan)
            self.notify_pending(scan)
            self.activateWindow()
            if self.isAutoFocusLock():
                self.execute_ewoks_task_without_propagation()
            else:
                self.raise_()
                self.show()

    def cancel(self, scan):
        # function call when we want to cancel the given scan
        if scan is None:
            return
        if scan in self._widget._processing_stack:
            self._widget._processing_stack.remove(scan)
        if (
            self._widget._processing_stack._data_currently_computed.get_identifier().to_str()
            == scan.get_identifier().to_str()
        ):
            # stop current processing
            self._widget._processing_stack.cancel()
            # if possible process next
            if self._widget._processing_stack.can_process_next():
                self._widget._processing_stack._process_next()

    def computeEstimatedCor(self) -> float | None:
        """callback when calculation of a estimated cor is requested"""
        scan = self.get_task_input_value("data", None)
        if scan is None:
            _logger.error("A scan must be defined before computing the CoR on it")
            return

        cor_params_info = self.getQAxisRP().get_simple_str()

        text = f"start automatic cor for {scan} with {cor_params_info}"
        _logger.inform(text)
        cor_estimation_process = CoRTask(
            inputs={
                ParametersKeys.COR_PARAMS_KEY: self.getQAxisRP(),
                "data": scan,
                "wait": True,
                "serialize_output_data": False,
            },
            process_id=-1,
        )
        try:
            cor_estimation_process.run()
        except Exception as e:
            text = f"Unable to run automatic cor calculation. Reason is {e}"
            _logger.error(text)
            msg = qt.QMessageBox(self)
            msg.setIcon(qt.QMessageBox.Critical)
            msg.setText(text)
            msg.show()
            return None
        else:
            cor = scan.cor_params.relative_cor_value
            text = f"automatic cor computed for {scan}: {cor} ({cor_params_info})"
            _logger.inform(text)
            self.setEstimatedCorPosition(value=cor)
            self.getAutomaticCorWindow().hide()
            return cor

    def _updateAccordingToResult(self):
        """
        Function call once the task has been executed. In order to update the GUI
        """
        scan = self.get_task_output_value("data", None)
        if scan is None:
            return
        if not isinstance(scan, TomwerScanBase):
            raise TypeError(
                f"scan is expected to be an instance of TomwerScanBase. Got ({type(scan)})"
            )

        self._widget.setCorScores(
            scan.multicor_params.scores,
            score_method=scan.multicor_params.score_method,
            img_width=scan.multicor_params.image_width,
        )
        if scan.multicor_params.autofocus is not None:
            self._widget.setCurrentCorValue(scan.multicor_params.autofocus)
        pm = ProcessManager()
        details = pm.get_dataset_details(dataset_id=scan.get_identifier(), process=self)
        current_state = pm.get_dataset_state(
            dataset_id=scan.get_identifier(), process=self
        )
        if current_state not in (
            DatasetState.CANCELLED,
            DatasetState.FAILED,
            DatasetState.SKIPPED,
        ):
            ProcessManager().notify_dataset_state(
                dataset=scan,
                process=self,
                details=details,
                state=DatasetState.WAIT_USER_VALIDATION,
            )
            self._raiseResults()

        if self._widget.isAutoFocusLock():
            _logger.processSucceed(
                f"multicor processing succeeded with {scan.cor_params.relative_cor_value} as cor value"
            )
            self.validate()

    def validate(self):
        """Function call when the user validate manually the cor to be picked"""
        scan = self.get_task_output_value("data", None)
        if scan is None:
            return
        selected_cor_value = self.getCurrentCorValue() or scan.multicor_params.autofocus
        # if validate is done manually then pick current cor value; else we are in 'auto mode' and get it from the autofocus.
        details = ProcessManager().get_dataset_details(
            dataset_id=scan.get_identifier(), process=self
        )
        if details is None:
            details = ""
        if selected_cor_value is None:
            infos = f"no selected cor value. {scan} skip SAAXIS"
            infos = "\n".join((infos, details))
            _logger.warning(infos)
            scan.cor_params.set_relative_value(None)
            pv_helpers.notify_skip(process=self, dataset=scan, details=infos)
        else:
            scan.cor_params.set_relative_value(selected_cor_value)
            save_reconstruction_parameters_to_cache(scan=scan)

            if scan.nabu_recons_params is not None:
                if "reconstruction" not in scan.nabu_recons_params:
                    scan.nabu_recons_params["reconstruction"] = {}
                scan.nabu_recons_params["reconstruction"][
                    "rotation_axis_position"
                ] = scan.cor_params.absolute_cor_value
            r_cor = scan.cor_params.relative_cor_value
            a_cor = scan.cor_params.absolute_cor_value
            infos = f"cor selected for {scan}: relative: {r_cor}, absolute: {a_cor}"
            infos = "\n".join((infos, details))
            pv_helpers.notify_succeed(process=self, dataset=scan, details=infos)

        self.propagate_downstream()

    # ewoks class implementation

    def _raiseResults(self):
        if not self.isAutoFocusLock():
            self.raise_()
            self.show()
            self._widget.showResults()

    def handleNewSignals(self):
        scan = self.get_task_input_value("data", None)
        if scan is not None:
            self.process(scan)

    def _output_changed(self):
        self._updateAccordingToResult()
        return super()._output_changed()

    # expose API

    def validateCurrentScan(self):
        self._widget.validateCurrentScan()

    def getCurrentCorValue(self):
        return self._widget.getCurrentCorValue()

    def setEstimatedCorPosition(self, value):
        self._widget.setEstimatedCorPosition(value=value)

    def load_sinogram(self):
        self._widget.loadSinogram()

    def lockAutofocus(self, lock):
        self._widget.lockAutofocus(lock=lock)

    def isAutoFocusLock(self):
        return self._widget.isAutoFocusLock()
