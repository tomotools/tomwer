# coding: utf-8
from __future__ import annotations

import logging

from orangewidget import gui
from orangewidget.settings import Setting
from orangewidget.widget import Input, Output
from processview.core.manager import DatasetState, ProcessManager
from silx.gui import qt

import tomwer.tasks.reconstruction.nabu.multipag
from orangecontrib.tomwer.orange.managedprocess import SuperviseOW
from orangecontrib.tomwer.orange.settings import CallbackSettingsHandler
from tomwer.core import settings
from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.scan.scanbase import TomwerScanBase, _TomwerBaseDock
from tomwer.core.utils.lbsram import is_low_on_memory
from tomwer.core.settings import ParametersKeys
from tomwer.synctools.axis import QCoRParams
from tomwer.synctools.multipag import QMultiPagParams

from ..utils import WidgetLongProcessing
from .MultiPagWindow import MultiPagWindow

_logger = logging.getLogger(__name__)


class SADeltaBetaOW(SuperviseOW, WidgetLongProcessing):
    """
    Widget for semi-automatic delta / beta calculation.
    It could be call 'MultiPagOW'. But in order to keep the backward compatibility as much as possible we kept the original name

    behavior within a workflow:

    * no delta / beta value will be loaded even if an "axis" window exists on
      the upper stream.

    * if autofocus option is lock:

        * launch the series of reconstruction (with research width defined)
          and the estimated center of rotation if defined. Once the
          reconstruction is ended and if the autofocus button is still lock
          it will select the cor with the highest
          value and mode to workflow downstream.

    * hint: you can define a "multi-step" half-automatic center of rotation
      research by creating several "sa_delta_beta" widget and reducing the
      research width.

    Details about :ref:`multi-pag score calculation`
    """

    name = "multi-pag (sa-delta/beta calculation)"
    id = "orange.widgets.tomwer.sa_delta_beta"
    description = "Reconstruct a slice with several delta / beta values."
    icon = "icons/delta_beta_range.png"
    priority = 22
    keywords = [
        "multi",
        "multi-pag",
        "tomography",
        "semi automatic",
        "half automatic",
        "axis",
        "delta-beta",
        "delta/beta",
        "delta",
        "beta",
        "tomwer",
        "reconstruction",
        "position",
        "center of rotation",
        "sadeltabetaaxis",
        "sa_delta_beta_axis",
        "sa_delta_beta",
        "sadeltabeta",
    ]

    ewokstaskclass = tomwer.tasks.reconstruction.nabu.multipag.MultiPagTask

    want_main_area = True
    resizing_enabled = True

    settingsHandler = CallbackSettingsHandler()

    sigScanReady = qt.Signal(TomwerScanBase)
    """Signal emitted when a scan is ready"""

    _ewoks_default_inputs = Setting(
        {
            "data": None,
            ParametersKeys.MULTIPAG_PARAMS_KEY: {},
            ParametersKeys.NABU_REC_PARAMS_KEY: {},
            ParametersKeys.SLURM_CLUSTER_KEY: {},
        }
    )

    class Inputs:
        data = Input(name="data", type=TomwerScanBase, default=True, multiple=False)
        data_recompute = Input(
            name="change recons params",
            type=_TomwerBaseDock,
            doc="recompute delta / beta",
        )
        cluster_in = Input(
            name=ParametersKeys.SLURM_CLUSTER_KEY,
            type=SlurmClusterConfiguration,
            doc="slurm cluster to be used",
            multiple=False,
        )
        nabu_rec_config = Input(
            name="rec config",
            type=dict,
            doc="nabu reconstruction configuration",
            multiple=False,
        )

    class Outputs:
        data = Output(name="data", type=TomwerScanBase)
        nabu_rec_config = Output(
            name="rec config",
            type=dict,
            doc="nabu reconstruction configuration",
        )

    def __init__(self, parent=None):
        """

        :param parent: QWidget parent or None
        """
        SuperviseOW.__init__(self, parent)
        WidgetLongProcessing.__init__(self)

        self._layout = gui.vBox(self.mainArea, self.name).layout()
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._widget = MultiPagWindow(
            Outputs=self.Outputs, parent=self, process_id=self.process_id
        )
        self._layout.addWidget(self._widget)

        # update configuration
        self.setConfiguration(self._ewoks_default_inputs)

        # connect signal / slot
        self._widget.sigConfigurationChanged.connect(self._updateSettings)
        self._widget._processing_stack.sigComputationStarted.connect(
            self._startProcessing
        )
        self._widget._processing_stack.sigComputationEnded.connect(self._endProcessing)
        # expose API
        self.wait_processing = self._widget.wait_processing

    def getCurrentCorValue(self):
        return self._widget.getCurrentCorValue()

    def load_sinogram(self):
        self._widget.loadSinogram()

    def compute(self):
        self._widget.compute()

    def lockAutofocus(self, lock):
        self._widget.lockAutofocus(lock=lock)

    def isAutoFocusLock(self):
        return self._widget.isAutoFocusLock()

    @Inputs.data
    def process(self, scan):
        if scan is None:
            return
        if scan.cor_params is None:
            scan.cor_params = QCoRParams()
        if scan.multipag_params is None:
            scan.multipag_params = QMultiPagParams()
        self._skipCurrentScan(new_scan=scan)

        if settings.isOnLbsram(scan) and is_low_on_memory(settings.get_lbsram_path()):
            self.notify_skip(
                scan=scan,
                details=f"sa-delta-beta has been skiped for {scan} because of low space in lbsram",
            )
            self.Outputs.nabu_rec_config.send(
                scan.nabu_recons_params,
            )
            self.Outputs.data.send(scan)
        else:
            self._widget.setScan(scan=scan)
            self.notify_pending(scan)
            self.activateWindow()
            if self.isAutoFocusLock():
                self.compute()
            else:
                self.raise_()
                self.show()

    @Inputs.data_recompute
    def reprocess(self, dataset):
        self.lockAutofocus(False)
        self.process(dataset)

    @Inputs.nabu_rec_config
    def setNabuRecConfig(self, config):
        self._widget.setNabuParams(config=config)

    @Inputs.cluster_in
    def setCluster(self, cluster):
        self._widget.setClusterConfiguration(config=cluster)

    def _skipCurrentScan(self, new_scan):
        scan = self._widget.getScan()
        # if the same scan has been run several scan
        if scan is None or str(scan) == str(new_scan):
            return
        current_scan_state = ProcessManager().get_dataset_state(
            dataset_id=scan.get_identifier(), process=self
        )
        if current_scan_state in (
            DatasetState.PENDING,
            DatasetState.WAIT_USER_VALIDATION,
        ):
            details = "Was pending and has been replaced by another scan."
            self.notify_skip(scan=scan, details=details)
            self.Outputs.nabu_rec_config.send(
                scan.nabu_recons_params,
            )
            self.Outputs.data.send(scan)

    def validateCurrentScan(self):
        self._widget.validateCurrentScan()

    def _updateSettings(self):
        self._ewoks_default_inputs = self.getConfiguration(
            allow_no_nabu_config_defined=True
        )

    def cancel(self, scan):
        if scan is None:
            return
        if scan in self._widget._processing_stack:
            self._widget._processing_stack.remove(scan)
        if (
            self._widget._processing_stack._data_currently_computed.get_identifier().to_str()
            == scan.get_identifier().to_str()
        ):
            # stop current processing
            self._widget._processing_stack.cancel()
            # if possible process next
            if self._widget._processing_stack.can_process_next():
                self._widget._processing_stack._process_next()

    def getWaitingOverlay(self):
        return self._widget._tabWidget._resultsViewer._plot.getWaiterOverlay()

    def _startProcessing(self, *args, **kwargs):
        self.getWaitingOverlay().show()
        super()._startProcessing()

    def _endProcessing(self, *args, **kwargs):
        self.getWaitingOverlay().hide()
        super()._endProcessing()

    # expose API
    def getConfiguration(self, allow_no_nabu_config_defined: bool = False):
        return self._widget.getConfiguration(
            allow_no_nabu_config_defined=allow_no_nabu_config_defined
        )

    def setConfiguration(self, config: dict):
        self._widget.setConfiguration(config=config)
