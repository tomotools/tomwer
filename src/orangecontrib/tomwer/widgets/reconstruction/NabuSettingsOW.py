from __future__ import annotations

from orangewidget import gui
from orangewidget.settings import Setting
from orangewidget.widget import Output, OWBaseWidget

from tomwer.gui.reconstruction.nabu.nabuconfig.NabuSettingsWindow import (
    NabuSettingsWindow,
)


class NabuSettingsOW(OWBaseWidget):
    """
    widget used to define settings to be used for a tomographic reconstruction using nabu.
    """

    # note of this widget should be the one registered on the documentation
    name = "nabu settings"
    id = "orange.widgets.tomwer.reconstruction.NabuSettingsOW.NabuSettingsOW"
    description = (
        "This widget defines the settings to be used for reconstruction using nabu."
    )
    icon = "icons/nabu_settings.svg"
    priority = 2
    keywords = [
        "tomography",
        "nabu",
        "reconstruction",
        "volume",
        "settings",
    ]

    want_control_area = False

    _confSetting = Setting(dict())

    class Outputs:
        config = Output(
            name="rec config", type=dict, doc="nabu reconstruction configuration"
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        _layout = gui.vBox(self.mainArea, self.name).layout()
        # main widget
        self._nabuWidget = NabuSettingsWindow(parent=self)
        _layout.addWidget(self._nabuWidget)

        self._nabuWidget.setConfiguration(config=self._confSetting)
        self._nabuWidget.sigConfigChanged.connect(self._updateSettingsVals)
        self._nabuWidget.sigConfigChanged.connect(self._triggerDownstream)

        # trigger the signal to avoid any user request
        self._triggerDownstream()

    def getConfiguration(self) -> dict:
        return self._nabuWidget.getConfiguration()

    def _updateSettingsVals(self):
        self._configuration = self.getConfiguration()

    def _triggerDownstream(self):
        self.Outputs.config.send(self.getConfiguration())
