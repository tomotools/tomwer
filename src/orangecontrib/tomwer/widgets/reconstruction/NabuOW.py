# coding: utf-8

from __future__ import annotations

import copy
import logging
from contextlib import AbstractContextManager

from orangewidget import gui
from orangewidget.settings import Setting
from orangewidget.widget import Input, Output
from silx.gui import qt
from orangecontrib.tomwer.widgets.reconstruction.BaseNabuRecOW import BaseNabuRecOW

import tomwer.tasks.reconstruction.nabu.slices
from tomwer.core.utils.dictutils import concatenate_dict
from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.reconstruction.params_cache import (
    load_reconstruction_parameters_from_cache,
    save_reconstruction_parameters_to_cache,
)
from tomwer.core.scan.scanbase import TomwerScanBase, _TomwerBaseDock
from tomwer.core.volume.volumefactory import VolumeFactory
from tomwer.core.settings import ParametersKeys
from tomwer.gui.reconstruction.nabu.slices.NabuSlicesWindow import NabuSlicesWindow
from tomwer.synctools.axis import QCoRParams
from tomwer.synctools.stacks.reconstruction.nabu import NabuSliceProcessStack

from ...orange.managedprocess import SuperviseOW
from ..utils import WidgetLongProcessing

_logger = logging.getLogger(__name__)


class NabuOW(WidgetLongProcessing, SuperviseOW, BaseNabuRecOW):
    """
    A simple widget managing the copy of an incoming folder to an other one

    :param parent: the parent widget
    """

    # note of this widget should be the one registered on the documentation
    name = "nabu slice reconstruction"
    id = "orange.widgets.tomwer.reconstruction.NabuOW.NabuOW"
    description = "This widget will call nabu for running a reconstruction "
    icon = "icons/nabu_slices.svg"
    priority = 12
    keywords = ["tomography", "nabu", "reconstruction", "FBP", "filter"]

    want_main_area = True
    resizing_enabled = True

    _ewoks_default_inputs = Setting(
        {
            ParametersKeys.NABU_REC_PARAMS_KEY: {},  # reconstruction parameters to be used for running nabu. Resources section will be overwrite from the widget
            "slices": {},  # slices to be reconstructed. Expected to be a dict with plane (XY) as keys
        }
    )

    ewokstaskclass = tomwer.tasks.reconstruction.nabu.slices.NabuSlicesTask

    sigScanReady = qt.Signal(TomwerScanBase)
    "Signal emitted when a scan is ended"

    class Inputs:
        reprocess = Input(
            name="change recons params",
            type=_TomwerBaseDock,
            doc="recompute slice with different parameters",
        )
        data = Input(
            name="data",
            type=TomwerScanBase,
            doc="one scan to be process",
            default=True,
            multiple=True,
        )
        cluster_in = Input(
            name=ParametersKeys.SLURM_CLUSTER_KEY,
            type=SlurmClusterConfiguration,
            doc="slurm cluster to be used",
            multiple=False,
        )
        nabu_rec_config = Input(
            name="rec config",
            type=dict,
            doc="nabu reconstruction configuration",
            multiple=False,
        )

    class Outputs:
        data = Output(name="data", type=TomwerScanBase, doc="one scan to be process")

        future_out = Output(
            name="future_tomo_obj",
            type=FutureTomwerObject,
            doc="data with some remote processing",
        )

        slice_urls = Output(name="slice urls", type=tuple, doc="tuple of urls created")

    class DialogCM(AbstractContextManager):
        """Simple context manager to hide / show button dialogs"""

        def __init__(self, dialogButtonsBox):
            self._dialogButtonsBox = dialogButtonsBox

        def __enter__(self):
            self._dialogButtonsBox.show()

        def __exit__(self, exc_type, exc_val, exc_tb):
            self._dialogButtonsBox.hide()

    def __init__(self, parent=None):
        SuperviseOW.__init__(self, parent)
        WidgetLongProcessing.__init__(self)
        BaseNabuRecOW.__init__(self)

        self.__exec_for_ci = False
        # processing tool
        self._processingStack = NabuSliceProcessStack(self, process_id=self.process_id)

        _layout = gui.vBox(self.mainArea, self.name).layout()
        # main widget
        self._nabuWidget = NabuSlicesWindow(parent=self)
        _layout.addWidget(self._nabuWidget)

        _layout.addItem(
            qt.QSpacerItem(100, 40, qt.QSizePolicy.Expanding, qt.QSizePolicy.Expanding)
        )
        # add button to validate when change reconstruction parameters is
        # called
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(self)
        self._buttons.setStandardButtons(types)
        _layout.addWidget(self._buttons)

        # set up
        self._buttons.hide()

        # load settings
        self.setConfiguration(self._ewoks_default_inputs)

        # connect signal / slot
        self._processingStack.sigComputationStarted.connect(self._startProcessing)
        self._processingStack.sigComputationEnded.connect(self._endProcessing)
        self._buttons.button(qt.QDialogButtonBox.Ok).clicked.connect(self.accept)
        self._nabuWidget.sigConfigChanged.connect(self._updateSettingsVals)

    @Inputs.data
    def process(self, scan, *args, **kwargs):
        assert isinstance(scan, (TomwerScanBase, type(None)))
        if scan is None:
            return
        scan_ = copy.copy(scan)
        scan_.clear_latest_reconstructions()

        _logger.info(f"add {scan} to the stack")
        # update the reconstruction mode if possible
        self._processingStack.add(scan_, self.getConfiguration())

    @Inputs.reprocess
    def reprocess(self, scan):
        """Recompute nabu with different parameters"""
        # wait for user to tune the reconstruction
        if scan is None:
            return

        if scan.cor_params is None or scan.cor_params.relative_cor_value is None:
            # try to retrieve last computed cor value from nabu process
            if scan.cor_params is None:
                scan.cor_params = QCoRParams()
            load_reconstruction_parameters_from_cache(scan=scan)

        self.show()
        with NabuOW.DialogCM(self._buttons):
            if self.__exec_for_ci is True:
                self._ciExec()
            else:
                if self.exec():
                    # for now The behavior for reprocessing is the sama as for processing
                    if hasattr(scan, "instance"):
                        self.process(scan.instance)
                    else:
                        self.process(scan)

    @Inputs.cluster_in
    def setCluster(self, slurm_cluster: SlurmClusterConfiguration | None):
        self.setClusterConfiguration(config=slurm_cluster)

    @Inputs.nabu_rec_config
    def setNabuRecConfig(self, nabu_rec_config: dict | None):
        self.setNabuParams(config=nabu_rec_config)

    def cancel(self, scan):
        if scan is None:
            return
        if scan in self._processingStack:
            self._processingStack.remove(scan)
        if (
            self._processingStack._data_currently_computed.get_identifier().to_str()
            == scan.get_identifier().to_str()
        ):
            # stop current processing
            self._processingStack.cancel()
            # if possible process next
            if self._processingStack.can_process_next():
                self._processingStack._process_next()

    def _endProcessing(self, scan, future_tomo_obj):
        WidgetLongProcessing._endProcessing(self, scan)
        if scan is not None:
            save_reconstruction_parameters_to_cache(scan=scan)
            # send scan
            self.Outputs.data.send(scan)
            # send slice urls
            slice_urls = []
            for rec_identifier in scan.latest_reconstructions:
                slice_urls.extend(
                    VolumeFactory.from_identifier_to_vol_urls(rec_identifier)
                )
            if len(slice_urls) > 0:
                slice_urls = tuple(slice_urls)
                # provide list of reconstructed slices
                self.Outputs.slice_urls.send(slice_urls)

            self.sigScanReady.emit(scan)
        if future_tomo_obj is not None:
            # send future scan
            self.Outputs.future_out.send(future_tomo_obj)

    def setDryRun(self, dry_run):
        self._processingStack.setDryRun(dry_run)

    def _ciExec(self):
        self.activateWindow()
        self.raise_()
        self.show()

    def _replaceExec_(self):
        """used for CI, replace the exec_ call ny"""
        self.__exec_for_ci = True

    def _updateSettingsVals(self):
        self._ewoks_default_inputs = self.getConfiguration(
            allow_no_nabu_config_defined=True
        )

    def getConfiguration(self, allow_no_nabu_config_defined: bool = False):
        """
        :param allow_no_nabu_config_defined: when we save the settings we don't want to raise an error when saving the configuration.
        But when getting it for reconstructing we want to
        """
        slices_config = self._nabuWidget.getConfiguration()
        nabu_param_and_cluster_config = super().getConfiguration()
        if (not allow_no_nabu_config_defined) and (
            not nabu_param_and_cluster_config[ParametersKeys.NABU_REC_PARAMS_KEY]
        ):
            raise ValueError(
                "No reconstruction parameters provided. You should have a 'nabu settings' widget connected upstream"
            )

        # overwrite nabu configuration from the GUI - overwriting the gpu / cpu percentage, gpu to be used...
        nabu_param_and_cluster_config[ParametersKeys.NABU_REC_PARAMS_KEY][
            "resources"
        ] = slices_config.pop("resources_overwritten", {})

        return concatenate_dict(
            nabu_param_and_cluster_config,
            slices_config,
        )

    def setConfiguration(self, config):
        # ignore slurm cluster. Defined by the upper widget
        super().setConfiguration(config=config)
        config.pop(ParametersKeys.SLURM_CLUSTER_KEY, None)

        # only the [ParametersKeys.NABU_REC_PARAMS_KEY]["resources"] can be overwritten here
        nabu_params = config.get(ParametersKeys.NABU_REC_PARAMS_KEY, {})
        if nabu_params is None:
            nabu_params = {}

        self._nabuWidget.setConfiguration(
            concatenate_dict(
                config,
                {"resources_overwritten": nabu_params},
            )
        )

    def keyPressEvent(self, event):
        """The event has to be filtered since we have some children
        that can be edited using the 'enter' key as defining the cor manually
        (see #481)). As we are in a dialog this automatically trigger
        'accepted'. See https://forum.qt.io/topic/5080/preventing-enter-key-from-triggering-ok-in-qbuttonbox-in-particular-qlineedit-qbuttonbox/5
        """
        if event.key() not in (qt.Qt.Key_Enter, qt.Qt.Key_Return):
            super().keyPressEvent(event)
