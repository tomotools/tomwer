"""
(Ewoks)Orange Widgets for pre or post processing stitching
"""

NAME = "stitching"

ID = "orangecontrib.tomwer.widgets.stitching"

DESCRIPTION = "widgets for data stitching"

LONG_DESCRIPTION = "Contains widget for stitching operation"

ICON = "../../widgets/icons/tomwer_stitching.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 12
