"""
(Ewoks)Orange Widgets for debugging. To be used at your own risk and peril
"""

NAME = "debug tools"

ID = "orangecontrib.tomwer.widgets.debugtools"

DESCRIPTION = "some widgets to help debugging"

LONG_DESCRIPTION = "some widgets to help debugging"

ICON = "../../widgets/icons/tomwer_debug_tools.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 20
