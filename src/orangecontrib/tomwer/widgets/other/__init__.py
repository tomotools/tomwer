"""
(Ewoks)Orange Widgets for tomography not matching any other category
"""

NAME = "other"

ID = "orangecontrib.tomwer.widgets.other"

DESCRIPTION = "some utils widgets"

LONG_DESCRIPTION = "some utils widgets"

ICON = "../../widgets/icons/tomwer_other.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 20
