"""
(Ewoks)Orange Widgets to 'control' the flow. Like defining input / output, stacking scans...
Those widget are not doing any 'core' processing.
"""

NAME = "data portal"

ID = "orangecontrib.tomwer.widgets.dataportal"

DESCRIPTION = "widgets for data portal publication"

LONG_DESCRIPTION = "Contains widget to publish data to the esrf data portal"

ICON = "../../widgets/icons/tomwer_data_portal.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 13
