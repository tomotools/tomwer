"""
(Ewoks)Orange Widgets to 'control' the flow. Like defining input / output, stacking scans...
Those widget are not doing any 'core' processing.
"""

NAME = "control"

ID = "orangecontrib.tomwer.widgets.control"

DESCRIPTION = "widgets for data control"

LONG_DESCRIPTION = "Contains widget to control the scan flow"

ICON = "../../widgets/icons/tomwer_control.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 1
