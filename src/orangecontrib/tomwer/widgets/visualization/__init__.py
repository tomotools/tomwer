"""
(Ewoks)Orange Widgets for tomography raw and processed data visualization
"""

NAME = "visualization"

ID = "orangecontrib.tomwer.widgets.visualization"

DESCRIPTION = "widgets for data visualization"

LONG_DESCRIPTION = (
    "Contains widgets for visualization of the acquisitions," " reconstruction..."
)

ICON = "../../widgets/icons/tomwer_visu.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 3
