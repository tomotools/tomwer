"""
(Ewoks)Orange Widgets for NXtomo edition
"""

NAME = "edit HDF5 - NXTomo"

ID = "orangecontrib.tomwer.widgets.edit"

DESCRIPTION = "widgets for editing NXTomo"

LONG_DESCRIPTION = "Contains widgets for editing NXtomo entries"

ICON = "../../widgets/icons/tomwer_edit.png"

BACKGROUND = "#C0CCFF"

PRIORITY = 8
