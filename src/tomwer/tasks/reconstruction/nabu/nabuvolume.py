"""reconstruction of a volume from nabu"""

from __future__ import annotations

import logging
import copy
from nabu import version as nabu_version
from processview.core.manager.manager import ProcessManager, DatasetState
from processview.core.superviseprocess import SuperviseProcess

from tomwer.core.cluster.cluster import SlurmClusterConfiguration
from tomwer.core.futureobject import FutureTomwerObject
from tomwer.tasks.task import Task
from tomwer.core.drac.processeddataset import DracReconstructedVolumeDataset
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils.scanutils import data_identifier_to_scan
from tomwer.core.volume.volumefactory import VolumeFactory
from tomwer.core.reconstruction.nabu import utils
from tomwer.core.reconstruction.nabu.nabucommon import (
    ResultsLocalRun,
    ResultSlurmRun,
)
from tomwer.core.reconstruction.nabu.nabuvolume import VolumeRunner
from tomwer.core.reconstruction.nabu.target import Target
from tomwer.core.reconstruction.utils.cor import copy_cor
from tomwer.io.utils import format_stderr_stdout

_logger = logging.getLogger(__name__)


class NabuVolumeTask(
    Task,
    SuperviseProcess,
    input_names=("data", ParametersKeys.NABU_REC_PARAMS_KEY),
    output_names=(
        "data",
        "volumes",
        "future_tomo_obj",
        "data_portal_processed_datasets",
    ),
    optional_input_names=(
        "dry_run",
        "serialize_output_data",
        ParametersKeys.SLURM_CLUSTER_KEY,
    ),
):
    """
    Task to launch a 'standard' nabu reconstruction.


    :param inputs_names: A dictionary containing the following keys:

        * ``data``: TomwerScanBase
            the scan we want to reconstruct

        * ``multipag_params``: dict
            the parametets to apply to MultiPag. Keys are:

        * ``nabu_params``: dict
            See :ref:`nabu reconstruction parameters` and `nabu configuration file <https://tomotools.gitlab-pages.esrf.fr/nabu/nabu_config_file.html#configuration-file>`_

    :param output_names:

        * ``data``: TomwerScanBase
        * ``volumes``: tuple[VolumeIdentifier]
            volume reconstructed. For now we expect to have a single volume
        * ``future_tomo_obj``: FutureTomwerObject
            future object in case the reconstruction is done remotely
        * ``data_portal_processed_datasets``: tuple[DracReconstructedVolumeDataset]
            `DracReconstructedVolumeDataset` to be processed if the we want to publish the reconstructed volume to the data portal

    :param optional_input_names:

        * ``cluster_config``: dict | None
            cluster configuration

            * ``cpu-per-task``: int
            * ``n_tasks``: int
            * ``n_jobs``: int
            * ``memory``: int
            * ``partition``: str
            * ``n_gpus``: int
            * ``job_name``: str
            * ``walltime``: str
            * ``python_venv``: str
                expected to be used for debug only. Prefer using modules when possible.
            * ``modules``: tuple[str]
                module to be loaded on script start up
            * ``sbatch_extra_params``: dict[str,str]

        * ``dry_run``: bool
            if true we will only create the nabu configuration file but not reconstruction will be launched

        * ``dump_roi``: bool
            If true will dump ROI used to compute the score in the output file. Can be useful for debugging

        * ``serialize_output_data`` : bool
            if true will serialize the data object.

    """

    def __init__(
        self,
        process_id=None,
        varinfo=None,
        inputs=None,
        node_id=None,
        node_attrs=None,
        execinfo=None,
    ):
        SuperviseProcess.__init__(self, process_id=process_id)
        Task.__init__(
            self,
            varinfo=varinfo,
            inputs=inputs,
            node_id=node_id,
            node_attrs=node_attrs,
            execinfo=execinfo,
        )
        self._dry_run = inputs.get("dry_run", False)
        self._current_processing = None

    def run(self):
        scan = data_identifier_to_scan(self.inputs.data)
        if scan is None:
            self.outputs.data = None
            return

        assert ParametersKeys.NABU_REC_PARAMS_KEY not in self.inputs.nabu_params
        assert ParametersKeys.SLURM_CLUSTER_KEY not in self.inputs.nabu_params
        # update scan reconstruction parameters used
        nabu_params = copy.deepcopy(self.inputs.nabu_params)
        scan.nabu_recons_params = nabu_params
        copy_cor(nabu_rec_params=nabu_params, scan=scan)
        scan.clear_latest_vol_reconstructions()

        cluster_config = self.get_input_value(ParametersKeys.SLURM_CLUSTER_KEY, None)
        if isinstance(cluster_config, SlurmClusterConfiguration):
            cluster_config = cluster_config.to_dict()

        nabu_params.pop("tomwer_slices", None)

        if "phase" in nabu_params and "delta_beta" in nabu_params["phase"]:
            pag_dbs = nabu_params["phase"]["delta_beta"]
            if isinstance(pag_dbs, str):
                pag_dbs = utils.retrieve_lst_of_value_from_str(pag_dbs, type_=float)
            if len(pag_dbs) > 1:
                raise ValueError(
                    "Several value of delta / beta found for volume reconstruction"
                )

        if isinstance(scan, TomwerScanBase):
            scan = scan
        elif isinstance(scan, dict):
            scan = ScanFactory.create_scan_object_frm_dict(scan)
        else:
            raise ValueError(f"input type {scan} is not managed")

        if cluster_config in (None, {}):
            target = Target.LOCAL
        else:
            target = Target.SLURM

        state = None
        details = None
        try:
            self._current_processing = VolumeRunner(
                scan=scan,
                config_nabu=nabu_params,
                cluster_config=cluster_config,
                dry_run=self._dry_run,
                target=target,
                process_name=self.program_name,
            )
            res = self._current_processing.run()[0]
        except Exception as e:
            _logger.error(f"Failed to process {scan}. Error is {e}")
            state = DatasetState.FAILED
            details = None
            future_tomo_obj = None
        else:
            # tag latest reconstructions
            if isinstance(res, ResultsLocalRun) and res.results_identifiers is not None:
                scan.set_latest_vol_reconstructions(res.results_identifiers)
            # create future if needed
            if isinstance(res, ResultSlurmRun):
                future_tomo_obj = FutureTomwerObject(
                    tomo_obj=scan,
                    futures=tuple(res.future_slurm_jobs),
                    process_requester_id=self.process_id,
                )

            else:
                future_tomo_obj = None

            if self._cancelled:
                state = DatasetState.CANCELLED
                details = "cancelled by user"
                _logger.info(f"Slices computation for {scan} cancelled")
            else:
                succeed = res.success
                stdouts = (
                    [
                        res.std_out,
                    ]
                    if hasattr(res, "std_out")
                    else []
                )
                stderrs = (
                    [
                        res.std_err,
                    ]
                    if hasattr(res, "std_err")
                    else []
                )

                if not succeed:
                    mess = f"Volume computed for {scan} failed."
                    _logger.processFailed(mess)
                    state = DatasetState.FAILED
                else:
                    mess = f"Volume computed for {scan}."
                    _logger.processSucceed(mess)
                    state = DatasetState.SUCCEED

                # format stderr and stdout
                elmts = [
                    format_stderr_stdout(stderr=stderr, stdout=stdout)
                    for stderr, stdout in zip(stderrs, stdouts)
                ]
                elmts.insert(0, mess)
                details = "\n".join(elmts)
        finally:
            ProcessManager().notify_dataset_state(
                dataset=scan,
                process=self,
                state=state,
                details=details,
            )

        if self.get_input_value("serialize_output_data", True):
            self.outputs.data = scan.to_dict()
        else:
            self.outputs.data = scan
        self.outputs.volumes = scan.latest_vol_reconstructions
        self.outputs.future_tomo_obj = future_tomo_obj

        # build screenshots
        data_portal_processed_datasets = []
        if scan.latest_vol_reconstructions is not None:
            for volume_id in scan.latest_vol_reconstructions:
                try:
                    volume = VolumeFactory.create_tomo_object_from_identifier(
                        identifier=volume_id
                    )
                except Exception as e:
                    _logger.error(
                        f"Fail to build volume from {volume_id}. Error is {e}"
                    )
                else:
                    icatReconstructedDataset = DracReconstructedVolumeDataset(
                        tomo_obj=volume,
                        source_scan=scan,
                    )
                    data_portal_processed_datasets.append(icatReconstructedDataset)

        self.outputs.data_portal_processed_datasets = tuple(
            data_portal_processed_datasets
        )

    def set_configuration(self, configuration: dict) -> None:
        Task.set_configuration(self, configuration=configuration)
        if "dry_run" in configuration:
            self.set_dry_run(bool(configuration["dry_run"]))

    @staticmethod
    def program_name():
        return "nabu-volume"

    @staticmethod
    def program_version():
        return nabu_version

    def set_dry_run(self, dry_run):
        self._dry_run = dry_run

    @property
    def dry_run(self):
        return self._dry_run

    def cancel(self):
        """
        stop current processing
        """
        self._cancelled = True
        if self._current_processing is not None:
            self._current_processing.cancel()
