"""reconstruction of a slice with multiple Center Of Rotation (CoR) values"""

from __future__ import annotations

import logging
import os
import h5py

import numpy
from processview.core.manager import DatasetState, ProcessManager
from processview.core.superviseprocess import SuperviseProcess
from silx.io.url import DataUrl
from silx.io.utils import h5py_read_dataset

from tomoscan.io import HDF5File

import tomwer.version
from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.reconstruction.cor import CoRParams
from tomwer.core.reconstruction.multi.reconstructions import (
    run_nabu_multicor,
)
from tomwer.core.reconstruction.nabu.slice.SliceInterpreter import (
    SliceInterpreter,
)
from tomwer.core.reconstruction.nabu.nabucommon import (
    ResultsLocalRun,
    ResultSlurmRun,
    ResultsWithStd,
)
from tomwer.core.reconstruction.multi.multicor.multicor import (
    PostProcessing,
    DEFAULT_RECONS_FOLDER,
)
from tomwer.core.reconstruction.multi.multicor.params import MultiCoRParams
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.reconstruction.utils.cor import relative_pos_to_absolute
from tomwer.tasks.task import Task
from tomwer.core.reconstruction.multi.scores.utils import get_scores
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils import logconfig
from tomwer.core.utils.scanutils import data_identifier_to_scan
from tomwer.core.utils.slurm import is_slurm_available
from tomwer.io.utils import format_stderr_stdout
from tomwer.io.utils.h5pyutils import EntryReader

from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.reconstruction.multi.multicor.params import ReconstructionMode
from tomwer.core.reconstruction.nabu.utils import (
    get_multi_cor_recons_volume_identifiers,
    get_nabu_multicor_file_prefix,
)


_logger = logging.getLogger(__name__)


class MultiCoRTask(
    Task,
    input_names=(
        "data",
        ParametersKeys.MULTICOR_PARAMS_KEY,
        ParametersKeys.NABU_REC_PARAMS_KEY,
    ),
    output_names=("data", "best_cor"),
    optional_input_names=(
        ParametersKeys.SLURM_CLUSTER_KEY,
        "dry_run",
        "dump_roi",
        "dump_process",
        "serialize_output_data",
        "pool_size",
        ParametersKeys.PLATFORM_RESOURCES,
        "process_id",
    ),
):
    """
    Main process to launch several reconstruction of a single slice with several Center Of Rotation (cor) values.

    :param inputs_names: A dictionary containing the following keys:

        * ``data``: TomwerScanBase
            the scan we want to reconstruct

        * ``nabu_params``: dict
            See :ref:`nabu reconstruction parameters`

        * ``multicor_params``: dict
            the parameters to apply to multicor. Keys are:

            * ``estimated_cor``: float
                center value of the CoR search
            * ``research_width``: float
                search width. Search will be made in [``estimated_cor`` - ``research_width``, ``estimated_cor`` + ``research_width``]
            * ``n_reconstruction``: int
                number of CoR to sample in the range.
            * ``slice_index``: int
                slice to reconstruct
            * ``output_dir``: str | None
                output directory. If none provided then will be saved to the default location ('multi_cor_results')
            * ``score_method``: str | ScoreMethod
                which score method to use for selecting the best CoR. Value must be in ["standard deviation", "total variation", "1 / (total variation)", "1 / std"]

    :param optional_input_names: A dictionary containing the following keys:

        * ``cluster_config``: dict | None | SlurmClusterConfiguration
            cluster configuration

            * ``cpu-per-task``: int
            * ``n_tasks``: int
            * ``n_jobs``: int
            * ``memory``: int
            * ``partition``: str
            * ``n_gpus``: int
            * ``job_name``: str
            * ``walltime``: str
            * ``python_venv``: str
                expected to be used for debug only. Prefer using modules when possible.
            * ``modules``: tuple[str]
                module to be loaded on script start up
            * ``sbatch_extra_params``: dict[str,str]

        * ``dry_run``: bool
            if true we will only create the nabu configuration file but not reconstruction will be launched

        * ``dump_roi``: bool
            If true will dump ROI used to compute the score in the output file. Can be useful for debugging

        * ``serialize_output_data`` : bool
            if true will serialize the data object.

        * ``pool_size``: int
            pool size for post processing (score calculation)

        * ``platform_resources``: dict
            some parameters that can be overwritten from the nabu reconstruction parameters. This is used for the connection with GUI.
            This usually contains platform resources

        * ``process``: SuperviseProcess
            if you are using a processview ProcessManager then you can update the status of this task through this manager

    .. warning::

        Contrary to the NabuSliceTask, NabuVolumeTask or CastVolumeTask As the multi cor is integrating the score calculation we will never get a future_tomo_scan as output

    .. hint::

        See `nabu multicor <https://tomotools.gitlab-pages.esrf.fr/nabu/cli_tools.html#nabu-multicor-perform-a-tomographic-reconstruction-of-a-single-slice-using-multiple-centers-of-rotation>`_

    """

    DEFAULT_POOL_SIZE = 10

    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self._std_outs = tuple()
        self._std_errs = tuple()
        self._current_processing = None
        self._cancelled = False

    @staticmethod
    def autofocus(scan) -> float | None:
        scores = scan.multicor_params.scores
        if scores is None:
            return
        score_method = scan.multicor_params.score_method
        best_cor, best_score = None, 0
        for cor, (_, score_cls) in scores.items():
            if score_cls is None:  # if score calculation failed
                continue
            score = score_cls.get(score_method)
            if score is None:
                continue
            if score > best_score:
                best_cor, best_score = cor, score
        scan.multicor_params.autofocus = best_cor
        if scan.cor_params is None:
            # create parameter if needed because will set it once he find the best cor
            scan.cor_params = CoRParams()
        scan.cor_params.frame_width = scan.dim_1
        scan.cor_params.set_relative_value(best_cor)
        return best_cor

    def _config_preprocessing(
        self,
        scan: TomwerScanBase,
        nabu_params: dict,
        slice_index: str | None,
        file_format: str,
        output_dir: str | None,
        cluster_config: dict,
    ):
        """convert general configuration to nabu - single reconstruction - configuration"""
        nabu_configurations = SliceInterpreter.interpret(
            nabu_params, scan=scan, slices=slice_index
        )
        if len(nabu_configurations) == 0:
            raise RuntimeWarning(
                "Unable to get a valid nabu configuration for " "reconstruction."
            )
        elif len(nabu_configurations) > 1:
            _logger.warning(
                "Several configuration found for nabu (you probably "
                "ask for several delta/beta value or several slices). "
                "Picking the first one."
            )

        # work on file name...
        if output_dir is None:
            output_dir = os.path.join(scan.path, DEFAULT_RECONS_FOLDER)

        nabu_configuration = nabu_configurations[0][0]
        if cluster_config == {}:
            cluster_config = None
        is_cluster_job = cluster_config is not None
        if is_cluster_job and not is_slurm_available():
            raise ValueError(
                "job on cluster requested but no access to slurm cluster found"
            )

        # handle reconstruction section
        if "reconstruction" not in nabu_configuration:
            nabu_configuration["reconstruction"] = {}
        nabu_configuration["reconstruction"]["rotation_axis_position"] = ""
        # handle output section
        if "output" not in nabu_configuration:
            nabu_configuration["output"] = {}
        nabu_configuration["output"]["location"] = output_dir
        nabu_configuration["output"]["file_format"] = file_format
        # handle resources section
        nabu_configuration["resources"] = nabu_configuration.get("resources", {})
        nabu_configuration["resources"]["method"] = "local"

        return nabu_configuration

    def _run_nabu_multicor(
        self,
        scan,
        nabu_config,
        cors,
        slice_index,
        file_format,
        cluster_config: dict | None,
        dry_run=False,
    ):
        if not (cluster_config is None or isinstance(cluster_config, dict)):
            raise TypeError(
                f"cluster_config is expected to be a dict. Get {type(cluster_config)} instead."
            )
        process_id = self.get_input_value("process_id", None)

        runner = run_nabu_multicor(
            nabu_config=nabu_config,
            scan=scan,
            cors=cors,
            slice_index=slice_index,
            dry_run=dry_run,
            file_format=file_format,
            cluster_config=cluster_config,
            process_id=process_id,
            instantiate_classes_only=True,
            output_file_prefix_pattern="cor_{file_name}_{value}",  # as the cor is evolving, create different files to make sure the name will be unique
        )

        future_tomo_obj = None
        recons_urls = dict()
        std_outs = []
        std_errs = []

        self._current_processing = runner
        try:
            result = runner.run()
        except TimeoutError as e:
            _logger.error(e)
        else:
            success = result.success
            if isinstance(result, ResultsWithStd):
                std_outs.append(result.std_out)
                std_errs.append(result.std_err)
            if isinstance(result, ResultsLocalRun):
                recons_urls = {
                    cor: recons for cor, recons in zip(cors, result.results_identifiers)
                }
            if isinstance(result, ResultSlurmRun):
                future_tomo_obj = FutureTomwerObject(
                    tomo_obj=scan,
                    process_requester_id=process_id,
                    futures=result.future_slurm_jobs,
                )
        return success, recons_urls, (future_tomo_obj,), std_outs, std_errs

    def _resolve_futures(
        self,
        scan,
        nabu_config,
        slice_index,
        file_format,
        cors,
        cor_reconstructions,
        future_tomo_objs: dict,
        output_dir,
    ):
        """
        in case the task is launching jobs over slurm wait for them to be finished before resuming 'standard processing'
        """
        if output_dir is None:
            output_dir = os.path.join(scan.path, DEFAULT_RECONS_FOLDER)

        file_prefix = get_nabu_multicor_file_prefix(scan)

        for future_tomo_obj in future_tomo_objs:
            if self._cancelled:
                break

            if future_tomo_obj is None:
                continue

            future_tomo_obj.results()
            if future_tomo_obj.cancelled() or future_tomo_obj.exceptions():
                continue

            for cor in cors:
                cor_nabu_ref = relative_pos_to_absolute(
                    relative_pos=cor,
                    det_width=scan.dim_1,
                )
                volume_identifiers = get_multi_cor_recons_volume_identifiers(
                    scan=scan,
                    slice_index=slice_index,
                    location=nabu_config["output"]["location"],
                    file_prefix=file_prefix,
                    file_format=file_format,
                    cors=(cor_nabu_ref,),
                )
                volume_identifier = volume_identifiers.get(cor_nabu_ref, None)
                if volume_identifier is None:
                    _logger.warning(
                        f"failed to load volume for {cor}. Something went wrong on slurm submission job"
                    )
                cor_reconstructions[cor] = volume_identifier

    def _post_processing(self, scan, slice_index, cor_reconstructions: dict):
        """
        compute score along the different slices

        :param cor_reconstructions: key is expected to be a float with the cor value and the value is expected to be a volume identifier (volume with a single frame)
        """
        post_processing = PostProcessing(
            slice_index=slice_index,
            scan=scan,
            cor_reconstructions=cor_reconstructions,
            pool_size=self.get_input_value("pool_size", self.DEFAULT_POOL_SIZE),
        )
        post_processing._cancelled = self._cancelled
        self._current_processing = post_processing
        return post_processing.run()

    def _compute_mess_details(self, mess=""):
        """
        util to join a message and nabu std err and std out
        """
        nabu_logs = []
        for std_err, std_out in zip(self._std_errs, self._std_outs):
            nabu_logs.append(format_stderr_stdout(stdout=std_out, stderr=std_err))
        self._nabu_log = nabu_logs
        nabu_logs.insert(0, mess)
        return "\n".join(nabu_logs)

    @staticmethod
    def _preprocess_slice_index(slice_index, mode: ReconstructionMode):
        if isinstance(slice_index, str):
            if not slice_index == "middle":
                raise ValueError(f"slice index {slice_index} not recognized")
            else:
                return slice_index
        elif not len(slice_index) == 1:
            raise ValueError(f"{mode.value} mode only manage one slice")
        else:
            return list(slice_index.values())[0]

    def get_output_dir(self, params: MultiCoRParams, scan: TomwerScanBase):
        output_dir = params.output_dir or None
        if output_dir is None:
            output_dir = os.path.join(scan.path, DEFAULT_RECONS_FOLDER)
        return output_dir

    def run(self):
        scan = data_identifier_to_scan(self.inputs.data)
        if scan is None:
            self.outputs.data = None
            return
        if isinstance(scan, TomwerScanBase):
            scan = scan
        elif isinstance(scan, dict):
            scan = ScanFactory.create_scan_object_frm_dict(scan)
        else:
            raise ValueError(f"input type of {scan}: {type(scan)} is not managed")

        process = self.get_process()
        if process:
            ProcessManager().notify_dataset_state(
                dataset=scan,
                process=process,
                state=DatasetState.ON_GOING,
            )

        configuration = self.inputs.multicor_params
        params = MultiCoRParams.from_dict(configuration)
        # insure output dir is created
        params.output_dir = self.get_output_dir(params=params, scan=scan)
        if not os.path.exists(params.output_dir):
            os.makedirs(params.output_dir)

        # try to find an estimated cor
        #  from a previously computed cor
        if params.estimated_cor is None and scan.cor_params is not None:
            relative_cor = scan.cor_params.relative_cor_value
            if relative_cor is not None and numpy.issubdtype(
                type(relative_cor), numpy.number
            ):
                params.estimated_cor = relative_cor
                _logger.info(
                    f"{scan}: set estimated cor from previously computed cor ({params.estimated_cor})"
                )

        if params.estimated_cor is None:
            raise ValueError("No estimated cor provided")
        #  from scan.x_rotation_axis_pixel_position
        if (
            params.estimated_cor is None
            and scan.x_rotation_axis_pixel_position is not None
        ):
            params.estimated_cor = scan.x_rotation_axis_pixel_position
            _logger.info(
                f"{scan}: set estimated cor from motor position ({params.estimated_cor})"
            )
        if scan.dim_1 is not None:
            params.image_width = scan.dim_1
        scan.multicor_params = params

        mode = ReconstructionMode(params.mode)
        if mode is not ReconstructionMode.VERTICAL:
            raise ValueError(f"{mode} is not handled for now")

        from tomwer.core.utils.dictutils import concatenate_dict

        nabu_config = self.inputs.nabu_params
        # update resources
        nabu_config["resources"] = concatenate_dict(
            nabu_config.get("resources", {}),
            self.get_input_value(ParametersKeys.PLATFORM_RESOURCES, {}),
        )
        multi_cor_params = self.inputs.multicor_params
        slice_index = multi_cor_params["slice_index"]
        nabu_output_config = nabu_config.get("output", {})
        file_format = nabu_output_config.get("file_format", "hdf5")
        slice_index = self._preprocess_slice_index(
            params.slice_indexes,
            mode=mode,
        )
        cluster_config = self.get_input_value(ParametersKeys.SLURM_CLUSTER_KEY, None)
        if isinstance(cluster_config, SlurmClusterConfiguration):
            cluster_config = cluster_config.to_dict()
        dry_run = self.get_input_value("dry_run", False)

        # step one: complete nabu configuration(s)
        nabu_config = self._config_preprocessing(
            scan=scan,
            nabu_params=nabu_config,
            slice_index=slice_index,
            file_format=file_format,
            output_dir=params.output_dir,
            cluster_config=cluster_config,
        )
        # step 2: run reconstructions
        cors_res = {}
        rois = {}

        try:
            (
                _,
                cor_reconstructions,
                future_tomo_objs,
                self._std_outs,
                self._std_errs,
            ) = self._run_nabu_multicor(
                scan=scan,
                nabu_config=nabu_config,
                cors=tuple(params.cors),
                slice_index=slice_index,
                file_format=file_format,
                cluster_config=cluster_config,
                dry_run=dry_run,
            )
        except Exception as e:
            _logger.error(e)
            mess = f"sa-axis -nabu- computation for {str(scan)} failed."
            state = DatasetState.FAILED
        else:
            # step 3: wait for future if any
            self._resolve_futures(
                scan=scan,
                nabu_config=nabu_config,
                slice_index=slice_index,
                file_format=file_format,
                cor_reconstructions=cor_reconstructions,
                cors=tuple(params.cors),
                future_tomo_objs=future_tomo_objs,
                output_dir=params.output_dir,
            )

            # step 4: run post processing (compute score for each slice)
            try:
                cors_res, rois = self._post_processing(
                    scan=scan,
                    slice_index=slice_index,
                    cor_reconstructions=cor_reconstructions,
                )
            except Exception as e:
                _logger.error(e)
                mess = f"sa-axis -post-processing- computation for {str(scan)} failed."
                state = DatasetState.FAILED
                cors_res = {}
            else:
                state = DatasetState.WAIT_USER_VALIDATION
                mess = "sa-axis computation succeeded"

        if self._cancelled:
            state = DatasetState.CANCELLED
            mess = "scan cancelled by the user"

        if process:
            ProcessManager().notify_dataset_state(
                dataset=scan,
                process=process,
                state=state,
                details=self._compute_mess_details(mess),
            )

        scan.multicor_params.scores = cors_res
        best_relative_cor = self.autofocus(scan=scan)

        if best_relative_cor is not None:
            scan.cor_params.set_relative_value(best_relative_cor)

        self._process_end(scan=scan, cors_res=cors_res, score_rois=rois)

        if self.get_input_value("serialize_output_data", True):
            self.outputs.data = scan.to_dict()
        else:
            self.outputs.data = scan
        self.outputs.best_cor = best_relative_cor

    def get_process(self) -> None | SuperviseProcess:
        process_id = self.get_input_value("process_id", None)
        if process_id is None:
            return None
        else:
            return ProcessManager().get_process(process_id=process_id)

    def _process_end(self, scan, cors_res, score_rois):
        assert isinstance(scan, TomwerScanBase)
        process = self.get_process()
        if process is None:
            return
        state = ProcessManager().get_dataset_state(
            dataset_id=scan.get_identifier(), process=process
        )

        if process is None:
            return
        if state not in (
            DatasetState.CANCELLED,
            DatasetState.FAILED,
            DatasetState.SKIPPED,
        ):
            try:
                extra = {
                    logconfig.DOC_TITLE: self._scheme_title,
                    logconfig.SCAN_ID: str(scan),
                }
                slice_index = self.inputs.multicor_params.get("slice_index", None)

                if cors_res is None:
                    info = f"fail to compute cor scores of slice {slice_index} for scan {scan}."
                    _logger.processFailed(info, extra=extra)
                    ProcessManager().notify_dataset_state(
                        dataset=scan,
                        process=process,
                        state=DatasetState.FAILED,
                        details=info,
                    )
                else:
                    info = (
                        f"cor scores of slice {slice_index} for scan {scan} computed."
                    )
                    _logger.processSucceed(info, extra=extra)
                    ProcessManager().notify_dataset_state(
                        dataset=scan,
                        process=process,
                        state=DatasetState.WAIT_USER_VALIDATION,
                        details=info,
                    )
            except Exception as e:
                _logger.error(e)
            else:
                if self.get_input_value("dump_process", True):
                    self.save_results_to_disk(scan=scan)
                    if self.get_input_value("dump_roi", False):
                        self.dump_rois(scan, score_rois=score_rois)

    @staticmethod
    def dump_rois(scan, score_rois):
        if scan.multicor_params.scores in (None, {}):
            return

        process_url = MultiCoRTask.get_results_url(scan=scan)

        with HDF5File(process_url.file_path(), mode="w") as h5f:
            nx_process = h5f.require_group(process_url.data_path())
            score_roi_grp = nx_process.require_group("score_roi")
            for cor, roi in score_rois.items():
                score_roi_grp[str(cor)] = roi
                score_roi_grp[str(cor)].attrs["interpretation"] = "image"

    @staticmethod
    def program_name():
        """Name of the program used for this processing"""
        return "semi-automatic axis"

    @staticmethod
    def program_version():
        """version of the program used for this processing"""
        return tomwer.version.version

    @staticmethod
    def definition():
        """definition of the process"""
        return "Semi automatic center of rotation / axis calculation"

    @staticmethod
    def get_results_url(scan: TomwerScanBase):
        file_path = os.path.join(scan.path, DEFAULT_RECONS_FOLDER, "tomwer_multicor.h5")
        return DataUrl(
            file_path=file_path,
            data_path=scan.entry or "entry",
            scheme="silx",
        )

    @staticmethod
    def save_results_to_disk(scan):
        if scan.multicor_params.scores in (None, {}):
            return

        saaxis_results_url = MultiCoRTask.get_results_url(scan=scan)

        # save it to the file
        with HDF5File(saaxis_results_url.file_path(), mode="w") as h5f:
            nx_process = h5f.require_group(saaxis_results_url.data_path())
            if "NX_class" not in nx_process.attrs:
                nx_process.attrs["NX_class"] = "NXprocess"

            results = nx_process.require_group("results")
            for cor, (url, score) in scan.multicor_params.scores.items():
                results_cor = results.require_group(str(cor))
                for method in ScoreMethod:
                    method_score = score.get(method)
                    if method_score is None:
                        results_cor[method.value] = "None"
                    else:
                        results_cor[method.value] = method_score

                link_path = os.path.relpath(
                    url.file_path(),
                    os.path.dirname(saaxis_results_url.file_path()),
                )
                results_cor["reconstructed_slice"] = h5py.ExternalLink(
                    link_path, url.data_path()
                )

    @staticmethod
    def load_results_from_disk(scan):
        saaxis_results_url = MultiCoRTask.get_results_url(scan=scan)

        if saaxis_results_url.file_path() is None or not os.path.exists(
            saaxis_results_url.file_path()
        ):
            _logger.warning(
                "Unable to find process file. Unable to read " "existing processing"
            )
            return None, None

        try:
            with EntryReader(saaxis_results_url) as h5f_entry_node:
                scores = get_scores(h5f_entry_node)
                if (
                    "results" in h5f_entry_node
                    and "center_of_rotation" in h5f_entry_node["results"]
                ):
                    selected = h5py_read_dataset(
                        h5f_entry_node["results"]["center_of_rotation"]
                    )
                else:
                    _logger.warning(f"no results found for {scan}")
                    selected = None
                return scores, selected
        except ValueError:
            _logger.warning(f"Data path ({saaxis_results_url.data_path()}) not found")
            return None, None

    def cancel(self):
        """
        stop current processing
        """
        if self._current_processing is not None:
            self._cancelled = True
            self._current_processing.cancel()
