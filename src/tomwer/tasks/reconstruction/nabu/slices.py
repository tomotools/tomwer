"""reconstruction of a set of slices using nabu. Slices can be along any directions"""

from __future__ import annotations

import gc
import logging
from nabu import version as nabu_version

from processview.core.superviseprocess import SuperviseProcess
from processview.core.manager.manager import ProcessManager, DatasetState

from tomwer.core.cluster import SlurmClusterConfiguration
from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.reconstruction.nabu.slice.slices import run_slices_reconstruction
from tomwer.core.reconstruction.nabu.nabucommon import (
    ResultsLocalRun,
    ResultSlurmRun,
    ResultsRun,
)
from tomwer.core.volume.volumefactory import VolumeFactory
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils.scanutils import data_identifier_to_scan
from tomwer.io.utils import format_stderr_stdout
from tomwer.tasks.task import Task

_logger = logging.getLogger(__name__)


class NabuSlicesTask(
    Task,
    SuperviseProcess,
    input_names=(
        "data",
        ParametersKeys.NABU_REC_PARAMS_KEY,
        "slices",
    ),
    optional_input_names=(
        ParametersKeys.SLURM_CLUSTER_KEY,
        "dry_run",
        "serialize_output_data",
    ),
    output_names=("data", ParametersKeys.NABU_REC_PARAMS_KEY, "future_tomo_obj"),
):
    """
    Task to reconstruct a set of slices.

    :param inputs_names: A dictionary containing the following keys:

        * ``data``: TomwerScanBase
            the scan we want to reconstruct

        * ``nabu_params``: dict
            See :ref:`nabu reconstruction parameters` and `nabu configuration file <https://tomotools.gitlab-pages.esrf.fr/nabu/nabu_config_file.html#configuration-file>`_

        * ``slices``: dict
            slices to be reconstructed. Expected to be provided as a dict with the following keys:

            * 'YZ': slices to be reconstructed along the X axis
            * 'XZ': slices to be reconstructed along the Y axis
            * 'XY': slices to be reconstructed along the Z axis
            Example:

            .. code-block:: python

                "slices" = {
                    "XY": "middle",
                    "YZ": "100:500:100",
                    "XZ": (20, 70),
                }

            .. warning::
                reconstruction speed is expected to be wildly different according to the reconstruction plane.
                XY is expected to be the fastest and YZ the slowest (reconstructing a single slice can take as much time as reconstructing the full volume in the worst case)

    :param optional_input_names: A dictionary containing the following keys:

        * ``cluster_config``: dict | None
            cluster configuration

            * ``cpu-per-task``: int
            * ``n_tasks``: int
            * ``n_jobs``: int
            * ``memory``: int
            * ``partition``: str
            * ``n_gpus``: int
            * ``job_name``: str
            * ``walltime``: str
            * ``python_venv``: str
                expected to be used for debug only. Prefer using modules when possible.
            * ``modules``: tuple[str]
                module to be loaded on script start up
            * ``sbatch_extra_params``: dict[str,str]

        * ``dry_run``: bool
            if true we will only create the nabu configuration file but not reconstruction will be launched

        * ``serialize_output_data`` : bool
            if true will serialize the data object.

    """

    def __init__(
        self,
        process_id=None,
        varinfo=None,
        inputs=None,
        node_id=None,
        node_attrs=None,
        execinfo=None,
    ):
        SuperviseProcess.__init__(self, process_id=process_id)
        Task.__init__(
            self,
            varinfo=varinfo,
            inputs=inputs,
            node_id=node_id,
            node_attrs=node_attrs,
            execinfo=execinfo,
        )
        self._dry_run = inputs.get("dry_run", False)
        self._current_processing = None
        # we can sometime call several time a nabu subprocess. The idea is to keep track of it
        # if we want to stop the processing

    def run(self):
        scan = data_identifier_to_scan(self.inputs.data)
        configuration = self.inputs.nabu_params

        self.outputs.nabu_params = None
        if scan is None:
            self.outputs.data = None
            return
        if isinstance(scan, TomwerScanBase):
            scan = scan
        elif isinstance(scan, dict):
            scan = ScanFactory.create_scan_object_frm_dict(scan)
        else:
            raise ValueError(f"input type of {scan}: {type(scan)} is not managed" "")
        assert isinstance(configuration, dict), "configuration is expected to be a dict"

        output_urls = []
        stderrs = []
        stdouts = []
        final_configs = []
        futures = []
        all_succeed = True
        cluster_config = self.get_input_value(ParametersKeys.SLURM_CLUSTER_KEY, None)
        if isinstance(cluster_config, SlurmClusterConfiguration):
            cluster_config = cluster_config.to_dict()
        is_cluster_job = cluster_config not in (None, {})
        # loop is required for distributed since version 2021
        single_slice_runner_instances = run_slices_reconstruction(
            config=configuration,
            cluster_config=cluster_config,
            scan=scan,
            dry_run=self._dry_run,
            process_id=self.process_id,
            instantiate_classes_only=True,
            slices=self.inputs.slices,
        )

        for slice_runner in single_slice_runner_instances:
            self._current_processing = slice_runner
            result = self._current_processing.run()[
                0
            ]  # we are expecting a single slice per run in this configuration
            if result is None:
                # in case of timeout or another issue. Log should already have been provided
                pass
            elif isinstance(result, ResultsLocalRun):
                assert (
                    not is_cluster_job
                ), "cluster job should not return ResultsLocalRun"
                stderrs.append(result.std_err)
                stdouts.append(result.std_out)
                output_urls.extend(result.results_identifiers)
                # if slice_index is None this mean that we are simply creating the
                # .cfg file for nabu full volume.
            elif isinstance(result, ResultSlurmRun):
                assert (
                    is_cluster_job
                ), "local reconstruction should not return ResultSlurmRun"
                stderrs.append(result.std_err)
                stdouts.append(result.std_out)
                futures.extend(result.future_slurm_jobs)
            elif not isinstance(result, ResultsRun):
                raise ValueError(
                    f"result is expected to be an instance of {ResultsRun} not {type(result)}"
                )

            # common treatments of results
            if result is not None:
                final_configs.append(result.config)
                all_succeed = all_succeed and result.success

        # update future object and scan latest reconstructions
        self._current_processing = None
        if not self._cancelled and is_cluster_job:
            future_tomo_obj = FutureTomwerObject(
                tomo_obj=scan,
                futures=tuple(futures),
                process_requester_id=self.process_id,
            )
            scan.set_latest_reconstructions(output_urls)
        else:
            # tag latest reconstructions
            scan.set_latest_reconstructions(output_urls)
            future_tomo_obj = None

        volume_urls = []
        for rec_identifier in scan.latest_reconstructions:
            volume_urls.extend(
                VolumeFactory.from_identifier_to_vol_urls(rec_identifier)
            )

        # update processes information / registration
        gc.collect()

        # TODO: check output files with the tomoscan validator ?
        if self._cancelled:
            state = DatasetState.CANCELLED
            details = "cancelled by user"
            future_tomo_obj = None
            _logger.info(f"Slices computation for {scan} cancelled")
        else:
            if not all_succeed:
                mess = f"Slices computation for {scan} failed."
                state = DatasetState.FAILED
                _logger.processFailed(mess)
            else:
                state = DatasetState.SUCCEED
                mess = f"Slices computed for {scan}."
                _logger.processSucceed(mess)
            elmts = [
                format_stderr_stdout(stderr=stderr, stdout=stdout)
                for stderr, stdout in zip(stderrs, stdouts)
            ]
            elmts.insert(0, mess)
            details = "\n".join(elmts)

        ProcessManager().notify_dataset_state(
            dataset=scan,
            process=self,
            state=state,
            details=details,
        )

        if self.get_input_value("serialize_output_data", True):
            self.outputs.data = scan.to_dict()
        else:
            self.outputs.data = scan
        self.outputs.nabu_params = scan.nabu_recons_params
        self.outputs.future_tomo_obj = future_tomo_obj

    @staticmethod
    def program_name():
        return "nabu-slices"

    @staticmethod
    def program_version():
        return nabu_version

    def cancel(self):
        """
        stop current processing
        """
        self._cancelled = True
        if self._current_processing is not None:
            self._current_processing.cancel()
