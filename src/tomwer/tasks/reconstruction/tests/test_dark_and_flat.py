# coding: utf-8
from __future__ import annotations


import shutil
import tempfile
import unittest

from tomwer.core.reconstruction.darkflat.params import DKRFRP
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils.scanutils import MockEDF, MockNXtomo

from tomwer.tasks.reconstruction.darkflat import DarkFlatTask
from tomwer.tasks.reconstruction.darkflatcopy import DarkFlatCopyTask


class TestDarkRefIO(unittest.TestCase):
    """Test inputs and outputs types of the handler functions"""

    def setUp(self):
        self.scan_folder = tempfile.mkdtemp()

        self.scan_edf = MockEDF.mockScan(
            scanID=self.scan_folder, nRadio=10, nRecons=1, nPagRecons=4, dim=10
        )
        self.mock_hdf5 = MockNXtomo(
            scan_path=self.scan_folder, n_proj=10, n_pag_recons=0
        )
        self.scan_hdf5 = self.mock_hdf5.scan

        self.recons_params = DKRFRP()

    def tearDown(self):
        shutil.rmtree(self.scan_folder)

    def testInputOutput(self):
        for scan, scan_type in zip(
            (self.scan_edf, self.scan_hdf5), ("edf scan", "hdf5 scan")
        ):
            for input_type in (dict, TomwerScanBase):
                for serialize_output_data in (True, False):
                    with self.subTest(
                        return_dict=serialize_output_data,
                        input_type=input_type,
                        scan_type=scan_type,
                    ):
                        input_obj = scan
                        if input_obj is dict:
                            input_obj = input_obj.to_dict()
                        process = DarkFlatTask(
                            inputs={
                                ParametersKeys.DARK_REF_KEY: self.recons_params,
                                "data": input_obj,
                                "serialize_output_data": serialize_output_data,
                            }
                        )
                        process.run()
                        out = process.outputs.data
                        if serialize_output_data:
                            self.assertTrue(isinstance(out, dict))
                        else:
                            self.assertTrue(isinstance(out, TomwerScanBase))


class TestDarkRefCopyIO(unittest.TestCase):
    """Test inputs and outputs types of the handler functions"""

    def setUp(self):
        self.scan_folder = tempfile.mkdtemp()

        self.scan_edf = MockEDF.mockScan(
            scanID=self.scan_folder, nRadio=10, nRecons=1, nPagRecons=4, dim=10
        )
        self.scan_hdf5 = MockNXtomo(
            scan_path=self.scan_folder, n_proj=10, n_pag_recons=0
        ).scan
        self.recons_params = DKRFRP()
        self._save_dir = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.scan_folder)

    def testInputOutput(self):
        for scan, scan_type in zip(
            (self.scan_edf, self.scan_hdf5), ("edf scan", "hdf5 scan")
        ):
            for input_type in (dict, TomwerScanBase):
                for serialize_output_data in (True, False):
                    with self.subTest(
                        return_dict=serialize_output_data,
                        input_type=input_type,
                        scan_type=scan_type,
                    ):
                        input_obj = scan
                        if input_obj is dict:
                            input_obj = input_obj.to_dict()
                        dkrf_process = DarkFlatCopyTask(
                            inputs={
                                ParametersKeys.DARK_REF_KEY: self.recons_params,
                                "data": input_obj,
                                "serialize_output_data": serialize_output_data,
                                "save_dir": self._save_dir,
                            }
                        )
                        dkrf_process.run()
                        out = dkrf_process.outputs.data
                        if serialize_output_data:
                            self.assertTrue(isinstance(out, dict))
                        else:
                            self.assertTrue(isinstance(out, TomwerScanBase))
