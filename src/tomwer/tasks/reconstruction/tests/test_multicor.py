# coding: utf-8
from __future__ import annotations

import os
import shutil
import tempfile
import unittest

from tomwer.core.reconstruction.multi.multicor.params import MultiCoRParams
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.core.settings import ParametersKeys
from tomwer.tasks.reconstruction.nabu.multicor import MultiCoRTask


class TestMultiCoreTask(unittest.TestCase):
    """Test the MultiCoRTask class"""

    def setUp(self) -> None:
        self.tempdir = tempfile.mkdtemp()
        dim = 10
        mock = MockNXtomo(
            scan_path=self.tempdir, n_proj=10, n_ini_proj=10, scan_range=180, dim=dim
        )
        mock.add_alignment_radio(index=10, angle=90)
        mock.add_alignment_radio(index=10, angle=0)
        self.scan = mock.scan

        self._default_cor_params = MultiCoRParams()
        self._default_cor_params.output_dir = os.path.join(self.tempdir, "output_dir")
        self._default_cor_params.slice_indexes = {"slice": "4"}
        self._default_cor_params.nabu_config = {}
        self._default_cor_params.dry_run = True
        self._default_cor_params.file_format = "hdf5"

    def tearDown(self) -> None:
        shutil.rmtree(self.tempdir)

    def test(self):
        process = MultiCoRTask(
            inputs={
                "data": self.scan,
                ParametersKeys.MULTICOR_PARAMS_KEY: self._default_cor_params.to_dict(),
                ParametersKeys.NABU_REC_PARAMS_KEY: {
                    "phase": {"method": "Paganin"},
                },
                "serialize_output_data": False,
            }
        )

        self._default_cor_params.estimated_cor = 11
        self._default_cor_params.research_width = 2
        process = MultiCoRTask(
            inputs={
                "data": self.scan,
                ParametersKeys.MULTICOR_PARAMS_KEY: self._default_cor_params.to_dict(),
                ParametersKeys.NABU_REC_PARAMS_KEY: {
                    "phase": {"method": "Paganin"},
                },
                "serialize_output_data": False,
            },
        )
        process.run()
        process = MultiCoRTask(
            inputs={
                "data": self.scan,
                ParametersKeys.MULTICOR_PARAMS_KEY: self._default_cor_params.to_dict(),
                ParametersKeys.NABU_REC_PARAMS_KEY: {
                    "phase": {"method": "Paganin"},
                },
                "serialize_output_data": False,
            },
        )
        process.run()
