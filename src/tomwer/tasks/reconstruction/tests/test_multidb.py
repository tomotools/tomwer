# coding: utf-8
from __future__ import annotations


import os
import shutil
import tempfile
import unittest

from tomwer.core.reconstruction.multi.multipag.params import MultiPagParams
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.core.settings import ParametersKeys
from tomwer.tasks.reconstruction.nabu.multipag import MultiPagTask


class TestMultiPagProcess(unittest.TestCase):
    """Test the MultiPagTask class"""

    def setUp(self) -> None:
        super().setUp()
        self.tempdir = tempfile.mkdtemp()
        dim = 10
        mock = MockNXtomo(
            scan_path=self.tempdir, n_proj=10, n_ini_proj=10, scan_range=180, dim=dim
        )
        self.scan = mock.scan

    def tearDown(self) -> None:
        shutil.rmtree(self.tempdir)
        super().tearDown()

    def test(self):
        process = MultiPagTask(
            inputs={
                "data": self.scan,
                ParametersKeys.MULTIPAG_PARAMS_KEY: MultiPagParams().to_dict(),
                ParametersKeys.NABU_REC_PARAMS_KEY: {"phase": {"mode": "Paganin"}},
                "serialize_output_data": False,
            }
        )

        default_multipag_params = MultiPagParams()
        default_multipag_params.output_dir = os.path.join(self.tempdir, "output_dir")
        default_multipag_params.dry_run = True

        process.run()
