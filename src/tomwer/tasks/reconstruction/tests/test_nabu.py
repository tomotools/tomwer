# coding: utf-8
from __future__ import annotations

import os
import shutil
import tempfile
import unittest
from glob import glob

from tomwer.tasks.reconstruction.nabu.slices import NabuSlicesTask
from tomwer.core.reconstruction.cor import CoRParams
from tomwer.core.reconstruction.nabu import settings as nabu_settings
from tomwer.core.scan.edfscan import EDFTomoScan
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.settings import ParametersKeys
from tomwer.tests.datasets import TomwerCIDatasets


class TestNabuDescUtils(unittest.TestCase):
    """Test the util function for creating the description of nabu"""

    def setUp(self):
        unittest.TestCase.setUp(self)
        self.top_dir = tempfile.mkdtemp()
        self.dataset = "scan_3_"
        data_test_dir = TomwerCIDatasets.get_dataset(f"edf_datasets/{self.dataset}")
        scan_folder = os.path.join(self.top_dir, self.dataset)
        shutil.copytree(data_test_dir, scan_folder)
        self.scan = ScanFactory.create_scan_object(scan_folder)

    def tearDown(self) -> None:
        shutil.rmtree(self.scan.path)

    def testDatasetDescriptor(self):
        self.scan.get_nabu_dataset_info()


class TestNabuAndAxis(unittest.TestCase):
    """Test that nabu process and axis process are compatible"""

    def setUp(self) -> None:
        self.dataset = "scan_3_"
        self._root_dir = tempfile.mkdtemp()
        data_test_dir = TomwerCIDatasets.get_dataset("edf_datasets/scan_3_")
        scan_folder = os.path.join(self._root_dir, self.dataset)
        shutil.copytree(data_test_dir, scan_folder)
        self.scan = EDFTomoScan(scan_folder)
        [os.remove(cfg_file) for cfg_file in glob(self.scan.path + os.sep + "*.cfg")]

        # initial configuration
        self.conf = get_nabu_default_config(output_dir=self.scan.path)

        self.nabu_process = NabuSlicesTask(
            inputs={
                "data": self.scan,
                ParametersKeys.NABU_REC_PARAMS_KEY: self.conf,
                "slices": {
                    "XY": "middle",
                },
                "dry_run": True,
                "serialize_output_data": False,
            },
        )
        self.scan._cor_params = CoRParams()
        self.nabu_cfg_folder = os.path.join(
            self.scan.path, nabu_settings.NABU_CFG_FILE_FOLDER
        )
        # make sure no .cfg files already exists
        assert len(glob(self.nabu_cfg_folder + os.sep + "*.cfg")) == 0

    def tearDown(self) -> None:
        shutil.rmtree(self._root_dir)

    def test_output_dir(self):
        self.conf["output"]["location"] = "{scan_basename}/output"
        self.nabu_process.set_configuration(self.conf)
        output_dir = self.scan.scan_basename() + os.sep + "output"
        self.assertFalse(os.path.exists(output_dir))
        self.nabu_process.run()
        self.assertTrue(os.path.exists(output_dir))
        nabu_conf_files = glob(
            output_dir + os.sep + nabu_settings.NABU_CFG_FILE_FOLDER + os.sep + "*.cfg"
        )
        self.assertEqual(len(nabu_conf_files), 1)

    def get_saved_rotation_axis(self, nabu_out_file):
        """return the saved value for 'rotation axis' in the nabu
        configuration file"""
        with open(nabu_out_file, "r") as nabu_file:
            lines = nabu_file
            for line in lines:
                if line.startswith("rotation_axis_position"):
                    return line.replace("\n", "").replace(" ", "").split("=")[-1]
            return None

    def testNoCOR(self):
        """test if axis is executed and no COR are set, nabu won't mind"""
        self.scan._cor_params.set_relative_value(None)
        self.nabu_process.run()

        nabu_conf_files = glob(self.nabu_cfg_folder + os.sep + "*.cfg")
        self.assertEqual(len(nabu_conf_files), 1)
        nabu_rotation_axis_value = self.get_saved_rotation_axis(nabu_conf_files[0])
        self.assertEqual(nabu_rotation_axis_value, "")

    def testCOR(self):
        """test that if cor is computed, cor will be registered for nabu"""
        self.scan._cor_params.set_relative_value(2.3)
        self.nabu_process.run()

        nabu_conf_files = glob(self.nabu_cfg_folder + os.sep + "*.cfg")
        self.assertEqual(len(nabu_conf_files), 1)
        nabu_rotation_axis_value = self.get_saved_rotation_axis(nabu_conf_files[0])
        self.assertEqual(
            nabu_rotation_axis_value,
            str(self.scan._cor_params.relative_cor_value + (self.scan.dim_1 / 2.0)),
        )


def get_nabu_default_config(output_dir):
    """:return a default configuration for nabu"""
    return {
        "phase": {
            "padding_type": "edge",
            "method": "",
            "delta_beta": "100.0",
            "margin": 50,
            "unsharp_sigma": 0,
            "unsharp_coeff": 0,
        },
        "preproc": {
            "ccd_filter_threshold": 0.04,
            "take_logarithm": True,
            "log_min_clip": 1e-06,
            "ccd_filter_enabled": 0,
            "log_max_clip": 10.0,
            "flatfield": 1,
        },
        "dataset": {
            "job_name": "hair_A1_50nm_tomo3_1_",
            "location": "/tmp_14_days/payno/hair_A1_50nm_tomo3_1_",
            "flat_file_prefix": "refHST",
            "projections_subsampling": 1,
            "binning": 1,
            "dark_file_prefix": "dark.edf",
            "binning_z": 1,
        },
        "reconstruction": {
            "positivity_constraint": 1,
            "start_z": 0,
            "end_y": -1,
            "optim_algorithm": "chambolle-pock",
            "enable_halftomo": 0,
            "end_z": -1,
            "iterations": 200,
            "start_x": 0,
            "fbp_filter_type": "ramlak",
            "angle_offset": 0.0,
            "end_x": -1,
            "method": "FBP",
            "angles_file": "",
            "rotation_axis_position": "",
            "start_y": 0,
            "preconditioning_filter": 1,
            "padding_type": "zeros",
            "weight_tv": 0.01,
            "translation_movements_file": "",
            "axis_correction_file": "",
        },
        "output": {"file_format": "hdf5", "location": output_dir},
    }
