# coding: utf-8
from __future__ import annotations

import os
import pytest
import h5py
import numpy
import configparser
from collections import namedtuple

from silx.io.url import DataUrl

from tomoscan.normalization import Method
from tomoscan.io import HDF5File

from nabu.pipeline.config import get_default_nabu_config
from nabu.pipeline.fullfield.nabu_config import (
    nabu_config as nabu_fullfield_default_config,
)

from tomwer.core.reconstruction.normalization import params
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.core.reconstruction.nabu import settings as nabu_settings
from tomwer.core.reconstruction.normalization.params import (
    SinoNormalizationParams,
)
from tomwer.core.reconstruction.output import (
    PROCESS_FOLDER_RECONSTRUCTED_SLICES,
)
from tomwer.core.settings import ParametersKeys
from tomwer.tasks.reconstruction.normalization import SinoNormalizationTask
from tomwer.tasks.reconstruction.nabu.slices import NabuSlicesTask


_norm_setting = namedtuple("_norm_setting", ["method", "source", "extra_infos"])

_nabu_norm_field = namedtuple(
    "_nabu_norm_field",
    ["sino_normalization", "has_sino_normalization_file"],
)


normalization_config_test = (
    # no normalization
    (
        _norm_setting(method="none", source=None, extra_infos=None),
        _nabu_norm_field(
            sino_normalization="",
            has_sino_normalization_file=False,
        ),
    ),
    # chebyshev normalization
    (
        _norm_setting(method="chebyshev", source=None, extra_infos=None),
        _nabu_norm_field(
            sino_normalization="chebyshev",
            has_sino_normalization_file=False,
        ),
    ),
    # divide by a scalar
    (
        _norm_setting(method="division", source="scalar", extra_infos={"value": 12.0}),
        _nabu_norm_field(
            sino_normalization="division",
            has_sino_normalization_file=True,
        ),
    ),
    # substract a roi
    (
        _norm_setting(
            method="subtraction",
            source="manual ROI",
            extra_infos={
                "start_x": 0.0,
                "end_x": 1.0,
                "start_y": 0.0,
                "end_y": 1.0,
                "calc_fct": "mean",
                "calc_area": "volume",
                "calc_method": "scalar",
            },
        ),
        _nabu_norm_field(
            sino_normalization="subtraction",
            has_sino_normalization_file=True,
        ),
    ),
    # substract from a dataset
    (
        _norm_setting(
            method="subtraction",
            source="from dataset",
            extra_infos={
                "dataset_url": DataUrl(
                    file_path="random_dataset.hdf5",
                    data_path="data",
                    scheme="silx",
                ),
                "calc_fct": "median",
                "calc_area": "volume",
                "calc_method": "scalar",
            },
        ),
        _nabu_norm_field(
            sino_normalization="subtraction",
            has_sino_normalization_file=True,
        ),
    ),
)


@pytest.mark.parametrize(
    "norm_setting, expected_nabu_conf", [item for item in normalization_config_test]
)
def test_normalization(norm_setting, expected_nabu_conf, tmp_path):
    """
    Insure normalization is correctly provided to nabu configuration file
    For this run a normalization process followed by a nabu process.
    """
    scan_dir = tmp_path / "scan"
    scan_dir.mkdir()

    nabu_cfg_folders = os.path.join(scan_dir, nabu_settings.NABU_CFG_FILE_FOLDER)
    os.makedirs(nabu_cfg_folders, exist_ok=True)

    random_dataset_file = os.path.join(nabu_cfg_folders, "random_dataset.hdf5")

    # create a random dataset if necessary
    with HDF5File(random_dataset_file, mode="w") as h5f:
        h5f["data"] = numpy.ones((20, 20))

    mock = MockNXtomo(
        scan_path=scan_dir,
        n_proj=10,
        n_ini_proj=10,
        scan_range=180,
        dim=20,
    )
    scan = mock.scan

    cfg_folder = os.path.join(
        str(scan_dir), PROCESS_FOLDER_RECONSTRUCTED_SLICES, "nabu_cfg_files"
    )
    cfg_file = os.path.join(cfg_folder, "entry_scanslice_000010_plane_XY.cfg")
    assert not os.path.exists(cfg_file)
    norm_params = SinoNormalizationParams(
        method=norm_setting.method,
        source=norm_setting.source,
        extra_infos=norm_setting.extra_infos,
    )

    normalization = SinoNormalizationTask(
        inputs={
            "data": scan,
            "configuration": norm_params.to_dict(),
            "serialize_output_data": False,
        },
        varinfo=None,
    )
    normalization.run()

    # insure the method is style valid
    assert scan.intensity_normalization.method.value == norm_setting.method
    assert (
        "tomwer_processing_res_code" in scan.intensity_normalization.get_extra_infos()
    )

    process = NabuSlicesTask(
        inputs={
            "data": scan,
            ParametersKeys.NABU_REC_PARAMS_KEY: get_default_nabu_config(
                nabu_fullfield_default_config
            ),
            "slices": {
                "XY": "middle",
            },
            "dry_run": True,
            "serialize_output_data": False,
        },
        varinfo=None,
    )
    process.run()
    assert os.path.exists(cfg_file)

    configuration = configparser.ConfigParser(allow_no_value=True)
    configuration.read(cfg_file)

    preproc_section = configuration["preproc"]
    sino_normalization = preproc_section.get("sino_normalization", "")
    sino_normalization_file = preproc_section.get("sino_normalization_file", "")
    assert sino_normalization == expected_nabu_conf.sino_normalization
    if expected_nabu_conf.has_sino_normalization_file:
        url = DataUrl(path=sino_normalization_file)
        assert url.is_valid()
    else:
        assert sino_normalization_file == ""
    assert (
        "tomwer_processing_res_code" in scan.intensity_normalization.get_extra_infos()
    )


@pytest.mark.parametrize("calc_fct", params._ValueCalculationFct.values())
def test_manual_roi(tmp_path, calc_fct):
    test_dir = tmp_path / "input"
    test_dir.mkdir()
    dim = 100
    scan = MockNXtomo(
        scan_path=test_dir,
        n_proj=2,
        n_ini_proj=2,
        scan_range=180,
        dim=dim,
        create_ini_dark=False,
        create_ini_flat=False,
        create_final_flat=False,
    ).scan

    with h5py.File(scan.master_file, mode="a") as h5f:
        dataset = h5f["/entry/instrument/detector/data"]
        assert dataset.shape == (2, 100, 100)  # pylint: disable=E1101
        del h5f["/entry/instrument/detector/data"]
        h5f["/entry/instrument/detector/data"] = numpy.arange(100 * 100 * 2).reshape(
            2, 100, 100
        )

    process_params = params.SinoNormalizationParams()
    process_params.method = Method.SUBTRACTION
    process_params.source = params._ValueSource.MANUAL_ROI
    expected_results = {
        "mean": numpy.array([800.5, 10800.5]),
        "median": numpy.array([800.5, 10800.5]),
    }

    process_params.extra_infos = {
        "start_x": 0,
        "end_x": 2,
        "start_y": 8,
        "end_y": 9,
        "calc_fct": calc_fct,
        "calc_area": "volume",
    }
    process = SinoNormalizationTask(
        inputs={
            "data": scan,
            "configuration": process_params,
            "serialize_output_data": False,
        }
    )
    process.run()
    res = scan.intensity_normalization.get_extra_infos().get("value")
    if isinstance(res, numpy.ndarray):
        numpy.testing.assert_array_equal(res, expected_results[calc_fct])
    else:
        numpy.testing.assert_array_equal(res, expected_results[calc_fct])
