from __future__ import annotations

import os
import numpy
import unittest
import shutil
import tempfile

from tomoscan.esrf.scan.utils import get_data

from tomwer.core.reconstruction.darkflat.params import DKRFRP, ReduceMethod
from tomwer.core.scan.nxtomoscan import NXtomoScan
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.settings import ParametersKeys
from tomwer.core.utils.scanutils import MockEDF, MockNXtomo
from tomwer.tasks.reconstruction.darkflat import DarkFlatTask
from tomwer.tests.datasets import TomwerCIDatasets
from tomwer.tasks.reconstruction.darkflat import (
    requires_reduced_dark_and_flat,
)


class TestDarkRefEdf(unittest.TestCase):
    """
    Test dark ref is correctly processing with h5 refs
    """

    def setUp(self) -> None:
        self.scan_folder = tempfile.mkdtemp()
        self.dark_data = numpy.array(  # pylint: disable=E1121
            numpy.random.random(4 * 200 * 200) * 100,
            dtype=numpy.uint16,
        ).reshape(4, 200, 200)
        self.flat_data = numpy.array(  # pylint: disable=E1121
            numpy.random.random(4 * 200 * 200) * 100,
            dtype=numpy.uint32,
        ).reshape(4, 200, 200)
        self.scan = MockEDF.mockScan(
            scanID=self.scan_folder,
            nRadio=10,
            nRecons=1,
            nPagRecons=4,
            dim=200,
            start_dark=True,
            start_flat=True,
            start_dark_data=self.dark_data,
            start_flat_data=self.flat_data,
        )
        self.recons_params = DKRFRP()
        self.recons_params.overwrite_dark = True
        self.recons_params.overwrite_ref = True

    def tearDown(self) -> None:
        shutil.rmtree(self.scan_folder)

    def testDark(self):
        """
        Test darks are computed when only dark are requested
        """
        method_to_test = (
            ReduceMethod.MEDIAN,
            ReduceMethod.MEAN,
            ReduceMethod.FIRST,
            ReduceMethod.LAST,
        )
        th_results = (
            numpy.median(self.dark_data, axis=0),
            numpy.mean(self.dark_data, axis=0),
            self.dark_data[0],
            self.dark_data[-1],
        )
        for method, th_res in zip(method_to_test, th_results):
            with self.subTest(method=method):
                process = DarkFlatTask(
                    inputs={
                        ParametersKeys.DARK_REF_KEY: self.recons_params,
                        "data": self.scan,
                        "serialize_output_data": False,
                    }
                )

                self.recons_params.dark_calc_method = method
                self.recons_params.flat_calc_method = ReduceMethod.NONE
                process.run()
                numpy.testing.assert_array_almost_equal(
                    self.scan.reduced_darks[0], th_res.astype(numpy.uint16)
                )

    def testFlat(self):
        """
        Test flats are computed when only flat are requested
        """
        method_to_test = (
            ReduceMethod.MEDIAN,
            ReduceMethod.MEAN,
            ReduceMethod.FIRST,
            ReduceMethod.LAST,
        )
        th_results = (
            numpy.median(self.flat_data, axis=0),
            numpy.mean(self.flat_data, axis=0),
            self.flat_data[0],
            self.flat_data[-1],
        )
        for method, th_res in zip(method_to_test, th_results):
            with self.subTest(method=method):
                self.recons_params.dark_calc_method = ReduceMethod.NONE
                self.recons_params.flat_calc_method = method
                process = DarkFlatTask(
                    inputs={
                        ParametersKeys.DARK_REF_KEY: self.recons_params,
                        "data": self.scan,
                        "serialize_output_data": False,
                    }
                )
                process.run()
                numpy.testing.assert_array_almost_equal(
                    self.scan.reduced_flats[0], th_res.astype(numpy.uint16)
                )


class TestDarkRefNx(unittest.TestCase):
    """
    Test dark ref is correctly processing with h5 refs
    """

    def setUp(self) -> None:
        super().setUp()
        dataset_name = "frm_edftomomill_twoentries.nx"
        self.scan_folder = tempfile.mkdtemp()
        self._file_path = os.path.join(self.scan_folder, dataset_name)
        shutil.copyfile(
            src=TomwerCIDatasets.get_dataset(f"h5_datasets/{dataset_name}"),
            dst=self._file_path,
        )
        self.scan = NXtomoScan(scan=self._file_path, entry="entry0000")
        self.recons_params = DKRFRP()
        self.recons_params.overwrite_dark = True
        self.recons_params.overwrite_ref = True

    def tearDown(self) -> None:
        shutil.rmtree(self.scan_folder)
        super().tearDown()

    def testDark(self):
        """
        Test darks are computed when only dark are requested
        """
        darks = self.scan.darks
        self.assertEqual(len(darks), 1)

        method_to_test = (
            ReduceMethod.MEAN,
            ReduceMethod.MEDIAN,
            ReduceMethod.FIRST,
            ReduceMethod.LAST,
        )
        for method in method_to_test:
            with self.subTest(method=method):
                self.recons_params.dark_calc_method = method
                self.recons_params.flat_calc_method = ReduceMethod.NONE
                process = DarkFlatTask(
                    inputs={
                        ParametersKeys.DARK_REF_KEY: self.recons_params,
                        "data": self.scan,
                        "serialize_output_data": False,
                    }
                )
                process.run()
                flats = self.scan.load_reduced_flats()
                self.assertEqual(len(flats), 0)
                darks = self.scan.load_reduced_darks()
                self.assertEqual(len(darks), 1)

    def testFlat(self):
        """
        Test flats are computed when only flat are requested
        """
        flats = self.scan.flats
        self.assertEqual(len(flats), 42)
        url_flat_serie_1 = [flats[index] for index in range(1, 22)]
        url_flat_serie_2 = [flats[index] for index in range(1521, 1542)]
        assert len(url_flat_serie_1) == 21
        assert len(url_flat_serie_2) == 21
        data_flat_serie_1 = [get_data(url) for url in url_flat_serie_1]
        data_flat_serie_2 = [get_data(url) for url in url_flat_serie_2]

        self.recons_params.overwrite_dark = True
        self.recons_params.overwrite_ref = True

        method_to_test = (
            ReduceMethod.MEAN,
            ReduceMethod.MEDIAN,
            ReduceMethod.FIRST,
            ReduceMethod.LAST,
        )
        for method in method_to_test:
            with self.subTest(method=method):
                if method is ReduceMethod.MEDIAN:
                    expected_res_s1 = numpy.median(data_flat_serie_1, axis=0)
                    expected_res_s2 = numpy.median(data_flat_serie_2, axis=0)
                elif method is ReduceMethod.MEAN:
                    expected_res_s1 = numpy.mean(data_flat_serie_1, axis=0)
                    expected_res_s2 = numpy.mean(data_flat_serie_2, axis=0)
                elif method is ReduceMethod.FIRST:
                    expected_res_s1 = data_flat_serie_1[0]
                    expected_res_s2 = data_flat_serie_2[0]
                elif method is ReduceMethod.LAST:
                    expected_res_s1 = data_flat_serie_1[-1]
                    expected_res_s2 = data_flat_serie_2[-1]
                else:
                    raise ValueError("method not managed")

                self.recons_params.dark_calc_method = ReduceMethod.NONE
                self.recons_params.flat_calc_method = method
                process = DarkFlatTask(
                    inputs={
                        ParametersKeys.DARK_REF_KEY: self.recons_params,
                        "data": self.scan,
                        "serialize_output_data": False,
                    }
                )
                process.run()

                darks = self.scan.load_reduced_darks()
                self.assertEqual(len(darks), 0)
                flats = self.scan.load_reduced_flats()
                self.assertEqual(len(flats), 2)
                self.assertTrue(1 in flats)
                self.assertTrue(1521 in flats)
                self.assertTrue(numpy.allclose(flats[1], expected_res_s1))
                self.assertTrue(numpy.allclose(flats[1521], expected_res_s2))

    def testDarkAndFlat(self):
        """
        Test darks and flats are computed when both are requested
        """
        flats = self.scan.flats
        self.assertEqual(len(flats), 42)
        url_flat_serie_1 = [flats[index] for index in range(1, 22)]
        url_flat_serie_2 = [flats[index] for index in range(1521, 1542)]
        assert len(url_flat_serie_1) == 21
        assert len(url_flat_serie_2) == 21
        data_flat_serie_1 = [get_data(url) for url in url_flat_serie_1]
        data_flat_serie_2 = [get_data(url) for url in url_flat_serie_2]

        self.recons_params.dark_calc_method = ReduceMethod.MEAN
        self.recons_params.flat_calc_method = ReduceMethod.MEDIAN
        darks = self.scan.darks
        self.assertEqual(len(darks), 1)
        dark_data = get_data(list(darks.values())[0])

        expected_flats_s1 = numpy.median(data_flat_serie_1, axis=0)
        expected_flats_s2 = numpy.median(data_flat_serie_2, axis=0)
        process = DarkFlatTask(
            inputs={
                ParametersKeys.DARK_REF_KEY: self.recons_params,
                "data": self.scan,
                "serialize_output_data": False,
            }
        )
        process.run()
        darks = self.scan.load_reduced_darks()
        flats = self.scan.load_reduced_flats()
        self.assertEqual(len(darks), 1)
        self.assertEqual(len(flats), 2)
        self.assertTrue(0 in darks)
        self.assertTrue(1 in flats)
        self.assertTrue(1521 in flats)
        self.assertTrue(numpy.allclose(flats[1], expected_flats_s1))
        self.assertTrue(numpy.allclose(flats[1521], expected_flats_s2))
        self.assertTrue(numpy.allclose(darks[0], dark_data))


class TestDarkRefIO(unittest.TestCase):
    """Test inputs and outputs types of the handler functions"""

    def setUp(self):
        self.scan_folder = tempfile.mkdtemp()

        self.scan_edf = MockEDF.mockScan(
            scanID=self.scan_folder, nRadio=10, nRecons=1, nPagRecons=4, dim=10
        )
        self.mock_hdf5 = MockNXtomo(
            scan_path=self.scan_folder, n_proj=10, n_pag_recons=0
        )
        self.scan_hdf5 = self.mock_hdf5.scan

        self.recons_params = DKRFRP()

    def tearDown(self):
        shutil.rmtree(self.scan_folder)

    def testInputOutput(self):
        for scan, scan_type in zip(
            (self.scan_edf, self.scan_hdf5), ("edf scan", "hdf5 scan")
        ):
            for input_type in (dict, TomwerScanBase):
                for serialize_output_data in (True, False):
                    with self.subTest(
                        return_dict=serialize_output_data,
                        input_type=input_type,
                        scan_type=scan_type,
                    ):
                        input_obj = scan
                        if input_obj is dict:
                            input_obj = input_obj.to_dict()
                        process = DarkFlatTask(
                            inputs={
                                ParametersKeys.DARK_REF_KEY: self.recons_params,
                                "data": input_obj,
                                "serialize_output_data": serialize_output_data,
                            }
                        )
                        process.run()
                        out = process.outputs.data
                        if serialize_output_data:
                            self.assertTrue(isinstance(out, dict))
                        else:
                            self.assertTrue(isinstance(out, TomwerScanBase))


def test_quick_run_necessary_edf(tmpdir):
    """test the `quick_run_necessary` function for EDFTomoScan"""
    scan = MockEDF.mockScan(scanID=str(tmpdir), start_dark=True, start_flat=True)
    assert scan.reduced_darks in (None, {})
    assert scan.reduced_flats in (None, {})
    requires_reduced_dark_and_flat(scan=scan)
    assert len(scan.reduced_darks) == 1
    assert len(scan.reduced_flats) == 1


def test_quick_run_necessary_hdf5(tmpdir):
    """test the `quick_run_necessary` function for NXtomoScan"""
    scan = MockNXtomo(
        scan_path=tmpdir,
        n_proj=20,
        n_ini_proj=20,
        dim=10,
    ).scan
    assert scan.reduced_darks in (None, {})
    assert scan.reduced_flats in (None, {})
    computed = requires_reduced_dark_and_flat(scan=scan)
    assert len(computed) == 2
    assert len(scan.reduced_darks) == 1
    assert len(scan.reduced_flats) == 1
