# coding: utf-8

from tomwer.tasks.visualization.volumeviewer import _VolumeViewerPlaceHolder


def test_volume_viewer():
    process = _VolumeViewerPlaceHolder(
        inputs={
            "data": None,
        }
    )
    process.run()
