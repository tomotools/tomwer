# coding: utf-8
from __future__ import annotations

import logging

from ewokscore.task import Task as EwoksTask
from tomwer.core.utils.scanutils import data_identifier_to_scan

logger = logging.getLogger(__name__)


class _ScanValidatorPlaceHolder(EwoksTask, input_names=["data"], output_names=["data"]):
    def run(self):
        self.outputs.data = data_identifier_to_scan(self.inputs.data)
