# coding: utf-8
from __future__ import annotations


import functools
import logging

from processview.core.manager import DatasetState, ProcessManager
from processview.core.superviseprocess import SuperviseProcess
from silx.gui import qt

from tomwer.tasks.reconstruction.nabu.multipag import (
    MultiPagTask,
)
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.settings import get_lbsram_path, isOnLbsram, ParametersKeys
from tomwer.core.utils.lbsram import is_low_on_memory
from tomwer.synctools.axis import QCoRParams
from tomwer.synctools.multipag import QMultiPagParams

from ..processingstack import FIFO, ProcessingThread

_logger = logging.getLogger(__name__)


class MultiPagStack(FIFO, qt.QObject):
    """Implementation of the `.CoRTask` but having a stack for treating
    scans and making computation in threads"""

    def __init__(self, multipag_params, process_id=None):
        qt.QObject.__init__(self)
        FIFO.__init__(self, process_id=process_id)
        assert multipag_params is not None
        self._dry_run = False
        self._process_fct = None

    def patch_processing(self, process_fct):
        self._computationThread.patch_processing(process_fct)

    def set_dry_run(self, dry_run):
        self._dry_run = dry_run

    def _process(self, data, configuration, callback=None):
        ProcessManager().notify_dataset_state(
            dataset=data,
            process=self,
            state=DatasetState.ON_GOING,
        )
        _logger.processStarted(f"start sa-delta-beta {data}")
        assert isinstance(data, TomwerScanBase)
        if data.cor_params is None:
            data.cor_params = QCoRParams()
        if data.multipag_params is None:
            data.multipag_params = QMultiPagParams()

        if ParametersKeys.MULTIPAG_PARAMS_KEY not in configuration:
            raise KeyError("no sa_delta_beta parametrization given")

        if ParametersKeys.NABU_REC_PARAMS_KEY not in configuration:
            raise KeyError("no nabu_params parametrization given")

        multipag_params = configuration[ParametersKeys.MULTIPAG_PARAMS_KEY]
        multipag_params = QMultiPagParams.from_dict(multipag_params)

        cluster_config = configuration.get(ParametersKeys.SLURM_CLUSTER_KEY, None)
        if not (cluster_config is None or isinstance(cluster_config, dict)):
            raise TypeError(
                f"'cluster_config' is expected to be None or an instance of dict. Got {type(cluster_config)}"
            )

        if isOnLbsram(data) and is_low_on_memory(get_lbsram_path()) is True:
            # if computer is running into low memory on lbsram skip it
            mess = f"low memory, skip sa-delta-beta-axis calculation {data.path}"
            ProcessManager().notify_dataset_state(
                dataset=data, process=self._process_id, state=DatasetState.SKIPPED
            )
            _logger.processSkipped(mess)
            data.multipag_params.set_value(None)
            if callback is not None:
                callback()
            self.scan_ready(scan=data)
            return

        self._data_currently_computed = data
        self._computationThread.init(
            data=data,
            multipag_params=multipag_params,
            nabu_params=configuration[ParametersKeys.NABU_REC_PARAMS_KEY],
            cluster_config=cluster_config,
        )
        # need to manage connect before starting it because
        fct_callback = functools.partial(self._end_threaded_computation, callback)
        self._computationThread.finished.connect(fct_callback)
        self._computationThread.start()

    def _end_computation(self, data, future_tomo_obj, callback):
        """
        callback when the computation thread is finished

        :param scan: pass if no call to '_computationThread is made'
        """
        assert isinstance(data, TomwerScanBase)
        FIFO._end_computation(
            self, data=data, future_tomo_obj=future_tomo_obj, callback=callback
        )

    def _end_threaded_computation(self, callback=None):
        assert self._data_currently_computed is not None
        self._computationThread.finished.disconnect()
        if callback:
            callback()
        FIFO._end_threaded_computation(self)

    def _create_processing_thread(self, process_id=None) -> qt.QThread:
        return _ProcessingThread(process_id=process_id)


class _ProcessingThread(ProcessingThread, SuperviseProcess):
    """
    Thread use to execute the processing of the axis position
    """

    def __init__(self, process_id=None):
        SuperviseProcess.__init__(self, process_id=process_id)
        try:
            ProcessingThread.__init__(self, process_id=process_id)
        except TypeError:
            ProcessingThread.__init__(self)
        self.db_value = None
        self._dry_run = False
        self._scan = None
        self._multipag_params = None
        self._nabu_params = None
        self._cluster_config = None

        self._patch_process_fct = None
        """function pointer to know which function to call for the axis
        calculation"""
        self._task = None
        # ewoks task for reconstruction

    def set_dry_run(self, dry_run):
        self._dry_run = dry_run

    def patch_processing(self, process_fct):
        self._patch_process_fct = process_fct

    def init(
        self, data, multipag_params: dict, nabu_params: dict, cluster_config: dict
    ) -> None:
        self._scan = data
        self._multipag_params = multipag_params
        self._nabu_params = nabu_params
        self._cluster_config = cluster_config

    def run(self):
        self.sigComputationStarted.emit()
        if self._patch_process_fct:
            scores = {}
            for db in self._multipag_params.delta_beta_values:
                scores[db] = self._patch_process_fct(db)
            self._scan.multipag_params.scores = scores
            MultiPagTask.autofocus(scan=self._scan)
            self.db_value = self._scan.multipag_params.autofocus
        else:
            self._task = MultiPagTask(
                inputs={
                    "data": self._scan,
                    ParametersKeys.NABU_REC_PARAMS_KEY: self._nabu_params,
                    ParametersKeys.MULTIPAG_PARAMS_KEY: self._multipag_params.to_dict(),
                    ParametersKeys.SLURM_CLUSTER_KEY: self._cluster_config,
                    "dry_run": self._dry_run,
                    "serialize_output_data": False,
                    "dump_process": False,
                },
                process_id=self.process_id,
            )

            self._task.run()
            self.db_value = self._task.outputs.best_db

    def cancel(self):
        if self._task is not None:
            self._task.cancel()
        self.quit()
