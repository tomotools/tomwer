# coding: utf-8
from __future__ import annotations


import logging

from silx.gui import qt

from tomwer.core.reconstruction.darkflat import params

logger = logging.getLogger(__name__)


class QDKRFRP(params.DKRFRP, qt.QObject):
    sigChanged = qt.Signal()
    """Signal emitted when at least one element of the dictionary change"""

    def __init__(self):
        qt.QObject.__init__(self)
        params.DKRFRP.__init__(self)

    def changed(self):
        self.sigChanged.emit()
