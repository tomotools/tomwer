"""gui settings"""

from silx.gui import qt


Y_AXIS_DOWNWARD = True
"""Default axis direction Y"""

EDITING_BACKGROUND_COLOR = qt.QColor("#c3d0d6")
"""Background color used to notify the user that application waits end of edition before taking it into account"""

TAB_LABEL_PLATFORM_SETTINGS = "platform settings"

TAB_LABEL_RECONSTRUCTION_SETTINGS = "reconstruction settings"
