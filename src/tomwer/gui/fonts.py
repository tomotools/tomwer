from silx.gui import qt

FONT_IMPORTANT = qt.QFont("Arial", 12)
FONT_MEDIUM = qt.QFont("Arial", 10)
FONT_SMALL = qt.QFont("Arial", 8)
