"""contains QLFileSystem class. Specific implementation of the QLineEdit to select a file path."""

from silx.gui import qt


class QLFileSystem(qt.QLineEdit):
    """
    QLineEdit with a completer using a QFileSystemModel
    """

    def __init__(self, text, parent, filters=None, **kwargs):
        super().__init__(parent=parent, **kwargs)
        self.completer = qt.QCompleter()
        self.completer.setCompletionRole(qt.QFileSystemModel.FilePathRole)
        model = qt.QFileSystemModel(self.completer)
        model.setRootPath(qt.QDir.currentPath())
        model.setOption(qt.QFileSystemModel.DontWatchForChanges)
        if filters is not None:
            model.setFilter(filters)
        self.completer.setModel(model)
        self.setCompleter(self.completer)
        if text is not None:
            self.setText(text)
