from silx.utils.enum import Enum as _Enum


class DisplayMode(_Enum):
    """possible display mode for the tomo object"""

    URL = "url"
    SHORT = "short"
