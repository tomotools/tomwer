import pytest
import numpy

from tomwer.gui.reconstruction.cor.AxisSettingsWidget import CoRSettingsTabWidget
from tomwer.synctools.axis import QCoRParams
from tomwer.tests.utils import skip_gui_test
from tomwer.tests.conftest import qtapp  # noqa F401


@pytest.mark.skipif(skip_gui_test(), reason="skip gui test")
def test_get_nabu_cor_opts(qtapp):  # noqa F811
    cor_params = QCoRParams()
    widget = CoRSettingsTabWidget(recons_params=cor_params)
    assert cor_params.get_nabu_cor_options_as_dict() == {
        "side": "right",
        "radio_angles": (0.0, numpy.pi),
        "slice_idx": "middle",
    }
    widget._optionsWidget._corOpts.setText("low_pass=2.0")
    widget._optionsWidget._corOpts.editingFinished.emit()
    assert cor_params.get_nabu_cor_options_as_dict() == {
        "side": "right",
        "radio_angles": (0.0, numpy.pi),
        "slice_idx": "middle",
        "low_pass": 2.0,
    }
    widget._optionsWidget._corOpts.setText("low_pass=2 ; high_pass=10")
    widget._optionsWidget._corOpts.editingFinished.emit()
    assert cor_params.get_nabu_cor_options_as_dict() == {
        "side": "right",
        "radio_angles": (0.0, numpy.pi),
        "slice_idx": "middle",
        "low_pass": 2.0,
        "high_pass": 10.0,
    }
    widget._calculationWidget.setEstimatedCorValue("left")
    assert cor_params.get_nabu_cor_options_as_dict() == {
        "side": "left",
        "radio_angles": (0.0, numpy.pi),
        "slice_idx": "middle",
        "low_pass": 2.0,
        "high_pass": 10.0,
    }
