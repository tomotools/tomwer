from __future__ import annotations

from silx.gui import qt

from tomwer.core.utils.char import BETA_CHAR, DELTA_CHAR
from tomwer.gui.reconstruction.nabu.volume.DeltaBetaSelector import DeltaBetaSelector


class DeltaBetaSelectorDialog(qt.QDialog):
    timeoutReached = qt.Signal()

    def __init__(self, values, parent=None, timeout=None):
        """

        :param values:
        :param parent:
        :param timeout: if a timeout is provided once reach this will
                        automatically reject the delta / beta selection.
                        This is needed when on lbsram to avoid 'locking'
                        a reconstruction. In sec.
        """
        qt.QDialog.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())

        self.mainWidget = DeltaBetaSelector(parent=self, values=values)
        self.layout().addWidget(self.mainWidget)

        types = qt.QDialogButtonBox.Ok | qt.QDialogButtonBox.Cancel
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        self.layout().addWidget(self._buttons)
        self._timeout = timeout
        if self._timeout is None:
            self.setWindowTitle(f"Select one value for {DELTA_CHAR} / {BETA_CHAR}")
        else:
            self.setWindowTitle(
                f"Select one value for {DELTA_CHAR} / {BETA_CHAR}. (close automatically in {self._timeout} sec.)"
            )

        # connect signal / slot
        self._buttons.accepted.connect(self.accept)
        self._buttons.rejected.connect(self.reject)

        # expose API
        self.getSelectedValue = self.mainWidget.getSelectedValue

        # add timers
        if timeout is not None:
            self._timer = qt.QTimer()
            self._timer.timeout.connect(self.reject)
            self._timer.start(timeout * 1000)
            self._displayTimer = qt.QTimer()
            self._displayTimer.timeout.connect(self._updateTitle)
            self._displayTimer.start(1000)
        else:
            self._timer = None

    def reject(self):
        if self._timer:
            self._timer.stop()
            self._timer.timeout.disconnect(self.reject)
            self._displayTimer.stop()
            self._displayTimer.timeout.disconnect(self._updateTitle)
            self._timer = None

        qt.QDialog.reject(self)

    def _updateTitle(self):
        self._timeout = self._timeout - 1
        if self._timeout <= 0:
            self.timeoutReached.emit()
            self.reject()
        else:
            self.setWindowTitle(
                f"Select one value for {DELTA_CHAR} / {BETA_CHAR}. (close automatically in {self._timeout} sec.)"
            )
            self._displayTimer.start(1000)
