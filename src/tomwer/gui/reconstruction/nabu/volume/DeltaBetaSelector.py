from __future__ import annotations

from silx.gui import qt

from tomwer.core.utils.char import BETA_CHAR, DELTA_CHAR


class DeltaBetaSelector(qt.QTableWidget):
    """Widget used to select a value of delta beta if several provided"""

    def __init__(self, values, parent=None):
        qt.QTableWidget.__init__(self, parent)
        self.setSelectionMode(qt.QAbstractItemView.SingleSelection)
        self.setHorizontalHeaderLabels([DELTA_CHAR + " / " + BETA_CHAR])
        self.setRowCount(0)
        self.setColumnCount(1)
        self.verticalHeader().hide()
        if hasattr(self.horizontalHeader(), "setSectionResizeMode"):  # Qt5
            self.horizontalHeader().setSectionResizeMode(0, qt.QHeaderView.Stretch)
        else:  # Qt4
            self.horizontalHeader().setResizeMode(0, qt.QHeaderView.Stretch)
        self.setAcceptDrops(False)

        # set up
        self.setValues(values=values)

    def setValues(self, values: tuple) -> None:
        self.setHorizontalHeaderLabels([DELTA_CHAR + " / " + BETA_CHAR])
        self.setRowCount(len(values))
        self.setColumnCount(1)
        for i_value, value in enumerate(values):
            _item = qt.QTableWidgetItem()
            _item.setText(str(value))
            _item.setFlags(qt.Qt.ItemIsEnabled | qt.Qt.ItemIsSelectable)
            self.setItem(i_value, 0, _item)
            _item.setSelected(i_value == 0)

    def getSelectedValue(self):
        sel = None
        for item in self.selectedItems():
            sel = item.text()
        return sel
