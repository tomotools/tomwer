from __future__ import annotations

from silx.gui import qt

from tomwer.core.utils.dictutils import concatenate_dict
from tomwer.gui.configuration.level import ConfigurationLevel
from tomwer.gui.reconstruction.nabu.platform import NabuPlatformSettings
from tomwer.gui.settings import (
    TAB_LABEL_PLATFORM_SETTINGS,
    TAB_LABEL_RECONSTRUCTION_SETTINGS,
)

from .NabuVolumeWidget import NabuVolumeWidget


class NabuVolumeWindow(qt.QTabWidget):
    """
    Widget that integrate the reconstruction settings and the platform (local) settings
    """

    sigConfigChanged = qt.Signal()

    def __init__(self, parent: qt.QWidget | None = None) -> None:
        super().__init__(parent)
        self._configurationLevel = ConfigurationLevel.OPTIONAL
        self._nabuSettingsWidget = NabuVolumeWidget(parent=self)
        self.addTab(self._nabuSettingsWidget, TAB_LABEL_RECONSTRUCTION_SETTINGS)

        self._platformSettingsWidget = NabuPlatformSettings(parent=self)
        self.addTab(self._platformSettingsWidget, TAB_LABEL_PLATFORM_SETTINGS)

        # connect signal / slot
        self._nabuSettingsWidget.sigConfigChanged.connect(self.sigConfigChanged)
        self._platformSettingsWidget.sigConfigChanged.connect(self.sigConfigChanged)

    def getConfiguration(self) -> dict:
        return concatenate_dict(
            self._nabuSettingsWidget.getConfiguration(),
            self._platformSettingsWidget.getConfiguration(),
        )

    def setConfiguration(self, config: dict) -> None:
        self._nabuSettingsWidget.setConfiguration(config=config)
        self._platformSettingsWidget.setConfiguration(config=config)

    def setConfigurationLevel(self, level):
        self._configurationLevel = ConfigurationLevel(level)
        self._nabuSettingsWidget.setConfigurationLevel(level=self._configurationLevel)
        self._platformSettingsWidget.setConfigurationLevel(
            level=self._configurationLevel
        )

    def getConfigurationLevel(self):
        return self._configuration_level
