from __future__ import annotations

from silx.gui import qt

from tomwer.core.reconstruction.nabu.stages import NabuStages
from tomwer.gui import icons
from tomwer.gui.reconstruction.nabu.nabuconfig.base import _NabuStageConfigBase

from .SliceSelector import SliceSelector


class NabuVolumeWidget(_NabuStageConfigBase, qt.QWidget):
    """
    Widget dedicated to manage the volume reconstruction from nabu
    """

    sigConfigChanged = qt.Signal()
    """Signal emitted when the configuration change"""

    def __init__(self, parent=None):
        qt.QWidget.__init__(self, parent=parent)
        _NabuStageConfigBase.__init__(self, stage=NabuStages.VOLUME)
        self.setLayout(qt.QGridLayout())

        # warning about requires parameters from a nabu slice reconstruction
        self._warning_widget = qt.QWidget(parent=self)
        self._warning_widget.setLayout(qt.QHBoxLayout())
        self._warningIconL = qt.QLabel("", parent=self)
        self._warningIconL.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum)
        self._warningIconL.setAlignment(qt.Qt.AlignRight | qt.Qt.AlignVCenter)
        self._warningIconR = qt.QLabel("", parent=self)
        self._warningIconR.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum)
        self._warningIconR.setAlignment(qt.Qt.AlignLeft | qt.Qt.AlignVCenter)
        warning_icon = icons.getQIcon("warning")
        self._warningIconL.setPixmap(warning_icon.pixmap(20, 20))
        self._warningIconR.setPixmap(warning_icon.pixmap(20, 20))
        self._warning_widget.layout().addWidget(self._warningIconL)

        self._labelWarning = qt.QLabel(
            "Requires parameters from nabu slice reconstruction.", parent=self
        )
        self._labelWarning.setToolTip(
            'This mean that a "nabu slice '
            'reconstruction" widget should exist upstream'
        )
        self._warning_widget.layout().addWidget(self._labelWarning)
        self._warning_widget.layout().addWidget(self._warningIconR)
        self.layout().addWidget(self._warning_widget, 0, 0, 1, 2)

        # volume from and to option
        self._groupBox = qt.QGroupBox("volume to reconstruct", parent=self)
        self._groupBox.setLayout(qt.QGridLayout())
        self.layout().addWidget(self._groupBox, 2, 0, 1, 2)

        self._fromSlice = SliceSelector(
            label="from slice",
            auto_alias="start",
            is_end=False,
            parent=self,
            grid_layout=self._groupBox.layout(),
            index_layout=0,
        )
        self._groupBox.layout().addWidget(self._fromSlice)
        self.registerWidget(self._groupBox, "required")

        self._toSlice = SliceSelector(
            label="to slice",
            auto_alias="end",
            is_end=True,
            parent=self,
            grid_layout=self._groupBox.layout(),
            index_layout=1,
        )
        self._groupBox.layout().addWidget(self._toSlice)

        # option to compute histogram
        self._histogramCB = qt.QCheckBox("compute volume histogram of values", self)
        self.layout().addWidget(self._histogramCB, 3, 0, 1, 2)
        self.registerWidget(self._histogramCB, "required")

        # spacer for style
        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding)
        self.layout().addWidget(spacer, 99, 0, 1, 1)

        # set up
        self._histogramCB.setChecked(True)

        # connect signal / slot
        self._fromSlice.valueChanged.connect(self._triggerSigConfChanged)
        self._toSlice.valueChanged.connect(self._triggerSigConfChanged)

    def _triggerSigConfChanged(self, *args, **kwargs):
        self.sigConfigChanged.emit()

    def getStartZ(self):
        return self._fromSlice.value()

    def setStartZ(self, value):
        self._fromSlice.setValue(value)

    def getEndZ(self):
        return self._toSlice.value()

    def setEndZ(self, value):
        self._toSlice.setValue(value)

    def setScan(self, scan):
        if len(scan.projections) > 0:
            self._fromSlice.setMaximum(len(scan.projections))
        else:
            self._fromSlice.setMaximum(999999999)

    def isHistogramRequested(self):
        return self._histogramCB.isChecked()

    def setHistogramRequested(self, requested):
        self._histogramCB.setChecked(requested)

    def getConfiguration(self):
        return {
            "reconstruction": {
                "start_z": self.getStartZ(),
                "end_z": self.getEndZ(),
            },
            "postproc": {
                "output_histogram": int(self.isHistogramRequested()),
            },
        }

    def setConfiguration(self, config: dict):
        if not isinstance(config, dict):
            raise TypeError(f"config is expected to be a dict. Got {type(config)}")
        if "start_z" in config.get("reconstruction", {}):
            self.setStartZ(config["reconstruction"]["start_z"])
        if "end_z" in config.get("reconstruction", {}):
            self.setEndZ(config["reconstruction"]["end_z"])
        if "postproc" in config:
            self.setPostProcConfiguration(config["postproc"])

    def setPostProcConfiguration(self, config):
        if "output_histogram" in config:
            self.setHistogramRequested(requested=bool(config["output_histogram"]))
