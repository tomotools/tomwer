from __future__ import annotations

from silx.gui import qt


class SliceSelector(qt.QWidget):
    """
    Widget to select a value for defining a ROI to reconstruction (start or
    end)
    """

    valueChanged = qt.Signal()
    """emit when the slice value change"""

    def __init__(
        self, label, auto_alias, is_end, grid_layout, index_layout, parent=None
    ):
        qt.QWidget.__init__(self, parent=parent)
        self._label = qt.QLabel(label, parent=self)
        grid_layout.addWidget(self._label, index_layout, 0, 1, 1)
        self._qcb = qt.QCheckBox(auto_alias)
        grid_layout.addWidget(self._qcb, index_layout, 1, 1, 1)
        grid_layout.addWidget(qt.QLabel("or", self), index_layout, 2, 1, 1)
        self._qle = qt.QLineEdit("0", self)
        self._validator = qt.QIntValidator()
        self._qle.setValidator(self._validator)
        grid_layout.addWidget(self._qle, index_layout, 3, 1, 1)
        self._is_end = is_end
        if is_end is True:
            self._qle.setToolTip("-1 will be the last slice")
            self._validator.setBottom(-1)
        else:
            self._validator.setBottom(0)

        # set up
        self._qcb.setChecked(True)
        self._qle.setEnabled(False)
        if is_end:
            self._qle.setText("-1")

        # connect signal / slot
        self._qcb.toggled.connect(self._qle.setDisabled)
        self._qcb.toggled.connect(self._triggerValueChanged)
        self._qle.editingFinished.connect(self._triggerValueChanged)

    def _triggerValueChanged(self, *args, **kwargs):
        self.valueChanged.emit()

    def setMaximum(self, maximum):
        self._validator.setTop(maximum)

    def value(self):
        if self._qcb.isChecked():
            return -1 if self._is_end else 0
        else:
            return int(self._qle.text())

    def setValue(self, value):
        self._qle.setText(str(value))
        if self._is_end:
            self._qcb.setChecked(value == -1)
        else:
            self._qcb.setChecked(value == 0)
