from __future__ import annotations


from silx.utils.enum import Enum as _Enum
from tomwer.core.reconstruction.nabu.stages import (
    NabuPreprocessing,
    NabuPhase,
    NabuProcessing,
)


class Stages(_Enum):
    """
    nabu configuration is split into several stages to help user defining it.
    """

    INI = "initialization"
    PRE = "pre-processing"
    PHASE = "phase"
    PROC = "processing"
    POST = "post-processing"
    VOLUME = "volume"

    @staticmethod
    def getStagesOrder():
        return (
            Stages.INI,
            Stages.PRE,
            Stages.PHASE,
            Stages.PROC,
            Stages.POST,
        )

    @staticmethod
    def getProcessEnum(stage):
        """Return the process Enum associated to the stage"""
        stage = Stages(stage)
        if stage is Stages.INI:
            raise NotImplementedError()
        elif stage is Stages.PRE:
            return NabuPreprocessing
        elif stage is Stages.PHASE:
            return NabuPhase
        elif stage is Stages.PROC:
            return NabuProcessing
        elif stage is Stages.POST:
            raise NotImplementedError()
        raise NotImplementedError()
