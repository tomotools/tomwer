from __future__ import annotations

from silx.gui import qt
from silx.gui import icons as silx_icons

from nabu.pipeline.config import get_default_nabu_config
from nabu.pipeline.config import generate_nabu_configfile
from nabu.pipeline.fullfield.nabu_config import (
    nabu_config as nabu_fullfield_default_config,
)
from nabu.pipeline.config import parse_nabu_config_file

from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.gui.configuration.action import FilterAction
from tomwer.gui.reconstruction.nabu.nabuconfig.NabuSettingsWidget import (
    NabuSettingsWidget,
)
from tomwer.gui.configuration.action import (
    BasicConfigurationAction,
    ExpertConfigurationAction,
    MinimalisticConfigurationAction,
)
from tomwer.gui.configuration.level import ConfigurationLevel


class NabuSettingsWindow(qt.QMainWindow):
    sigConfigChanged = qt.Signal()
    """Signal emitted when the configuration change"""

    def __init__(self, parent, flow_direction="vertical"):
        qt.QMainWindow.__init__(self, parent=parent)
        self.setWindowFlags(qt.Qt.Widget)

        self._mainWidget = NabuSettingsWidget(
            parent=self, flow_direction=flow_direction
        )
        self.setCentralWidget(self._mainWidget)
        style = qt.QApplication.style()

        # add toolbar
        toolbar = qt.QToolBar(self)
        toolbar.setMovable(False)
        toolbar.setFloatable(False)
        self.addToolBar(qt.Qt.TopToolBarArea, toolbar)

        # add filtering
        self._filterAction = FilterAction(toolbar)
        toolbar.addAction(self._filterAction)
        self._filterAction.triggered.connect(self._filteringChanged)

        # add configuration mode
        self.__configurationModesAction = qt.QAction(self)
        self.__configurationModesAction.setCheckable(False)
        menu = qt.QMenu(self)
        self.__configurationModesAction.setMenu(menu)
        toolbar.addAction(self.__configurationModesAction)

        self.__configurationModesGroup = qt.QActionGroup(self)
        self.__configurationModesGroup.setExclusive(True)
        self.__configurationModesGroup.triggered.connect(self._userModeChanged)

        self._minimalisticAction = MinimalisticConfigurationAction(toolbar)
        menu.addAction(self._minimalisticAction)
        self.__configurationModesGroup.addAction(self._minimalisticAction)
        self._basicConfigAction = BasicConfigurationAction(toolbar)
        menu.addAction(self._basicConfigAction)
        self.__configurationModesGroup.addAction(self._basicConfigAction)
        self._expertConfiguration = ExpertConfigurationAction(toolbar)
        menu.addAction(self._expertConfiguration)
        self.__configurationModesGroup.addAction(self._expertConfiguration)

        # add load option
        self.__loadAction = qt.QAction(self)
        self.__loadAction.setToolTip("load nabu configuration from a text file")
        # load_icon = style.standardIcon(qt.QStyle.SP_)
        load_icon = silx_icons.getQIcon("document-open")
        self.__loadAction.setIcon(load_icon)
        toolbar.addAction(self.__loadAction)
        self.__loadAction.triggered.connect(self._loadParameters)

        # add save option
        self.__saveAction = qt.QAction(self)
        self.__saveAction.setToolTip("save nabu configuration to a text file")
        save_icon = silx_icons.getQIcon("document-save")
        self.__saveAction.setIcon(save_icon)
        toolbar.addAction(self.__saveAction)
        self.__saveAction.triggered.connect(self._saveParameters)

        # reset configuration option
        self.__resetAction = qt.QAction(self)
        self.__resetAction.setToolTip("reset nabu configuration")
        reset_icon = style.standardIcon(qt.QStyle.SP_DialogResetButton)
        self.__resetAction.setIcon(reset_icon)
        toolbar.addAction(self.__resetAction)
        self.__resetAction.triggered.connect(self._resetParameters)

        # toolbar spacer
        self.__tSpacer = qt.QWidget(toolbar)
        self.__tSpacer.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Preferred)
        toolbar.addWidget(self.__tSpacer)

        # connect signal / slot
        self._mainWidget.sigConfigChanged.connect(self.sigConfigChanged)

        # set up
        self._resetParameters()
        self._filterAction.setChecked(True)
        self._basicConfigAction.setChecked(True)
        self._userModeChanged()

    def getConfiguration(self):
        configuration = self._mainWidget.getConfiguration()
        assert (
            "configuration_level" not in configuration
        ), "configuration_level is a reserved key"
        configuration["configuration_level"] = self.getConfigurationLevel().value
        return configuration

    def setConfiguration(self, config):
        conf = config.copy()
        if "configuration_level" in conf:
            configuration_level = conf["configuration_level"]
            del conf["configuration_level"]
        else:
            configuration_level = None
        self._mainWidget.setConfiguration(conf)
        if configuration_level is not None:
            self.setConfigurationLevel(level=configuration_level)

    def _saveParameters(self):
        # request output file
        config = self.getConfiguration()

        fname = self.askForNabuConfigFile()
        if fname is None:
            return
        generate_nabu_configfile(
            fname,
            nabu_fullfield_default_config,
            config=config,
            options_level="advanced",
        )

    def _resetParameters(self):
        # reset nabu settings
        default_config = get_default_nabu_config(nabu_fullfield_default_config)
        self._mainWidget._configuration._preProcessingWidget._sinoRingsOpts.resetConfiguration()
        default_config["tomwer_slices"] = "middle"
        default_config["preproc"]["ccd_filter_enabled"] = False
        default_config["preproc"]["double_flatfield_enabled"] = False
        default_config["preproc"]["flatfield"] = True
        default_config["preproc"]["take_logarithm"] = True
        self.setConfiguration(default_config)

    def _loadParameters(self):
        inputFile = self.askForNabuConfigFile(acceptMode=qt.QFileDialog.AcceptOpen)
        import os

        if inputFile and os.path.exists(inputFile):
            config = parse_nabu_config_file(inputFile)
            self.setConfiguration(config)

    def askForNabuConfigFile(  # pragma: no cover
        self, acceptMode=qt.QFileDialog.AcceptSave
    ):
        dialog = qt.QFileDialog(self)
        dialog.setNameFilters(
            [
                "Configuration files (*.cfg *.conf *.config)",
                "Any files (*)",
            ]
        )

        dialog.setAcceptMode(acceptMode)
        dialog.setFileMode(qt.QFileDialog.AnyFile)

        if not dialog.exec():
            dialog.close()
            return

        filesSelected = dialog.selectedFiles()
        if filesSelected is not None and len(filesSelected) > 0:
            output = filesSelected[0]
            if not output.endswith((".cfg", ".conf", ".config")):
                output = f"{output}.conf"
            return output

    def _userModeChanged(self, *args, **kwargs):
        selectedAction = self.__configurationModesGroup.checkedAction()
        self.__configurationModesAction.setIcon(selectedAction.icon())
        self.__configurationModesAction.setToolTip(selectedAction.tooltip())
        self._mainWidget.setConfigurationLevel(self.getConfigurationLevel())
        self.sigConfigChanged.emit()

    def _filteringChanged(self, *args, **kwargs):
        self._mainWidget.setFilteringActive(self.isFilteringActive())

    def isFilteringActive(self):
        return self._filterAction.isChecked()

    def setConfigurationLevel(self, level):
        level = ConfigurationLevel(level)
        if level == ConfigurationLevel.REQUIRED:
            self._minimalisticAction.setChecked(True)
        elif level == ConfigurationLevel.ADVANCED:
            self._expertConfiguration.setChecked(True)
        elif level == ConfigurationLevel.OPTIONAL:
            self._basicConfigAction.setChecked(True)
        else:
            raise ValueError("Level not recognize")
        self._userModeChanged()

    def getConfigurationLevel(self):
        if self._basicConfigAction.isChecked():
            return ConfigurationLevel.OPTIONAL
        elif self._expertConfiguration.isChecked():
            return ConfigurationLevel.ADVANCED
        elif self._minimalisticAction.isChecked():
            return ConfigurationLevel.REQUIRED
        else:
            raise ValueError("Level not recognize")

    # expose API

    def setOutputDir(self, dir: str):
        self._mainWidget.setOutputDir(dir=dir)

    def setScan(self, scan: TomwerScanBase):
        self._mainWidget.setScan(scan=scan)

    def getMode(self):
        return self._mainWidget.getMode()

    def setMode(self, mode):
        self._mainWidget.setMode(mode=mode)
