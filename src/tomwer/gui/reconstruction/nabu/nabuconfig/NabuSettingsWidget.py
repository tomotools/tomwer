from __future__ import annotations

from silx.gui import qt


from tomwer.core.reconstruction.nabu.mode import NabuMode
from tomwer.gui.reconstruction.nabu.nabuflow import NabuFlowArea
from tomwer.gui.configuration.level import ConfigurationLevel
from tomwer.gui.utils.flow import FlowDirection
from tomwer.gui.reconstruction.nabu.nabuconfig.nabuconfig import NabuConfiguration
from tomwer.gui import icons as tomwer_icons


class NabuSettingsWidget(qt.QWidget):
    """
    Widget containing the entire gui for nabu (control flow + parameters
    settings)
    """

    sigConfigChanged = qt.Signal()
    """Signal emitted when the configuration change"""

    def __init__(self, parent, flow_direction="vertical"):
        qt.QWidget.__init__(self, parent=parent)
        flow_direction = FlowDirection(flow_direction)
        self.setLayout(qt.QGridLayout())
        self._filteringActive = True
        self._configuration_level = ConfigurationLevel.OPTIONAL

        # reconstruction type
        self._widget_recons = qt.QWidget(parent=self)
        self._widget_recons.setLayout(qt.QHBoxLayout())
        self._modeLabel = qt.QLabel("mode:")
        self._widget_recons.layout().addWidget(self._modeLabel)
        self._nabuModeCB = qt.QComboBox(parent=self)
        for mode in NabuMode:
            self._nabuModeCB.addItem(mode.value)
        self._widget_recons.layout().addWidget(self._nabuModeCB)
        self.layout().addWidget(self._widget_recons, 0, 1, 1, 2)

        # spacer
        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum)
        self.layout().addWidget(spacer, 1, 8, 1, 1)

        # flow
        self._flow = NabuFlowArea(parent=self, direction=flow_direction)
        self._flow.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum)
        self.layout().addWidget(self._flow, 1, 1, 5, 1)

        # nabu configuration
        self._configurationScrollArea = qt.QScrollArea(self)
        self._configurationScrollArea.setWidgetResizable(True)
        self._configuration = NabuConfiguration(parent=self)
        self._configurationScrollArea.setWidget(self._configuration)
        self._configurationScrollArea.setHorizontalScrollBarPolicy(
            qt.Qt.ScrollBarAlwaysOff
        )
        self.layout().addWidget(self._configurationScrollArea, 1, 2, 5, 1)
        self._configuration.setSizePolicy(
            qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum
        )
        self._configurationScrollArea.setSizePolicy(
            qt.QSizePolicy.Minimum, qt.QSizePolicy.Minimum
        )

        # expose API
        self.setIniProcessing = self._flow.setIniProcessing
        self.setPreProcessing = self._flow.setPreProcessing
        self.setPhaseProcessing = self._flow.setPhaseProcessing
        self.setProcessing = self._flow.setProcessing
        self.setPostProcessing = self._flow.setPostProcessing

        self.setOutputDir = self._configuration.setOutputDir
        self.getActiveProcess = self._flow.getProcessFocused

        # set up
        pre_processing = ("pre processing",)
        roman_one_icon = tomwer_icons.getQIcon("roman_one")
        self.setPreProcessing(pre_processing, icons=(roman_one_icon,))
        phase_processing = ("phase",)
        roman_two_icon = tomwer_icons.getQIcon("roman_two")
        phase_icons = (roman_two_icon,)
        self.setPhaseProcessing(phase_processing, icons=phase_icons)
        processing = ("reconstruction",)
        processing_icons = (tomwer_icons.getQIcon("roman_three"),)
        self.setProcessing(processes=processing, icons=processing_icons)
        post_processing = ("save",)
        post_processing_icons = (tomwer_icons.getQIcon("roman_four"),)
        self.setPostProcessing(post_processing, icons=post_processing_icons)
        index_mode = self._nabuModeCB.findText(NabuMode.AUTO.value)
        assert index_mode >= 0, "full filed should be registered in the widget"
        self._nabuModeCB.setCurrentIndex(index_mode)
        self._flow.setMaximumWidth(240)

        # signal / slot connections
        self._flow.sigConfigurationChanged.connect(self._processSelectionChanged)
        self._flow.sigResetConfiguration.connect(self._processSelectionChanged)
        self._configuration.sigConfChanged.connect(self._triggerConfigChanged)
        self._nabuModeCB.currentIndexChanged.connect(self._triggerConfigChanged)

    def _triggerConfigChanged(self, *args, **kwargs):
        self.sigConfigChanged.emit()

    def getConfiguration(self):
        conf = self._configuration.getConfiguration()
        conf["reconstruction"][
            "enable_halftomo"
        ] = self.getMode().to_nabu_enable_halftomo_value()
        return conf

    def setConfiguration(self, config):
        self._configuration.setConfiguration(config=config)

    def setDeltaBetaValue(self, value):
        self._configuration.setDeltaBetaValue(value=value)

    def getMode(self):
        return NabuMode(self._nabuModeCB.currentText())

    def setMode(self, mode: int | str | NabuMode):
        mode = NabuMode.from_nabu_enable_halftomo_value(mode)
        idx = self._nabuModeCB.findText(mode.value)
        self._nabuModeCB.setCurrentIndex(idx)

    def _processSelectionChanged(self, *arg):
        if self.isConfigFiltered():
            self.updateConfigurationFilter()

    def isConfigFiltered(self):
        return self._filteringActive

    def updateConfigurationFilter(self, *args, **kwargs):
        if self.isConfigFiltered():
            stage, option = self.getActiveProcess()
        else:
            stage = None
            option = None
        self._configuration.applyFilter(stage=stage, option=option)
        self._configuration.setConfigurationLevel(self.getConfigurationLevel())
        # force scroll bar to update
        self._configurationScrollArea.updateGeometry()

    def setConfigurationLevel(self, level):
        level = ConfigurationLevel(level)
        self._configuration_level = level
        self.updateConfigurationFilter()

    def getConfigurationLevel(self):
        return self._configuration_level

    def setFilteringActive(self, active):
        self._filteringActive = active
        self.updateConfigurationFilter()
