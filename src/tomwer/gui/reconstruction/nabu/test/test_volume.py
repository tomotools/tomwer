from __future__ import annotations

from tomwer.tests.conftest import qtapp  # noqa F401
from tomwer.gui.reconstruction.nabu.volume.NabuVolumeWindow import NabuVolumeWindow


def test_SlurmSettingsWindow(qtapp):  # noqa F811
    """
    test that slurm SettingsWindow can load all the different slurm settings modes
    """
    widget = NabuVolumeWindow()
    assert widget.getConfiguration() == {
        "postproc": {
            "output_histogram": 1,
        },
        "reconstruction": {
            "end_z": -1,
            "start_z": 0,
        },
        "resources": {
            "cpu_mem_fraction": 0.9,
            "gpu_mem_fraction": 0.9,
        },
    }

    new_config = {
        "postproc": {
            "output_histogram": 0,
        },
        "reconstruction": {
            "end_z": 10,
            "start_z": 22,
        },
        "resources": {
            "cpu_mem_fraction": 0.7,
            "gpu_mem_fraction": 0.8,
        },
    }
    widget.setConfiguration(new_config)
    assert widget.getConfiguration() == new_config
