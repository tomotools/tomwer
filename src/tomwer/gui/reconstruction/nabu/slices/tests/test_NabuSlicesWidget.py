from tomwer.tests.conftest import qtapp  # noqa F401
from tomwer.gui.reconstruction.nabu.slices.NabuSlicesWidget import NabuSlicesWidget


def test_NabuSlicesWidget(
    qtapp,  # noqa F811
):
    widget = NabuSlicesWidget()

    assert widget.getConfiguration() == {
        "XY": "middle",
        "XZ": None,
        "YZ": None,
    }

    configuration = {
        "XY": "1, 3, 5",
        "XZ": "middle",
        "YZ": "100:200:20",
    }

    widget.setConfiguration(configuration)
    assert widget.getConfiguration() == configuration
