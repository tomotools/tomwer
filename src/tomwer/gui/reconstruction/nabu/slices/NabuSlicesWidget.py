from __future__ import annotations


from silx.gui import qt

from tomwer.core.reconstruction.nabu.plane import NabuPlane
from tomwer.gui.reconstruction.nabu.slices.SliceGroupBox import SliceGroupBox
from tomwer.gui import icons as tomwer_icons
from tomwer.gui.utils.illustrations import _IllustrationWidget


class NabuSlicesWidget(qt.QWidget):
    """Widget dedicated to reconstruct a set of singles slices"""

    sigConfigChanged = qt.Signal()
    """Emit when the configuration has changed"""

    PIXMAP_WIDTH = 30
    PIXMAP_HEIGHT = 30

    MODELIZATION_WIDTH = 400
    MODELIZATION_HEIGHT = 320

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.setLayout(qt.QGridLayout())

        # slices to be reconstruct along XY plane
        self._fastSpeedFrame = qt.QLabel()
        fast_speed = tomwer_icons.getQIcon("high_speed")
        self._fastSpeedFrame.setPixmap(
            fast_speed.pixmap(self.PIXMAP_WIDTH, self.PIXMAP_HEIGHT)
        )
        self._fastSpeedFrame.setFixedSize(self.PIXMAP_WIDTH, self.PIXMAP_HEIGHT)
        tooltip_speed_XY = "Reconstruction along XY plane is expected to the fastest"
        self._fastSpeedFrame.setToolTip(tooltip_speed_XY)
        self.layout().addWidget(self._fastSpeedFrame, 0, 0, 1, 1)

        self._XYSlices = SliceGroupBox(parent=self, title="XY plane")
        self.layout().addWidget(self._XYSlices, 0, 1, 1, 1)

        # slices to be reconstruct along XZ plane
        self._mediumSpeedFrame = qt.QLabel()
        medium_speed = tomwer_icons.getQIcon("medium_low_speed")
        self._mediumSpeedFrame.setPixmap(
            medium_speed.pixmap(self.PIXMAP_WIDTH, self.PIXMAP_HEIGHT)
        )
        tooltip_speed_XZ = "Reconstruction along XY plane is expected to the take more time than XY due to file format constraints"
        self._mediumSpeedFrame.setToolTip(tooltip_speed_XZ)
        self.layout().addWidget(self._mediumSpeedFrame, 1, 0, 1, 1)

        self._XZSlices = SliceGroupBox(parent=self, title="XZ plane")
        self._XZSlices.setChecked(False)
        self.layout().addWidget(self._XZSlices, 1, 1, 1, 1)

        # slices to be reconstruct along YZ plane
        self._lowSpeedFrame = qt.QLabel()
        low_speed = tomwer_icons.getQIcon("low_speed")
        self._lowSpeedFrame.setPixmap(
            low_speed.pixmap(self.PIXMAP_WIDTH, self.PIXMAP_HEIGHT)
        )
        tooltip_speed_YZ = "Reconstruction along YZ plane is expected to be the worst case scenario. It might take almost as long as reconstructing the full volume - per slice"
        self._lowSpeedFrame.setToolTip(tooltip_speed_YZ)
        self.layout().addWidget(self._lowSpeedFrame, 2, 0, 1, 1)

        self._YZSlices = SliceGroupBox(parent=self, title="YZ plane")
        self._YZSlices.setChecked(False)
        self.layout().addWidget(self._YZSlices, 2, 1, 1, 1)

        self._modelizationIllustration = _IllustrationWidget(
            parent=self, img="modelization_sample_stage", use_svg=False
        )
        self._modelizationIllustration.setFixedSize(
            qt.QSize(self.MODELIZATION_WIDTH, self.MODELIZATION_HEIGHT)
        )
        self._modelizationIllustration.setSizePolicy(
            qt.QSizePolicy.Expanding, qt.QSizePolicy.Expanding
        )
        self._modelizationIllustration._updatePixmap()
        self.layout().addWidget(self._modelizationIllustration, 0, 2, 3, 1)

        # connect signal / slot
        self._XYSlices.sigSlicesChanged.connect(self.sigConfigChanged)
        self._XZSlices.sigSlicesChanged.connect(self.sigConfigChanged)
        self._YZSlices.sigSlicesChanged.connect(self.sigConfigChanged)

    def getConfiguration(self) -> dict:
        return {
            NabuPlane.XY.value: self._XYSlices.getSlices(),
            NabuPlane.XZ.value: self._XZSlices.getSlices(),
            NabuPlane.YZ.value: self._YZSlices.getSlices(),
        }

    def setConfiguration(self, config: dict) -> dict:
        for plane, widget in {
            NabuPlane.XY: self._XYSlices,
            NabuPlane.XZ: self._XZSlices,
            NabuPlane.YZ: self._YZSlices,
        }.items():
            assert isinstance(
                config, dict
            ), f"config should be an instance of dict. Got {type(config)}."
            slices = config.get(plane.value, None)
            if slices is not None:
                widget.setSlices(slices)

    def sizeHint(self) -> qt.QSize:
        return qt.QSize(
            self.MODELIZATION_WIDTH * 2,
            self.MODELIZATION_HEIGHT,
        )
