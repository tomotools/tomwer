from __future__ import annotations


from silx.gui import qt

from tomwer.core.utils.dictutils import concatenate_dict
from tomwer.gui.settings import TAB_LABEL_PLATFORM_SETTINGS
from tomwer.gui.reconstruction.nabu.platform import NabuPlatformSettings
from tomwer.gui.reconstruction.nabu.slices.NabuSlicesWidget import NabuSlicesWidget


class NabuSlicesWindow(qt.QMainWindow):
    """
    Main window for slices reconstruction.
    Embed:
    * the NabuSlicesWidget (interface to define the slices to be reconstruct)
    * the NabuPlatformSettings (interface to define platform specificities - gpus...)
    """

    sigConfigChanged = qt.Signal()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self._tabs = qt.QTabWidget()
        self._slicesWidget = NabuSlicesWidget()
        self._tabs.addTab(
            self._slicesWidget,
            "slices",
        )

        self._platformWidget = NabuPlatformSettings()
        self._tabs.addTab(
            self._platformWidget,
            TAB_LABEL_PLATFORM_SETTINGS,
        )
        self.setCentralWidget(self._tabs)

        # connect signal / slot
        self._slicesWidget.sigConfigChanged.connect(self.sigConfigChanged)
        self._platformWidget.sigConfigChanged.connect(self.sigConfigChanged)

    def getConfiguration(self) -> dict:
        return concatenate_dict(
            {
                "slices": self._slicesWidget.getConfiguration(),
            },
            {
                "resources_overwritten": self._platformWidget.getConfiguration()[
                    "resources"
                ],
            },
        )

    def setConfiguration(self, config: dict) -> None:
        self._slicesWidget.setConfiguration(config=config.get("slices", {}))
        self._platformWidget.setConfiguration(
            config=config.get("resources_overwritten", {})
        )
