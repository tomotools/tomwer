from __future__ import annotations


from silx.gui import qt

from tomwer.gui.utils.scrollarea import QComboBoxIgnoreWheel
from tomwer.core.reconstruction.nabu.slice.NabuSliceMode import NabuSliceMode
from tomwer.gui import icons
from tomwer.gui.utils.inputwidget import SelectionLineEdit


class SliceGroupBox(qt.QGroupBox):
    """GroupBox to define slice(s) to be reconstructed"""

    sigSlicesChanged = qt.Signal()
    """Signal emitted when the selected slices change"""

    def __init__(self, parent, title: str = "slices"):
        qt.QGroupBox.__init__(self, title, parent)
        self.setCheckable(True)
        self.setLayout(qt.QHBoxLayout())
        # mode
        self._modeCB = QComboBoxIgnoreWheel(parent=self)
        for mode in NabuSliceMode:
            self._modeCB.addItem(mode.value)
        self.layout().addWidget(self._modeCB)
        self._modeCB.setFocusPolicy(qt.Qt.FocusPolicy.NoFocus)

        # slice line edit
        self._sliceQLE = SelectionLineEdit(parent=self, allow_negative_indices=True)
        self.layout().addWidget(self._sliceQLE)

        # warning icon
        warningIcon = icons.getQIcon("warning")
        self._noSliceWarning = qt.QLabel("")
        self._noSliceWarning.setPixmap(warningIcon.pixmap(20, state=qt.QIcon.On))
        self._noSliceWarning.setToolTip(
            "no slice defined, No slice reconstruction will be done"
        )
        self.layout().addWidget(self._noSliceWarning)

        # spacer
        spacer = qt.QWidget(parent=self)
        spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding)
        self.layout().addWidget(spacer)

        # set up
        self.setChecked(True)
        self.setMode(NabuSliceMode.MIDDLE)
        self._sliceQLE.setVisible(False)
        self._noSliceWarning.setVisible(False)

        # connect signal / slot
        self._modeCB.currentIndexChanged.connect(self._updateSliceQLEVisibility)
        self._modeCB.currentIndexChanged.connect(self.sigSlicesChanged)
        self._sliceQLE.editingFinished.connect(self.sigSlicesChanged)
        self._sliceQLE.textChanged.connect(self._updateNoSliceWarningVisibility)
        self.toggled.connect(self.sigSlicesChanged)

    def _updateSliceQLEVisibility(self, *args, **kwargs):
        self._sliceQLE.setVisible(self.getMode() == NabuSliceMode.OTHER)
        self._updateNoSliceWarningVisibility()

    def _updateNoSliceWarningVisibility(self):
        def no_other_slice_defined() -> bool:
            slices_defined = self._sliceQLE.text().replace(" ", "")
            slices_defined = self._sliceQLE.text().replace(",", "")
            slices_defined = self._sliceQLE.text().replace(";", "")
            return slices_defined == ""

        self._noSliceWarning.setVisible(
            self.getMode() is NabuSliceMode.OTHER and no_other_slice_defined()
        )

    def setMode(self, mode: NabuSliceMode):
        mode = NabuSliceMode(mode)
        item_index = self._modeCB.findText(mode.value)
        self._modeCB.setCurrentIndex(item_index)
        old = self.blockSignals(True)
        self._updateSliceQLEVisibility()
        self.blockSignals(old)

    def getMode(self) -> NabuSliceMode:
        mode = NabuSliceMode(self._modeCB.currentText())
        return mode

    def _getSliceSelected(self):
        if self.getMode() is NabuSliceMode.MIDDLE:
            return NabuSliceMode.MIDDLE.value
        else:
            return self._sliceQLE.text()

    def getSlices(self):
        """Slice selected"""
        if self.isChecked():
            return self._getSliceSelected()
        else:
            return None

    def setSlices(self, slices):
        if slices is None:
            self.setChecked(False)
        else:
            self.setChecked(True)
            if slices != NabuSliceMode.MIDDLE.value:
                self._sliceQLE.setText(slices)
                self.setMode(NabuSliceMode.OTHER)
            else:
                self.setMode(NabuSliceMode.MIDDLE)
