from __future__ import annotations

from silx.gui import qt
from .nabuconfig.NabuSettingsWindow import NabuSettingsWindow


class NabuSettingsDialog(qt.QDialog):
    sigComputationRequested = qt.Signal()
    """Signal emitted when a computation is requested"""

    def __init__(self, parent):
        qt.QDialog.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())

        self._widget = NabuSettingsWindow(self)
        self.layout().addWidget(self._widget)

        self._computePB = qt.QPushButton("compute", self)
        self._buttons = qt.QDialogButtonBox(self)
        self._buttons.addButton(self._computePB, qt.QDialogButtonBox.ActionRole)
        self.layout().addWidget(self._buttons)

        # set up

        # expose API
        self.setOutputDir = self._widget.setOutputDir

    def setScan(self, scan):
        self._widget.setScan(scan)

    def getWidget(self):
        return self._widget

    def getConfiguration(self):
        return self._widget.getConfiguration()
