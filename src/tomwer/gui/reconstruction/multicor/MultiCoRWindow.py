from __future__ import annotations

import numpy
from silx.gui import qt

from tomwer.core.reconstruction.multi.multicor.params import ReconstructionMode
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.reconstruction.nabu.utils import slice_index_to_int
from tomwer.gui import icons
from tomwer.gui.reconstruction.multicor.NabuAutoCorDiag import NabuAutoCorDiag
from tomwer.gui.reconstruction.multicor.MultiCoRTabWidget import MultiCoRTabWidget
from tomwer.gui.reconstruction.scores.control import ControlWidget
from tomwer.gui.utils.buttons import TabBrowsersButtons
from tomwer.gui.utils.scandescription import ScanNameLabelAndShape
from tomwer.synctools.axis import QCoRParams


class MultiCoRWindow(qt.QMainWindow):
    """
    Widget used to determine launch several slice reconstruction and ease center of rotation search
    """

    sigValidated = qt.Signal()
    sigStartSinogramLoad = qt.Signal()
    sigEndSinogramLoad = qt.Signal()
    sigConfigurationChanged = qt.Signal()

    def __init__(self, parent=None, backend=None):
        qt.QMainWindow.__init__(self, parent)
        self._scan = None
        self._automaticCorWidget = None
        self._qaxis_rp = QCoRParams()
        self.setWindowFlags(qt.Qt.Widget)
        # central widget
        self._mainWidget = qt.QWidget(self)
        self._mainWidget.setLayout(qt.QVBoxLayout())

        self._scanInfo = ScanNameLabelAndShape(self)
        self._mainWidget.layout().addWidget(self._scanInfo)
        self._tabWidget = MultiCoRTabWidget(self, backend=backend)
        self._mainWidget.layout().addWidget(self._tabWidget)
        self.setCentralWidget(self._mainWidget)
        # next and previous buttons for browsing the tab widget
        self._browserButtons = TabBrowsersButtons(self)
        self._dockWidgetBrwButtons = qt.QDockWidget(self)
        self._dockWidgetBrwButtons.setWidget(self._browserButtons)
        self.addDockWidget(qt.Qt.BottomDockWidgetArea, self._dockWidgetBrwButtons)
        self._dockWidgetBrwButtons.setFeatures(qt.QDockWidget.DockWidgetMovable)
        # control widget (validate, compute, cor positions)
        self._multicorControl = ControlWidget(self)
        self._dockWidgetCtrl = qt.QDockWidget(self)
        self._dockWidgetCtrl.setWidget(self._multicorControl)
        self.addDockWidget(qt.Qt.BottomDockWidgetArea, self._dockWidgetCtrl)
        self._dockWidgetCtrl.setFeatures(qt.QDockWidget.DockWidgetMovable)

        # connect signal / slot
        self._multicorControl.sigValidateRequest.connect(self.sigValidated)
        self._tabWidget.sigStartSinogramLoad.connect(self.sigStartSinogramLoad)
        self._tabWidget.sigEndSinogramLoad.connect(self.sigEndSinogramLoad)
        self._tabWidget.sigConfigurationChanged.connect(self.sigConfigurationChanged)
        self._tabWidget.sigReconstructionSliceChanged.connect(self._updateSinogramLine)
        self._tabWidget.sigAutoCorRequested.connect(self._autoCorRequested)
        self._tabWidget.sigReconstructionRangeChanged.connect(
            self._estimatedCorValueChanged
        )
        self._browserButtons.sigNextReleased.connect(self._showNextPage)
        self._browserButtons.sigPreviousReleased.connect(self._showPreviousPage)

    def clearResults(self):
        self._tabWidget._resultsViewer.clear()

    def showResults(self):
        self._tabWidget.showResults()

    def getAutomaticCorWindow(self):
        if self._automaticCorWidget is None:
            self._automaticCorWidget = NabuAutoCorDiag(self, qarixrp=self._qaxis_rp)
            self._automaticCorWidget.setWindowTitle(
                "compute estimated center of rotation"
            )
            auto_cor_icon = icons.getQIcon("a")
            self._automaticCorWidget.setWindowIcon(auto_cor_icon)
            self._automaticCorWidget.sigRequestAutoCor.connect(self.computeEstimatedCor)
        return self._automaticCorWidget

    def compute(self):
        """force compute of the current scan"""
        self._multicorControl.sigComputationRequest.emit()

    def getConfiguration(self) -> dict:
        return self._tabWidget.getConfiguration()

    def setConfiguration(self, config: dict):
        self._tabWidget.setConfiguration(config)

    def getQAxisRP(self):
        return self._qaxis_rp

    def setScan(self, scan):
        self._scan = scan
        self._tabWidget.setScan(scan)
        self._scanInfo.setScan(scan)
        self._updateSinogramLine()
        self._loadEstimatedCorFromScan(scan)

    def _loadEstimatedCorFromScan(self, scan):
        if scan.cor_params is not None:
            relative_cor = scan.cor_params.relative_cor_value
        else:
            relative_cor = None
        if relative_cor is None:
            relative_cor = scan.x_rotation_axis_pixel_position

        if relative_cor is not None and numpy.issubdtype(
            type(relative_cor), numpy.number
        ):
            self.setEstimatedCorPosition(relative_cor)

    def getScan(self):
        return self._scan

    def _updateSinogramLine(self):
        r_slice = self.getReconstructionSlices()
        if r_slice == "middle":
            line = slice_index_to_int(slice_index="middle", scan=self._scan, axis="XY")
        else:
            line = list(r_slice.values())[0]
        self._tabWidget.getSinogramViewer().setLine(line)

    def _autoCorRequested(self):
        window = self.getAutomaticCorWindow()
        window.activateWindow()
        window.raise_()
        window.show()

    def _estimatedCorValueChanged(self):
        cors = self.getCors("absolute")
        sino_viewer = self._tabWidget._sliceAndCorWidget._sinogramViewer
        estimated_cor = self.getEstimatedCorPosition("absolute")
        if estimated_cor == "middle":
            estimated_cor = 0

        if len(cors) < 2:
            return
        elif len(cors) > 2:
            other_cors = cors[1:-1]
        else:
            other_cors = ()
        sino_viewer.setCorRange(
            cor_min=cors[0],
            cor_max=cors[-1],
            estimated_cor=estimated_cor,
            other_cors=other_cors,
        )

    def _showNextPage(self, *args, **kwargs):
        idx = self._tabWidget.currentIndex()
        idx += 1
        if idx < self._tabWidget.count():
            self._tabWidget.setCurrentIndex(idx)

    def _showPreviousPage(self, *args, **kwargs):
        idx = self._tabWidget.currentIndex()
        idx -= 1
        if idx >= 0:
            self._tabWidget.setCurrentIndex(idx)

    def close(self):
        self._tabWidget.close()
        self._tabWidget = None
        super().close()

    # expose API
    def setCorScores(
        self,
        scores: dict,
        score_method: ScoreMethod,
        img_width: int | None = None,
        update_only_scores: bool = False,
    ):
        self._tabWidget.setCorScores(
            scores=scores,
            score_method=score_method,
            img_width=img_width,
            update_only_scores=update_only_scores,
        )

    def setImgWidth(self, width):
        self._tabWidget.setImgWidth(width=width)

    def setVoxelSize(self, dim0: int, dim1: int, dim2: int, units: str | float) -> None:
        self._tabWidget.setVoxelSize(dim0=dim0, dim1=dim1, dim2=dim2, units=units)

    def setVolumeSize(
        self, dim0: int, dim1: int, dim2: int, units: str | float
    ) -> None:
        self._tabWidget.setVolumeSize(dim0=dim0, dim1=dim1, dim2=dim2, units=units)

    def setCurrentCorValue(self, value: float) -> None:
        self._tabWidget.setCurrentCorValue(value=value)

    def getCurrentCorValue(self) -> float | None:
        return self._tabWidget.getCurrentCorValue()

    def getEstimatedCorPosition(self, mode: str = "relative") -> float | str:
        return self._tabWidget.getEstimatedCorPosition(mode=mode)

    def setEstimatedCorPosition(self, value: str | float):
        self._tabWidget.setEstimatedCorPosition(value=value)

    def getNReconstruction(self) -> int:
        return self._tabWidget.getNReconstruction()

    def setNReconstruction(self, n: int):
        self._tabWidget.setNReconstruction(n=n)

    def getResearchWidth(self) -> float:
        return self._tabWidget.getResearchWidth()

    def setResearchWidth(self, width: float):
        self._tabWidget.setResearchWidth(width=width)

    def getReconstructionSlices(self) -> tuple:
        return self._tabWidget.getReconstructionSlices()

    def setReconstructionSlices(self, slices) -> tuple:
        self._tabWidget.setReconstructionSlices(slices=slices)

    def getSlicesRange(self) -> tuple:
        return self._tabWidget.getSlicesRange()

    def setSlicesRange(self, min_index: int, max_index: int):
        self._tabWidget.setSlicesRange(min_index=min_index, max_index=max_index)

    def getCors(self, reference) -> tuple | str:
        return self._tabWidget.getCors(reference)

    def getMode(self) -> ReconstructionMode:
        return self._tabWidget.getReconstructionMode()

    def loadSinogram(self) -> None:
        self._tabWidget.loadSinogram()

    def saveReconstructedSlicesTo(self, output_folder: str) -> None:
        self._tabWidget.saveReconstructedSlicesTo(output_folder=output_folder)

    def getScoreMethod(self) -> ScoreMethod:
        return self._tabWidget.getScoreMethod()

    def lockAutofocus(self, lock: bool):
        self._tabWidget.lockAutoFocus(lock=lock)

    def isAutoFocusLock(self) -> bool:
        return self._tabWidget.isAutoFocusLock()

    def hideAutoFocusButton(self) -> None:
        return self._tabWidget.hideAutoFocusButton()
