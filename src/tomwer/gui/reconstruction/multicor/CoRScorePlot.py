from __future__ import annotations

from tomwer.gui.reconstruction.scores.scoreplot import CorSelection
from tomwer.gui.reconstruction.scores.scoreplot import ScorePlot as _ScorePlot
from tomwer.tasks.reconstruction.nabu.multicor import MultiCoRTask


class CoRScorePlot(_ScorePlot, constructor=CorSelection):
    """Score plot dedicated to center of rotation.
    Redefine the current score value to display both the absolute
    and the relative values
    """

    def _updateScores(self):
        scan = self.getScan()
        img_width = None
        if scan is not None:
            if scan.multicor_params:
                scan.multicor_params.score_method = self.getScoreMethod()
                img_width = scan.dim_1
                # update autofocus
                MultiCoRTask.autofocus(scan)

        self.setVarScores(
            scores=self._scores,
            score_method=self.getScoreMethod(),
            img_width=img_width,
            update_only_scores=True,
        )

    def _applyAutofocus(self):
        scan = self.getScan()
        if scan is None:
            return
        if scan.multicor_params:
            best_cor = scan.multicor_params.autofocus
            if best_cor:
                self._varSlider.setVarValue(best_cor)
