from __future__ import annotations

from silx.gui import qt

from tomwer.core.reconstruction.cor import mode as axis_mode
from tomwer.gui.reconstruction.cor.CalculationWidget import CalculationWidget


class NabuAutoCorDiag(qt.QDialog):
    """
    GUI to compute an estimation of the Center Of Rotation
    """

    class CalculationWidget(CalculationWidget):
        def _modeChanged(self, *args, **kwargs):
            super()._modeChanged()
            self.getMethodLockPB().hide()

    sigRequestAutoCor = qt.Signal()
    """emit when user request auto cor"""

    def __init__(self, parent=None, qarixrp=None):
        assert qarixrp is not None, "An instance of QAxisRP should be provided"
        qt.QDialog.__init__(self, parent)
        self.setLayout(qt.QVBoxLayout())
        self._automatic_cor = NabuAutoCorDiag.CalculationWidget(
            parent=self,
            cor_params=qarixrp,
        )
        self._automatic_cor.getMethodLockPB().hide()

        qcb = self._automatic_cor._qcbPosition
        for mode in (axis_mode.CoRMethod.manual,):
            idx = qcb.findText(mode.value)
            if idx >= 0:
                qcb.removeItem(idx)

        self.layout().addWidget(self._automatic_cor)
        spacer = qt.QWidget(self)
        spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding)
        self.layout().addWidget(spacer)

        # buttons
        types = qt.QDialogButtonBox.Ok
        self._buttons = qt.QDialogButtonBox(parent=self)
        self._buttons.setStandardButtons(types)
        self.layout().addWidget(self._buttons)

        self._buttons.button(qt.QDialogButtonBox.Ok).clicked.connect(
            self.sigRequestAutoCor
        )
        self._buttons.button(qt.QDialogButtonBox.Ok).setText("compute")
