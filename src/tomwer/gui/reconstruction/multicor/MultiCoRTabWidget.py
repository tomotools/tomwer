from __future__ import annotations

from silx.gui import qt

from tomwer.core.reconstruction.multi.multicor.params import ReconstructionMode
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.reconstruction.multi.multicor.params import MultiCoRParams
from tomwer.core.settings import ParametersKeys
from tomwer.gui import icons
from tomwer.gui.reconstruction.nabu.platform import NabuPlatformSettings
from tomwer.gui.reconstruction.multicor.corrangeselector import _SliceAndCorWidget
from tomwer.gui.reconstruction.multicor.CoRScorePlot import CoRScorePlot
from tomwer.gui.settings import TAB_LABEL_PLATFORM_SETTINGS


class MultiCoRTabWidget(qt.QTabWidget):
    sigConfigurationChanged = qt.Signal()
    """signal emitted each time the 'input' configuration changed.
    like slice to reconstruct, number of reconstruction, research width,
    nabu reconstruction parameters..."""

    sigStartSinogramLoad = qt.Signal()
    sigEndSinogramLoad = qt.Signal()

    def __init__(self, parent, backend=None):
        qt.QTabWidget.__init__(self, parent)
        # select slice & cor range
        self._sliceAndCorWidget = _SliceAndCorWidget(self)
        sinogram_icon = icons.getQIcon("sinogram")
        self.addTab(self._sliceAndCorWidget, sinogram_icon, "slice && cor range")
        # platform settings
        self._localPlatformSettings = NabuPlatformSettings(self)
        settings_icons = icons.getQIcon("parameters")
        self.addTab(
            self._localPlatformSettings, settings_icons, TAB_LABEL_PLATFORM_SETTINGS
        )
        # results
        self._resultsViewer = CoRScorePlot(self, variable_name="cor", backend=backend)
        results_icon = icons.getQIcon("results")
        self.addTab(self._resultsViewer, results_icon, "reconstructed slices")

        # connect signal / slot
        self._sliceAndCorWidget._multiCoROpts.sigConfigurationChanged.connect(
            self._configurationChanged
        )
        self._resultsViewer.sigConfigurationChanged.connect(self._configurationChanged)
        self.sigReconstructionSliceChanged = (
            self._sliceAndCorWidget.sigReconstructionSliceChanged
        )
        self.sigAutoCorRequested = self._sliceAndCorWidget.sigAutoCorRequested
        self.sigReconstructionRangeChanged = (
            self._sliceAndCorWidget.sigReconstructionRangeChanged
        )
        self._localPlatformSettings.sigConfigChanged.connect(
            self.sigConfigurationChanged
        )

        # connect signal / slots
        self._sliceAndCorWidget.sigStartSinogramLoad.connect(self.sigStartSinogramLoad)
        self._sliceAndCorWidget.sigEndSinogramLoad.connect(self.sigEndSinogramLoad)

    def showResults(self):
        self.setCurrentWidget(self._resultsViewer)

    def _configurationChanged(self, *args, **kwargs):
        self.sigConfigurationChanged.emit()

    def lockAutoFocus(self, lock):
        self._resultsViewer.lockAutoFocus(lock=lock)

    def isAutoFocusLock(self):
        return self._resultsViewer.isAutoFocusLock()

    def hideAutoFocusButton(self):
        self._resultsViewer.hideAutoFocusButton()

    def getSinogramViewer(self):
        return self._sliceAndCorWidget._sinogramViewer

    def getCors(self, reference: str = "relative"):
        """Return cors to be computed"""
        if reference not in ("relative", "absolute"):
            raise ValueError("reference should be 'absolute' or 'relative'")
        return MultiCoRParams.compute_cors(
            estimated_cor=self.getEstimatedCorPosition(reference),
            research_width=self.getResearchWidth(),
            n_reconstruction=self.getNReconstruction(),
        )

    def loadPreprocessingParams(self):
        """load reconstruction nabu if tomwer has already process this
        dataset. Not done for now"""
        return False

    def setScan(self, scan):
        self._resultsViewer.setScan(scan)
        self._sliceAndCorWidget.setScan(scan)

    def getConfiguration(self):
        return {
            ParametersKeys.MULTICOR_PARAMS_KEY: {
                "research_width": self.getResearchWidth(),
                "n_reconstruction": self.getNReconstruction(),
                "slice_index": self.getReconstructionSlices(),
                "score_method": self.getScoreMethod().value,
                "estimated_cor": self.getEstimatedCorPosition(),
            },
            "workflow": {
                "autofocus_lock": self.isAutoFocusLock(),
            },
            ParametersKeys.PLATFORM_RESOURCES: {
                **self._localPlatformSettings.getConfiguration()["resources"],
            },
        }

    def setConfiguration(self, config: dict):
        if not isinstance(config, dict):
            raise TypeError(f"config should be a dictionary. Got {type(config)}")
        autofocus_lock = config.get("workflow", {}).get("autofocus_lock", None)
        if autofocus_lock is not None:
            self.lockAutoFocus(autofocus_lock)

        # handle multicor_params
        multicor_params = config.get(ParametersKeys.MULTICOR_PARAMS_KEY, {}) or {}

        research_width = multicor_params.get("research_width", None)
        if research_width is not None:
            self.setResearchWidth(research_width)
        n_reconstruction = multicor_params.get("n_reconstruction", None)
        if n_reconstruction is not None:
            self.setNReconstruction(n_reconstruction)
        estimated_cor = multicor_params.get("estimated_cor", None)
        if estimated_cor is not None:
            self.setEstimatedCorPosition(estimated_cor)
        slice_indexes = multicor_params.get("slice_index", None)
        if slice_indexes is not None:
            self.setReconstructionSlices(slice_indexes)
        score_method = multicor_params.get("score_method", None)
        if score_method:
            self.setScoreMethod(score_method)
        slice_index = multicor_params.get("slice_index", None)
        if slice_index is not None:
            self.setReconstructionSlices(slices=slice_index)

        # handle resources
        self._localPlatformSettings.setConfiguration(
            config={"resources": config.get(ParametersKeys.PLATFORM_RESOURCES, {})},
        )

    def close(self):
        self._resultsViewer.close()
        self._resultsViewer = None
        self._sliceAndCorWidget.close()
        self._sliceAndCorWidget = None
        super().close()

    # expose function API
    def getScoreMethod(self):
        return self._resultsViewer.getScoreMethod()

    def setScoreMethod(self, method):
        self._resultsViewer.setScoreMethod(method)

    def setCorScores(
        self,
        scores: dict,
        score_method: ScoreMethod,
        img_width: int | None = None,
        update_only_scores: bool = False,
    ):
        self._resultsViewer.setVarScores(
            scores=scores,
            score_method=score_method,
            img_width=img_width,
            update_only_scores=update_only_scores,
        )

    def setImgWidth(self, width: int):
        self._resultsViewer.setImgWidth(width=width)

    def setVoxelSize(self, dim0: int, dim1: int, dim2: int, units: str | float) -> None:
        self._resultsViewer.setVoxelSize(dim0=dim0, dim1=dim1, dim2=dim2, unit=units)

    def setVolumeSize(self, dim0: int, dim1: int, dim2: int, units: str | float):
        self._resultsViewer.setVolumeSize(dim0=dim0, dim1=dim1, dim2=dim2, unit=units)

    def setCurrentCorValue(self, value: float) -> None:
        self._resultsViewer.setCurrentVarValue(value=value)

    def getCurrentCorValue(self) -> float | None:
        return self._resultsViewer.getCurrentVarValue()

    def getEstimatedCorPosition(self, mode: str = "relative") -> float | str:
        return self._sliceAndCorWidget._multiCoROpts.getEstimatedCorPosition(mode=mode)

    def setEstimatedCorPosition(self, value: float) -> None:
        self._sliceAndCorWidget._multiCoROpts.setEstimatedCorPosition(value=value)

    def getNReconstruction(self) -> int:
        return self._sliceAndCorWidget._multiCoROpts.getNReconstruction()

    def setNReconstruction(self, n: int) -> None:
        self._sliceAndCorWidget._multiCoROpts.setNReconstruction(n=n)

    def getResearchWidth(self) -> float:
        return self._sliceAndCorWidget._multiCoROpts.getResearchWidth()

    def setResearchWidth(self, width: float):
        self._sliceAndCorWidget._multiCoROpts.setResearchWidth(width=width)

    def getReconstructionSlices(self) -> tuple:
        return self._sliceAndCorWidget._multiCoROpts.getReconstructionSlices()

    def setReconstructionSlices(self, slices: dict | int | str):
        self._sliceAndCorWidget._multiCoROpts.setReconstructionSlices(slices=slices)

    def getReconstructionMode(self) -> ReconstructionMode:
        return self._sliceAndCorWidget._multiCoROpts.getReconstructionMode()

    def setReconstructionMode(self, mode: ReconstructionMode):
        self._sliceAndCorWidget._multiCoROpts.setReconstructionMode(mode=mode)

    def getFrameWidth(self) -> float:
        return self._sliceAndCorWidget._multiCoROpts.getFrameWidth()

    def setFrameWidth(self, width: float):
        self._sliceAndCorWidget._multiCoROpts.setFrameWidth(width=width)

    def getSlicesRange(self) -> tuple[int]:
        return self._sliceAndCorWidget._multiCoROpts.getSlicesRange()

    def setSlicesRange(self, min_index: int, max_index: int):
        self._sliceAndCorWidget._multiCoROpts.setSlicesRange(
            min_index=min_index, max_index=max_index
        )

    def loadSinogram(self) -> None:
        self._sliceAndCorWidget.loadSinogram()

    def saveReconstructedSlicesTo(self, output_folder: str):
        self._resultsViewer.saveReconstructedSlicesTo(output_folder=output_folder)
