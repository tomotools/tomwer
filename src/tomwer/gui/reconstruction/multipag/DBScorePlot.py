from __future__ import annotations

from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.gui.reconstruction.scores.scoreplot import DelaBetaSelection
from tomwer.gui.reconstruction.scores.scoreplot import ScorePlot as _ScorePlot
from tomwer.tasks.reconstruction.nabu.multipag import MultiPagTask


class DBScorePlot(_ScorePlot, constructor=DelaBetaSelection):
    """Score plot dedicated to center delta / beta values."""

    def _updateScores(self) -> None:
        scan = self.getScan()
        if scan is not None:
            assert isinstance(scan, TomwerScanBase)
            if scan.multipag_params:
                scan.multipag_params.score_method = self.getScoreMethod()
                # update autofocus
                MultiPagTask.autofocus(scan)
        self.setVarScores(
            scores=self._scores,
            score_method=self.getScoreMethod(),
            update_only_scores=True,
        )

    def _applyAutofocus(self) -> None:
        scan = self.getScan()
        if scan is None:
            return
        if scan.multipag_params:
            best_db = scan.multipag_params.autofocus
            if best_db:
                self._varSlider.setVarValue(best_db)
