from __future__ import annotations

from silx.gui import qt

from tomwer.core.settings import ParametersKeys
from tomwer.gui import icons
from tomwer.gui.reconstruction.multipag.DBScorePlot import DBScorePlot
from tomwer.gui.reconstruction.multipag.MultiPagSelectionWidget import (
    MultiPagSelectionWidget,
)
from tomwer.gui.reconstruction.nabu.platform import NabuPlatformSettings
from tomwer.gui.settings import TAB_LABEL_PLATFORM_SETTINGS


class MultiPagTabWidget(qt.QTabWidget):
    sigConfigurationChanged = qt.Signal()
    """Signal emit when the configuration changes"""

    def __init__(self, parent=None, backend=None):
        qt.QTabWidget.__init__(self, parent=parent)

        self._deltaBetaSelectionWidget = MultiPagSelectionWidget(self)
        delta_beta_icon = icons.getQIcon("delta_beta")
        self.addTab(
            self._deltaBetaSelectionWidget, delta_beta_icon, "delta beta values"
        )
        # platform settings
        self._localPlatformSettings = NabuPlatformSettings(self)
        settings_icons = icons.getQIcon("parameters")
        self.addTab(
            self._localPlatformSettings, settings_icons, TAB_LABEL_PLATFORM_SETTINGS
        )
        # results
        self._resultsViewer = DBScorePlot(self, variable_name="db", backend=backend)
        results_icon = icons.getQIcon("results")
        self.addTab(self._resultsViewer, results_icon, "reconstructed slices")

        # connect signal / slot
        self._deltaBetaSelectionWidget.sigConfigurationChanged.connect(
            self._configurationChanged
        )
        self._resultsViewer.sigConfigurationChanged.connect(self._configurationChanged)
        self._localPlatformSettings.sigConfigChanged.connect(
            self.sigConfigurationChanged
        )

    # expose function API
    def saveReconstructedSlicesTo(self, output_folder):
        self._resultsViewer.saveReconstructedSlicesTo(output_folder=output_folder)

    def setDeltaBetaScores(self, *args, **kwargs):
        self._resultsViewer.setVarScores(*args, **kwargs)

    def setCurrentVarValue(self, *args, **kwargs):
        self._resultsViewer.setCurrentVarValue(*args, **kwargs)

    def getCurrentVarValue(self):
        return self._resultsViewer.getCurrentVarValue()

    def showResults(self):
        self.setCurrentWidget(self._resultsViewer)

    def _configurationChanged(self, *args, **kwargs):
        self.sigConfigurationChanged.emit()

    def lockAutoFocus(self, lock):
        self._resultsViewer.lockAutoFocus(lock=lock)

    def isAutoFocusLock(self):
        return self._resultsViewer.isAutoFocusLock()

    def hideAutoFocusButton(self):
        self._resultsViewer.hideAutoFocusButton()

    def getDeltaBetaValues(self):
        """Return db values to be computed"""
        return self._deltaBetaSelectionWidget.getDeltaBetaValues()

    def setDeltaBetaValues(self, values):
        """set db values to be computed"""
        self._deltaBetaSelectionWidget.setDeltaBetaValues(values)

    def loadPreprocessingParams(self):
        """load reconstruction nabu if tomwer has already process this
        dataset. Not done for now"""
        return False

    def setScan(self, scan):
        self._resultsViewer.setScan(scan)
        self._deltaBetaSelectionWidget.setScan(scan)

    def getReconstructionSlice(self):
        return self._deltaBetaSelectionWidget.getSlice()

    def setReconstructionSlice(self, slice_):
        return self._deltaBetaSelectionWidget.setSlice(slice_=slice_)

    def getConfiguration(self) -> dict:
        return {
            ParametersKeys.MULTIPAG_PARAMS_KEY: {
                "phase": {
                    "padding_type": self._deltaBetaSelectionWidget.getPaddingType().value,
                    "unsharp_coeff": self._deltaBetaSelectionWidget.getUnsharpCoeff(),
                    "unsharp_sigma": self._deltaBetaSelectionWidget.getUnsharpSigma(),
                },
                "score_method": self.getScoreMethod().value,
                "delta_beta_values": self.getDeltaBetaValues(),
                "slice_index": self.getReconstructionSlice(),
            },
            ParametersKeys.NABU_REC_PARAMS_KEY: {
                **self._localPlatformSettings.getConfiguration(),
            },
        }

    def setConfiguration(self, config: dict):
        if not isinstance(config, dict):
            raise TypeError(f"config should be a dictionary. Got {type(config)}")
        # handle multipag_params
        multipag_params = config.get(ParametersKeys.MULTIPAG_PARAMS_KEY, {})
        db_values = multipag_params.get("delta_beta_values", None)
        if db_values is not None:
            self.setDeltaBetaValues(db_values)
        padding_type = multipag_params.get("phase", {}).get("padding_type", None)
        if padding_type is not None:
            self._deltaBetaSelectionWidget.setPaddingType(padding_type=padding_type)

        unsharp_coeff = multipag_params.get("phase", {}).get("unsharp_coeff", None)
        if unsharp_coeff is not None:
            self._deltaBetaSelectionWidget.setUnsharpCoeff(coeff=unsharp_coeff)

        unsharp_sigma = multipag_params.get("phase", {}).get("unsharp_sigma", None)
        if unsharp_sigma is not None:
            self._deltaBetaSelectionWidget.setUnsharpSigma(sigma=unsharp_sigma)

        slice_index = multipag_params.get("slice_index", None)
        if slice_index is not None:
            self.setReconstructionSlice(slice_index)

        score_method = multipag_params.get("score_method", None)
        if score_method:
            self.setScoreMethod(score_method)

        # handle resources
        self._localPlatformSettings.setConfiguration(
            config=config.get(ParametersKeys.NABU_REC_PARAMS_KEY, {})
        )

    def getScoreMethod(self):
        return self._resultsViewer.getScoreMethod()

    def setScoreMethod(self, method):
        self._resultsViewer.setScoreMethod(method)

    def close(self):
        self._resultsViewer.close()
        self._resultsViewer = None
        self._deltaBetaSelectionWidget.close()
        self._deltaBetaSelectionWidget = None
        super().close()

    def setSliceRange(self, min_, max_):
        self._deltaBetaSelectionWidget.setSliceRange(min_, max_)
