from __future__ import annotations

from silx.gui import qt

from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.gui.reconstruction.multipag.MultiPagTabWidget import MultiPagTabWidget
from tomwer.gui.reconstruction.scores.control import ControlWidget
from tomwer.gui.utils.buttons import TabBrowsersButtons
from tomwer.gui.utils.scandescription import ScanNameLabelAndShape
from tomwer.synctools.multipag import QMultiPagParams


class MultiPagWindowBase(qt.QMainWindow):
    """
    Base window to compute several Paganin delta / beta value and select the best value
    """

    def __init__(self, parent=None, backend=None):
        qt.QMainWindow.__init__(self, parent)
        self._db_values = []
        self._urls = []

        self._scan = None
        self._qdeltabeta_rp = QMultiPagParams()
        self.setWindowFlags(qt.Qt.Widget)
        # central widget
        self._mainWidget = qt.QWidget(self)
        self._mainWidget.setLayout(qt.QVBoxLayout())
        self._scanInfo = ScanNameLabelAndShape(self)
        self._mainWidget.layout().addWidget(self._scanInfo)
        self._tabWidget = MultiPagTabWidget(self, backend=backend)
        self._mainWidget.layout().addWidget(self._tabWidget)
        self.setCentralWidget(self._mainWidget)
        # next and previous buttons for browsing the tab widget
        self._browserButtons = TabBrowsersButtons(self)
        self._dockWidgetBrwButtons = qt.QDockWidget(self)
        self._dockWidgetBrwButtons.setWidget(self._browserButtons)
        self.addDockWidget(qt.Qt.BottomDockWidgetArea, self._dockWidgetBrwButtons)
        self._dockWidgetBrwButtons.setFeatures(qt.QDockWidget.DockWidgetMovable)
        # control widget (validate, compute, cor positions)
        self._sadbControl = ControlWidget(self)
        self._dockWidgetCtrl = qt.QDockWidget(self)
        self._dockWidgetCtrl.setWidget(self._sadbControl)
        self.addDockWidget(qt.Qt.BottomDockWidgetArea, self._dockWidgetCtrl)
        self._dockWidgetCtrl.setFeatures(qt.QDockWidget.DockWidgetMovable)

        # expose signals
        self.sigConfigurationChanged = self._tabWidget.sigConfigurationChanged

        # connect signal / slot
        self._browserButtons.sigNextReleased.connect(self._showNextPage)
        self._browserButtons.sigPreviousReleased.connect(self._showPreviousPage)
        self._sadbControl.sigComputationRequest.connect(self._launchReconstructions)
        self._sadbControl.sigValidateRequest.connect(self._validate)

    def showResults(self):
        self._tabWidget.showResults()

    def compute(self):
        """force compute of the current scan"""
        """force compute of the current scan"""
        self._sadbControl.sigComputationRequest.emit()

    def getConfiguration(self) -> dict:
        return self._tabWidget.getConfiguration()

    def setConfiguration(self, config: dict):
        self._tabWidget.setConfiguration(config)

    def getQDeltaBetaRP(self):
        return self._qdeltabeta_rp

    def setScan(self, scan):
        self._scan = scan
        self._tabWidget.setScan(scan)
        self._scanInfo.setScan(scan)

    def getScan(self):
        return self._scan

    def lockAutofocus(self, lock):
        self._tabWidget.lockAutoFocus(lock=lock)

    def isAutoFocusLock(self):
        return self._tabWidget.isAutoFocusLock()

    def hideAutoFocusButton(self):
        return self._tabWidget.hideAutoFocusButton()

    # START -- API to be implemented by child class

    def _launchReconstructions(self):
        """callback when we want to launch the reconstruction of the
        slice for n delta/beta value"""
        raise NotImplementedError("Base class")

    def _validate(self):
        raise NotImplementedError("Base class")

    # END -- API to be implemented by child class

    def _showNextPage(self, *args, **kwargs):
        idx = self._tabWidget.currentIndex()
        idx += 1
        if idx < self._tabWidget.count():
            self._tabWidget.setCurrentIndex(idx)

    def _showPreviousPage(self, *args, **kwargs):
        idx = self._tabWidget.currentIndex()
        idx -= 1
        if idx >= 0:
            self._tabWidget.setCurrentIndex(idx)

    def close(self):
        self._tabWidget.close()
        self._tabWidget = None
        super().close()

    def setDBScores(
        self,
        scores: dict,
        score_method: str | ScoreMethod,
        img_width=None,
        update_only_scores=False,
    ):
        """

        :param scores: cor value (float) as key and
                            tuple(url: DataUrl, score: float) as value
        """
        self._tabWidget.setDeltaBetaScores(
            scores=scores,
            score_method=score_method,
            img_width=img_width,
            update_only_scores=update_only_scores,
        )

    def getScoreMethod(self):
        return self._tabWidget.getScoreMethod()

    def setCurrentDeltaBetaValue(self, value):
        self._tabWidget.setCurrentVarValue(value)

    def getCurrentDeltaBetaValue(self):
        return self._tabWidget.getCurrentVarValue()

    def setSlicesRange(self, min_, max_):
        self._tabWidget.setSliceRange(min_, max_)

    def saveReconstructedSlicesTo(self, output_folder):
        self._tabWidget.saveReconstructedSlicesTo(output_folder=output_folder)
