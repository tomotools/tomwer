from __future__ import annotations

from typing import Iterable

import numpy
from silx.gui import qt

from tomwer.core.reconstruction.nabu.utils import retrieve_lst_of_value_from_str
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.gui.configuration.level import ConfigurationLevel
from tomwer.gui.reconstruction.multicor.sliceselector import SliceSelector
from tomwer.gui.reconstruction.nabu.nabuconfig.phase import _NabuPhaseConfig


class MultiPagSelectionWidget(qt.QWidget):
    """Widget used to select the range of delta / beta to use"""

    sigConfigurationChanged = qt.Signal()
    """emit when configuration change"""

    _DEFAULT_VERTICAL_SLICE_MODE = ("middle", "other")

    def __init__(self, parent=None):
        qt.QWidget.__init__(self, parent)
        self.setWindowFlags(qt.Qt.Widget)
        self.setLayout(qt.QVBoxLayout())

        # slice selection
        self._sliceGB = qt.QGroupBox("slice", self)
        self.layout().addWidget(self._sliceGB)
        self._sliceGB.setLayout(qt.QGridLayout())
        self._label = qt.QLabel("slice", self)
        self._sliceGB.layout().addWidget(self._label, 0, 0, 1, 1)
        sl_spacer = qt.QWidget(self)
        sl_spacer.setSizePolicy(qt.QSizePolicy.Expanding, qt.QSizePolicy.Minimum)
        self._sliceGB.layout().addWidget(sl_spacer, 0, 5, 1, 1)

        self._defaultSlicesCB = qt.QComboBox(self)
        for mode in self._DEFAULT_VERTICAL_SLICE_MODE:
            self._defaultSlicesCB.addItem(mode)
        self._sliceGB.layout().addWidget(self._defaultSlicesCB, 0, 1, 1, 1)

        self._sliceSelectionQSB = SliceSelector(self, insert=False, invert_y_axis=True)
        self._sliceSelectionQSB.addSlice(value=0, name="Slice", color="green")
        self._sliceSelectionQSB.setFixedSize(qt.QSize(250, 250))
        self._sliceGB.layout().addWidget(self._sliceSelectionQSB, 1, 0, 1, 5)

        # paganin main window
        self._paganinGB = qt.QGroupBox("paganin", self)
        self._paganinGB.setLayout(qt.QVBoxLayout())
        self._mainWindow = _NabuPhaseConfig(self)
        self._mainWindow.setConfigurationLevel(ConfigurationLevel.ADVANCED)
        self._mainWindow._ctfOpts.hide()
        self._paganinGB.layout().addWidget(self._mainWindow)
        self._mainWindow._methodCB.hide()
        self._mainWindow._methodLabel.hide()
        self.layout().addWidget(self._paganinGB)

        # spacer
        widget_spacer = qt.QWidget(self)
        widget_spacer.setSizePolicy(qt.QSizePolicy.Minimum, qt.QSizePolicy.Expanding)
        self.layout().addWidget(widget_spacer)

        # connect signal / slot
        self._sliceSelectionQSB.sigSlicesChanged.connect(self._updated)
        self._mainWindow.sigConfChanged.connect(self._updated)
        self._defaultSlicesCB.currentIndexChanged.connect(self._updated)
        self._defaultSlicesCB.currentIndexChanged.connect(self._updateModeCB)

        self.setSlice("middle")

    def _updated(self, *args, **kwargs):
        self.sigConfigurationChanged.emit()

    def setSlice(self, slice_: str | int):
        if isinstance(slice_, str):
            if slice_ != "middle":
                raise ValueError(f"Slice should be 'middle' or an int. Not {slice_}")
            else:
                self.setMode("middle")
        elif isinstance(slice_, int):
            self.setMode("other")
            self._sliceSelectionQSB.setSliceValue("Slice", slice_)
        elif isinstance(slice_, dict) and "Slice" in slice_:
            self.setMode("other")
            self._sliceSelectionQSB.setSliceValue("Slice", slice_["Slice"])
        else:
            raise TypeError(f"slice should be an int or 'middle'. Not {type(slice_)}")

    def setSliceRange(self, min_, max_):
        self._sliceSelectionQSB.setSlicesRange(min_, max_)

    def getMode(self):
        return self._defaultSlicesCB.currentText()

    def setMode(self, mode):
        if mode not in self._DEFAULT_VERTICAL_SLICE_MODE:
            raise ValueError(
                f"mode should be in {self._DEFAULT_VERTICAL_SLICE_MODE}. Not {mode}."
            )
        idx = self._defaultSlicesCB.findText(mode)
        self._defaultSlicesCB.setCurrentIndex(idx)
        self._updateModeCB()

    def _updateModeCB(self):
        self._sliceSelectionQSB.setVisible(self.getMode() == "other")

    def getSlice(self):
        if self.getSliceMode() == "middle":
            return "middle"
        else:
            return self._sliceSelectionQSB.getSlicesValue()

    def getSliceMode(self):
        return self._defaultSlicesCB.currentText()

    def setScan(self, scan: TomwerScanBase):
        self._sliceSelectionQSB.setSlicesRange(0, scan.dim_2)

    def getDeltaBetaValues(self) -> numpy.array:
        db_values = self._mainWindow._paganinOpts.getDeltaBeta()
        return retrieve_lst_of_value_from_str(db_values, type_=float)

    def setDeltaBetaValues(self, values: numpy.array | Iterable):
        values = numpy.array(values)
        step = None
        if len(values) > 3:
            deltas = values[1:] - values[:-1]
            if deltas.min() == deltas.max():
                step = deltas.min()

        deltaBetaQLE = self._mainWindow._paganinOpts._deltaBetaQLE
        if step is None:
            deltaBetaQLE.setText(",".join([str(value) for value in values]))
        else:
            deltaBetaQLE.setText(
                "{from_}:{to_}:{step_}".format(
                    from_=values.min(), to_=values.max(), step_=step
                )
            )

    def getPaddingType(self):
        return self._mainWindow.getPaddingType()

    def setPaddingType(self, padding_type: str):
        self._mainWindow.setPaddingType(padding_type=padding_type)

    def getUnsharpCoeff(self) -> float:
        return self._mainWindow.getUnsharpCoeff()

    def setUnsharpCoeff(self, coeff) -> float:
        return self._mainWindow.setUnsharpCoeff(coeff=coeff)

    def getUnsharpSigma(self) -> float:
        return self._mainWindow.getUnsharpSigma()

    def setUnsharpSigma(self, sigma):
        self._mainWindow.setUnsharpSigma(sigma=sigma)
