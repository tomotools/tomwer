from __future__ import annotations

import logging

from silx.gui import qt

from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.reconstruction.cor import mode as axis_mode
from tomwer.gui.utils.buttons import PadlockButton
from tomwer.gui.utils.qt_utils import block_signals
from tomwer.synctools.axis import QCoRParams
from tomwer.gui.utils.scrollarea import QComboBoxIgnoreWheel
from tomwer.gui.reconstruction.cor.EstimatedCORWidget import EstimatedCORWidget

_logger = logging.getLogger(__name__)


class CalculationWidget(qt.QWidget):
    """
    Main widget to select the algorithm to use for COR calculation
    Used as a tab of the AxisSettingsTabWidget
    """

    sigModeChanged = qt.Signal(str)
    """signal emitted when the algorithm for computing COR changed"""

    sigLockModeChanged = qt.Signal(bool)
    """signal emitted when the mode has been lock or unlock"""
    sigUpdateXRotAxisPixelPosOnNewScan = qt.Signal()
    sigYAxisInvertedChanged = qt.Signal(bool)

    def __init__(self, parent, cor_params):
        assert isinstance(cor_params, QCoRParams)
        qt.QWidget.__init__(self, parent)
        self._cor_params = None
        self.setLayout(qt.QVBoxLayout())

        # algorithm
        self._modeWidget = qt.QWidget(parent=self)
        self._modeWidget.setLayout(qt.QHBoxLayout())
        self.layout().addWidget(self._modeWidget)

        self.__rotAxisSelLabel = qt.QLabel("algorithm to compute cor")
        self._modeWidget.layout().addWidget(self.__rotAxisSelLabel)
        self._qcbPosition = QComboBoxIgnoreWheel(self)

        algorithm_groups = (
            # radio group
            (
                axis_mode.CoRMethod.centered,
                axis_mode.CoRMethod.global_,
                axis_mode.CoRMethod.growing_window_radios,
                axis_mode.CoRMethod.sliding_window_radios,
                axis_mode.CoRMethod.octave_accurate_radios,
            ),
            # sino group
            (
                axis_mode.CoRMethod.growing_window_sinogram,
                axis_mode.CoRMethod.sino_coarse_to_fine,
                axis_mode.CoRMethod.sliding_window_sinogram,
                axis_mode.CoRMethod.fourier_angles,
            ),
            # composite coarse to fine
            (
                axis_mode.CoRMethod.composite_coarse_to_fine,
                axis_mode.CoRMethod.near,
            ),
            # read
            (axis_mode.CoRMethod.read,),
            # manual
            (axis_mode.CoRMethod.manual,),
        )
        current_pos = 0
        for i_grp, algorithm_group in enumerate(algorithm_groups):
            if i_grp != 0:
                self._qcbPosition.insertSeparator(current_pos)
                current_pos += 1
            for cor_algorithm in algorithm_group:
                self._qcbPosition.addItem(cor_algorithm.value)
                idx = self._qcbPosition.findText(cor_algorithm.value)
                self._qcbPosition.setItemData(
                    idx,
                    axis_mode.AXIS_MODE_METADATAS[cor_algorithm].tooltip,
                    qt.Qt.ToolTipRole,
                )
                current_pos += 1

        self._modeWidget.layout().addWidget(self._qcbPosition)

        # method lock button
        self._lockMethodPB = PadlockButton(parent=self._modeWidget)
        self._lockMethodPB.setToolTip(
            "Lock the method to compute the cor. \n"
            "This will automatically call the "
            "defined algorithm each time a scan is received."
        )
        self._modeWidget.layout().addWidget(self._lockMethodPB)

        # estimated cor
        self._estimatedCorWidget = EstimatedCORWidget(self, cor_params=cor_params)
        self.layout().addWidget(self._estimatedCorWidget)

        # connect signal / slot
        self._qcbPosition.currentIndexChanged.connect(self._modeChanged)
        self._lockMethodPB.sigLockChanged.connect(self.lockMode)
        self._estimatedCorWidget.sigUpdateXRotAxisPixelPosOnNewScan.connect(
            self.sigUpdateXRotAxisPixelPosOnNewScan
        )
        self._estimatedCorWidget.sigYAxisInvertedChanged.connect(
            self.sigYAxisInvertedChanged
        )

        # set up interface
        self._estimatedCorWidget._updateVisibleSides(mode=self.getMode())
        self.setAxisParams(cor_params)

    def getMethodLockPB(self) -> qt.QPushButton:
        return self._lockMethodPB

    def setEstimatedCorValue(self, value):
        self._estimatedCorWidget.setEstimatedCor(value=value)
        # note: force to update the side values.
        self._estimatedCorWidget._updateVisibleSides(mode=self.getMode())

    def getEstimatedCor(self):
        return self._estimatedCorWidget.getEstimatedCor()

    def updateXRotationAxisPixelPositionOnNewScan(self) -> bool:
        return self._estimatedCorWidget.updateXRotationAxisPixelPositionOnNewScan()

    def setUpdateXRotationAxisPixelPositionOnNewScan(self, update: bool):
        self._estimatedCorWidget.setUpdateXRotationAxisPixelPositionOnNewScan(
            update=update
        )

    def _modeChanged(self, *args, **kwargs):
        mode = self.getMode()
        with block_signals(self._qcbPosition):
            with block_signals(self._cor_params):
                self._estimatedCorWidget._updateVisibleSides(mode)
                self._cor_params.mode = mode.value
            self._cor_params.changed()
            self.sigModeChanged.emit(mode.value)

    def isModeLock(self):
        return self._lockMethodPB.isLocked()

    def setModeLock(self, mode=None):
        """set a specific mode and lock it.

        :param mode: mode to lock. If None then keep the current mode
        """
        if mode is not None:
            mode = axis_mode.CoRMethod(mode)
        if mode is None and axis_mode.AXIS_MODE_METADATAS[self.getMode()].is_lockable():
            raise ValueError(
                "Unable to lock the current mode is not an automatic algorithm"
            )
        elif (
            mode != self.getMode() and axis_mode.AXIS_MODE_METADATAS[mode].is_lockable()
        ):
            raise ValueError("Unable to lock %s this is not a lockable mode")

        if mode is not None:
            self.setMode(mode)
        if not self._lockMethodPB.isLocked():
            with block_signals(self._lockMethodPB):
                self._lockMethodPB.setLock(True)
        self.lockMode(True)

    def lockMode(self, lock):
        with block_signals(self._lockMethodPB):
            self._lockMethodPB.setLock(lock)
            self._qcbPosition.setEnabled(not lock)

        self.sigLockModeChanged.emit(lock)

    def getMode(self):
        """Return algorithm to use for axis calculation"""
        return axis_mode.CoRMethod(self._qcbPosition.currentText())

    def setMode(self, mode: axis_mode.CoRMethod):
        with block_signals(self._qcbPosition):
            index = self._qcbPosition.findText(mode.value)
            if index >= 0:
                self._qcbPosition.setCurrentIndex(index)
            else:
                raise ValueError("Unable to find mode", mode)
            self._lockMethodPB.setVisible(mode not in (axis_mode.CoRMethod.manual,))
            mode_metadata = axis_mode.AXIS_MODE_METADATAS[mode]
            estimated_cor_widget_visible = (
                mode_metadata.allows_estimated_cor_as_numerical_value
                or len(mode_metadata.valid_sides) > 0
            )
            self._estimatedCorWidget.setVisible(estimated_cor_widget_visible)
            self._estimatedCorWidget._updateVisibleSides(mode=mode)

    def setAxisParams(self, axis):
        with block_signals(self):
            if self._cor_params is not None:
                self._cor_params.sigChanged.disconnect(self._cor_params_changed)
            self._cor_params = axis
            if self._cor_params.mode in (axis_mode.CoRMethod.manual,):
                # those mode cannot be handled by the auto calculation dialog
                self._cor_params.mode = axis_mode.CoRMethod.growing_window_radios
            self._cor_params.sigChanged.connect(self._cor_params_changed)
            self._cor_params_changed()

    def _cor_params_changed(self, *args, **kwargs):
        if self._cor_params.mode != self.getMode():
            # setMode will force to update visible side. So avoid to reset it if not necessary
            self.setMode(self._cor_params.mode)

    def setScan(self, scan: TomwerScanBase | None):
        self._estimatedCorWidget.setPixelSize(pixel_size_m=scan.x_pixel_size)
        self._estimatedCorWidget.setImageWidth(image_width=scan.dim_1)
