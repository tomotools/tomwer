# coding: utf-8
from __future__ import annotations


import os
import shutil
import tempfile

import h5py
import numpy
import pytest
from silx.gui import qt
from silx.gui.utils.testutils import TestCaseQt
from silx.io.url import DataUrl
from pyunitsystem.metricsystem import MetricSystem

from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.gui.reconstruction.multicor.dimensionwidget import DimensionWidget
from tomwer.gui.reconstruction.multicor.MultiCoRWindow import MultiCoRWindow
from tomwer.tests.utils import skip_gui_test


@pytest.mark.skipif(skip_gui_test(), reason="skip gui test")
class TestDimensionWidget(TestCaseQt):
    """Test that the axis widget work correctly"""

    def setUp(self):
        self._window = DimensionWidget(title="test window")

    def tearDown(self):
        self._window.setAttribute(qt.Qt.WA_DeleteOnClose)
        self._window.close()

    def testDimension(self):
        """Insure setting dimensions values and unit are correct"""
        self.assertTrue(self._window.unit() is MetricSystem.MILLIMETER)
        # check initial values
        self.assertEqual(
            self._window.getValues(),
            (0.001, 0.001, 0.001, MetricSystem.METER),
        )
        # set dim 0 to 20 mm
        self._window._dim0ValueQLE.setValue(20)
        self._window._dim0ValueQLE.editingFinished.emit()
        self.qapp.processEvents()
        self.assertEqual(
            self._window.getValues(),
            (0.02, 0.001, 0.001, MetricSystem.METER),
        )
        # set dim 1 to 0.6 millimeter
        self._window._dim1ValueQLE.setValue(0.6)
        self._window._dim1ValueQLE.editingFinished.emit()
        self.qapp.processEvents()
        self.assertEqual(
            self._window.getValues(),
            (0.02, 0.0006, 0.001, MetricSystem.METER),
        )
        # change display to micrometer
        self._window.setUnit(MetricSystem.MICROMETER)
        self.assertEqual(
            self._window.getValues(),
            (0.02, 0.0006, 0.001, MetricSystem.METER),
        )
        self.assertEqual(
            self._window._dim0ValueQLE.value(),
            20 * MetricSystem.MILLIMETER.value / MetricSystem.MICROMETER.value,
        )
        self._window._dim2ValueQLE.setValue(500)
        self._window._dim2ValueQLE.editingFinished.emit()
        self.qapp.processEvents()
        self.assertEqual(
            self._window.getValues(),
            (0.02, 0.0006, 0.0005, MetricSystem.METER),
        )

    def testConstructorWithColors(self):
        """Insure passing colors works"""
        DimensionWidget(title="title", dims_colors=("#ffff5a", "#62efff", "#ff5bff"))

    def testConstructorWithdimNames(self):
        """Insure passing colors works"""
        DimensionWidget(title="title", dims_name=("x", "y", "z"))


@pytest.mark.skipif(skip_gui_test(), reason="skip gui test")
class TestMultiCoRWindow(TestCaseQt):
    """Test the MultiCoRWindow interface"""

    _N_OUTPUT_URLS = 10

    def setUp(self):
        super().setUp()
        self._window = MultiCoRWindow()
        self.folder = tempfile.mkdtemp()
        self._output_urls = []
        self._cor_scores = {}

        for i in range(self._N_OUTPUT_URLS):
            output_file = os.path.join(self.folder, f"recons_{i}.h5")
            with h5py.File(output_file, mode="a") as h5f:
                h5f["data"] = numpy.random.random(100 * 100).reshape(100, 100)
                url = DataUrl(file_path=output_file, data_path="data", scheme="silx")
                assert url.is_valid()
            self._output_urls.append(url)
            score = ComputedScore(
                tv=numpy.random.randint(10),
                std=numpy.random.randint(100),
            )
            self._cor_scores[i] = (url, score)

        # create a scan
        self.folder = tempfile.mkdtemp()
        dim = 10
        mock = MockNXtomo(
            scan_path=self.folder, n_proj=10, n_ini_proj=10, scan_range=180, dim=dim
        )
        mock.add_alignment_radio(index=10, angle=90)
        mock.add_alignment_radio(index=10, angle=0)
        self.scan = mock.scan

        self._window.setScan(self.scan)

    def tearDown(self):
        self._window.setAttribute(qt.Qt.WA_DeleteOnClose)
        self._window.close()
        self._window = None
        shutil.rmtree(self.folder)
        super().tearDown()

    def testSetResults(self):
        """Test setting results and saving result to a folder"""
        self._window.setCorScores(self._cor_scores, score_method="standard deviation")
        # test saving snapshots
        with tempfile.TemporaryDirectory() as output_png_imgs:
            final_dir = os.path.join(output_png_imgs, "test/create/it")
            self._window.saveReconstructedSlicesTo(final_dir)
            assert os.path.exists(final_dir)
            assert len(os.listdir(final_dir)) == len(self._cor_scores)
