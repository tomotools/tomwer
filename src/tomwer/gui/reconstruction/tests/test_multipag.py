# coding: utf-8
from __future__ import annotations


import os
import shutil
import tempfile

import h5py
import numpy
import pytest
from silx.gui import qt
from silx.gui.utils.testutils import TestCaseQt
from silx.io.url import DataUrl

from tomwer.core.reconstruction.multi.multipag.params import MultiPagParams
from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.gui.reconstruction.multipag.MultiPagWindowBase import MultiPagWindowBase
from tomwer.tests.utils import skip_gui_test


@pytest.mark.skipif(skip_gui_test(), reason="skip gui test")
class TestMultiPagWindowBase(TestCaseQt):
    """Test the MultiPagWindowBase interface"""

    _N_OUTPUT_URLS = 10

    def setUp(self):
        super().setUp()
        self._window = MultiPagWindowBase()
        self.folder = tempfile.mkdtemp()
        self._output_urls = []
        self._cor_scores = {}

        for i in range(self._N_OUTPUT_URLS):
            output_file = os.path.join(self.folder, f"recons_{i}.h5")
            with h5py.File(output_file, mode="a") as h5f:
                h5f["data"] = numpy.random.random(100 * 100).reshape(100, 100)
                url = DataUrl(file_path=output_file, data_path="data", scheme="silx")
                assert url.is_valid()
            self._output_urls.append(url)
            score = ComputedScore(
                tv=numpy.random.randint(10),
                std=numpy.random.randint(100),
            )
            self._cor_scores[i] = (url, score)

        # create a scan
        self.folder = tempfile.mkdtemp()
        dim = 10
        mock = MockNXtomo(
            scan_path=self.folder, n_proj=10, n_ini_proj=10, scan_range=180, dim=dim
        )
        mock.add_alignment_radio(index=10, angle=90)
        mock.add_alignment_radio(index=10, angle=0)
        self.scan = mock.scan

        self._window.setScan(self.scan)

    def tearDown(self):
        self._window.setAttribute(qt.Qt.WA_DeleteOnClose)
        self._window.close()
        self._window = None
        shutil.rmtree(self.folder)
        super().tearDown()

    def testGetConfiguration(self):
        """Insure configuration getter works"""
        conf = self._window.getConfiguration()
        # the configuration should be convertible to
        conf = MultiPagParams.from_dict(conf)
        self.assertEqual(conf.slice_indexes, "middle")

        self.assertEqual(conf._dry_run, False)
        self.assertEqual(conf.score_method, ScoreMethod.TV_INVERSE)
        self.assertEqual(conf.output_dir, None)
        self.assertEqual(conf.scores, None)

    def testSetResults(self):
        """Test setting results"""
        self._window.setDBScores(self._cor_scores, score_method="total variation")
        # test saving snapshots
        with tempfile.TemporaryDirectory() as output_png_imgs:
            final_dir = os.path.join(output_png_imgs, "test/create/it")
            self._window.saveReconstructedSlicesTo(final_dir)
            assert os.path.exists(final_dir)
            assert len(os.listdir(final_dir)) == len(self._cor_scores)
