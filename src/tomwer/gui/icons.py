# coding: utf-8
"""Set of icons for buttons.

Use :func:`getQIcon` to create Qt QIcon from the name identifying an icon.
"""


import logging
import os
import weakref

from silx.gui import qt
from silx.utils import weakref as silxweakref
from silx.utils.deprecation import deprecated

import tomwer.resources

_logger = logging.getLogger(__name__)
"""Module logger"""


_cached_icons = weakref.WeakValueDictionary()
"""Cache loaded icons in a weak structure"""


_supported_formats = None
"""Order of file format extension to check"""


class AbstractAnimatedIcon(qt.QObject):
    """Store an animated icon.

    It provides an event containing the new icon everytime it is updated."""

    def __init__(self, parent=None):
        """Constructor

        :param parent: Parent of the QObject
        :raises: ValueError when name is not known
        """
        qt.QObject.__init__(self, parent)

        self.__targets = silxweakref.WeakList()
        self.__currentIcon = None

    iconChanged = qt.Signal(qt.QIcon)
    """Signal sent with a QIcon every time the animation changed."""

    def register(self, obj):
        """Register an object to the AnimatedIcon.
        If no object are registered, the animation is paused.
        Object are stored in a weaked list.

        :param obj: An object
        """
        if obj not in self.__targets:
            self.__targets.append(obj)
        self._updateState()

    def unregister(self, obj):
        """Remove the object from the registration.
        If no object are registered the animation is paused.

        :param object obj: A registered object
        """
        if obj in self.__targets:
            self.__targets.remove(obj)
        self._updateState()

    def hasRegistredObjects(self) -> bool:
        """Returns true if any object is registered."""
        return len(self.__targets)

    def isRegistered(self, obj) -> bool:
        """Returns true if the object is registered in the AnimatedIcon.

        :param object obj: An object
        """
        return obj in self.__targets

    def currentIcon(self) -> qt.QIcon:
        """Returns the icon of the current frame."""
        return self.__currentIcon

    def _updateState(self):
        """Update the object according to the connected objects."""
        pass

    def _setCurrentIcon(self, icon):
        """Store the current icon and emit a `iconChanged` event.

        :param qt.QIcon icon: The current icon
        """
        self.__currentIcon = icon
        self.iconChanged.emit(self.__currentIcon)


class MovieAnimatedIcon(AbstractAnimatedIcon):
    """Store a looping QMovie to provide icons for each frames.
    Provides an event with the new icon every time the movie frame
    is updated."""

    def __init__(self, filename, parent=None):
        """Constructor

        :param filename: An icon name to an animated format
        :param parent: Parent of the QObject
        :raises: ValueError when name is not known
        """
        AbstractAnimatedIcon.__init__(self, parent)

        qfile = getQFile(filename)
        self.__movie = qt.QMovie(qfile.fileName(), qt.QByteArray(), parent)
        self.__movie.setCacheMode(qt.QMovie.CacheAll)
        self.__movie.frameChanged.connect(self.__frameChanged)
        self.__cacheIcons = {}

        self.__movie.jumpToFrame(0)
        self.__updateIconAtFrame(0)

    def __frameChanged(self, frameId):
        """Callback every time the QMovie frame change
        :param frameId: Current frame id
        """
        self.__updateIconAtFrame(frameId)

    def __updateIconAtFrame(self, frameId):
        """
        Update the current stored QIcon

        :param frameId: Current frame id
        """
        if frameId in self.__cacheIcons:
            icon = self.__cacheIcons[frameId]
        else:
            icon = qt.QIcon(self.__movie.currentPixmap())
            self.__cacheIcons[frameId] = icon
        self._setCurrentIcon(icon)

    def _updateState(self):
        """Update the movie play according to internal stat of the
        AnimatedIcon."""
        self.__movie.setPaused(not self.hasRegistredObjects())


class MultiImageAnimatedIcon(AbstractAnimatedIcon):
    """Store a looping QMovie to provide icons for each frames.
    Provides an event with the new icon every time the movie frame
    is updated."""

    def __init__(self, filename, parent=None):
        """Constructor

        :param filename: An icon name to an animated format
        :param parent: Parent of the QObject
        :raises: ValueError when name is not known
        """
        AbstractAnimatedIcon.__init__(self, parent)

        self.__frames = []
        for i in range(100):
            try:
                pixmap = getQPixmap("%s/%02d" % (filename, i))
            except ValueError:
                break
            icon = qt.QIcon(pixmap)
            self.__frames.append(icon)

        if len(self.__frames) == 0:
            raise ValueError("Animated icon '%s' do not exists" % filename)

        self.__frameId = -1
        self.__timer = qt.QTimer(self)
        self.__timer.timeout.connect(self.__increaseFrame)
        self.__updateIconAtFrame(0)

    def __increaseFrame(self):
        """Callback called every timer timeout to change the current frame of
        the animation
        """
        frameId = (self.__frameId + 1) % len(self.__frames)
        self.__updateIconAtFrame(frameId)

    def __updateIconAtFrame(self, frameId):
        """
        Update the current stored QIcon

        :param frameId: Current frame id
        """
        self.__frameId = frameId
        icon = self.__frames[frameId]
        self._setCurrentIcon(icon)

    def _updateState(self):
        """Update the object to wake up or sleep it according to its use."""
        if self.hasRegistredObjects():
            if not self.__timer.isActive():
                self.__timer.start(100)
        else:
            if self.__timer.isActive():
                self.__timer.stop()


class AnimatedIcon(MovieAnimatedIcon):
    """Store a looping QMovie to provide icons for each frames.
    Provides an event with the new icon every time the movie frame
    is updated.

    It may not be available anymore for the silx release 0.6.

    .. deprecated:: 0.5
       Use :class:`MovieAnimatedIcon` instead.
    """

    @deprecated
    def __init__(self, filename, parent=None):
        MovieAnimatedIcon.__init__(self, filename, parent=parent)


def getWaitIcon():
    """Returns a cached version of the waiting AbstractAnimatedIcon."""
    return getAnimatedIcon("process-working")


def getAnimatedIcon(name):
    """Create an AbstractAnimatedIcon from a resource name.

    The resource name can be prefixed by the name of a resource directory. For
    example "silx:foo.png" identify the resource "foo.png" from the resource
    directory "silx".

    If no prefix are specified, the file with be returned from the silx
    resource directory with a specific path "gui/icons".

    See also :func:`silx.resources.register_resource_directory`.

    Try to load a mng or a gif file, then try to load a multi-image animated
    icon.

    In Qt5 mng or gif are not used, because the transparency is not very well
    managed.

    :param name: Name of the icon, in one of the defined icons
                     in this module.
    :return: Corresponding AbstractAnimatedIcon
    :raises: ValueError when name is not known
    """
    key = name + "__anim"
    if key not in _cached_icons:
        qtMajorVersion = int(qt.qVersion().split(".")[0])
        icon = None

        # ignore mng and gif in Qt5
        if qtMajorVersion != 5:
            try:
                icon = MovieAnimatedIcon(name)
            except ValueError:
                icon = None

        if icon is None:
            try:
                icon = MultiImageAnimatedIcon(name)
            except ValueError:
                icon = None

        if icon is None:
            raise ValueError("Not an animated icon name: %s", name)

        _cached_icons[key] = icon
    else:
        icon = _cached_icons[key]
    return icon


def getQIcon(name):
    """Create a QIcon from its name.

    The resource name can be prefixed by the name of a resource directory. For
    example "silx:foo.png" identify the resource "foo.png" from the resource
    directory "silx".

    If no prefix are specified, the file with be returned from the silx
    resource directory with a specific path "gui/icons".

    See also :func:`silx.resources.register_resource_directory`.

    :param name: Name of the icon, in one of the defined icons
                     in this module.
    :return: Corresponding QIcon
    :raises: ValueError when name is not known
    """
    if name not in _cached_icons:
        qfile = getQFile(name)
        pixmap = qt.QPixmap(qfile.fileName())
        icon = qt.QIcon(pixmap)
        _cached_icons[name] = icon
    else:
        icon = _cached_icons[name]
    return icon


def getQPixmap(name):
    """Create a QPixmap from its name.

    The resource name can be prefixed by the name of a resource directory. For
    example "silx:foo.png" identify the resource "foo.png" from the resource
    directory "silx".

    If no prefix are specified, the file with be returned from the silx
    resource directory with a specific path "gui/icons".

    See also :func:`silx.resources.register_resource_directory`.

    :param name: Name of the icon, in one of the defined icons
                     in this module.
    :return: Corresponding QPixmap
    :raises: ValueError when name is not known
    """
    qfile = getQFile(name)
    return qt.QPixmap(qfile.fileName())


def getQFile(name) -> qt.QFile:
    """Create a QFile from an icon name. Filename is found
    according to supported Qt formats.

    The resource name can be prefixed by the name of a resource directory. For
    example "silx:foo.png" identify the resource "foo.png" from the resource
    directory "silx".

    If no prefix are specified, the file with be returned from the silx
    resource directory with a specific path "gui/icons".

    See also :func:`silx.resources.register_resource_directory`.

    :param name: Name of the icon, in one of the defined icons
                     in this module.
    :return: Corresponding QFile
    :raises: ValueError when name is not known
    """
    global _supported_formats
    if _supported_formats is None:
        _supported_formats = []
        supported_formats = qt.supportedImageFormats()
        order = ["svg", "png", "jpg"]
        for format_ in order:
            if format_ in supported_formats:
                _supported_formats.append(format_)
        if len(_supported_formats) == 0:
            _logger.error("No format supported for icons")
        else:
            _logger.debug("Format %s supported", ", ".join(_supported_formats))

    for format_ in _supported_formats:
        format_ = str(format_)
        filename = tomwer.resources._resource_filename(
            "%s.%s" % (name, format_), default_directory=os.path.join("gui", "icons")
        )
        qfile = qt.QFile(filename)
        if qfile.exists():
            return qfile
        elif os.path.exists(filename):
            mess = filename + " exists, but is not supported by qt"
            _logger.warning(mess)

    raise ValueError("Not an icon name: %s" % name)
