from __future__ import annotations
from silx.utils.enum import Enum as _Enum


class ProcessDataOutputDirMode(_Enum):
    IN_SCAN_FOLDER = "same folder as scan"
    PROCESSED_DATA_FOLDER = "PROCESSED_DATA folder"
    OTHER = "other"
