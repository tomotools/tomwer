"""Base class for multi reconstruction parameters"""

from __future__ import annotations

from tomwer.core.cluster.cluster import SlurmClusterConfiguration
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.settings import ParametersKeys


class MultiBaseParams:
    """Base class for multi reconstruction parameters"""

    _VALID_FILE_FORMAT = ("hdf5", "h5", "hdf", "npy", "npz", "tiff", "jp2", "jp2k")

    def __init__(self):
        self._n_reconstruction = 20
        self._slice_indexes = "middle"
        self._dry_run = False
        self._output_dir = None
        self._score_method = ScoreMethod.TV_INVERSE
        self._scores = None
        self._autofocus = None
        "scores. expected cor value as key and a tuple (score, url) as value"
        self._file_format = "hdf5"
        self._image_width = None
        self._cluster_config = None

    @property
    def autofocus(self):
        return self._autofocus

    @autofocus.setter
    def autofocus(self, autofocus):
        self._autofocus = autofocus

    @property
    def n_reconstruction(self):
        return self._n_reconstruction

    @n_reconstruction.setter
    def n_reconstruction(self, n):
        self._n_reconstruction = n

    @property
    def slice_indexes(self) -> dict | str | None:
        return self._slice_indexes

    @slice_indexes.setter
    def slice_indexes(self, indexes: dict | str | None):
        if isinstance(indexes, str):
            if not indexes == "middle":
                raise ValueError("the only valid indexes values is 'middle'")
        elif not isinstance(indexes, (type(None), dict)):
            raise TypeError(
                f"index should be an instance of int or None and not {type(indexes)}"
            )
        self._slice_indexes = indexes

    @property
    def dry_run(self) -> bool:
        return self._dry_run

    @dry_run.setter
    def dry_run(self, dry_run: bool):
        if not isinstance(dry_run, bool):
            raise ValueError("dry_run should be a bool")
        self._dry_run = dry_run

    @property
    def output_dir(self) -> str | None:
        """nabu cfg_files output dir. If not provided will use nabu slice output with saaxis/cfg_files as postfix"""
        return self._output_dir

    @output_dir.setter
    def output_dir(self, output_dir: str | None) -> None:
        if not isinstance(output_dir, (str, type(None))):
            raise TypeError("output_dir should be None or a str")
        self._output_dir = output_dir

    @property
    def score_method(self):
        return self._score_method

    @score_method.setter
    def score_method(self, method):
        self._score_method = ScoreMethod(method)

    @property
    def scores(self) -> dict | None:
        return self._scores

    @scores.setter
    def scores(self, scores: dict | None):
        if not isinstance(scores, (type(None), dict)):
            raise TypeError("scores should be None or a dictionary")
        self._scores = scores

    @property
    def file_format(self) -> str:
        return self._file_format

    @file_format.setter
    def file_format(self, format_: str):
        if not isinstance(format_, str):
            raise TypeError("format should be a str")
        if format_ not in self._VALID_FILE_FORMAT:
            raise ValueError(
                f"requested format ({format_}) is invalid. valid ones are {self._VALID_FILE_FORMAT}"
            )
        self._file_format = format_

    @property
    def cluster_config(self) -> dict | None:
        return self._cluster_config

    @cluster_config.setter
    def cluster_config(self, config: dict | None):
        if isinstance(config, SlurmClusterConfiguration):
            config = config.to_dict()
        if not isinstance(config, (dict, type(None))):
            raise TypeError(
                "config is expected to be None, a dict or SlurmClusterConfiguration"
            )
        self._cluster_config = config

    def to_dict(self) -> dict:
        return {
            "slice_index": self.slice_indexes or "",
            "dry_run": self.dry_run,
            "output_dir": self.output_dir or "",
            "score_method": self.score_method.value,
            ParametersKeys.SLURM_CLUSTER_KEY: (
                self.cluster_config if self.cluster_config is not None else ""
            ),
        }

    def load_from_dict(self, dict_: dict):
        if "slice_index" in dict_:
            slice_index = dict_["slice_index"]
            if slice_index == "":
                slice_index = None
            self.slice_indexes = slice_index
        if "dry_run" in dict_:
            self.dry_run = bool(dict_["dry_run"])
        if "output_dir" in dict_:
            output_dir = dict_["output_dir"]
            if output_dir == "":
                output_dir = None
            self.output_dir = output_dir
        if "score_method" in dict_:
            self.score_method = ScoreMethod(dict_["score_method"])
        if ParametersKeys.SLURM_CLUSTER_KEY in dict_:
            if dict_[ParametersKeys.SLURM_CLUSTER_KEY] in (None, ""):
                self.cluster_config = None
            else:
                self.cluster_config = dict_[ParametersKeys.SLURM_CLUSTER_KEY]

    @staticmethod
    def from_dict(dict_):
        res = MultiBaseParams()
        res.load_from_dict(dict_=dict_)
        return res
