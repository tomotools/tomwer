"""contains core functions for multiPag calculation"""

from __future__ import annotations

import logging

import numpy
from multiprocessing import Pool

from tomoscan.esrf.scan.utils import get_data

from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.reconstruction.multi.scores.compute import (
    apply_roi,
    compute_score,
    get_disk_mask_radius,
)
from tomwer.core.volume.volumefactory import VolumeFactory


_logger = logging.getLogger(__name__)


DEFAULT_RECONS_FOLDER = "multi_pag_results"


class PostProcessing:
    """class used to run SA-delta-beta post-processing on reconstructed slices"""

    DEFAULT_POOL_SIZE = 10

    def __init__(self, db_reconstructions, scan, pool_size) -> None:
        self._db_reconstructions = db_reconstructions
        self._scan = scan
        self._cancelled = False
        self.pool_size = pool_size

    @staticmethod
    def compute_score(item: tuple):
        db, (url, data), mask_disk_radius, cancelled = item
        if data is None:
            score = None
            data_roi = None
        else:
            assert data.ndim == 2
            data_roi = apply_roi(data=data, radius=mask_disk_radius, url=url)

            # move data_roi to [0-1] range
            #  preprocessing: get percentile 0 and 99 from image and
            #  "clean" highest and lowest pixels from it
            min_p, max_p = numpy.percentile(data_roi, (1, 99))
            data_roi_int = data_roi[...]
            data_roi_int[data_roi_int < min_p] = min_p
            data_roi_int[data_roi_int > max_p] = max_p
            data_roi_int = (data_roi_int - min_p) / (max_p - min_p)

            score = ComputedScore(
                tv=compute_score(data=data_roi_int, method=ScoreMethod.TV),
                std=compute_score(data=data_roi_int, method=ScoreMethod.STD),
            )
        return {db: (url, score)}, {db: data_roi}

    def run(self):
        datasets = self.load_datasets()
        assert isinstance(datasets, dict)
        mask_disk_radius = get_disk_mask_radius(datasets)
        with Pool(self.pool_size) as pool:
            res = pool.map(
                self.compute_score,
                [
                    (
                        *item,
                        mask_disk_radius,
                        self._cancelled,
                    )
                    for item in datasets.items()
                ],
            )
        scores = {}
        rois = {}
        for mydict in res:
            myscores, myrois = mydict
            scores.update(myscores)
            rois.update(myrois)
        return scores, rois

    @staticmethod
    def _load_dataset(item: tuple):
        db, volume_identifier = item
        slice_url = None
        # in case the try processing fails
        try:
            volume = VolumeFactory.create_tomo_object_from_identifier(volume_identifier)
            volumes_urls = tuple(volume.browse_data_urls())
            if len(volumes_urls) > 1:
                _logger.warning(
                    f"found a volume with mode that one url ({volumes_urls})"
                )
            slice_url = volumes_urls[0]
            data = get_data(slice_url)
        except Exception as e:
            _logger.error(
                f"Fail to compute a score for {volume_identifier}. Reason is {e}"
            )
            return {db: (slice_url, None)}
        else:
            if data.ndim == 3:
                if data.shape[0] == 1:
                    data = data.reshape(data.shape[1], data.shape[2])
                elif data.shape[2] == 1:
                    data = data.reshape(data.shape[0], data.shape[1])
                else:
                    raise ValueError(f"Data is expected to be 2D. Not {data.ndim}D")
            elif data.ndim == 2:
                pass
            else:
                raise ValueError(f"Data is expected to be 2D. Not {data.ndim}D")

            return {db: (slice_url, data)}

    def load_datasets(self):
        with Pool(self.pool_size) as pool:
            res = pool.map(
                self._load_dataset,
                self._db_reconstructions.items(),
            )
        datasets_ = {}
        for mydict in res:
            datasets_.update(mydict)

        return datasets_

    def cancel(self):
        self._cancelled = True
