from __future__ import annotations

from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod


class ComputedScore:
    """
    Score holder.

    Will all result of the different score calculation method and getter on those and their inverse
    """

    def __init__(self, tv, std, tomo_consistency=None):
        self._tv = tv
        self._std = std
        self._tomo_consistency = tomo_consistency

    @property
    def total_variation(self):
        return self._tv

    @property
    def std(self):
        return self._std

    @property
    def tomo_consistency(self):
        return self._tomo_consistency

    def get(self, method: ScoreMethod):
        method = ScoreMethod(method)
        if method is ScoreMethod.TV:
            return self.total_variation / float(10e5)
        elif method is ScoreMethod.TV_INVERSE:
            return (1.0 / self.total_variation) * float(10e5)
        elif method is ScoreMethod.STD:
            return self.std
        elif method is ScoreMethod.STD_INVERSE:
            return 1.0 / self.std
        elif method is ScoreMethod.TOMO_CONSISTENCY:
            return self.tomo_consistency
        else:
            raise ValueError(f"{method} is an unrecognized method")

    def __eq__(self, __value: object) -> bool:
        if not isinstance(__value, ComputedScore):
            return False
        else:
            return (
                self.total_variation == __value.total_variation
                and self.std == __value.std
                and self.tomo_consistency == __value.tomo_consistency
            )

    def __str__(self) -> str:
        return f"std: {self.std} - tv: {self.total_variation} - tomo-consistency: {self.tomo_consistency}"
