from enum import Enum


class ScoreMethod(Enum):
    STD = "standard deviation"
    TV = "total variation"
    TV_INVERSE = "1 / (total variation)"
    STD_INVERSE = "1 / std"
    TOMO_CONSISTENCY = "tomo consistency"
