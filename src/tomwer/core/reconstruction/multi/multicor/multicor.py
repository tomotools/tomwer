"""contains core multicor functions"""

from __future__ import annotations

import logging

import numpy
from multiprocessing import Pool
from silx.io.url import DataUrl

from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore
from tomwer.core.reconstruction.multi.scores.compute import (
    apply_roi,
    compute_score,
    get_disk_mask_radius,
)
from tomwer.core.reconstruction.multi.scores.ScoreMethod import ScoreMethod
from tomwer.core.volume.volumefactory import VolumeFactory
from tomwer.io.utils.utils import get_slice_data

_logger = logging.getLogger(__name__)


DEFAULT_RECONS_FOLDER = "multi_cor_results"


class PostProcessing:
    """class used to run SA-axis post-processing on reconstructed slices"""

    def __init__(self, cor_reconstructions, slice_index, scan, pool_size) -> None:
        self._cor_reconstructions = cor_reconstructions
        self._slice_index = slice_index
        self._scan = scan
        self._cancelled = False
        self.pool_size = pool_size

    @staticmethod
    def compute_score(item: tuple):
        cor, (url, data), mask_disk_radius, cancelled = item
        if cancelled:
            return (None, None), None

        if data is None:
            score = None
            data_roi = None
        else:
            if not isinstance(data, numpy.ndarray):
                raise TypeError(
                    f"data should be a numpy array. Get {type(data)} instead"
                )
            assert data.ndim == 2, f"data should be 2D. Get {data.ndim} instead"
            data_roi = apply_roi(data=data, radius=mask_disk_radius, url=url)

            # move data_roi to [0-1] range
            #  preprocessing: get percentile 0 and 99 from image and
            #  "clean" highest and lowest pixels from it
            min_p, max_p = numpy.percentile(data_roi, (1, 99))
            data_roi_int = data_roi[...]
            data_roi_int[data_roi_int < min_p] = min_p
            data_roi_int[data_roi_int > max_p] = max_p
            data_roi_int = (data_roi_int - min_p) / (max_p - min_p)

            score = ComputedScore(
                tv=compute_score(data=data_roi_int, method=ScoreMethod.TV),
                std=compute_score(data=data_roi_int, method=ScoreMethod.STD),
                tomo_consistency=None,
            )
        return {cor: (url, score)}, {cor: data_roi}

    def run(self):
        datasets = self.load_datasets()
        assert isinstance(datasets, dict)
        mask_disk_radius = get_disk_mask_radius(datasets)
        with Pool(self.pool_size) as pool:
            res = pool.map(
                self.compute_score,
                [
                    (
                        *item,
                        mask_disk_radius,
                        self._cancelled,
                    )
                    for item in datasets.items()
                ],
            )
        scores = {}
        rois = {}
        for mydict in res:
            myscores, myrois = mydict
            scores.update(myscores)
            rois.update(myrois)
        return scores, rois

    @staticmethod
    def _load_dataset(item: tuple):
        cor, volume_identifier = item
        if volume_identifier is None:
            return {cor: (None, None)}

        volume = VolumeFactory.create_tomo_object_from_identifier(volume_identifier)
        urls = tuple(volume.browse_data_urls())
        if len(urls) == 0:
            _logger.error(
                f"volume {volume.get_identifier().to_str()} has no url / slices. Unable to load any data."
            )
            return {cor: (None, None)}
        if len(urls) != 1:
            _logger.error(
                f"volume is expected to have at most one url (single slice volume). get {len(urls)} - most likely nabu reconstruction failed. Do you have GPU ? Are the requested COR values valid ? - Especially for Half-acquisition"
            )
        url = urls[0]
        if not isinstance(url, (DataUrl, str)):
            raise TypeError(f"url is expected to be a str or DataUrl not {type(url)}")

        try:
            data = get_slice_data(url=url)
        except Exception as e:
            _logger.error(f"Fail to compute a score for {url.path()}. Reason is {e}")
            return {cor: (url, None)}
        else:
            if data.ndim == 3:
                if data.shape[0] == 1:
                    data = data.reshape(data.shape[1], data.shape[2])
                elif data.shape[2] == 1:
                    data = data.reshape(data.shape[0], data.shape[1])
                else:
                    raise ValueError(f"Data is expected to be 2D. Not {data.ndim}D")
            elif data.ndim == 2:
                pass
            else:
                raise ValueError("Data is expected to be 2D. Not {data.ndim}D")
            return {cor: (url, data)}

    def load_datasets(self):
        with Pool(self.pool_size) as pool:
            res = pool.map(
                self._load_dataset,
                self._cor_reconstructions.items(),
            )
        datasets_ = {}
        for mydict in res:
            datasets_.update(mydict)
        return datasets_

    def cancel(self):
        self._cancelled = True
