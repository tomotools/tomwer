from __future__ import annotations

from silx.utils.enum import Enum as _Enum


class NabuPaddingType(_Enum):
    ZEROS = "zeros"
    EDGES = "edges"
