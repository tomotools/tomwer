from __future__ import annotations

import copy
import functools
import logging
import os
from typing import Iterable

from nabu.pipeline.config import generate_nabu_configfile
from nabu.pipeline.fullfield.nabu_config import (
    nabu_config as nabu_fullfield_default_config,
)
from silx.io.utils import open as open_hdf5

from tomwer.core.reconstruction.nabu.plane import NabuPlane
from tomwer.core.scan.edfscan import EDFTomoScan
from tomwer.core.scan.nxtomoscan import NXtomoScan
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.utils import docstring

from tomwer.core.reconstruction.output import (
    PROCESS_FOLDER_RECONSTRUCTED_VOLUMES,
)
from tomwer.core.reconstruction.nabu.NabuBaseReconstructor import (
    NabuBaseReconstructor,
)

from . import settings, utils
from .target import Target

_logger = logging.getLogger(__name__)


class VolumeRunner(NabuBaseReconstructor):
    """
    Class used to reconstruct a full volume with Nabu.
    Locally or on a cluster.
    """

    EXPECTS_SINGLE_SLICE = False

    def __init__(
        self,
        scan: TomwerScanBase,
        config_nabu,
        cluster_config: dict | None,
        dry_run: bool,
        target: Target,
        process_name: str,
    ) -> None:
        super().__init__(
            scan=scan,
            dry_run=dry_run,
            target=target,
            cluster_config=cluster_config,
            process_name=process_name,
        )
        self._config = config_nabu

    @property
    def configuration(self):
        return self._config

    @property
    def processed_data_folder_name(self):
        """return the specific processed folder name associated to this type of reconstruction."""
        return PROCESS_FOLDER_RECONSTRUCTED_VOLUMES

    @docstring(NabuBaseReconstructor)
    def run(self) -> Iterable:
        dataset_params = self.scan.get_nabu_dataset_info()
        if "dataset" in self.configuration:
            dataset_params.update(self.configuration["dataset"])
        self.configuration["dataset"] = dataset_params

        self.configuration["resources"] = self.configuration.get("resources", {})
        self.configuration["resources"]["method"] = "local"

        if "output" not in self.configuration:
            self.configuration["output"] = {}
        config_slices, cfg_folder = self._treateOutputConfig(self.configuration)

        # force overwriting results
        config_slices["output"].update({"overwrite_results": 1})

        # check and clamp `start_z` and `end_z`
        if "reconstruction" in self.configuration:
            for key in ("start_z", "end_z"):
                value = config_slices["reconstruction"].get(key)
                if value is None:
                    continue

                value = int(value)
                if self.scan.dim_2 is not None and value >= self.scan.dim_2:
                    _logger.warning(
                        f"{key} > max_size (radio height: {self.scan.dim_2}). Set it to -1 (maximum)"
                    )
                    value = -1
                config_slices["reconstruction"][key] = value

        name = (
            config_slices["output"]["file_prefix"] + settings.NABU_CONFIG_FILE_EXTENSION
        )
        if not isinstance(self.scan, EDFTomoScan):
            name = "_".join((self.scan.entry.lstrip("/"), name))
        conf_file = os.path.join(cfg_folder, name)
        _logger.info(f"{self.scan}: create {conf_file}")

        # make sure output location exists
        os.makedirs(config_slices["output"]["location"], exist_ok=True)

        # add some tomwer metadata and save the configuration
        # note: for now the section is ignored by nabu but shouldn't stay that way
        with utils.TomwerInfo(config_slices) as config_to_dump:
            generate_nabu_configfile(
                conf_file,
                nabu_fullfield_default_config,
                config=config_to_dump,
                options_level="advanced",
            )
            return tuple(
                [
                    self._process_config(
                        config_to_dump=config_to_dump,
                        config_file=conf_file,
                        info="nabu volume reconstruction",
                        file_format=config_slices["output"]["file_format"],
                        process_name=self.process_name,
                    )
                ]
            )

    @docstring(NabuBaseReconstructor)
    def _get_futures_slurm_callback(self, config_to_dump) -> tuple:
        # add callback to set slices reconstructed urls
        class CallBack:
            # we cannot create a future directly because distributed enforce
            # the callback to have a function signature with only the future
            # as single parameter.
            def __init__(self, f_partial, scan) -> None:
                self.f_partial = f_partial
                self.scan = scan

            def process(self, fn):
                if fn.done() and not (fn.cancelled() or fn.exception()):
                    # update reconstruction urls only if processing succeed.
                    recons_identifiers = self.f_partial()
                    self.scan.add_latest_vol_reconstructions(recons_identifiers)

        file_format = config_to_dump["output"]["file_format"]
        callback = functools.partial(
            utils.get_recons_volume_identifier,
            file_prefix=config_to_dump["output"]["file_prefix"],
            location=config_to_dump["output"]["location"],
            file_format=file_format,
            scan=self.scan,
            slice_index=None,
            axis=NabuPlane.XY,  # for volume we always reconstruct along XY plane
        )

        return (CallBack(callback, self.scan),)

    def _treateOutputConfig(self, config) -> tuple:
        """

        :return: (nabu config dict, nabu extra options)
        """
        config = copy.deepcopy(config)
        config, nabu_cfg_folder = super()._treateOutputSliceConfig(config)
        os.makedirs(config["output"]["location"], exist_ok=True)

        # adapt config_s to specific volume treatment
        if "postproc" in config:
            config["postproc"] = config["postproc"]

        # make sure start_[x] and end_[x] come from config
        for key in ("start_x", "end_x", "start_y", "end_y", "start_z", "end_z"):
            if key in config:
                config["reconstruction"][key] = config[key]
                del config[key]

        return config, nabu_cfg_folder

    @docstring(NabuBaseReconstructor)
    def _get_file_basename_reconstruction(self, pag, db, ctf, axis):
        """

        :param pag: is it a Paganin reconstruction
        :param db: delta / beta parameter
        :param axis: axis over which the reconstruction goes. For volume always expected to be z. So ignored in the function
        :return: basename of the file reconstructed (without any extension)
        """
        assert type(db) in (int, type(None))
        assert not pag == ctf == True, "cannot ask for both pag and ctf active"
        if isinstance(self.scan, NXtomoScan):
            basename, _ = os.path.splitext(self.scan.master_file)
            basename = os.path.basename(basename)
            try:
                # if there is more than one entry in the file append the entry name to the file basename
                with open_hdf5(self.scan.master_file) as h5f:
                    if len(h5f.keys()) > 1:
                        basename = "_".join((basename, self.scan.entry.strip("/")))
            except Exception:
                pass
        else:
            basename = os.path.basename(self.scan.path)

        if pag:
            return "_".join((basename + "pag", "db" + str(db).zfill(4), "vol"))
        elif ctf:
            return "_".join((basename + "ctf", "db" + str(db).zfill(4), "vol"))
        else:
            return "_".join((basename, "vol"))
