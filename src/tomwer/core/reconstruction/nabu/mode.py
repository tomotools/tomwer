from __future__ import annotations

from silx.utils.enum import Enum as _Enum


class NabuMode(_Enum):
    # helical mode will be added in the future
    FULL_FIELD = "standard acquisition"
    HALF_ACQ = "half acquisition"
    AUTO = "auto"

    def to_nabu_enable_halftomo_value(self):
        if self is NabuMode.FULL_FIELD:
            return 0
        elif self is NabuMode.HALF_ACQ:
            return 1
        elif self is NabuMode.AUTO:
            return "auto"
        else:
            raise ValueError(f"{self} not handled")

    @classmethod
    def from_nabu_enable_halftomo_value(cls, value):
        if value == 0:
            value = NabuMode.FULL_FIELD
        elif value == 1:
            value = NabuMode.HALF_ACQ
        elif value == "auto":
            value = NabuMode.AUTO

        return NabuMode(value)
