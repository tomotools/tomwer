"""Test the"""

from __future__ import annotations

import os
import shutil
from glob import glob

import pytest

from tomwer.core.reconstruction.nabu import settings as nabu_settings
from tomwer.core.reconstruction.nabu.slice.SliceInterpreter import SliceInterpreter
from tomwer.core.reconstruction.nabu.slice.slices import run_slices_reconstruction
from tomwer.core.scan.edfscan import EDFTomoScan
from tomwer.tests.datasets import TomwerCIDatasets


def get_nabu_config_output(scan):
    return os.path.join(scan.path, nabu_settings.NABU_CFG_FILE_FOLDER)


def get_nabu_ini_files(nabu_cfg_folder):
    "count number of .cfg files in a given folder"
    return glob(nabu_cfg_folder + os.sep + "*.cfg")


@pytest.fixture
def edf_scan(tmp_path):
    """Fixture that copy an existing dataset to a new temporary folder"""
    dataset = "scan_3_"
    data_test_dir = TomwerCIDatasets.get_dataset("edf_datasets/scan_3_")
    scan_folder = tmp_path / dataset
    shutil.copytree(data_test_dir, scan_folder)
    scan = EDFTomoScan(scan_folder)

    nabu_cfg_folder = get_nabu_config_output(scan)
    [os.remove(cfg_file) for cfg_file in glob(scan.path + os.sep + "*.cfg")]
    if os.path.exists(nabu_cfg_folder):
        shutil.rmtree(nabu_cfg_folder)

    assert len(get_nabu_ini_files(nabu_cfg_folder=nabu_cfg_folder)) == 0
    return scan


@pytest.fixture
def config(edf_scan):
    return {
        "phase": {
            "padding_type": "edge",
            "method": "",
            "delta_beta": "100.0",
            "margin": 50,
            "unsharp_sigma": 0,
            "unsharp_coeff": 0,
        },
        "preproc": {
            "ccd_filter_threshold": 0.04,
            "take_logarithm": True,
            "log_min_clip": 1e-06,
            "ccd_filter_enabled": 0,
            "log_max_clip": 10.0,
            "flatfield": 1,
        },
        "dataset": {
            "job_name": "hair_A1_50nm_tomo3_1_",
            "location": "/tmp_14_days/payno/hair_A1_50nm_tomo3_1_",
            "flat_file_prefix": "refHST",
            "projections_subsampling": 1,
            "binning": 1,
            "dark_file_prefix": "dark.edf",
            "binning_z": 1,
        },
        "reconstruction": {
            "positivity_constraint": 1,
            "start_z": 0,
            "end_y": -1,
            "optim_algorithm": "chambolle-pock",
            "enable_halftomo": 0,
            "end_z": -1,
            "iterations": 200,
            "start_x": 0,
            "fbp_filter_type": "ramlak",
            "angle_offset": 0.0,
            "end_x": -1,
            "method": "FBP",
            "angles_file": "",
            "rotation_axis_position": "",
            "start_y": 0,
            "preconditioning_filter": 1,
            "padding_type": "zeros",
            "weight_tv": 0.01,
            "translation_movements_file": "",
            "axis_correction_file": "",
        },
        "output": {"file_format": "hdf5", "location": edf_scan.path},
    }


def testNoSlices(tmp_path, edf_scan, config):
    config["phase"]["method"] = ""
    slices = SliceInterpreter().interpret(config=config, scan=edf_scan, slices=None)
    assert len(slices) == 0

    run_slices_reconstruction(scan=edf_scan, config=config, dry_run=True, slices={})

    assert len(get_nabu_ini_files(get_nabu_config_output(edf_scan))) == 0


@pytest.mark.parametrize("with_pag", (True, False))
def test_single_slice(tmp_path, edf_scan, config, with_pag: bool):
    """Test the behavior if we only request one slice without Paganin"""
    config["phase"]["method"] = "Paganin" if with_pag else ""
    slices_as_str = "1"
    slices = SliceInterpreter().interpret(
        config=config,
        scan=edf_scan,
        slices=slices_as_str,
    )
    assert len(slices) == 1

    run_slices_reconstruction(
        scan=edf_scan, config=config, slices={"XY": slices_as_str}, dry_run=True
    )
    nabu_files = get_nabu_ini_files(get_nabu_config_output(edf_scan))

    assert len(nabu_files) == 1
    if with_pag:
        file_name = (
            f"{os.path.basename(edf_scan.path)}slice_pag_000001_db0100_plane_XY.cfg"
        )
    else:
        file_name = f"{os.path.basename(edf_scan.path)}slice_000001_plane_XY.cfg"
    expected_cfg_file_name = os.path.join(
        get_nabu_config_output(edf_scan),
        file_name,
    )
    assert expected_cfg_file_name in nabu_files


@pytest.mark.parametrize("with_pag", (True, False))
def test_several_slices(tmp_path, edf_scan, config, with_pag: bool):
    """Test the behavior if we only request one slice without Paganin"""
    config["phase"]["method"] = "Paganin" if with_pag else ""
    slices_as_tuple = (1, 4, 12)
    slices_as_str = str(slices_as_tuple)

    slices = SliceInterpreter().interpret(
        config=config, scan=edf_scan, slices=slices_as_str
    )
    assert len(slices) == len(slices_as_tuple)

    run_slices_reconstruction(
        scan=edf_scan, config=config, dry_run=True, slices={"YZ": slices_as_str}
    )

    nabu_files = get_nabu_ini_files(get_nabu_config_output(edf_scan))
    assert len(nabu_files) == len(slices_as_tuple)

    def get_expected_file_name(slice_idx):
        if with_pag:
            file_name = f"{os.path.basename(edf_scan.path)}slice_pag_{str(slice_idx).zfill(6)}_db0100_plane_YZ.cfg"
        else:
            file_name = f"{os.path.basename(edf_scan.path)}slice_{str(slice_idx).zfill(6)}_plane_YZ.cfg"
        return os.path.join(
            get_nabu_config_output(edf_scan),
            file_name,
        )

    expected_files = [
        get_expected_file_name(slice_index) for slice_index in slices_as_tuple
    ]

    for expected_file in expected_files:
        assert expected_file in nabu_files


def test_several_pags_several_slices(tmp_path, edf_scan, config):
    """behavior when several paganin slices are requested with several
    delta / beta values"""
    slices_as_tuple = (1, 4, 12)
    slices_as_str = str(slices_as_tuple)
    config["phase"]["method"] = "Paganin"
    pag_dbs = [200, 300, 600, 700]
    config["phase"]["delta_beta"] = str(pag_dbs)

    slices = SliceInterpreter().interpret(
        config=config, scan=edf_scan, slices=slices_as_str, rec_plane="XZ"
    )
    assert len(slices) == len(slices_as_tuple) * len(pag_dbs)

    run_slices_reconstruction(
        scan=edf_scan, config=config, dry_run=True, slices={"XZ": slices_as_str}
    )
    nabu_files = get_nabu_ini_files(get_nabu_config_output(edf_scan))
    assert len(nabu_files) == len(slices_as_tuple) * len(pag_dbs)

    def get_expected_file_name(slice_idx, db: int):
        file_name = f"{os.path.basename(edf_scan.path)}slice_pag_{str(slice_idx).zfill(6)}_db{str(db).zfill(4)}_plane_XZ.cfg"
        return os.path.join(
            get_nabu_config_output(edf_scan),
            file_name,
        )

    expected_files = []
    for slice_index in slices_as_tuple:
        for db in pag_dbs:
            expected_files.append(get_expected_file_name(slice_idx=slice_index, db=db))

    for expected_file in expected_files:
        assert expected_file in nabu_files


@pytest.mark.parametrize("slices_as_str", ("[2, 3, 7]", "(6, 9,56)", "5;6; 9"))
def test_several_slices_using_python_slices(tmp_path, edf_scan, config, slices_as_str):
    """test that slices if provided as a python slice is correctly handled"""
    slices = SliceInterpreter().interpret(
        config=config, scan=edf_scan, slices=slices_as_str
    )
    assert len(slices) == 3

    run_slices_reconstruction(
        scan=edf_scan, config=config, dry_run=True, slices={"YZ": slices_as_str}
    )

    nabu_files = get_nabu_ini_files(get_nabu_config_output(edf_scan))
    assert len(nabu_files) == 3


@pytest.mark.parametrize("multipag_as_str", ("[100, 200, ]", "(225, 230)", "5;6;"))
def test_multipag_using_python_slices(tmp_path, edf_scan, config, multipag_as_str):
    """test that slices if provided as a python slice is correctly handled"""
    slices_as_tuple = 1
    slices_as_str = str(slices_as_tuple)
    config["phase"]["method"] = "Paganin"
    config["phase"]["delta_beta"] = multipag_as_str

    slices = SliceInterpreter().interpret(
        config=config, scan=edf_scan, slices=slices_as_str, rec_plane="XZ"
    )
    assert len(slices) == 2

    run_slices_reconstruction(
        scan=edf_scan, config=config, dry_run=True, slices={"XZ": slices_as_str}
    )
    nabu_files = get_nabu_ini_files(get_nabu_config_output(edf_scan))
    assert len(nabu_files) == 2
