from __future__ import annotations

from silx.utils.enum import Enum as _Enum


class RingCorrectionMethod(_Enum):
    NONE = "None"
    MUNCH = "munch"
    VO = "vo"
    MEAN_SUBTRACTION = "mean-subtraction"
    MEAN_DIVISION = "mean-division"
