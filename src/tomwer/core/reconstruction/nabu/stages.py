from __future__ import annotations


from silx.utils.enum import Enum as _Enum


class NabuStages(_Enum):
    INI = "initialization"
    PRE = "pre-processing"
    PHASE = "phase"
    PROC = "processing"
    POST = "post-processing"
    VOLUME = "volume"

    @staticmethod
    def getStagesOrder():
        return (
            NabuStages.INI,
            NabuStages.PRE,
            NabuStages.PHASE,
            NabuStages.PROC,
            NabuStages.POST,
        )

    @staticmethod
    def getProcessEnum(stage):
        """Return the process Enum associated to the stage"""
        stage = NabuStages.from_value(stage)
        if stage is NabuStages.INI:
            raise NotImplementedError()
        elif stage is NabuStages.PRE:
            return NabuPreprocessing
        elif stage is NabuStages.PHASE:
            return NabuPhase
        elif stage is NabuStages.PROC:
            return NabuProcessing
        elif stage is NabuStages.POST:
            return NabuPostProcessing
        raise NotImplementedError()


class NabuPreprocessing(_Enum):
    """Define all the preprocessing action possible and the order they
    are applied on"""

    FLAT_FIELD_NORMALIZATION = "flat field normalization"
    CCD_FILTER = "hot spot correction"

    @staticmethod
    def getPreProcessOrder():
        return (
            NabuPreprocessing.FLAT_FIELD_NORMALIZATION,
            NabuPreprocessing.CCD_FILTER,
        )


class NabuPhase(_Enum):
    """Define all the phase action possible and the order they
    are applied on"""

    PHASE = "phase retrieval"
    UNSHARP_MASK = "unsharp mask"
    LOGARITHM = "logarithm"

    @staticmethod
    def getPreProcessOrder():
        return (NabuPhase.PHASE, NabuPhase.UNSHARP_MASK, NabuPhase.LOGARITHM)


class NabuProcessing(_Enum):
    """Define all the processing action possible"""

    RECONSTRUCTION = "reconstruction"

    @staticmethod
    def getProcessOrder():
        return (NabuProcessing.RECONSTRUCTION,)


class NabuPostProcessing(_Enum):
    """Define all the post processing action available"""

    SAVE_DATA = "save"

    @staticmethod
    def getProcessOrder():
        return (NabuPostProcessing.SAVE_DATA,)
