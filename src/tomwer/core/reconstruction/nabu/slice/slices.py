from __future__ import annotations

import logging

from nabu.pipeline.config import get_default_nabu_config
from nabu.pipeline.fullfield.nabu_config import (
    nabu_config as nabu_fullfield_default_config,
)
from processview.core.manager.manager import ProcessManager

from tomwer.core.futureobject import FutureTomwerObject
from tomwer.core.reconstruction.nabu.nabucommon import (
    ResultsLocalRun,
    ResultSlurmRun,
    ResultsRun,
)
from tomwer.core.reconstruction.nabu.plane import NabuPlane
from tomwer.core.reconstruction.nabu.slice.SingleSliceRunner import SingleSliceRunner
from tomwer.core.reconstruction.nabu.target import Target
from tomwer.core.reconstruction.utils.cor import copy_cor
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.utils.slurm import is_slurm_available

from .SliceInterpreter import SliceInterpreter

_logger = logging.getLogger(__name__)


def run_slices_reconstruction(
    scan: TomwerScanBase,
    config: dict,
    slices: dict,
    cluster_config: dict | None = None,
    dry_run: bool = False,
    advancement=None,
    process_id: int | None = None,
    instantiate_classes_only: bool = False,
) -> tuple:
    """
    call nabu for a reconstruction on scan with the given configuration

    :param scan: scan to reconstruct
    :param config: configuration to run the reconstruction.
                        Contains nabu reconstruction parameters and slurm cluster
                        configuration if requested (key: `slurm-cluster`).
    :param slices: slices to be reconstruct. For each plane to reconstruct we expect to have the slices ot reconstruct as a str
    :param dry_run: do we want to run dry
    :param stderr: file to redirect stderr
    :param stdout: file to redirect stdout
    :param advancement: optional Progress class to display advancement
    :param process_id: optional process id
    :param instantiate_class_only: if we don't want to run the SingleSliceRunner but only return them. Use case: we want to keep a hand on processing and it can be cancelled

    :return: (all_succeed, stdouts, stderrs, final_configs, future_scan)
        * all_succeed: bool, True if all the reconstruction succeed or if all job request succeed.
        * stdouts: list of stdout of job reconstruction or job requests
        * stderrs: list of stderr of job reconstruction or job requests
        * final_configs: list of configurations submits to nabu
        * future_scan: FutureTomwerScan | None future scan containing futures pointing to job submited to the cluster. None if local reconstruction

    Behavior: will clear the last slices reconstructed
    """
    _logger.info(f"start reconstruction of {scan}")
    if not isinstance(slices, dict):
        raise TypeError(
            f"'slices' is expected to be a dict with {NabuPlane} as keys and str representing the slices to be reconstructed as value. Got {type(slices)}"
        )

    if cluster_config == {}:
        cluster_config = None
    if not (isinstance(cluster_config, dict) or cluster_config is None):
        raise TypeError(
            f"'cluster_config' should be None or an instance of dict. Got {type(cluster_config)}"
        )

    is_cluster_job = cluster_config is not None
    if is_cluster_job and not is_slurm_available():
        raise ValueError(
            "job on cluster requested but no access to slurm cluster found"
        )

    # beam shape is not directly used by nabu (uses ctf_geometry directly)
    config.get("phase", {}).pop("beam_shape", None)

    # if scan contains some center of position copy it to nabu
    copy_cor(scan=scan, nabu_rec_params=config)

    _logger.info(f"set nabu reconstruction parameters to {scan}")

    # update nabu recons_params used
    sc_config = get_default_nabu_config(nabu_fullfield_default_config)
    sc_config.update(config)
    scan.nabu_recons_params = sc_config

    # filter empty slices
    slices = dict(
        filter(
            lambda item: item[1],  # filter if the slice value is None or empty
            slices.items(),
        )
    )

    # create all 'scalar' nabu configuration to be run
    nabu_configurations = []
    for rec_plane, slices_in_plane in slices.items():
        nabu_configurations.extend(
            SliceInterpreter.interpret(
                config=config, slices=slices_in_plane, rec_plane=rec_plane, scan=scan
            )
        )

    output_urls = []
    stderrs = []
    stdouts = []
    final_configs = []
    futures = []
    instantiated_classes = []
    all_succeed = True
    if advancement is not None:
        advancement.total = len(nabu_configurations)
    scan.clear_latest_reconstructions()
    for nabu_configuration in nabu_configurations:
        l_config, slice_index = nabu_configuration
        result = run_single_slice_reconstruction(
            nabu_config=l_config,
            cluster_config=cluster_config,
            scan=scan,
            slice_index=slice_index,
            dry_run=dry_run,
            instantiate_class_only=instantiate_classes_only,
            axis=l_config.get("reconstruction", {}).get("slice_plane", "XY"),
        )

        # specific treatments of results
        if result is None:
            # in case of timeout or another issue. Log should already have been provided
            pass
        elif instantiate_classes_only:
            instantiated_classes.append(result)
            continue
        if slice_index is None:
            continue
        elif isinstance(result, ResultsLocalRun):
            assert not is_cluster_job, "cluster job should not return ResultsLocalRun"
            stderrs.append(result.std_err)
            stdouts.append(result.std_out)
            output_urls.extend(result.results_identifiers)
            # if slice_index is None this mean that we are simply creating the
            # .cfg file for nabu full volume.
        elif isinstance(result, ResultSlurmRun):
            assert (
                is_cluster_job
            ), "local reconstruction should not return ResultSlurmRun"
            stderrs.append(result.std_err)
            stdouts.append(result.std_out)
            futures.extend(result.future_slurm_jobs)
        elif not isinstance(result, ResultsRun):
            raise ValueError(
                f"result is expected to be an instance of {ResultsRun} not {type(result)}"
            )

        # common treatments of results
        if result is not None:
            final_configs.append(result.config)
            all_succeed = all_succeed and result.success

        if advancement is not None:
            advancement.update()

    if instantiate_classes_only:
        return instantiated_classes
    if is_cluster_job:
        future_tomo_obj = FutureTomwerObject(
            tomo_obj=scan,
            futures=tuple(futures),
            process_requester_id=process_id,
        )
        scan.set_latest_reconstructions(output_urls)
        return all_succeed, stdouts, stderrs, final_configs, future_tomo_obj
    else:
        # tag latest reconstructions
        scan.set_latest_reconstructions(output_urls)
        return all_succeed, stdouts, stderrs, final_configs, None


def run_single_slice_reconstruction(
    scan,
    nabu_config,
    dry_run,
    slice_index: int | str | None,
    process_id: int | None = None,
    cluster_config: dict | None = None,
    add_to_latest_reconstructions=True,
    instantiate_class_only=False,
    axis: NabuPlane = NabuPlane.XY,
) -> ResultsRun | None:
    """
    # TODO: might need something like a context or an option "keep" slice in memory

    :param scan:
    :param nabu_config: configuration of nabu process
    :param cluster_config: configuration of cluster (slurm-cluster only for now)
    :param dry_run:
    :param slice_index: slice index to reconstruct.
                                             If str should be "middle"
    :param local:
    :param stdout: file to redirect stdout
    :param stderr: file to redirect stderr
    :param add_to_latest_reconstructions: if true add reconstructed slice to the latest reconstruction.
                                               We wan't to avoid this treatment for saaxis and sadeltebeta for example
    :param instantiate_class_only: if we don't want to run the SingleSliceRunner but only return them. Use case: we want to keep a hand on processing and it can be cancelled
    :return: result of the slice reconstruction if succeed to launch it.
    """
    # TODO: remove local from the function signature
    target = Target.SLURM if cluster_config not in ({}, None) else Target.LOCAL
    axis = NabuPlane.from_axis(axis)
    # FIXEME: nabu fails if outer_circle activated and if the axis != z
    if axis != NabuPlane.XY and nabu_config.get("reconstruction", {}).get(
        "clip_outer_circle", False
    ):
        nabu_config["reconstruction"]["clip_outer_circle"] = False

    if process_id is not None:
        try:
            process_name = ProcessManager().get_process(process_id=process_id).name
        except KeyError:
            process_name = "unknow"
    else:
        process_name = ""

    slice_reconstructor = SingleSliceRunner(
        scan=scan,
        config=nabu_config,
        dry_run=dry_run,
        slice_index=slice_index,
        axis=axis,
        target=target,
        cluster_config=cluster_config,
        add_to_latest_reconstructions=add_to_latest_reconstructions,
        process_name=process_name,
    )
    if instantiate_class_only:
        return slice_reconstructor

    try:
        results = slice_reconstructor.run()
    except TimeoutError as e:
        _logger.error(e)
        return None
    else:
        assert len(results) == 1, "only one slice should be reconstructed"
        return results[0]
