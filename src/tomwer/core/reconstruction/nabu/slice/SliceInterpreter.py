from __future__ import annotations

import copy
import logging
import numpy

from tomwer.core.reconstruction.nabu.plane import NabuPlane
from tomwer.core.reconstruction.nabu.slice.NabuSliceMode import NabuSliceMode
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.core.reconstruction.nabu.utils import retrieve_lst_of_value_from_str

_logger = logging.getLogger(__name__)


class SliceInterpreter:
    """Small class that generate all the nabu configuration to be run from one 'advanced' nabu configuration"""

    @classmethod
    def interpret(
        cls,
        config: dict,
        slices: str | None | int[tuple[int]],
        scan: TomwerScanBase,
        rec_plane: NabuPlane = NabuPlane.XY,
    ) -> tuple[dict]:
        """
        Generate a nabu configuration 'ready to be processed' from one 'nabu raw configuration'
        """
        if not isinstance(config, dict):
            raise TypeError("config is expected to be a dict")

        type_err_msg = (
            f"slices is {type(slices)}. Expected it to be None, str or a tuple of int"
        )
        if isinstance(slices, (tuple, numpy.ndarray, list)):
            if len(slices) > 0 and type(slices) not in (int, numpy.number):
                raise TypeError(type_err_msg)
        elif not (slices is None or isinstance(slices, (str, int))):
            raise TypeError(type_err_msg)

        assert "slices" not in config
        assert "slice_plane" not in config
        rec_plane = NabuPlane(rec_plane or NabuPlane.XY)

        if slices is None:
            scalar_slices = []
        else:
            reconstruction_axis = rec_plane
            scalar_slices = list(
                NabuSliceMode.getSlices(
                    slices,
                    scan=scan,
                    axis=reconstruction_axis,
                )
            )

            scalar_slices = list(
                filter(
                    lambda slice_index: cls.filter_slice(
                        slice_index=int(slice_index),
                        axis=reconstruction_axis,
                        max_axis_0_size=scan.dim_2,  # z
                        max_axis_1_size=scan.dim_1,  # y
                        max_axis_2_size=scan.dim_1,  # x
                    ),
                    scalar_slices,
                )
            )

        if "phase" in config and "delta_beta" in config["phase"]:
            pag_dbs = config["phase"]["delta_beta"]
            if isinstance(pag_dbs, str):
                pag_dbs = retrieve_lst_of_value_from_str(pag_dbs, type_=float)
            if len(pag_dbs) == 0:
                pag_dbs = (None,)
        else:
            pag_dbs = (None,)

        nabu_config = copy.deepcopy(config)
        if "reconstruction" not in nabu_config:
            nabu_config["reconstruction"] = {}
        nabu_config["reconstruction"]["slice_plane"] = rec_plane.value
        res = []
        for slice_ in scalar_slices:
            for pag_db in pag_dbs:
                local_config = copy.deepcopy(nabu_config)
                if pag_db is not None:
                    local_config["phase"]["delta_beta"] = str(pag_db)
                res.append((local_config, slice_))
        return tuple(res)

    @staticmethod
    def filter_slice(
        slice_index: int,
        axis: NabuPlane,
        max_axis_0_size: int,
        max_axis_1_size: int,
        max_axis_2_size: int,
    ):
        """remove slices that 'cannot' be reconstructed (out of bounds)"""
        if axis is NabuPlane.XY:
            index_max = max_axis_0_size
        elif axis is NabuPlane.XZ:
            index_max = max_axis_1_size
        elif axis is NabuPlane.YZ:
            index_max = max_axis_2_size
        else:
            raise ValueError

        if index_max is None:
            return True

        index_max = index_max - 1

        if slice_index > index_max:
            _logger.error(
                f"slice index {slice_index} requested. But slice index must be in 0-{index_max} - ignore this request"
            )
            return False
        return True
