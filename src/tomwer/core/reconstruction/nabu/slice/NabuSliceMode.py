from __future__ import annotations


from tomwer.core.reconstruction.nabu.utils import retrieve_lst_of_value_from_str
from tomwer.core.reconstruction.nabu.plane import NabuPlane
from silx.utils.enum import Enum as _Enum


class NabuSliceMode(_Enum):
    MIDDLE = "middle"
    OTHER = "other"

    @staticmethod
    def getSlices(slices, scan, axis=NabuPlane.XY) -> tuple:
        res = []
        try:
            mode = NabuSliceMode(slices)
        except ValueError:
            try:
                res = retrieve_lst_of_value_from_str(slices, type_=int)
            except Exception:
                pass
        else:
            if mode == mode.MIDDLE:
                axis = NabuPlane.from_axis(axis)
                if axis is NabuPlane.XY:
                    n_slice = scan.dim_2 or 2048
                elif axis in (NabuPlane.YZ, NabuPlane.XZ):
                    n_slice = scan.dim_1 or 2048
                else:
                    raise NotImplementedError(f"unknow axis {axis}")
                res.append(n_slice // 2)
            else:
                raise ValueError(
                    "there should be only two ways of defining "
                    "slices: middle one or other, by giving "
                    "an unique value or a list or a tuple"
                )
        return tuple(res)
