from __future__ import annotations

import functools
import logging
import os

from nabu.pipeline.config import _extract_nabuconfig_keyvals, generate_nabu_configfile
from nabu.pipeline.fullfield.nabu_config import (
    nabu_config as nabu_fullfield_default_config,
)
from silx.io.utils import open as open_hdf5

from tomwer.core.reconstruction.nabu import settings as nabu_settings
from tomwer.core.reconstruction.nabu.NabuBaseReconstructor import NabuBaseReconstructor
from tomwer.core.reconstruction.nabu.plane import NabuPlane
from tomwer.core.reconstruction.nabu.target import Target
from tomwer.core.reconstruction.nabu.utils import (
    TomwerInfo,
    get_recons_volume_identifier,
    slice_index_to_int,
)
from tomwer.core.reconstruction.output import PROCESS_FOLDER_RECONSTRUCTED_SLICES
from tomwer.core.scan.edfscan import EDFTomoScan
from tomwer.core.scan.nxtomoscan import NXtomoScan
from tomwer.core.scan.scanbase import TomwerScanBase
from tomwer.utils import docstring

_logger = logging.getLogger(__name__)


class SingleSliceRunner(NabuBaseReconstructor):
    def __init__(
        self,
        scan: TomwerScanBase,
        config: dict,
        dry_run: bool,
        slice_index: int | str | None,
        axis: NabuPlane,
        target: Target,
        cluster_config: dict | None,
        process_name: str,
        add_to_latest_reconstructions: bool = True,
    ) -> None:
        super().__init__(
            scan=scan,
            dry_run=dry_run,
            target=target,
            cluster_config=cluster_config,
            process_name=process_name,
            axis=axis,
        )
        self._slice_index = slice_index
        if not isinstance(config, dict):
            raise TypeError(f"config is expected to be a dictionary not {type(dict)}")
        self._config = config
        self._add_to_latest_reconstructions = add_to_latest_reconstructions

    @property
    def slice_index(self):
        return self._slice_index

    @property
    def config(self):
        return self._config

    @property
    def add_to_latest_reconstructions(self):
        return self._add_to_latest_reconstructions

    @property
    def processed_data_folder_name(self):
        """return the specific processed folder name associated to this type of reconstruction."""
        return PROCESS_FOLDER_RECONSTRUCTED_SLICES

    @docstring(NabuBaseReconstructor)
    def only_create_config_file(self):
        return self.slice_index is None

    @docstring(NabuBaseReconstructor)
    def run(self) -> tuple:
        """
        If the target is local will wait for the reconstruction to be finish to return

        :raise: TIMEOUT_SLURM_JOB_SUBMISSION if not all workers spwan
        """
        if isinstance(self.slice_index, str):
            self._slice_index = slice_index_to_int(
                slice_index=self.slice_index,
                scan=self.scan,
                axis=self.axis,
            )
        elif (
            isinstance(self.slice_index, float)
            and int(self.slice_index) == self.slice_index
        ):
            self._slice_index = int(self.slice_index)
        elif not isinstance(self.slice_index, (int, type(None))):
            raise TypeError(
                f"slice index is expected to an int or 'middle' or None and not {type(self.slice_index)}"
            )
        config_complete = _extract_nabuconfig_keyvals(nabu_fullfield_default_config)
        config_complete["dataset"] = self.scan.get_nabu_dataset_info()
        for key in config_complete.keys():
            if key in self.config:
                config_complete[key].update(self.config[key])
        config = config_complete

        config["resources"] = config.get("resources", {})
        config["resources"]["method"] = "local"

        # force overwrite results
        if "output" not in config:
            config["output"] = {}

        config["output"].update({"overwrite_results": 1})

        config, cfg_folder = self._treateOutputSliceConfig(config)
        # the policy is to save nabu .cfg file at the same location as the
        # force overwrite results
        if self.slice_index is not None:
            if self.axis is NabuPlane.YZ:
                config["reconstruction"]["start_x"] = self.slice_index
                config["reconstruction"]["end_x"] = self.slice_index
            elif self.axis is NabuPlane.XZ:
                config["reconstruction"]["start_y"] = self.slice_index
                config["reconstruction"]["end_y"] = self.slice_index
            elif self.axis is NabuPlane.XY:
                config["reconstruction"]["start_z"] = self.slice_index
                config["reconstruction"]["end_z"] = self.slice_index
            else:
                raise ValueError(
                    f"self.axis has an invalid value: {self.axis} when expected to be in {NabuPlane.values()}"
                )

        if self.slice_index is not None:
            os.makedirs(config["output"]["location"], exist_ok=True)

        name = (
            config["output"]["file_prefix"] + nabu_settings.NABU_CONFIG_FILE_EXTENSION
        )
        if not isinstance(self.scan, EDFTomoScan):
            name = "_".join((self.scan.entry.lstrip("/"), name))
        conf_file = os.path.join(cfg_folder, name)

        _logger.info(f"{self.scan}: create {conf_file}")
        # add some tomwer metadata and save the configuration
        # note: for now the section is ignored by nabu but shouldn't stay that way
        with TomwerInfo(config) as config_to_dump:
            generate_nabu_configfile(
                conf_file,
                nabu_fullfield_default_config,
                config=config_to_dump,
                options_level="advanced",
            )

        return tuple(
            [
                self._process_config(
                    config_to_dump=config_to_dump,
                    config_file=conf_file,
                    info="nabu slice reconstruction",
                    file_format=config_to_dump["output"]["file_format"],
                    process_name=self.process_name,
                ),
            ]
        )

    @docstring(NabuBaseReconstructor)
    def _get_futures_slurm_callback(self, config_to_dump):
        if self.add_to_latest_reconstructions:
            # add callback to set slices reconstructed urls
            class CallBack:
                # we cannot create a future directly because distributed enforce
                # the callback to have a function signature with only the future
                # as single parameter.
                def __init__(self, f_partial, scan) -> None:
                    self.f_partial = f_partial
                    self.scan = scan

                def process(self, fn):
                    if fn.done() and not (fn.cancelled() or fn.exception()):
                        # update reconstruction urls only if processing succeed.
                        recons_urls = self.f_partial()
                        self.scan.add_latest_reconstructions(recons_urls)

            file_format = config_to_dump["output"]["file_format"]
            callback = functools.partial(
                get_recons_volume_identifier,
                file_prefix=config_to_dump["output"]["file_prefix"],
                location=config_to_dump["output"]["location"],
                file_format=file_format,
                scan=self.scan,
                slice_index=None,
                axis=self.axis,
            )

            return (CallBack(callback, self.scan),)
        else:
            return super()._get_futures_slurm_callback(config_to_dump)

    @staticmethod
    def get_file_basename_reconstruction(
        scan,
        pag,
        ctf,
        db,
        slice_index: str | int,
        axis: NabuPlane,
    ):
        axis = NabuPlane.from_axis(axis)
        if pag:
            assert db is not None, "if paganin defined, db should not be None"
        if slice_index is not None:
            slice_index = slice_index_to_int(slice_index, scan=scan, axis=axis)

        assert type(db) in (int, type(None))
        if isinstance(scan, NXtomoScan):
            basename, _ = os.path.splitext(scan.master_file)
            basename = os.path.basename(basename)
            try:
                with open_hdf5(scan.master_file) as h5f:
                    if len(h5f.keys()) > 1:
                        # if there is more than one entry in the file append the entry name to the file basename
                        basename = "_".join((basename, scan.entry.lstrip("/")))
            except Exception:
                pass
        else:
            basename = os.path.basename(scan.path)
        if slice_index is None:
            if pag:
                return "_".join((basename + "pag", "db" + str(db).zfill(4)))
            elif ctf:
                return "_".join((basename + "ctf", "db" + str(db).zfill(4)))
            else:
                return basename
        else:
            if pag:
                return "_".join(
                    (
                        basename + "slice_pag",
                        str(slice_index).zfill(6),
                        "db" + str(db).zfill(4),
                        "plane",
                        axis.value,
                    )
                )
            elif ctf:
                return "_".join(
                    (
                        basename + "slice_ctf",
                        str(slice_index).zfill(6),
                        "db" + str(db).zfill(4),
                        "plane",
                        axis.value,
                    )
                )
            else:
                return "_".join(
                    (
                        basename + "slice",
                        str(slice_index).zfill(6),
                        "plane",
                        axis.value,
                    )
                )

    @docstring(NabuBaseReconstructor)
    def _get_file_basename_reconstruction(self, pag, db, ctf, axis):
        """

        :param pag: is it a paganin reconstruction
        :param db: delta / beta parameter
        :return: basename of the file reconstructed (without any extension)
        """
        return self.get_file_basename_reconstruction(
            scan=self.scan,
            db=db,
            pag=pag,
            slice_index=self.slice_index,
            ctf=ctf,
            axis=axis,
        )
