"""contain utils for score process"""

from __future__ import annotations


try:
    __import__("nabu")
except ImportError:
    has_nabu = False
else:
    has_nabu = True

from typing import Iterable

from tomoscan.identifier import VolumeIdentifier

from tomwer.core.volume.volumefactory import VolumeFactory


class ResultsRun:
    """
    Base class of results for nabu
    """

    def __init__(self, success, config) -> None:
        self.__success = success
        self.__config = config

    @property
    def success(self) -> bool:
        return self.__success

    @property
    def config(self) -> dict:
        return self.__config

    def __str__(self) -> str:
        return f"result from nabu run: {'succeed' if self.success else 'failed'} with \n - config:{self.config} \n"


class ResultsWithStd(ResultsRun):
    """Nabu result with std"""

    def __init__(self, std_out, std_err, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__std_err = std_err
        self.__std_out = std_out

    @property
    def std_out(self) -> str:
        return self.__std_out

    @property
    def std_err(self) -> str:
        return self.__std_err

    def __str__(self) -> str:
        res = super().__str__()
        res += f"\n {self.std_out} \n {self.std_err}"
        return res


class ResultsLocalRun(ResultsWithStd):
    """Nabu result when run locally.
    If this is the case we should be able to retrieve directly the results urls"""

    def __init__(
        self,
        results_identifiers: tuple,
        *args,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)
        if not isinstance(results_identifiers, Iterable):
            raise TypeError(
                f"results_urls is expected to be an Iterable not {type(results_identifiers)}"
            )

        # check all identifiers
        def check_identifier(identifier):
            if isinstance(identifier, str):
                vol = VolumeFactory.create_tomo_object_from_identifier(
                    identifier=identifier
                )
                return vol.get_identifier()
            elif not isinstance(identifier, VolumeIdentifier):
                raise TypeError(
                    f"identifiers are expected to be VolumeIdentifier. Get {type(identifier)} instead."
                )
            else:
                return identifier

        self.__results_identifiers = tuple(
            [
                check_identifier(identifier=identifier)
                for identifier in results_identifiers
            ]
        )

    @property
    def results_identifiers(self) -> tuple:
        return self.__results_identifiers

    def __str__(self) -> str:
        res = super().__str__()
        res += f"\n - result urls: {self.results_identifiers}"
        return res


class ResultSlurmRun(ResultsWithStd):
    """Nabu result when run on slurm. on this case we expect to get a future and a distributed client"""

    def __init__(
        self,
        future_slurm_jobs: tuple,
        job_id: int | None,
        *args,
        **kwargs,
    ) -> None:
        super().__init__(*args, **kwargs)
        self.__future_slurm_jobs = future_slurm_jobs
        self.__job_id = job_id

    @property
    def future_slurm_jobs(self):
        return self.__future_slurm_jobs

    @property
    def job_id(self) -> int | None:
        return self.__job_id

    def __str__(self) -> str:
        res = super().__str__()
        res += f"\n - future job slurms: {self.future_slurm_jobs} \n"
        return res
