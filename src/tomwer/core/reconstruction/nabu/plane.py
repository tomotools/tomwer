"""Define nabu Axis"""

from silx.utils.enum import Enum as _Enum


class NabuPlane(_Enum):
    YZ = "YZ"
    XZ = "XZ"
    XY = "XY"

    @classmethod
    def from_axis(cls, axis: int):
        if axis == 0:
            return NabuPlane.XY
        elif axis == 1:
            return NabuPlane.XZ
        elif axis == 2:
            return NabuPlane.YZ
        else:
            return NabuPlane(axis)
