from __future__ import annotations


from silx.utils.enum import Enum as _Enum


class NabuPhaseMethod(_Enum):
    """
    Nabu phase method
    """

    PAGANIN = "Paganin"
    CTF = "CTF"
    NONE = "None"

    @classmethod
    def from_str(cls, value):
        if value in (None, ""):
            return NabuPhaseMethod.NONE
        elif isinstance(value, str):
            if value.lower() == "paganin":
                return NabuPhaseMethod.PAGANIN
            elif value.lower() == "none":
                return NabuPhaseMethod.NONE
            elif value.lower() == "ctf":
                return NabuPhaseMethod.CTF
        else:
            return NabuPhaseMethod(value)
