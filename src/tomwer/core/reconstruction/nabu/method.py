from __future__ import annotations

from silx.utils.enum import Enum as _Enum


class NabuReconstructionMethods(_Enum):
    FBP = "FBP"
