from __future__ import annotations

import logging
from tomwer.core.scan.scanbase import TomwerScanBase

_logger = logging.getLogger(__name__)


def relative_pos_to_absolute(relative_pos: float, det_width: int):
    """
    Convert relative center of rotation to absolute.
    """
    # Compute the midpoint differently for even and odd widths
    midpoint = det_width / 2.0
    return relative_pos + midpoint


def absolute_pos_to_relative(absolute_pos: float, det_width: int):
    """
    Convert absolute center of rotation to relative.
    """
    # Compute the midpoint differently for even and odd widths
    midpoint = det_width / 2.0
    return absolute_pos - midpoint


def copy_cor(scan: TomwerScanBase, nabu_rec_params: dict):
    """
    copy the value of CoR registered at the scan level to the 'nabu_rec_params'

    """
    if not isinstance(nabu_rec_params, dict):
        raise TypeError(
            f"nabu_rec_params expects an instance of dict. Got {type(nabu_rec_params)}"
        )

    if not isinstance(scan, TomwerScanBase):
        raise TypeError(f"scan expects an instance of TomwerScanBase. Got {type(scan)}")

    if scan.cor_params is None or scan.cor_params.relative_cor_value is None:
        _logger.debug(
            f"{scan} has no CoR value registered. Unable to copy it to the nabu reconstruction parameters"
        )
        return

    if "reconstruction" not in nabu_rec_params:
        nabu_rec_params["reconstruction"] = {}

    if scan.cor_params.relative_cor_value is None or scan.dim_1 is None:
        _logger.debug("missing metadata to compute CoR position")
        return

    absolute_cor = relative_pos_to_absolute(
        relative_pos=scan.cor_params.relative_cor_value,
        det_width=scan.dim_1,
    )
    nabu_rec_params["reconstruction"]["rotation_axis_position"] = str(absolute_cor)
    _logger.debug(f"{scan}: set nabu cor to {absolute_cor}")
