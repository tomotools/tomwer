# coding: utf-8
from __future__ import annotations

import datetime
from collections import OrderedDict


from tomwer.core.scan.blissscan import BlissScan
from tomwer.core.scan.scanbase import TomwerScanBase


class LastReceivedScansDict(OrderedDict):
    """List the received scan from the first received to the last received"""

    def __init__(self, limit=None):
        """Simple structure in order to store the received last elements and
        the time of acquisition
        """
        assert limit is None or (type(limit) is int and limit > 0)
        OrderedDict.__init__(self)
        self.limit = limit

    def add(self, scan):
        assert isinstance(scan, (TomwerScanBase, BlissScan))
        self[str(scan)] = datetime.datetime.now()
        if self.limit is not None and len(self) > self.limit:
            self.pop(list(self.keys())[0])
