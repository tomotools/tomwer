import h5py
from nxtomo.paths.nxtomo import get_paths as get_nexus_paths
from tomwer.tasks.reconstruction.cor import CoRTask
from tomwer.tests.conftest import nxtomo_scan_360  # noqa F811
from tomwer.core.reconstruction.cor import CoRParams
from tomwer.core.settings import ParametersKeys


def test_read_x_rotation_axis_pixel_position(nxtomo_scan_360):  # noqa F811
    """
    test reading of the estimated cor from the motor using scan metadata
    """
    nexus_paths = get_nexus_paths(None)

    x_rotation_axis_pixel_position_path = "/".join(
        [
            nxtomo_scan_360.entry,
            nexus_paths.INSTRUMENT_PATH,
            nexus_paths.nx_instrument_paths.DETECTOR_PATH,
            nexus_paths.nx_detector_paths.X_ROTATION_AXIS_PIXEL_POSITION
            or nexus_paths.nx_detector_paths.ESTIMATED_COR_FRM_MOTOR_PATH,
        ]
    )
    with h5py.File(nxtomo_scan_360.master_file, mode="r") as h5f:
        assert x_rotation_axis_pixel_position_path not in h5f

    cor_params = CoRParams()
    cor_params.mode = "read"
    task = CoRTask(
        inputs={
            "data": nxtomo_scan_360,
            ParametersKeys.COR_PARAMS_KEY: cor_params,
        }
    )
    # test the task when there is no metadata
    task.run()
    assert nxtomo_scan_360.cor_params.absolute_cor_value is None
    assert nxtomo_scan_360.cor_params.relative_cor_value is None

    # test the task when there is metadata
    with h5py.File(nxtomo_scan_360.master_file, mode="a") as h5f:
        h5f[x_rotation_axis_pixel_position_path] = 12.5

    nxtomo_scan_360.clear_cache()
    task.run()
    assert nxtomo_scan_360.cor_params.absolute_cor_value == 22.5
    assert nxtomo_scan_360.cor_params.relative_cor_value == 12.5
