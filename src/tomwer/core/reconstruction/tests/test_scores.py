# coding: utf-8
from __future__ import annotations

import numpy
import pytest

from tomwer.core.reconstruction.multi.scores.compute import (
    METHOD_TO_FCT,
    compute_score_contrast_std,
)


def test_img_contrast_std_score():
    """simple test of the API to call compute_score_contrast_std"""
    data = numpy.random.random(100 * 100).reshape(100, 100)
    compute_score_contrast_std(data)


@pytest.mark.parametrize("method_name", METHOD_TO_FCT.keys())
def test_method_to_function(method_name):
    """Test the dictionary used to for linking the score method to the
    callback function"""
    data = numpy.random.random(100 * 100).reshape(100, 100)

    res = METHOD_TO_FCT[method_name](data)
    assert isinstance(res, float)
