class MissingUrlForCoR(Exception):
    """Dedicated exception in the case of Not having url for CoR search"""

    pass
