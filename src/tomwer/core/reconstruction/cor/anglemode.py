# coding: utf-8

from silx.utils.enum import Enum as _Enum


class CorAngleMode(_Enum):
    use_0_180 = "0-180"
    use_90_270 = "90-270"
    manual_selection = "manual"
