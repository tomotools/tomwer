from silx.utils.enum import Enum as _Enum


class Side(_Enum):
    LEFT = "left"
    RIGHT = "right"
    CENTER = "center"
    ALL = "all"
