# coding: utf-8
"""contains scan classes and function. Abstraction layer for scans, based on tomoscan classes.
See https://gitlab.esrf.fr/tomotools/tomoscan
"""

TYPES = ["EDF", "HDF5"]
