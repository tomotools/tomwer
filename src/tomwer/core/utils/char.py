# coding: utf-8

ALPHA_CHAR = (b"\xce\xb1").decode("utf-8")

BETA_CHAR = (b"\xce\xb2").decode("utf-8")

GAMMA_CHAR = (b"\xce\xb3").decode("utf-8")

DELTA_CHAR = (b"\xce\xb4").decode("utf-8")

PSI_CHAR = (b"\xcf\x88").decode("utf-8")

THETA_CHAR = (b"\xce\xb8").decode("utf-8")

MU_CHAR = (b"\xce\xbc").decode("utf-8")

DEGREE_CHAR = (b"\xc2\xb0").decode("utf-8")
