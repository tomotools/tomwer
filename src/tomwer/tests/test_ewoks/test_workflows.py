# coding: utf-8
from __future__ import annotations


import os
import pytest
import tempfile
from glob import glob

from ewoks import execute_graph
from ewokscore.graph import analysis, load_graph
from ewokscore.graph.validate import validate_graph

from tomwer.core.utils.scanutils import HDF5MockContext
from tomwer.core.reconstruction.output import (
    PROCESS_FOLDER_RECONSTRUCTED_VOLUMES,
)
from tomwer.core.settings import ParametersKeys

try:
    import nabu  # noqa F401
except ImportError:
    has_nabu = False
else:
    has_nabu = True


@pytest.mark.skipif(not has_nabu, reason="nabu not installed")
def test_simple_workflow_nabu():
    """Test the workflow: darflat -> axis -> nabu slices -> nabu volume"""

    with HDF5MockContext(
        scan_path=os.path.join(tempfile.mkdtemp(), "scan_test"), n_proj=100
    ) as scan:
        # insure no cfg yet
        assert len(glob(os.path.join(scan.path, "*.cfg"))) == 0

        graph = load_graph(
            source={
                "nodes": [
                    {
                        "id": "darkflat",
                        "task_type": "class",
                        "task_identifier": "tomwer.tasks.reconstruction.darkflat.DarkFlatTask",
                    },
                    {
                        "id": "axis",
                        "task_type": "class",
                        "task_identifier": "tomwer.tasks.reconstruction.cor.CoRTask",
                        "default_inputs": [
                            {
                                "name": ParametersKeys.COR_PARAMS_KEY,
                                "value": {
                                    "MODE": "manual",
                                    "POSITION_VALUE": 0.2,
                                },
                            },
                        ],
                    },
                    {
                        "id": "nabu slices",
                        "task_type": "class",
                        "task_identifier": "tomwer.tasks.reconstruction.nabu.slices.NabuSlicesTask",
                        "dry_run": True,  # as this is a mock dataset avoid to reconstruct it and only check for the .cfg file created
                        "default_inputs": [
                            {
                                "name": ParametersKeys.NABU_REC_PARAMS_KEY,
                                "value": {},
                            },
                            {
                                "name": "slices",
                                "value": {"XY": 2},
                            },
                        ],
                    },
                    {
                        "id": "nabu volume",
                        "task_type": "class",
                        "task_identifier": "tomwer.tasks.reconstruction.nabu.nabuvolume.NabuVolumeTask",
                        "dry_run": True,
                        "default_inputs": [
                            {
                                "name": "dry_run",
                                "value": True,
                            }
                        ],
                    },
                ],
                "links": [
                    {
                        "source": "darkflat",
                        "target": "axis",
                        "map_all_data": True,
                    },
                    {
                        "source": "axis",
                        "target": "nabu slices",
                        "data_mapping": [  # same as all arguments but just here to test both
                            {
                                "source_output": "data",
                                "target_input": "data",
                            },
                        ],
                    },
                    {
                        "source": "nabu slices",
                        "target": "nabu volume",
                        "map_all_data": True,
                    },
                ],
            }
        )
        assert graph.is_cyclic is False, "graph is expected to be acyclic"
        assert analysis.start_nodes(graph=graph.graph) == {
            "darkflat"
        }, "start node should be a single task `darkflat`"
        validate_graph(graph.graph)
        result = execute_graph(
            graph,
            inputs=[
                {"id": "darkflat", "name": "data", "value": scan},
            ],
        )
        assert analysis.end_nodes(graph=graph.graph) == {
            "nabu volume"
        }, "should only have one result nodes"
        assert "data" in result, f"cannot find `nabu volume` in {result}"
        assert os.path.exists(
            os.path.join(
                scan.path, PROCESS_FOLDER_RECONSTRUCTED_VOLUMES, "nabu_cfg_files"
            )
        ), "nabu has not been executed (even in dry mode)"
