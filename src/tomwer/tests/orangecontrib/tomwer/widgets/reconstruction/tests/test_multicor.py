# coding: utf-8
from __future__ import annotations

import logging
import pickle

import numpy
from orangecanvas.scheme.readwrite import literal_dumps
from silx.io.url import DataUrl

from orangecontrib.tomwer.widgets.reconstruction.SAAxisOW import SAAxisOW as _MultiCoROW
from tomwer.core.reconstruction.multi.scores.ComputedScore import ComputedScore
from tomwer.core.utils.scanutils import MockNXtomo
from tomwer.core.settings import ParametersKeys

logger = logging.getLogger(__name__)


class MultiCoROW(_MultiCoROW):
    def __init__(self, parent=None):
        self._scans_finished = []
        super().__init__(parent)

    def processing_finished(self, scan):
        self._scans_finished.append(scan)

    @property
    def scans_finished(self):
        return self._scans_finished

    def close(self):
        self._scans_finished = {}
        super().close()


FRAME_DIM = 100


def create_scan(output_dir):
    return MockNXtomo(
        scan_path=output_dir,
        n_ini_proj=20,
        n_proj=20,
        n_alignement_proj=2,
        create_final_flat=False,
        create_ini_dark=True,
        create_ini_flat=True,
        n_refs=1,
        dim=FRAME_DIM,
    ).scan


def patch_score(*args, **kwargs):
    """Function to save some result"""
    return DataUrl(
        file_path="/no_existing/path.hdf5",
        data_path="/no_existing_data_path",
        scheme="silx",
    ), ComputedScore(
        tv=numpy.random.random(),
        std=numpy.random.random(),
    )


def test_MultiCoROW(
    qtapp,  # noqa F811
    tmp_path,
):
    source_dir = tmp_path / "source"
    source_dir.mkdir()

    widget = MultiCoROW()
    widget.show()

    # test configuration serialization
    pickle.dumps(widget._widget.getConfiguration())

    # test configuration is compatible with 'litteral dumps'
    literal_dumps(widget._widget.getConfiguration())

    # test behavior when 'auto focus' is lock (so widget is automated, should take the value with the higher score)
    widget.lockAutofocus(False)

    assert widget._widget.getConfiguration() == {
        "platform_resources": {
            "cpu_mem_fraction": 0.9,
            "gpu_mem_fraction": 0.9,
        },
        ParametersKeys.MULTICOR_PARAMS_KEY: {
            "estimated_cor": 0.0,
            "n_reconstruction": 30,
            "research_width": 10.0,
            "score_method": "1 / (total variation)",
            "slice_index": "middle",
        },
        "workflow": {
            "autofocus_lock": False,
        },
    }

    new_config = {
        "platform_resources": {
            "cpu_mem_fraction": 0.24,
            "gpu_mem_fraction": 0.36,
        },
        ParametersKeys.MULTICOR_PARAMS_KEY: {
            "estimated_cor": 2.2,
            "n_reconstruction": 20,
            "research_width": 5.0,
            "score_method": "1 / (total variation)",
            "slice_index": {
                "Slice": 12,
            },
        },
        "workflow": {
            "autofocus_lock": False,
        },
    }

    widget._widget.setSlicesRange(0, 20)
    widget.setConfiguration(new_config)
    assert widget._widget.getConfiguration() == new_config
