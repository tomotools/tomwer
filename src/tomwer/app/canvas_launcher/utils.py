from __future__ import annotations

from importlib.metadata import version as get_version

try:
    __import__("nxtomomill")
except ImportError:
    has_nxtomomill = False
else:
    has_nxtomomill = True

try:
    __import__("nabu")
except ImportError:
    has_nabu = False
else:
    has_nabu = True

try:
    __import__("nxtomo")
except ImportError:
    has_nxtomo = False
else:
    has_nxtomo = True


def get_tomotools_stack_versions() -> dict[str, str]:
    """Return the version of the main libraries used by tomwer"""
    stack = {"tomwer": get_version("tomwer")}

    if has_nabu:
        stack["nabu"] = get_version("nabu")
    if has_nxtomo:
        stack["nxtomo"] = get_version("nxtomo")
    if has_nxtomomill:
        stack["nxtomomill"] = get_version("nxtomomill")
    stack["tomoscan"] = get_version("tomoscan")
    stack["ewokscore"] = get_version("ewokscore")
    stack["ewoksorange"] = get_version("ewoksorange")
    stack["sluurp"] = get_version("sluurp")

    return stack
