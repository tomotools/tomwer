#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Application to reconstruct a slice for a set of center of rotation value. Interface to nabu multicor"""

from __future__ import annotations

import argparse
import logging
import signal
import sys

import silx
from silx.gui import qt

from tomwer.tasks.reconstruction.darkflat import requires_reduced_dark_and_flat
from tomwer.tasks.reconstruction.nabu.multicor import MultiCoRTask
from tomwer.core.scan.scanfactory import ScanFactory
from tomwer.core.utils.resource import increase_max_number_file
from tomwer.core.settings import ParametersKeys
from tomwer.gui import icons
from orangecontrib.tomwer.widgets.reconstruction.SAAxisOW import (
    SAAxisOW as _MultiCoRWindow,
)
from tomwer.gui.reconstruction.nabu.nabuconfig.NabuSettingsWidget import (
    NabuSettingsWidget,
)
from tomwer.gui.utils.splashscreen import getMainSplashScreen
from tomwer.synctools.axis import QCoRParams
from tomwer.synctools.multicor import QMultiCoRParams

logging.basicConfig(level=logging.WARNING)
_logger = logging.getLogger(__name__)


class MultiCoRWindow(_MultiCoRWindow):
    """Widget to add missing NabuSettings to the MultiCoRWindow"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._recSettingsWidget = NabuSettingsWidget(parent=self)
        self._widget._tabWidget.insertTab(
            1, self._recSettingsWidget, "reconstruction parameters"
        )
        self._recSettingsWidget.sigConfigChanged.connect(self._recSettingsHaveChanged)

        # set up initial settings
        self._recSettingsHaveChanged()

    def _recSettingsHaveChanged(self):
        self.set_dynamic_input(
            ParametersKeys.NABU_REC_PARAMS_KEY,
            self._recSettingsWidget.getConfiguration(),
        )


def main(argv):
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "scan_path",
        help="For EDF acquisition: provide folder path, for HDF5 / nexus "
        "provide the master file",
        default=None,
    )
    parser.add_argument(
        "--entry", help="For Nexus files: entry in the master file", default=None
    )
    parser.add_argument(
        "--debug",
        dest="debug",
        action="store_true",
        default=False,
        help="Set logging system in debug mode",
    )
    parser.add_argument(
        "--read-existing",
        dest="read_existing",
        action="store_true",
        default=False,
        help="Load latest sa-delta-beta processing from *_tomwer_processes.h5 "
        "if exists",
    )
    parser.add_argument(
        "--dump-roi",
        dest="dump_roi",
        action="store_true",
        default=False,
        help="Save roi where the score is computed on the .hdf5",
    )
    parser.add_argument(
        "--use-opengl-plot",
        help="Use OpenGL for plots (instead of matplotlib)",
        action="store_true",
        default=False,
    )

    options = parser.parse_args(argv[1:])

    if options.debug:
        logging.root.setLevel(logging.DEBUG)

    increase_max_number_file()

    global app  # QApplication must be global to avoid seg fault on quit
    app = qt.QApplication.instance() or qt.QApplication(["tomwer"])
    splash = getMainSplashScreen()
    qt.QApplication.setOverrideCursor(qt.Qt.WaitCursor)
    qt.QApplication.processEvents()

    qt.QLocale.setDefault(qt.QLocale(qt.QLocale.English))
    qt.QLocale.setDefault(qt.QLocale.c())
    signal.signal(signal.SIGINT, sigintHandler)
    sys.excepthook = qt.exceptionHandler

    timer = qt.QTimer()
    timer.start(500)
    # Application have to wake up Python interpreter, else SIGINT is not
    # catch
    timer.timeout.connect(lambda: None)
    # define the process_index is any tomwer_processes_existing
    if options.debug:
        _logger.setLevel(logging.DEBUG)

    scan = ScanFactory.create_scan_object(
        scan_path=options.scan_path, entry=options.entry
    )
    requires_reduced_dark_and_flat(scan=scan, logger_=_logger)

    if options.use_opengl_plot:
        silx.config.DEFAULT_PLOT_BACKEND = "gl"
    else:
        silx.config.DEFAULT_PLOT_BACKEND = "matplotlib"

    window = MultiCoRWindow()
    window.setWindowTitle("saaxis")
    window.setWindowIcon(icons.getQIcon("tomwer"))
    if scan.cor_params is None:
        scan.cor_params = QCoRParams()
    if scan.multicor_params is None:
        scan.multicor_params = QMultiCoRParams()
    # force load of reduced_flats and reduced_darks
    scan.reduced_flats
    scan.reduced_darks

    window.set_dynamic_input("dump_roi", options.dump_roi)
    window.process(scan)

    if options.read_existing is True:
        scores, selected = MultiCoRTask.load_results_from_disk(scan)
        if scores is not None:
            window.setCorScores(scores, score_method="standard deviation")
            if selected not in (None, "-"):
                window.setCurrentCorValue(selected)

    splash.finish(window)
    window.show()
    qt.QApplication.restoreOverrideCursor()
    exit(app.exec())


def getinputinfo():
    return "tomwer saaxis [scanDir]"


def sigintHandler(*args):
    """Handler for the SIGINT signal."""
    qt.QApplication.quit()


if __name__ == "__main__":
    main(sys.argv)
