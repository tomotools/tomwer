Contribute
----------

.. toctree::
   :hidden:

   contribute_code.rst

Any type of contribution is welcome (reporting an issue, fixing a bug, correcting typos, etc.). Here is a short summary of the entry points:

.. grid:: 2
    :gutter: 1

    .. grid-item-card::
        :link: https://requests.esrf.fr/plugins/servlet/desk/portal/41
        :shadow: none

        .. line-block::
            :octicon:`megaphone;1em;sd-text-info` Request new feature
            (with an ESRF account)

    .. grid-item-card::
        :link: https://gitlab.esrf.fr/tomotools/minutes/-/issues/new
        :shadow: none

        :octicon:`light-bulb;1em;sd-text-info` Request new feature

    .. grid-item-card::
        :link: https://gitlab.esrf.fr/tomotools/minutes/-/issues/new
        :shadow: none

        :octicon:`bug;1em;sd-text-info` Submit bug report

    .. grid-item-card::
        :link: contribute_code
        :link-type: ref
        :shadow: none

        :octicon:`git-pull-request;1em;sd-text-info` Contribute code
