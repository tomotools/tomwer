.. _contribute_code:

Contribute To code
------------------

How to Contribute
'''''''''''''''''

The best way to submit a Pull Request (PR) to tomwer is to:

* :octicon:`repo-forked` Fork the project from the GitLab interface.
* :octicon:`git-branch` Create a branch on your fork with the modifications you want to apply.
* :octicon:`git-pull-request` Submit the PR.

Then, a regular developer of the project will review it, inform you if any modifications are needed, and ultimately :octicon:`git-merge` merge it.

If this is an important bug fix, a minor release will be made. Otherwise, you will benefit from it in the next release.

:octicon:`info` Typically, we do two or three releases per year (sometimes four in leap years).

You can also use the GitLab Web IDE to create modifications and submit a PR. However, this is not recommended if you have several modifications or commits to make.

.. note::
    All the :ref:`tomotools_suite` software uses black and pylint for code formatting and linting.

.. note::
    All the :ref:`tomotools_suite` software has continuous integration (CI) to ensure that added code doesn't break existing features.
