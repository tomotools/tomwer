Videos
------

Several demo videos have been created for tomwer. They are all available at https://www.youtube.com/@tomotools.

Tools Used
''''''''''

* `vokoscreen <https://linuxecke.volkoh.de/vokoscreen/vokoscreen.html>`_ for recording
* `kdenlive <https://kdenlive.org/fr/>`_ for editing

Converting a Video to a GIF
'''''''''''''''''''''''''''

.. code-block:: bash

    ffmpeg -i input.mp4 -i tomwer/resources/gui/icons/tomwer.png -filter_complex "[0:v][1:v] overlay=1700:900:enable='between(t,0,20)'" -pix_fmt yuv420p -c:a copy output.mp4

Video Archives
''''''''''''''

Videos are also saved in https://gitlab.esrf.fr/tomotools/tomwer_raw_materials as a backup.
