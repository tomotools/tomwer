.. _tomotools_suite:

Tomotools Suite
===============

The tomotools suite is a set of developments focused on tomography.
It was started with the `EBS upgrade <https://www.esrf.fr/about/upgrade>`_, and the goal is to allow scientists to reconstruct datasets acquired with the new machine.

We will only focus on the software side.

.. _raw input format:

Raw Input Format
----------------

The first targeted input is the Bliss files (using the `HDF5 file format <https://docs.hdfgroup.org/hdf5/v1_14/_intro_h_d_f5.html>`_).
The second target is the legacy format of ESRF data, Spec-EDF, for backward compatibility and because not all beamlines have moved to Bliss after the restart.

.. _nxtomo default input format:

Nexus (NXtomo): Default Input Format
------------------------------------

As one of the goals of the ESRF is to create open-source and 'sharable' software, it has been decided that the default input format will be `Nexus <https://manual.nexusformat.org/index.html>`_.

Since we are focusing on tomography, the Nexus application to use is the `NXtomo application definition <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_.
It contains all the information necessary for data analysis.

From Raw Input to NXtomo (nxtomomill)
-------------------------------------

To move from the :ref:`raw input format` to the :ref:`nxtomo default input format`, we developed a tool named `nxtomomill <https://nxtomomill.readthedocs.io/en/latest/>`_.
It can convert either `Bliss files to NXtomo <https://nxtomomill.readthedocs.io/en/latest/tutorials/h52nx.html>`_ or `EDF-Spec files to NXtomo <https://nxtomomill.readthedocs.io/en/latest/tutorials/edf2nx.html>`_.

Reading / Editing an NXtomo (nxtomo project)
--------------------------------------------

To allow users to easily edit an NXtomo, we created the `nxtomo project <https://gitlab.esrf.fr/tomotools/nxtomo>`_.
Its goal is to stay focused on the Nexus format while maintaining a simple and user-friendly interface.

Data Processing (nabu)
----------------------

To reconstruct a volume from radiographs, users can use `nabu <https://gitlab.esrf.fr/tomotools/nabu>`_.
The expected input is an `NXtomo application definition <https://manual.nexusformat.org/classes/applications/NXtomo.html>`_, but for convenience, raw Spec files are also accepted.

Automating (tomwer)
-------------------

To automate processing (define and execute a workflow), users can define a workflow from the :ref:`canvas`.
It also allows direct connection with the acquisition and triggers a workflow once the acquisition is finished.

Tomoscan
--------

To abstract the reading of raw data (NXtomo or another format, if necessary), we built `tomoscan <https://gitlab.esrf.fr/tomotools/tomoscan>`_.
This was the primary goal of the library. Over time, it has also become a common library for most of the software (`nabu <https://gitlab.esrf.fr/tomotools/nabu>`_ and `tomwer <https://gitlab.esrf.fr/tomotools/tomwer>`_).
Recently (2023), we also added code to handle reading/saving volumes.

.. tomotools_suite
.. grid:: 4
   :gutter: 0
   :margin: 3

   .. grid-item::
      .. figure:: img/nxtomo.png
         :target: https://nxtomo.readthedocs.io/en/latest/
         :height: 100px

         nxtomo

   .. grid-item::
      .. figure:: img/nxtomomill.svg
         :target: https://nxtomomill.readthedocs.io/en/latest/
         :height: 100px

         nxtomomill

   .. grid-item::
      .. figure:: img/tomoscan.svg
         :target: https://tomoscan-esrf.readthedocs.io/en/latest/
         :height: 100px

         tomoscan

   .. grid-item::
      .. figure:: img/nabu.svg
         :target: http://www.silx.org/pub/nabu/doc/
         :height: 100px

         nabu
