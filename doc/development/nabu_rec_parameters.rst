.. _nabu reconstruction parameters:

Nabu reconstruction parameters
------------------------------

The reconstruction parameters are split along several sections:

* dataset
* output
* phase
* preproc
* reconstruction

This is the `nabu configuration file <https://tomotools.gitlab-pages.esrf.fr/nabu/nabu_config_file.html#configuration-file>`_ provided as a dict.

You can obtain nabu default configuration directly from nabu

.. code-block:: python

    from nabu.pipeline.fullfield.nabu_config import nabu_config


dataset
"""""""

* ``binning``: int
* ``binning_z``: int
* ``exclude_projections``: str
* ``projections_subsampling``


output
""""""

* ``file_format``: str
* ``location``: str
* ``output_dir_mode``: str
    :warning: this is a tomwer specific values. See


phase
"""""

* ``beam_shape``: str
* ``ctf_advanced_params``: str 
* ``ctf_geometry``: str
* ``ctf_translations_file``: str
* ``method``: str
* ``unsharp_coeff``: int
* ``unsharp_sigma``: int

preproc
"""""""

* ``autotilt_options``: str
* ``ccd_filter_enabled``: int
* ``ccd_filter_threshold``: float
* ``dff_sigma``: float
* ``double_flatfield_enabled``: int
* ``flatfield``: int
* ``log_max_clip``: float
* ``log_min_clip``: float
* ``normalize_srcurrent``: int
* ``rotate_projections_center``: str
* ``sino_rings_correction``: str
* ``sino_rings_options``: str
* ``take_logarithm``: bool
* ``tilt_correction``: str

reconstruction
""""""""""""""

* ``angle_offset``: float
* ``angles_file``: str
* ``axis_correction_file``: str
* ``centered_axis``: int
* ``clip_outer_circle``: int
* ``enable_halftomo``: str
* ``end_x``: int
* ``end_y``: int
* ``end_z``: int
* ``fbp_filter_type``: str
* ``iterations``: int
* ``method``: str
* ``optim_algorithm``: str
* ``padding_type``: str
* ``positivity_constraint``: int
* ``preconditioning_filter``: int
* ``rotation_axis_position``: str | float
* ``start_x``: int
* ``start_y``: int
* ``start_z``: int
* ``translation_movements_file``: str
* ``weight_tv``: float
