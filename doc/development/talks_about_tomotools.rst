Talks About Tomotools
---------------------

* `NOBUGS talk (September 2022) - video <https://www.youtube.com/watch?v=MYL5tbBJX9Q>`_
* `APS talk (March 2023) - PDF <https://gitlab.esrf.fr/tomotools/tomwer_raw_materials/-/blob/17df89e8c729a90d37515b5d8954f3309f6123b9/APS_2023/APS_seminar.pdf>`_
