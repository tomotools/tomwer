.. _release process:

Releases
--------

We typically do two or three releases of each component of the tomotools suite per year.
Each new major or minor version must have its own branch in the project to facilitate bug fixes and ensure the deployment of stable software.

Bug fixes are usually backported to the latest releases, and that’s it.

For each final release, a tag must also be created and pushed to the Git project.

.. code-block:: bash

    git tag -a v{version} -m "tag note"

Currently, we generate both build distributions (wheel) and source distributions:

.. code-block:: bash

    python -m build

Afterward, you must push the distribution to PyPI (using `twine <https://pypi.org/project/twine/>`_ for example).
