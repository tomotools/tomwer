Orange Settings
---------------

Orange settings are stored under `.local`. For example:

.. code-block:: bash

    rm /users/opid19/.local/share/Orange/3.5.0.dev/widgets/[WidgetPath].pickle

This is where the widget settings are stored. In the case of the `Ftseries`, if the structure of the `FastSetupDefaultGlobal` is updated, you will need to remove this file to reset the new parameter set.
