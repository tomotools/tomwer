Communication Between Bliss and Tomwer
--------------------------------------

The communication between Tomwer and Bliss is ensured via remote procedure calls (RPC) using JSON-RPC.
Bliss acts as the client, notifying about the advancement of the acquisition, while Tomwer is the server, capable of launching processing tasks when an acquisition is finished.

This architecture avoids dependency on Bliss and ensures easier communication across versions of both software over time.
