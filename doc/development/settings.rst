Settings
--------

Settings are defined in the **tomwer/core/settings.py** file.

Basic Settings
''''''''''''''

Some parameters can be tuned in tomwer:

+--------------------------------+-------------------------------------------+----------------------------------------------------------+
| Name                           | Definition                                | Default Value                                            |
+================================+===========================================+==========================================================+
| **MAX_MEM_USED**               | Maximum percentage of memory usage at     | 80                                                       |
|                                | which tomwer should skip some processing  |                                                          |
+--------------------------------+-------------------------------------------+----------------------------------------------------------+
| **BEAMLINE_NAME**              | To communicate with Tango, we need to     | id19                                                     |
|                                | define a Tango device from the beamline   |                                                          |
|                                | name, Tango port, and the bliss session   |                                                          |
+--------------------------------+-------------------------------------------+----------------------------------------------------------+
| **TANGO_PORT**                 | To communicate with Tango, we need to     | 20000                                                    |
|                                | define a Tango device from the beamline   |                                                          |
|                                | name, Tango port, and the bliss session   |                                                          |
+--------------------------------+-------------------------------------------+----------------------------------------------------------+
| **BLISS_SESSION_NAMES**        | All the bliss sessions tomwer should be   | ('HRTOMO',)                                              |
|                                | able to access                            |                                                          |
+--------------------------------+-------------------------------------------+----------------------------------------------------------+
| **DEFAULT_BLISS_SESSION_NAME** | The session proposed by default by tomwer.| 'HRTOMO'                                                 |
|                                | It should be contained in                 |                                                          |
|                                | "BLISS_SESSION_NAMES".                    |                                                          |
+--------------------------------+-------------------------------------------+----------------------------------------------------------+

Advanced Settings
'''''''''''''''''

+----------------------------+-------------------------------------------+----------------------------------------------------------+
| Name                       | Definition                                | Default Value                                            |
+============================+===========================================+==========================================================+
| __LBSRAM_PATH              | During processing, tomwer checks          | '/lbsram'                                                |
|                            | automatically if we are on a large buffer |                                                          |
|                            | system (LBS). Tomwer considers it to be   |                                                          |
|                            | the case if the partition exists. If true,|                                                          |
|                            | tomwer will automatically launch folder   |                                                          |
|                            | synchronization between                   |                                                          |
|                            | $__LBSRAM_PATH'/dataset_folder            |                                                          |
|                            | and DEST_PATH'/dataset_folder.            |                                                          |
|                            | These variables are also used to          |                                                          |
|                            | automatically adjust some widgets and     |                                                          |
|                            | processes, such as the default output     |                                                          |
|                            | directory.                                |                                                          |
+----------------------------+-------------------------------------------+----------------------------------------------------------+
| __DEST_PATH                | During processing, tomwer checks          | ''                                                       |
|                            | automatically if we are on a large buffer |                                                          |
|                            | system (LBS). Tomwer considers it to be   |                                                          |
|                            | the case if the partition exists. If true,|                                                          |
|                            | tomwer will automatically launch folder   |                                                          |
|                            | synchronization between                   |                                                          |
|                            | $__LBSRAM_PATH'/dataset_folder            |                                                          |
|                            | and DEST_PATH'/dataset_folder.            |                                                          |
|                            | These variables are also used to          |                                                          |
|                            | automatically adjust some widgets and     |                                                          |
|                            | processes, such as the default output     |                                                          |
|                            | directory.                                |                                                          |
+----------------------------+-------------------------------------------+----------------------------------------------------------+
