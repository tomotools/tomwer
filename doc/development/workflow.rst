Workflow
========

Tomwer contains several types of tasks:

- **Control tasks**: These allow control of the workflow (e.g., from the GUI). They define which scan will go through a branch of the workflow and transfer some data.
- **Visualization tasks**: These help to view information regarding the scan.
- **Reconstruction tasks**: These processes add data to the scan, such as the reconstruction of a slice.
- **Compute cluster tasks**: Utilities to trigger tasks on Slurm and help resume processing.
- **Edit NXtomo tasks**: Utilities to edit NXtomo files.
- **Debug tasks**: Tasks for developers to help with debugging and creating some 'live' workarounds.
- **Other tasks**: Tasks with different purposes that don't fit into the other categories.
