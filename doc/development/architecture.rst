Architecture
------------

The idea is to decouple the graphical part from the processing part.
The project contains the core package, which should be able to perform all actions without any call to the Qt GUI in the end.

Non-Graphical Workflow
''''''''''''''''''''''

We can launch Orange workflows from the command line using the interpreter.

However, you should follow the structure:

    orangecontrib.esrf.core.xxx
    orangecontrib.esrf.widgets.xxxWidgets

The `orangecontrib.esrf.core` should remain Qt-free.
