Documentation
-------------

* The latest documentation is available here: https://tomotools.gitlab-pages.esrf.fr/tomwer/
* Previous version documentation can be found here: https://readthedocs.org/projects/tomwer/

To generate the HTML documentation, you can use:

.. code-block:: bash

   sphinx-build doc build/html

To generate the HTML help documentation, please use:

.. code-block:: bash

   sphinx-build -b htmlhelp doc build/htmlhelp

Orange Widget Documentation
'''''''''''''''''''''''''''

The Orange Canvas allows users to access help for each widget (using F5, for example).

For this, the add-on must define the path to the documentation and the pattern to access widget help.
This is defined in `orangecontrib/tomwer/__init__.py` under the `WIDGET_HELP_PATH` variable.
It defines a list of entry points to be treated and a pattern to determine the widget help.

Adding some 'extra layers,' such as `grid-item-card`, adds complexity to the widget documentation.

To simplify this, we created the `doc/canvas.widget.widget.rst` file, which contains simple links to all widgets and eases the linking process to Orange Canvas help.
So, please ensure widget help is available from there.
