.. _opengl_display:

OpenGL Display
--------------

Since version 1.2, relevant applications have a `use-opengl-plot` option, which allows the use of the OpenGL backend for silx plots (instead of matplotlib). This speeds up the display of large images (projections, reconstructed slices, sinograms, etc.).

As of today, there are known limitations regarding its usage:

* `Using OpenGL through SSH <https://www.silx.org/doc/silx/latest/troubleshooting.html#using-opengl-through-ssh>`_
* `Enabling OpenGL forwarding <https://www.silx.org/doc/silx/latest/troubleshooting.html#enabling-opengl-forwarding>`_
