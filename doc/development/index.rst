Development
===========

.. toctree::
   :maxdepth: 1

   changelog.rst
   contribute.rst
   tomotools.rst
   talks_about_tomotools.rst
   settings.rst
   environment.rst
   workflow.rst
   releases.rst
   video.rst
   link_with_bliss.rst
   orange.rst
   documentation.rst
   backend.rst
   architecture.rst
   nabu_rec_parameters.rst

.. tomotools_suite
.. grid:: 4
   :gutter: 0
   :margin: 3

   .. grid-item::

      .. figure:: img/nxtomo.png
         :target: https://nxtomo.readthedocs.io/en/latest/
         :height: 100px

         nxtomo

   .. grid-item::

      .. figure:: img/nxtomomill.svg
         :target: https://nxtomomill.readthedocs.io/en/latest/
         :height: 100px

         nxtomomill

   .. grid-item::

      .. figure:: img/tomoscan.svg
         :target: https://tomoscan-esrf.readthedocs.io/en/latest/
         :height: 100px

         tomoscan

   .. grid-item::

      .. figure:: img/nabu.svg
         :target: http://www.silx.org/pub/nabu/doc/
         :height: 100px

         nabu