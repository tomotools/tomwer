Environment Variables
----------------------

A set of environment variables can be used with esrf-orange:

   - **TARGET_OUTPUT_FOLDER**: Default output directory of the `TransfertWidget`.
   - **TOMWER_DEFAULT_INPUT_DIR**: If set and exists, all `FileDialog` instances will open from this location.
   - **DATADIR**: Root directory for the observation of `DataWatcherWidget`.
   - **ORANGE_WEB_LOG**: If not set, will attempt to send logs to grayscale. If set to `True`, it will also send logs. If set to `False`, no log messages will be emitted.
   - **ORANGE_COLOR_STDOUT_LOG**: Adds colored logs to stdout. However, this will break logs for the Orange log view, as the color is directly inserted into the log message. This is used for development purposes only. This variable will be set when launching orange-canvas. Use `--color-stdout-logs` to activate it.
   - **BEACON_HOST**: Location of the Redis server, required by the `DataListener`.
