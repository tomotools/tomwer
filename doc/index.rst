.. _start:

====== 
Tomwer
======

.. toctree::
   :maxdepth: 3
   :hidden:

   tutorials/index.rst
   user_guide/index.rst
   api.rst
   canvas/index.rst
   development/index.rst
   applications/index.rst



Tomwer's main goal is to automate tomography treatment once the data has been acquired. It is composed of:

* A high-level library with:
   * `ewoks <https://ewoks.readthedocs.io/en/latest>`_ tasks
   * A dedicated GUI for most of the tasks
   * Utilities
* A :ref:`canvas` to create workflows
* A set of :ref:`stand_alone_applications`

.. figure:: img/tomwer_start_short.gif
    :scale: 100 %

    Workflow processing

Tomwer is part of the :ref:`tomotools_suite` and has been developed by the `Software group <http://www.esrf.eu/Instrumentation/software>`_ of the `European Synchrotron <https://www.esrf.fr/>`_. It relies on several 'core' projects:

.. _eco_system:

.. grid:: 4
   :gutter: 0
   :margin: 4

   .. grid-item::
      .. figure:: img/eco_system/ewoks.png
         :target: https://ewoks.readthedocs.io/en/latest
         :height: 80px

         ewoks

   .. grid-item::
      .. figure:: img/eco_system/silx.png
         :target: https://github.com/silx-kit/silx
         :height: 80px

         silx

   .. grid-item::
      .. figure:: img/eco_system/orange.png
         :target: https://github.com/biolab/orange3
         :height: 80px

         orange3

   .. grid-item::
      .. figure:: img/eco_system/hdf5.png
         :target: https://github.com/h5py/h5py
         :height: 80px

         h5py - HDF5


.. warning::

   in tomwer 1.4 the 'scan list' (aka 'data list') has been removed. The 'scan selector' (aka 'data selector') should be used to replace it.

