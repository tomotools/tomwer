.. _cancel task:

Task Cancellation
=================

Since version 1.2, it is possible to cancel tasks—either `ongoing` tasks (tasks currently being processed) or `pending` tasks (tasks registered on a stack, waiting for resources).

Not all tasks have this option yet. Possible 'cancellable tasks' are:

* nabu slices
* nabu volume
* multi-cor
* multi-pag

To cancel a task, go to the 'object supervisor' interface (if not activated, you can access it from the canvas > view menu and then activate 'object supervisor'). 
Then, right-click on the dataset processing you want to cancel and click 'cancel'.

.. image:: img/cancellation/example_cancellation_when_on_going.png
   :width: 300px
   :align: center
   :alt: cancellation of an ongoing task

Once cancelled, the state of the dataset vs. process will be replaced by a `cancelled` state. Propagation will be stopped, and downstream processing or widgets will not be executed.

.. image:: img/cancellation/example_cancellation_after_cancel.png
   :width: 300px
   :align: center
   :alt: view of the task after cancellation

`Here is the video tutorial on how to cancel a task <https://youtu.be/MTMtw4oKV0I>`_
