.. _sinogram normalization:

sino normalization
==================

.. image:: img/sino_normalization/main_screen.png

Behavior
--------

The goal of this widget is to allow the user to define a normalization to be applied to sinograms.
The normalization options are:

* **None**: No extra normalization will be applied to the sinogram.
* **chebyshev**: Chebyshev normalization will be applied to the sinograms.
* **division**: A value or an array of values will divide the sinogram (during the Nabu preprocessing step).
* **subtraction**: A value or an array of values will subtract from the sinogram (during the Nabu preprocessing step).

For **division** and **subtraction**, the user can provide:

* A scalar value, which will remain constant.
* A region of interest (ROI) to compute one scalar value *per projection* to be applied to the sinogram.
* A URL to be provided directly to Nabu. In this case, the dataset can be provided locally or globally.
  - If provided locally, the path will be updated for each scan.
  - If provided globally, it will remain constant.
