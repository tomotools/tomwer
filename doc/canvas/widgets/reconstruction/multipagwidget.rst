.. _multi-pag:

multi-pag
=========

.. image:: img/multipag/multipag.png
   :width: 500px
   :align: center

Behavior
--------

This widget is used to compute the optimal :math:`\frac{\delta}{\beta}` value for Paganin filtering.

It runs a series of slice reconstructions for a set of :math:`\frac{\delta}{\beta}` values and computes a score for each slice (details below).

If the `autofocus` is locked, it will automatically select the :math:`\frac{\delta}{\beta}` value that generates the highest score.

If a center of rotation has already been computed upstream (from the "cor" or "multi-cor" widgets), it will be used for the reconstruction.

To speed up reconstructions, you can increase the "horizontal binning" in the Nabu properties (in the `reconstruction settings` tab) under the binning and subsampling section.

.. image:: img/multipag/nabu_settings.png
   :width: 300px
   :align: center

.. _multi-pag score calculation:

Score calculation
------------------

Currently, there are two ways of computing a :math:`\frac{\delta}{\beta}` reconstruction score (the same methods used for multipag). These methods can be set in the "results" tab:

* **Standard deviation**: For readability, we multiply the score by 100.

* **Total variation**, calculated by the formula:

    .. math::
        tv = \sum{\sqrt{\nabla_{x}^2 + \nabla_{y}^2}}

    To avoid very low score values, we divide by 10e5.

* **1 / Standard deviation**

* **1 / Total variation**

The score is computed on the circumscribed square of the largest common circumscribed circle of all reconstructed slices.
Using a square ROI simplifies calculations compared to a disk (for example, gradient calculations).

.. image:: img/multicor/score_roi.png
   :align: center

Demo
----

There are several video demonstrations for the sa-delta-beta widget:

* `As a standalone widget <http://www.edna-site.org/pub/doc/tomwer/video/standalone_applications/tomwer_sa_delta_beta_standalone.mp4>`_
* `As part of a workflow with user interaction during processing <https://www.youtube.com/watch?v=A8bZtEwLF7I&list=PLddRXwP6Z6F8bX5lh_fPSOtdGjIIDVebq>`_
