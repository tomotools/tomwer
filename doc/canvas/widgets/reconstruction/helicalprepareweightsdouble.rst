.. _NabuHelicalPrepareWeightsDoubleOW:

helical prepare weights double
==============================

.. image:: img/helical/prepareweightsdouble_workflow.png

This widget is used to call the Nabu `prepare-weights-double` function. It defines a weights map and a double flat field.

Options
-------

.. image:: img/helical/prepareweightsdouble_interface.png

The goal of this widget is to compute a weights map and a double flat field based on user settings.

The output can then be provided to Nabu to compute a slice/volume using the 'helical' and 'FBP' methods.

The options you can modify are:

* **output_file**: Location of the result containing the double flat field and the weights map. By default, the result will be stored near the .nx file under the name 'map_and_doubleff.hdf5'.
   
* **transition width**: Determines how the weights are apodized near the upper and lower borders.

   For example, for frames that are 2048x544 with a transition width of 50, the result will look like:

    .. image:: img/helical/weights_double_res_1.png

   With a transition width of 200, the result will look like:

    .. image:: img/helical/weights_double_res_2.png
