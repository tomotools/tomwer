Reconstruction Widgets
======================

.. toctree::
   :maxdepth: 1
   :hidden:

   corwidget
   darkflatwidget
   nabuslicewidget
   nabuvolumewidget
   multicorwidget
   multipagwidget
   castvolumewidget
   intensitynormalization
   helicalprepareweightsdouble

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/cor.svg

      :doc:`corwidget`

   .. grid-item-card::
      :img-top: img/icon/reduce_dark_flat.svg

      :doc:`darkflatwidget`

   .. grid-item-card::
      :img-top: img/icon/nabu_slice.svg

      :doc:`nabuslicewidget`

   .. grid-item-card::
      :img-top: img/icon/nabu_volume.svg

      :doc:`nabuvolumewidget`

   .. grid-item-card::
      :img-top: img/icon/multi_cor.svg

      :doc:`multicorwidget`

   .. grid-item-card::
      :img-top: img/icon/multi_pag.svg

      :doc:`multipagwidget`

   .. grid-item-card::
      :img-top: img/icon/nabu_cast.svg

      :doc:`castvolumewidget`

   .. grid-item-card::
      :img-top: img/icon/intensity_normalization.svg

      :doc:`intensitynormalization`

   .. grid-item-card::
      :img-top: img/icon/nabu_prepare_weights_double.svg

      :doc:`helicalprepareweightsdouble`
