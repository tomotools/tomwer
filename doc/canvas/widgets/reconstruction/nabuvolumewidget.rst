.. _nabu_volume_reconstruction:

nabu volume reconstruction
==========================

.. image:: img/nabuvolumereconstruction.png

Behavior
--------

Allows the reconstruction of a volume from `start_z` to `end_z`.

.. warning:: This widget will use the reconstruction settings from the 'nabu slice reconstruction' widget.
             Therefore, the 'nabu slice reconstruction' widget must be processed upstream.
