.. _nabu slice reconstruction:

nabu slice reconstruction
=========================

.. image:: img/nabuwidget.png

Interface
---------

The interface allows you to define a set of parameters to tune the reconstruction.

You can filter the parameters by selecting a specific step in the Nabu pipeline on the left side of the widget.
To activate or deactivate the filter, click on the first tool button at the bottom left.

By default, you only access the 'basic' reconstruction options. To select more options, you can activate the 'advanced options' from the 'level' tool button (to the right of the filter button).
