reduced darks and flats
=======================

.. image:: img/darkrefwidget.png

Behavior
--------

This widget generates dark and/or flat fields for an acquisition.

* **Average**: Computes the mean value.
* **Median**: Computes the median value.

There are two options available:

* `remove if existing`: This will remove dark **and** reference files if they already exist.
* `skip if already existing`: This will skip dark and/or flat field generation if the files are already present.

.. note:: The dark and/or flat fields will be generated inside the acquisition directory.

.. note:: You can access this process without using Orange by running:

    .. code-block:: bash

        tomwer darkflat [acquisition folder]

File pattern
------------

To find the reference and dark files, we use Python's `re library <https://docs.python.org/3/library/re.html>`_.
Here are a few examples of how to use it:

* `.*conti_dark.*`: Filters files containing the phrase `conti_dark`.
* `darkend[0-9]{3,4}`: Filters files named `darkend` followed by three or four digits (with the .edf extension).
* `ref*.*[0-9]{3,4}_[0-9]{3,4}`: Filters files named `ref`, followed by any character and ending with X_Y, where X and Y are groups of three or four digits.

For more examples and usage, see the `re library page <https://docs.python.org/3/library/re.html>`_.

How-to videos
-------------

.. youtube:: aBLTWmuMTcQ
   :width: 500
