.. _cast volume:

cast volume
===========

.. image:: img/castvolumewidget.png

Interface
---------

The interface allows the user to define the type of cast to apply and the output directory.

The output directory can contain patterns. Recognized patterns are:

* 'scan_dir_name': returns the name of the directory containing the acquisition (not the full path).
* 'scan_basename': returns the basename of the directory containing the acquisition.
* 'scan_parent_dir_basename': returns the basename of the parent directory containing the acquisition.
