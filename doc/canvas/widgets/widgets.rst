Widgets
=======

.. toctree::
   :maxdepth: 0
   :hidden:

   control/advancement.rst
   control/datadiscovery.rst
   control/datalistener.rst
   control/dataselectorwidget.rst
   control/datatransfertwidget.rst
   control/datavalidatorwidget.rst
   control/datawatcherwidget.rst
   control/emailnotifier.rst
   control/notifier.rst
   control/filterwidget.rst
   control/nxtomoconcatenate.rst
   control/nxtomomill_edf2nx_widget.rst
   control/nxtomomill_h52nx_widget.rst
   control/reducedarkflatselectorwidget.rst
   control/seriesofobjects.rst
   control/singletomoobj.rst
   control/timerwidget.rst
   control/volumeselector.rst
   reconstruction/corwidget.rst
   reconstruction/darkflatwidget.rst
   reconstruction/nabuslicewidget.rst
   reconstruction/nabuvolumewidget.rst
   reconstruction/multicorwidget.rst
   reconstruction/multipagwidget.rst
   reconstruction/castvolumewidget.rst
   reconstruction/intensitynormalization.rst
   reconstruction/helicalprepareweightsdouble.rst
   cluster/futuresupervisor.rst
   cluster/slurmcluster.rst
   visualization/diffviewer.rst
   visualization/slicestack.rst
   visualization/sinogramviewer.rst
   visualization/volumeviewer.rst
   visualization/dataviewerwidget.rst
   visualization/radiostack.rst
   visualization/samplemovedwidget.rst
   visualization/nxtomometadataviewer.rst
   edit/darkflatpatch.rst
   edit/imagekeyeditor.rst
   edit/imagekeyupgrader.rst
   edit/nxtomoeditor.rst
   data_portal/publish_processed_data.rst
   other/pythonscript.rst
   other/tomoobjshub.rst

Control
-------
.. toctree::
   :maxdepth: 2

   control/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: control/img/icon/advancement.svg

      :doc:`control/advancement`

   .. grid-item-card::
      :img-top: control/img/icon/datadiscovery.svg

      :doc:`control/datadiscovery`

   .. grid-item-card::
      :img-top: control/img/icon/datalistener.svg

      :doc:`control/datalistener`

   .. grid-item-card::
      :img-top: control/img/icon/dataselector.svg

      :doc:`control/dataselectorwidget`

   .. grid-item-card::
      :img-top: control/img/icon/datatransfer.svg

      :doc:`control/datatransfertwidget`

   .. grid-item-card::
      :img-top: control/img/icon/datavalidator.png

      :doc:`control/datatransfertwidget`

   .. grid-item-card::
      :img-top: control/img/icon/datawatcher.svg

      :doc:`control/datawatcherwidget`

   .. grid-item-card::
      :img-top: control/img/icon/emailnotifier.svg

      :doc:`control/emailnotifier`

   .. grid-item-card::
      :img-top: control/img/icon/notifier.svg

      :doc:`control/notifier`

   .. grid-item-card::
      :img-top: control/img/icon/nxconcatenate.svg

      :doc:`control/nxtomoconcatenate`

   .. grid-item-card::
      :img-top: control/img/icon/edf2nx.svg

      :doc:`control/nxtomomill_edf2nx_widget`

   .. grid-item-card::
      :img-top: control/img/icon/h52nx.svg

      :doc:`control/nxtomomill_h52nx_widget`

   .. grid-item-card::
      :img-top: control/img/icon/filter.svg

      :doc:`control/filterwidget`

   .. grid-item-card::
      :img-top: control/img/icon/reducedarkflatselector.svg

      :doc:`control/reducedarkflatselectorwidget`

   .. grid-item-card::
      :img-top: control/img/icon/singletomoobj.svg

      :doc:`control/singletomoobj`

   .. grid-item-card::
      :img-top: control/img/icon/timer.png

      :doc:`control/timerwidget`

   .. grid-item-card::
      :img-top: control/img/icon/seriesofobjects.svg

      :doc:`control/seriesofobjects`

   .. grid-item-card::
      :img-top: control/img/icon/volumeselector.svg

      :doc:`control/volumeselector`


Reconstruction
--------------
.. toctree::
   :maxdepth: 2

   reconstruction/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: reconstruction/img/icon/cor.svg

      :doc:`reconstruction/corwidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/reduce_dark_flat.svg

      :doc:`reconstruction/darkflatwidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/nabu_slice.svg

      :doc:`reconstruction/nabuslicewidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/nabu_volume.svg

      :doc:`reconstruction/nabuvolumewidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/multi_cor.svg

      :doc:`reconstruction/multicorwidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/multi_pag.svg

      :doc:`reconstruction/multipagwidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/nabu_cast.svg

      :doc:`reconstruction/castvolumewidget`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/intensity_normalization.svg

      :doc:`reconstruction/intensitynormalization`

   .. grid-item-card::
      :img-top: reconstruction/img/icon/nabu_prepare_weights_double.svg

      :doc:`reconstruction/helicalprepareweightsdouble`

Cluster
-------
.. toctree::
   :maxdepth: 2

   cluster/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: cluster/img/icon/slurm_cluster.svg

      :doc:`cluster/slurmcluster`

   .. grid-item-card::
      :img-top: cluster/img/icon/future_supervisor.svg

      :doc:`cluster/futuresupervisor`

Visualization
-------------
.. toctree::
   :maxdepth: 2

   visualization/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: visualization/img/icon/diff_viewer.svg

      :doc:`visualization/diffviewer`

   .. grid-item-card::
      :img-top: visualization/img/icon/slice_stack.svg

      :doc:`visualization/slicestack`

   .. grid-item-card::
      :img-top: visualization/img/icon/sinogram_viewer.png

      :doc:`visualization/sinogramviewer`

   .. grid-item-card::
      :img-top: visualization/img/icon/volume_viewer.svg

      :doc:`visualization/volumeviewer`

   .. grid-item-card::
      :img-top: visualization/img/icon/nx_tomo_metadata_viewer.svg

      :doc:`visualization/nxtomometadataviewer`

   .. grid-item-card::
      :img-top: visualization/img/icon/data_viewer.png

      :doc:`visualization/dataviewerwidget`

   .. grid-item-card::
      :img-top: visualization/img/icon/radio_stack.svg

      :doc:`visualization/radiostack`

   .. grid-item-card::
      :img-top: visualization/img/icon/sample_moved.svg

      :doc:`visualization/samplemovedwidget`

HDF5 Edition
------------
.. toctree::
   :maxdepth: 2

   edit/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: edit/img/icon/patch_dark_flat.svg

      :doc:`edit/darkflatpatch`

   .. grid-item-card::
      :img-top: edit/img/icon/image_key_editor.svg

      :doc:`edit/imagekeyeditor`

   .. grid-item-card::
      :img-top: edit/img/icon/image_key_upgrader.svg

      :doc:`edit/imagekeyupgrader`

   .. grid-item-card::
      :img-top: edit/img/icon/nx_tomo_editor.svg

      :doc:`edit/nxtomoeditor`            


Data portal
-----------
.. toctree::
   :maxdepth: 2

   data_portal/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: data_portal/img/icon/publish_processed_data.svg

      :doc:`data_portal/publish_processed_data`

Other
-----
.. toctree::
   :maxdepth: 2

   other/index.rst

.. grid:: 5

   .. grid-item-card::
      :img-top: other/img/icon/python_script.svg

      :doc:`other/pythonscript`

   .. grid-item-card::
      :img-top: other/img/icon/tomo_obj_hub.svg

      :doc:`other/tomoobjshub`
