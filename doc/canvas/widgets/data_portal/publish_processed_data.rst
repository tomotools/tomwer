.. _publish_processed_data:

Data Portal Publishing 
======================

This widget is intended to be used after the reconstruction of a Nabu volume.

It will publish the folder containing the Nabu volume to the data portal, along with associated metadata, and link it to the raw data.

When the scan enters the widget, all the 'unlocked' fields will be automatically updated (beamline, proposal, dataset).

If some values cannot be found by Tomwer, the user can still provide them manually.
If the user wants to 'force' certain values, they can provide and lock them. In this case, the values will not be updated.

.. image:: img/publish_processed_data.png
   :width: 600px
   :align: center
