diff frame viewer
=================

.. image:: img/diffviewer.png

Allow comparison of two frames from an acquisition.

Users can also apply a relative or an absolute shift on those images.