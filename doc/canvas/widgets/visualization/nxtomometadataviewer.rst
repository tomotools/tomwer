nxtomo-metadata-viewer
======================

.. image:: img/nxtomometadataviewer.png
   :width: 600px
   :align: center

This widget displays metadata contained in a NXtomo. The layout is exactly the same as the NXtomo editor, except that in this case, you cannot edit any metadata.
