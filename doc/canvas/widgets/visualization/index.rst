Visualization Widgets
======================

.. toctree::
   :maxdepth: 1
   :hidden:

   diffviewer
   slicestack
   sinogramviewer
   volumeviewer
   dataviewerwidget
   radiostack
   samplemovedwidget
   nxtomometadataviewer

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/diff_viewer.svg

      :doc:`diffviewer`

   .. grid-item-card::
      :img-top: img/icon/slice_stack.svg

      :doc:`slicestack`

   .. grid-item-card::
      :img-top: img/icon/sinogram_viewer.png

      :doc:`sinogramviewer`

   .. grid-item-card::
      :img-top: img/icon/volume_viewer.svg

      :doc:`volumeviewer`

   .. grid-item-card::
      :img-top: img/icon/nx_tomo_metadata_viewer.svg

      :doc:`nxtomometadataviewer`

   .. grid-item-card::
      :img-top: img/icon/data_viewer.png

      :doc:`dataviewerwidget`

   .. grid-item-card::
      :img-top: img/icon/radio_stack.svg

      :doc:`radiostack`

   .. grid-item-card::
      :img-top: img/icon/sample_moved.svg

      :doc:`samplemovedwidget`
