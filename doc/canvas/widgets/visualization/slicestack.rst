slice stack
===========

.. image:: img/slicestackwidget.png

This widget memorized all slice received and allow to browse through them.


Demo
----

.. youtube:: 3X9ePy8eSg4
   :width: 500
