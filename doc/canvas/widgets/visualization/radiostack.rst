radio stack
===========

.. image:: img/radiostackwidget.png
   :width: 600px
   :align: center

This widget stores all received radiographs and allows you to browse through them.
