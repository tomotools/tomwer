volume viewer
=============

.. image:: img/volumeviewer.png

Behavior
--------

Displays the last volume reconstructed (from the 'nabu volume reconstruction' widget).
