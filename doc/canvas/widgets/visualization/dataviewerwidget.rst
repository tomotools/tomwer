data viewer
===========

.. image:: img/dataviewer.png

Allow browsing through data (raw or normalized radios) and reconstructed slices.
