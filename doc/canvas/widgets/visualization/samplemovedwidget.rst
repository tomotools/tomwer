sample moved
============

.. image:: img/samplemoved.png

A visualization widget used to display specific radiographs to help determine if the sample moved during an acquisition.

Behavior
--------

* Shows two images side by side.
* If possible, determines the angle of the radiograph.
* Each plot can display the image or its symmetry along the x-axis.
