sinogram viewer
===============

This widget can be used to compute and display a specific sinogram from an acquisition.

.. image:: img/sinogramviewer.png

Interface
---------

You can select a specific line to compute the sinogram.

You can also define a subsampling factor to speed up the computation. In this case, only one line per n projections will be read.

Since computation time can be costly, you must validate the parameters each time you change them to trigger the creation of the sinogram.
