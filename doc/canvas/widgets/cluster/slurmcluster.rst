slurm cluster
=============

This widget allows you to configure a Slurm cluster to be used.

.. image:: img/slurm_cluster.png
   :width: 800px
   :align: center

This will trigger a :ref:`cluster_config object` object when modified.
