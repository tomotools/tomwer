future supervisor
=================

This widget allows you to supervise 'future' jobs.

.. image:: img/slurm_supervisor.png
   :width: 800px
   :align: center

These are jobs running remotely (on a Slurm cluster, for example).

All processes launching a job will generate one line per scan if remote processing is requested.

You can filter jobs by their status using the 'filters group'. From it, you can choose to display only pending or canceled jobs, for example.

Conversion
''''''''''

This widget can also trigger conversion from :ref:`future_data object` to :ref:`data object`.

* Automatically: **if the 'convert back to data when finished'** option is activated.
* Manually: by selecting lines to convert and clicking the **convert selected** button.

This allows you to retrieve a :ref:`data object` and continue processing (to visualize results, etc.).
