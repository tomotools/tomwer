Cluster Widgets
===============

.. toctree::
   :maxdepth: 1
   :hidden:

   futuresupervisor
   slurmcluster

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/slurm_cluster.svg

      :doc:`slurmcluster`

   .. grid-item-card::
      :img-top: img/icon/future_supervisor.svg

      :doc:`futuresupervisor`
