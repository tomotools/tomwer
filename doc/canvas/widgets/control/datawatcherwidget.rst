scan watcher
============

.. image:: img/datawatcher.png

This widget observes a folder and its subfolders for a given path, waiting for acquisitions to finish.
The widget will wait indefinitely until an acquisition is completed.
Once an acquisition is finished, a signal containing the folder path is emitted.

* **'Select folder'**: Opens a dialog to select the top-level directory to observe. 
* **'Start observation'**: Starts the observation. 
* **'Stop observation'**: Stops the observation. 

Environment variable
--------------------

The widget will automatically set the value of `DATADIR` if such an environment variable exists.

.. note::

    Internally, the observation is run from a thread managed by `ftseries`.
