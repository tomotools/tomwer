single tomo obj
===============

.. image:: img/single_tomo_obj.png

This widget allows users to define a single tomo object (a volume or scan) and use it directly.

Users can copy/paste a file path into the `QLineEdit`. This will trigger a search for objects from this file (or folder).

Once the editing is finished and if the identifier is valid, it will trigger the associated object (a volume or a scan).
