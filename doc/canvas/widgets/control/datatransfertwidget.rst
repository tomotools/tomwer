data transfer
=============

This widget ensures the data transfer of the received data to the specified directory.
To change the output directory, press the 'select output folder' button.
For now, the output directory is automatically computed.

Environment variable
''''''''''''''''''''

The output directory will be automatically set to `TARGET_OUTPUT_FOLDER` if the environment variable exists.
