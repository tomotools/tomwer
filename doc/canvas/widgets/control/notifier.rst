notifier
========

.. image:: img/notifier.png

The notifier will pop up and emit a ringtone for a few seconds when receiving an object.

Once the object is received, the next widget(s) will be triggered immediately (without waiting for the pop-up to be closed).
