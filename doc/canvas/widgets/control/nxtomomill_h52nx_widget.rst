nxtomomill h52nx (bliss-HDF5)
=============================

.. image:: img/nxtomomill.png

Behavior
--------

This widget allows loading data from bliss-HDF5 and converting them to Nexus (NXTomo) format, still using the HDF5 format.

.. note::

    The datalistener will also make a direct call to nxtomomill `h52nx`


To add new file(s) to be converted you can either:

* click on the "Add" button. This will open a dialog and allow you to select the file you want.
* drag / drop the file you want to convert

    .. image:: img/nxtomomill/drag_drop_visa.gif

* copy / paste a list of files as a string (result of a `ls` command for example)
