scan validator
==============

.. image:: img/datavalidator.png

This widget displays the results of a reconstruction and asks the user whether they want to validate the reconstruction.
The user can also request modifications to the reconstruction parameters.

Behavior
''''''''

Buttons
*******

* **'Change reconstruction parameters'**: Sends a signal to the `ftseries` widget and asks the user for new reconstruction parameters (then runs the reconstruction again).
* **'Validate'**: Sends a signal to the next connected boxes indicating that this data is okay for the next operations.

This inherits from [ImageStackViewerWidget](imagestackviewer.html).
