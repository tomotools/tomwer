timer
=====

.. image:: img/timerwidget.png

This widget allows postponing the treatment of a scan to the next session by specifying a time.
