scan filter
===========

.. image:: img/filterwidget.png

This widget allows filtering of certain widgets according to the name of the scan folder.
