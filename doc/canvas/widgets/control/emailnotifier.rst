email notifier
==============

This widget allows you to send a notification via email when a `tomo object` (either a scan or a volume) is encountered.

The email text and subject can use predefined keywords provided as `{keyword}`. Here is the list of keywords:

* `{tomo_obj_short_id}`: The short identifier of the tomo object.
* `{tomo_obj_id}`: The full ID of the tomo object.
* `{ls_tomo_obj}`: Output of the `ls` command on the scan folder.
* `{timestamp}`: The current time.
* `{footnote}`: A footnote defined by tomwer.
* `{dataset_processing_states}`: A list of dataset states regarding the encountered processes.

.. image:: img/email_notification/email_compose.png

.. image:: img/email_notification/email_settings.png

.. note:: This widget is designed to work within ESRF and is expected to fail outside.
