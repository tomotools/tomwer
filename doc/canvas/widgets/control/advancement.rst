advancement
===========

.. image:: img/advancement.png

This widget displays the advancement of the dataset versus processes.
This interface is also available from the canvas menu: "view" -> "object supervisor."

Some control options are available via the right-click menu:

On the dataset name (first column):
* `clear`: clears the dataset line
* `copy`: copies the full dataset identifier

On the table 'dataset vs process' cells:
* `info`: provides extra information about the processing of this dataset. For example, for nabu, it will contain the output log.
* `cancel`: cancels the processing. Warning: not all processes can be canceled. If a process cannot be canceled, a message will be displayed.
* `reprocess`: reprocesses the dataset.
