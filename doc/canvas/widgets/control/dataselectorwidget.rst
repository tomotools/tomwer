scan selector
=============

.. image:: img/scanselectorwidget.png

Lists all received scans and allows the user to select a specific one to be moved to the next boxes.
