series of objects
=================

This widget allows you to define a series of objects and provides input to the 'series' input, such as for stitching or advanced correction algorithms.
