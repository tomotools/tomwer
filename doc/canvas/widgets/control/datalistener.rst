.. _data listener:

scan listener
=============

.. image:: img/datalistener/datalistener_control.png
   :width: 400px
   :align: center

The widget, once activated, will listen to the Bliss/Tango server for **HDF5** acquisitions.

When an acquisition is finished, it will trigger the conversion from HDF5 to the generic 'NXTomo' format.
Then this HDF5 scan will be pushed and processed by connected nodes.

control window
--------------

.. image:: img/datalistener/datalistener_control.png
   :width: 400px
   :align: center

From this window, you can start or stop listening to the Bliss/Tango server.

Warning: Listening should be started at the beginning of the acquisition; otherwise, it won't receive the initial information about the acquisition creation.

observations window
-------------------

.. image:: img/datalistener/datalistener_observation.png
   :width: 800px
   :align: center

This interface lists all the acquisitions that the listener is currently aware of.

It also tracks the known progress of each acquisition.

settings window
---------------

.. image:: img/datalistener/datalistener_settings.png
   :width: 600px
   :align: center

This widget can be used to configure the acquisition settings.

The listener needs to know the name of the beamline, the port used for the Tango server, and the name of the Bliss session.

history window
--------------

.. image:: img/datalistener/datalistener_history.png
   :width: 400px
   :align: center

Here you can find the 20 most recent successful acquisitions.
