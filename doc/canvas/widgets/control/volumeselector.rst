volume selector
===============

This widget helps the user define a volume.
The defined volume can then be used as a 'volume' input by other widgets such as the volume viewer, cast-volume, stitching, etc.
