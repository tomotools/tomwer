.. _NXtomoConcatenateOW:

NXtomo concatenation
====================

.. image:: img/nxtomoconcatenate/nxtomo_concatenate_workflow.png
   :width: 400px
   :align: center

This widget allows you to concatenate several NXtomo (also known as NXtomoScan) into a single NXtomo.

Options
-------

.. image:: img/nxtomoconcatenate/nxtomo_concatenate_interface.png
   :width: 400px
   :align: center

From this window, you can configure the output file for the NXtomo, including the data path where the NXtomo will be saved.

You can also choose to overwrite the entry if it already exists.

Tutorial
--------

`See the video explaining how it can be used. <http://www.edna-site.org/pub/doc/tomwer/video/canvas/helical/concatenate_nxtomos.mp4>`_
