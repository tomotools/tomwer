Control Widgets
===============

.. toctree::
   :maxdepth: 1
   :hidden:

   advancement
   datadiscovery
   datalistener
   dataselectorwidget
   datatransfertwidget
   datavalidatorwidget
   datawatcherwidget
   emailnotifier
   notifier
   filterwidget
   nxtomoconcatenate
   nxtomomill_edf2nx_widget
   nxtomomill_h52nx_widget
   reducedarkflatselectorwidget
   seriesofobjects
   singletomoobj
   timerwidget
   volumeselector

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/advancement.svg

      :doc:`advancement`

   .. grid-item-card::
      :img-top: img/icon/datadiscovery.svg

      :doc:`datadiscovery`

   .. grid-item-card::
      :img-top: img/icon/datalistener.svg

      :doc:`datalistener`

   .. grid-item-card::
      :img-top: img/icon/dataselector.svg

      :doc:`dataselectorwidget`

   .. grid-item-card::
      :img-top: img/icon/datatransfer.svg

      :doc:`datatransfertwidget`

   .. grid-item-card::
      :img-top: img/icon/datavalidator.png

      :doc:`datavalidatorwidget`

   .. grid-item-card::
      :img-top: img/icon/datawatcher.svg

      :doc:`datawatcherwidget`

   .. grid-item-card::
      :img-top: img/icon/emailnotifier.svg

      :doc:`emailnotifier`

   .. grid-item-card::
      :img-top: img/icon/notifier.svg

      :doc:`notifier`

   .. grid-item-card::
      :img-top: img/icon/nxconcatenate.svg

      :doc:`nxtomoconcatenate`

   .. grid-item-card::
      :img-top: img/icon/edf2nx.svg

      :doc:`nxtomomill_edf2nx_widget`

   .. grid-item-card::
      :img-top: img/icon/h52nx.svg

      :doc:`nxtomomill_h52nx_widget`

   .. grid-item-card::
      :img-top: img/icon/filter.svg

      :doc:`filterwidget`

   .. grid-item-card::
      :img-top: img/icon/reducedarkflatselector.svg

      :doc:`reducedarkflatselectorwidget`

   .. grid-item-card::
      :img-top: img/icon/singletomoobj.svg

      :doc:`singletomoobj`

   .. grid-item-card::
      :img-top: img/icon/timer.png

      :doc:`timerwidget`

   .. grid-item-card::
      :img-top: img/icon/seriesofobjects.svg

      :doc:`seriesofobjects`

   .. grid-item-card::
      :img-top: img/icon/volumeselector.svg

      :doc:`volumeselector`
