scan discovery
==============

.. image:: img/scandiscovery.png

This widget can be used to browse a folder and sub-folder for scans.
Possible scan types are NXtomo (default), bliss-hdf5, and spec-edf.
Each scan found will be sent to the downstream workflow.
It looks similar to the DataWatcher, but it is very different:

* It will not check for any fields or attributes to validate that the scan is finished. As a consequence, it does not create an 'observer' for each possible scan, waiting for them to be finished.
* It runs once and stops. There is no cache mechanism to ensure each scan is sent at most once.

.. note:: The bliss-hdf5 scan will need to be converted to NXtomo using the h52nx-nxtomomill widget to be processed because this type of scan is not handled natively by the other bricks.
