reduce dark-flat selector
=========================

.. image:: img/reduce_dark_flat_selector/interface.png

Behavior
--------

This widget allows the user to collect and display several reduced darks/flats and select the one(s) they want to use for the reconstruction.

Reduced darks and flats can be collected from the following widgets:

* **Reduced darks and flats**:
  From the 'reduced dark(s)' and 'reduced flat(s)' channels. These are triggered each time new reduced darks/reduced flats are computed.
* **Single tomo obj**:
  If the tomo obj is a scan **and** it already has some reduced darks or flats computed, the channels 'reduced dark(s)' and 'reduced flat(s)' will be automatically triggered on construction.
* **Python script**:
  You can export the 'configuration' output as a dictionary and connect it either to 'reduced dark(s)' or 'reduced flat(s)'.

.. image:: img/reduce_dark_flat_selector/workflow_example.png
