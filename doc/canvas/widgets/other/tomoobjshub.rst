Tomo objs hub
=============

Some useful widgets for users when writing Python scripts.

Currently, from the 'Python script' widget, we cannot trigger tomo-obj one by one. However, this can be done using this widget.

.. image:: img/tomoobjshub_example.png
