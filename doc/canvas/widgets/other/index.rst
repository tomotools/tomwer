Other Widgets
=============

.. toctree::
   :maxdepth: 1
   :hidden:

   pythonscript
   tomoobjshub

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/python_script.svg

      :doc:`pythonscript`

   .. grid-item-card::
      :img-top: img/icon/tomo_obj_hub.svg

      :doc:`tomoobjshub`
