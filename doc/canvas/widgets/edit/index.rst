HDF5 Edition Widgets - NXTomo
=============================

.. toctree::
   :maxdepth: 1
   :hidden:

   darkflatpatch
   imagekeyeditor
   imagekeyupgrader
   nxtomoeditor

.. grid:: 5

   .. grid-item-card::
      :img-top: img/icon/patch_dark_flat.svg

      :doc:`darkflatpatch`

   .. grid-item-card::
      :img-top: img/icon/image_key_editor.svg

      :doc:`imagekeyeditor`

   .. grid-item-card::
      :img-top: img/icon/image_key_upgrader.svg

      :doc:`imagekeyupgrader`

   .. grid-item-card::
      :img-top: img/icon/nx_tomo_editor.svg

      :doc:`nxtomoeditor`            

