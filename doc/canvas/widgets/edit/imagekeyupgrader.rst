image-key-upgrader
==================

.. image:: img/image_key_upgrader.png
   :width: 600px
   :align: center

The *image-key-upgrader* widget allows the user to modify **all frames** with a specific `image_key` type (dark field, flat field, invalid, or projections) to another type.

While the *image-key-editor* lets the user select frames by indices, this widget selects the frames to be modified directly based on their `image_key`.

.. warning:: This widget is specifically dedicated to NXtomo (also known as NXtomoScan internally).
