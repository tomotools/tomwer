nxtomo-editor
=============

.. image:: img/nxtomo-editor.png
   :width: 600px
   :align: center

The *nxtomo-editor* widget allows the user to modify scalar fields.

This provides a way to make minor modifications to a NXtomo when needed.

An 'automation' of the editing process can be done using the 'lock' in the last column. There is one lock per field that can be automated.

If some fields are locked, then when a new NXtomo is loaded for editing:

   * Locked fields will not be updated (based on the NXtomo value).
   * The dialog will not pop up or be raised.
   * If at least one field is locked, the incoming NXtomo will be updated and trigger downstream processing.

.. note:: x_translation and y_translation can be edited. For stitching, for example, it can be useful to provide these values along with pixel size to facilitate the detection of overlaps.
    However, only scalar values can be provided. If you have non-scalar values, you will need to edit them another way (e.g., using a Python widget or script).

How-to videos
-------------

The interface is the same as the widget from the Orange Canvas. Once you launch the GUI as a CLI with `nxtomo-editor my_nx.nx`, you can perform the same operations.

.. youtube:: KwwCYXnP3YI
   :width: 500
