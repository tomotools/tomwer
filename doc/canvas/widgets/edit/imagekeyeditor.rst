image-key-editor
================

.. image:: img/image_key_editor.png
   :width: 600px
   :align: center

The *image-key-editor* application provides a tool to modify the frame type of an NXTomo entry.

Please see the `video for editing image keys <https://youtu.be/4yrEBrAmwtY>`_.

This is essentially the graphical extension of a feature from nxtomomill: `patch-nx <https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/patch_nx.html>`_.

.. warning:: This only manages HDF5 files.
