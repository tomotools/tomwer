dark-flat-patch
===============

.. image:: img/dark_flat_patch_widget.png
   :width: 500px
   :align: center

The *dark-ref-patch* widget allows the user to include a series of dark and/or flat images at the beginning and/or end of an acquisition.

This will update the NXtomo entry of the acquisition.

.. warning:: This only manages HDF5 files.

.. warning:: If a series of darks/flats already exists, it will be added to the existing one without any invalidation. If you need to invalidate some frames first, please use the image-key-editor beforehand.
