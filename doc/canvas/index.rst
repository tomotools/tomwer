.. _canvas:

Canvas
======

The canvas is based on the `Orange Canvas <https://github.com/biolab/orange3>`_ allowing end-users to define their workflow for tomography data processing.
Each task in the workflow is an `ewoks task <https://ewoks.readthedocs.io/en/latest>`_, enabling the connection of tomography tasks together and with other techniques.

.. toctree::
   :maxdepth: 3
   :hidden:

   objects
   slurm_cluster
   example_future_scan_object
   cancel_task
   widgets/widgets.rst

Control
-------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/control/img/icon/advancement.svg

      :doc:`widgets/control/advancement`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/datadiscovery.svg

      :doc:`widgets/control/datadiscovery`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/datalistener.svg

      :doc:`widgets/control/datalistener`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/dataselector.svg

      :doc:`widgets/control/dataselectorwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/datatransfer.svg

      :doc:`widgets/control/datatransfertwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/datavalidator.png

      :doc:`widgets/control/datatransfertwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/datawatcher.svg

      :doc:`widgets/control/datawatcherwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/emailnotifier.svg

      :doc:`widgets/control/emailnotifier`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/notifier.svg

      :doc:`widgets/control/notifier`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/nxconcatenate.svg

      :doc:`widgets/control/nxtomoconcatenate`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/edf2nx.svg

      :doc:`widgets/control/nxtomomill_edf2nx_widget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/h52nx.svg

      :doc:`widgets/control/nxtomomill_h52nx_widget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/filter.svg

      :doc:`widgets/control/filterwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/reducedarkflatselector.svg

      :doc:`widgets/control/reducedarkflatselectorwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/singletomoobj.svg

      :doc:`widgets/control/singletomoobj`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/timer.png

      :doc:`widgets/control/timerwidget`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/seriesofobjects.svg

      :doc:`widgets/control/seriesofobjects`

   .. grid-item-card::
      :img-top: widgets/control/img/icon/volumeselector.svg

      :doc:`widgets/control/volumeselector`

Reconstruction
--------------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/cor.svg

      :doc:`widgets/reconstruction/corwidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/reduce_dark_flat.svg

      :doc:`widgets/reconstruction/darkflatwidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/nabu_slice.svg

      :doc:`widgets/reconstruction/nabuslicewidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/nabu_volume.svg

      :doc:`widgets/reconstruction/nabuvolumewidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/multi_cor.svg

      :doc:`widgets/reconstruction/multicorwidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/multi_pag.svg

      :doc:`widgets/reconstruction/multipagwidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/nabu_cast.svg

      :doc:`widgets/reconstruction/castvolumewidget`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/intensity_normalization.svg

      :doc:`widgets/reconstruction/intensitynormalization`

   .. grid-item-card::
      :img-top: widgets/reconstruction/img/icon/nabu_prepare_weights_double.svg

      :doc:`widgets/reconstruction/helicalprepareweightsdouble`

Cluster
-------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/cluster/img/icon/slurm_cluster.svg

      :doc:`widgets/cluster/futuresupervisor`

   .. grid-item-card::
      :img-top: widgets/cluster/img/icon/future_supervisor.svg

      :doc:`widgets/cluster/futuresupervisor`


Visualization
-------------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/diff_viewer.svg

      :doc:`widgets/visualization/diffviewer`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/slice_stack.svg

      :doc:`widgets/visualization/slicestack`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/sinogram_viewer.png

      :doc:`widgets/visualization/sinogramviewer`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/volume_viewer.svg

      :doc:`widgets/visualization/volumeviewer`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/nx_tomo_metadata_viewer.svg

      :doc:`widgets/visualization/nxtomometadataviewer`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/data_viewer.png

      :doc:`widgets/visualization/dataviewerwidget`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/radio_stack.svg

      :doc:`widgets/visualization/radiostack`

   .. grid-item-card::
      :img-top: widgets/visualization/img/icon/sample_moved.svg

      :doc:`widgets/visualization/samplemovedwidget`

HDF5 Edition - NXTomo
---------------------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/edit/img/icon/patch_dark_flat.svg

      :doc:`widgets/edit/darkflatpatch`

   .. grid-item-card::
      :img-top: widgets/edit/img/icon/image_key_editor.svg

      :doc:`widgets/edit/imagekeyeditor`

   .. grid-item-card::
      :img-top: widgets/edit/img/icon/image_key_upgrader.svg

      :doc:`widgets/edit/imagekeyupgrader`

   .. grid-item-card::
      :img-top: widgets/edit/img/icon/nx_tomo_editor.svg

      :doc:`widgets/edit/nxtomoeditor`            


Data portal
-----------

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/data_portal/img/icon/publish_processed_data.svg

      :doc:`widgets/data_portal/publish_processed_data`


Other
-----

.. grid:: 5

   .. grid-item-card::
      :img-top: widgets/other/img/icon/python_script.svg

      :doc:`widgets/other/pythonscript`

   .. grid-item-card::
      :img-top: widgets/other/img/icon/tomo_obj_hub.svg

      :doc:`widgets/other/tomoobjshub`
