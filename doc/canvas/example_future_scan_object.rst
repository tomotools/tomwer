.. _example of a future_scan creation:

Example of a Future Scan Creation in a Python Widget
====================================================

This is the simplest script you can create to generate a `FutureTomwerScan` from a Python script.

.. code-block:: python

    from distributed import Client
    from tomwer.core.cluster import SlurmClusterManager
    from tomwer.core.scan.futurescan import FutureTomwerScan

    cluster_configuration = in_configuration
    # create a cluster
    cluster = SlurmClusterManager().get_cluster(
        cluster_configuration,
        project_name="my awesome project",
    )


    def my_pprint(*args):
        print(args)


    # submit job
    future_slurm_job = client.submit(
        my_pprint,
        "this",
        "will",
        "be",
        "printed",
    )

    # create tomwer future scan
    # note: once the future scan is done, it can be converted back to tomwer
    future_data_out = FutureTomwerScan(
        scan=in_data,
        futures=[
            future_slurm_job,
        ],
    )


.. warning:: The Python widget should be fed by both a :ref:`data object` and a :ref:`cluster_config object`.
