tomwer image-key-upgrader
=========================

Purpose
-------

The *image-key-upgrader* application provides a tool to modify a frame key type (e.g., projections, darks) to another type.

.. warning:: This only handles NXtomo.

.. code-block:: bash

    tomwer image-key-upgrader my_nexus_file nxtomo_entry

.. image:: img/image_key_upgrader.png
   :width: 600px
   :align: center

Usage
-----

::

    usage: tomwer image-key-upgrader [-h] scan_path entry

    positional arguments:
    scan_path   NXtomo file to edit.
    entry       Entry to process.

    options:
    -h, --help  Show this help message and exit.
