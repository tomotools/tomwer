.. _stand_alone_applications:

Other Applications
==================

Several applications are bundled with tomwer.

To launch an application, simply use:

   .. code-block:: bash

      tomwer [application_name] [application parameters]

You can access general application help using:

   .. code-block:: bash

      tomwer help

Or access specific help for an application by using:

   .. code-block:: bash

      tomwer [application_name] --help

For example:

   .. code-block:: bash

      tomwer cor --help

Here are all the applications available:

.. toctree::
   :maxdepth: 1
   :hidden:

   cor.rst
   canvas.rst
   darkflatpatch.rst
   diffframe.rst
   imagekeyeditor.rst
   imagekeyupgrader.rst
   nabu.rst
   nxtomoeditor.rst
   radiostack.rst
   multicor.rst
   multipag.rst
   reducedarkflat.rst
   samplemoved.rst
   scanviewer.rst
   sinogramviewer.rst
   slicestack.rst
   stopdatalistener.rst
   z_stitching.rst

.. grid:: 4

   .. grid-item-card::
      :img-top: img/app_icons/cor.svg

      :doc:`cor`

   .. grid-item-card::
      :img-top: img/app_icons/canvas.png

      :doc:`canvas`

   .. grid-item-card::
      :img-top: img/app_icons/darkflat.svg

      :doc:`reducedarkflat`

   .. grid-item-card::
      :img-top: img/app_icons/patch_dark_flat.svg

      :doc:`darkflatpatch`

   .. grid-item-card::
      :img-top: img/app_icons/diffframe.svg

      :doc:`diffframe`

   .. grid-item-card::
      :img-top: img/app_icons/image_key_editor.svg

      :doc:`imagekeyeditor`

   .. grid-item-card::
      :img-top: img/app_icons/image_key_upgrader.svg

      :doc:`imagekeyupgrader`
                            
   .. grid-item-card::
      :img-top: img/app_icons/nabu.svg

      :doc:`nabu`

   .. grid-item-card::
      :img-top: img/app_icons/nx_tomo_editor.svg

      :doc:`nxtomoeditor`

   .. grid-item-card::
      :img-top: img/app_icons/radiosstack.svg

      :doc:`radiostack`

   .. grid-item-card::
      :img-top: img/app_icons/multicor.svg

      :doc:`multicor`

   .. grid-item-card::
      :img-top: img/app_icons/multipag.svg

      :doc:`multipag`

   .. grid-item-card::
      :img-top: img/app_icons/sample_moved.svg

      :doc:`samplemoved`

   .. grid-item-card::
      :img-top: img/app_icons/scan_viewer.svg

      :doc:`scanviewer`
      
   .. grid-item-card::
      :img-top: img/app_icons/sinogramviewer.png

      :doc:`sinogramviewer`

   .. grid-item-card::
      :img-top: img/app_icons/slicesstack.svg

      :doc:`slicestack`

   .. grid-item-card::
      :img-top: img/app_icons/bash_icon.svg

      :doc:`stopdatalistener`

   .. grid-item-card::
      :img-top: img/app_icons/z_stitching.svg

      :doc:`z_stitching`

.. note::

    All applications with silx plot (those plotting radiographs, slices, etc.) have two backends: matplotlib and OpenGL (`--use-opengl-plot` option).
    The OpenGL backend speeds up display when you start having a large number of pixels to display.

    OpenGL comes with some constraints. See :ref:`opengl_display` for more details.
