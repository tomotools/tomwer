tomwer reduce-dark-flat
=======================

Purpose
-------

Application to compute reduced darks and flats for a given scan


Usage
-----

::

    tomwer reduce-dark-flat [-h] [scan_path [scan_path ...]]

Options
-------

::

  -h, --help            show this help message and exit
  --entry ENTRY         an entry can be specify in case of hdf5 the master file
  --dark-method DARK_METHOD
                        Define the method to be used for computing dark
  --flat-method FLAT_METHOD
                        Define the method to be used for computing flat
  --no-gui              Will run directly the dark and ref without any interaction
  --overwrite           Overwrite dark/flats if exists
  --debug               Set logging system in debug mode
