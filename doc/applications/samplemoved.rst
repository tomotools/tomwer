tomwer samplemoved
==================

Purpose
-------

The *tomwer samplemoved* command is provided to help the user determine if a sample moved during an acquisition.

.. image:: ../img/sampleMoved.png
   :width: 800px
   :align: center

Usage
-----

::

    tomwer samplemoved [-h] [scan_path [scan_path ...]]

Options
-------

::

  scan_path             Path to the acquisition folder to observe.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

::

    tomwer samplemoved myscan/
