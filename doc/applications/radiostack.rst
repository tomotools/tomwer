tomwer radio-stack
==================

Purpose
-------

The *tomwer radio-stack* command is used to view all existing slices under a root directory.
It will parse all subfolders for existing reconstructions and store them into a browsable stack.
Slices will be sorted first by folder, then by slice index.

.. image:: img/radiostackwidget.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer radio-stack [-h] [scan_path [scan_path ...]]

Options
-------

::

  scan_path             Path of the root folder.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

::

    tomwer radio-stack my_scan.nx
