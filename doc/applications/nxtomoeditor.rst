tomwer nxtomo-editor
====================

Purpose
-------

The *tomwer nxtomo-editor* allows users to edit some of the NXtomo metadata.

When you launch the application, available fields (such as energy, pixel size, etc.) will be loaded.

Users can then edit those fields and overwrite them in the NXtomo by clicking the `validate` button.

.. image:: img/nxtomo-editor.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer nxtomo-editor [-h] [--debug] scan_path entry

Options
-------

::

  scan_path             Nexus file (HDF5 file) to edit.

  entry                 Path to the file where the NXtomo starts. Usually named 'entry', or 'entryXXXX'.

  debug                 Activate debug logs.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer nxtomo-editor nexus_file.nx entry0000

How-to Videos
-------------

.. youtube:: KwwCYXnP3YI
   :width: 500
