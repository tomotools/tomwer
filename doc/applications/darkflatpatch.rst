tomwer dark-ref-patch
=====================

Purpose
-------

The *dark-ref-patch* application allows the user to include a series of dark and/or flat fields at the end and/or the beginning of an acquisition.

This will update the NXtomo entry of the acquisition.

This is essentially the graphical extension of a feature from nxtomomill `patch-nx <https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/patch_nx.html>`_.

.. warning:: This only manages HDF5 files.

.. warning:: If a series of dark/flat fields already exists, the new ones will be added to the existing series. No invalidation is performed. So if you need to invalidate some frames, please use the image-key-editor first.

.. code-block:: bash

    tomwer dark-ref-patch my_nexus_file nxtomo_entry

.. image:: img/dark_ref_patch.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer dark-ref-patch nexus_file entry [-h]