tomwer image-key-editor
=======================

Purpose
-------

The *image-key-editor* application provides a tool to modify the frame type of an NXtomo entry.
This is essentially the graphical extension of a feature from nxtomomill `patch-nx <https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/patch_nx.html>`_.

.. warning:: This only handles NXtomo.

.. code-block:: bash

    tomwer image-key-editor my_nexus_file nxtomo_entry

.. image:: img/image_key_editor.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer image-key-editor nexus_file entry [-h]
