tomwer multi-cor
================

Purpose
-------

The *tomwer multi-cor* command provides an interface to reconstruct a slice over a range of centers of rotation and compute a score for each to determine the best center of rotation.

Usage
-----

::

    tomwer multi-cor [-h] [--entry ENTRY] [--debug] [--read-existing] [--dump-roi] scan_path

Options
-------

::

  scan_path             For EDF acquisition: provide the folder path. For HDF5/Nexus: provide the master file.

  entry                 For Nexus files: entry in the master file.

  read-existing         Load the latest multi-pag processing if dedicated folder (multipag_results) exists

  debug                 Activate debug logs.

  dump-roi              Save the region of interest (ROI) where the score is computed to the .hdf5 file (for debugging purposes).

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer multi-cor nexus_file.nx --entry entry0000

How-to Videos
-------------

.. youtube:: ezKjbpmA2FQ
   :width: 500
