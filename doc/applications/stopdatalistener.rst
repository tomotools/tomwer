tomwer stop-data-listener
=========================

Purpose
-------

Send a SIGTERM or SIGKILL signal to the process occupying the RPC-server port. The process must have been launched by tomwer and from the same user.

Usage
-----

::

    tomwer stop-data-listener [-h] [--port port] [--sigkill]

Options
-------

::

  port             Port of the RPC-server if it differs from the tomwer settings (this might be needed if the port changes over time).

  sigkill          Send a SIGKILL signal instead of a SIGTERM.

  -h, --help       Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer stop-data-listener

    tomwer stop-data-listener --sigkill
