tomwer diff-frame
=================

Purpose
-------

Tool to compare two frames from an acquisition.

.. image:: img/diffframewidget.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer diff-frame scan_path [-h] [--entry]

Options
-------

::

  scan-path             Path to the acquisition folder. Can be a folder for EDF acquisition or a master file for HDF5.

  entry                 Entry for .hdf5 files.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

::

    tomwer diff-frame --scan-path path_to_my_scan/
    tomwer diff-frame --scan-path path_to_my_scan/ --entry entry0000
