tomwer z-stitching
==================

Purpose
-------

Open an interface to define and launch stitching along the z-axis (also known as axis 2).
Stitching can be done on projections (Nxtomo) or reconstructed volumes.

CLI
---

.. code-block:: bash

    usage: tomwer z-stitching [-h] [--debug] [--config CONFIG_FILE] [--use-opengl-plot] [tomo_objs ...]

    positional arguments:
    tomo_objs             All tomo objects to be stitched together.

    options:
    -h, --help            Show this help message and exit.
    --debug               Set the logging system to debug mode.
    --config CONFIG_FILE, --config-file CONFIG_FILE
                          Provide a stitching configuration file to load parameters from.
    --use-opengl-plot     Use OpenGL for plots (instead of matplotlib).

.. image:: img/z_stitching.png
   :width: 500px
   :align: center

Examples of Usage
-----------------

.. code-block:: bash

    tomwer z-stitching dataset000?.nx

    tomwer z-stitching vol*.hdf5

How-to Videos
-------------

.. youtube:: Yhrde42Xxxs
   :width: 500
