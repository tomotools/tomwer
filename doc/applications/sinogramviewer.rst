tomwer sinogram-viewer
======================

Purpose
-------

The *tomwer sinogram-viewer* provides a graphical user interface to display a sinogram.

.. image:: img/sinogramviewer.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer sinogram-viewer [-h] [--line LINE] [--subsampling SUBSAMPLING] scan_path [--entry ENTRY]

Options
-------

::

  scan-path             Path to the acquisition folder. Can be a folder for EDF acquisition or a master file for HDF5.

  entry                 Entry for .hdf5 files.

  line                  Line to extract from radiographs to create the sinogram. Defaults to the middle of the projections.

  subsampling           Define a subsampling rate to generate the sinogram in order to speed up its creation.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer sinogram-viewer --scan-path path_to_my_scan/ --entry entry0000

    tomwer sinogram-viewer --scan-path path_to_my_scan/
