tomwer multi-pag
================

Purpose
-------

The *tomwer multi-pag* command provides an interface to reconstruct a slice over a range of delta/beta values and ultimately select the most appropriate value.

Usage
-----

::

    tomwer multi-pag [-h] [--entry ENTRY] [--debug] [--read-existing] [--dump-roi] scan_path

Options
-------

::

  scan_path             For EDF acquisition: provide the folder path. For HDF5/Nexus: provide the master file.

  entry                 For Nexus files: entry in the master file.

  read-existing         Load the latest sa-delta-beta processing from *_tomwer_processes.h5 if it exists.

  debug                 Activate debug logs.

  dump-roi              Save the region of interest (ROI) where the score is computed to the .hdf5 file (for debugging purposes).

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer multi-pag nexus_file.nx --entry entry0000
