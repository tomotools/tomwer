tomwer scan-viewer
==================

Purpose
-------

The *tomwer scan-viewer* provides a graphical user interface to browse a scan. From it, you can access projections, darks, flats, and reconstructed slices.
Projections can be displayed in their normalized form or unprocessed.

.. image:: img/scanviewer.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer scan-viewer [-h] scan_path [entry]

Options
-------

::

  scan-path             Path to the acquisition folder. Can be a folder for EDF acquisition or a master file for HDF5.

  entry                 Entry for .hdf5 / Nexus files.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

.. code-block:: bash

    tomwer scan-viewer --scan-path my_file.nx --entry entry0000

    tomwer scan-viewer --scan-path path_to_my_scan/
