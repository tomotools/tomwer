tomwer nabu
===========

Purpose
-------

The *tomwer nabu* provides a graphical user interface to call nabu.

.. image:: img/nabuwidget.png
   :width: 600px
   :align: center

Usage
-----

::

    tomwer nabu [-h] [--scan-path scan_path --output-dir --dry-run --debug]

Options
-------

::

  scan-path             Path to the acquisition folder. Can be a folder for EDF acquisition or a master file for HDF5.

  entry                 Entry for .hdf5 files.

  output-dir            Directory where the reconstructions will be saved.

  debug                 Activate debug logs.

  dry-run               Skip the call to nabu but create the .conf files.

  -h, --help            Show this help message and exit.

Examples of Usage
-----------------

::

    tomwer nabu --scan-path path_to_my_scan/
    tomwer nabu --scan-path path_to_my_scan/ --entry entry0000
