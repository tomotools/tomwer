tomwer canvas
=============

Purpose
-------

The *tomwer canvas* command launches the canvas to define and execute workflows.

.. image:: img/canvas.png
   :width: 600px
   :align: center


Usage
-----

::

    tomwer canvas [-h] [workflow-file] [--use-opengl-plot]

Options
-------

::

  workflow-file          Path to an already defined workflow.

  --use-opengl-plot      Use OpenGL backend for silx plots.

  -h, --help             Show this help message and exit.

  --no-discovery         Don't run widget discovery (use full cache instead).

  --force-discovery      Force full widget discovery (invalidate cache).

  --load-widget-settings Load widget settings stored.

  --no-welcome           Don't show the welcome dialog.

  --no-splash            Don't show the splash screen.

  -l LOG_LEVEL, --log-level=LOG_LEVEL
                         Set the logging level (0, 1, 2, 3, 4).

  --style=STYLE          Set the QStyle to use.

  --stylesheet=STYLESHEET
                         Application-level CSS stylesheet to use.

  --qt=QT                Additional arguments for QApplication.

  --all-addon            Display only add-on widgets.

  --no-color-stdout-logs, --no-colored-logs
                         Instead of having logs in the log view, color logs for
                         stdout.

Examples of Usage
-----------------

::

    tomwer canvas my_workflow.ows
