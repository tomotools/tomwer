Scan / Process Supervision - Advancements
-----------------------------------------

From the Orange canvas, you can display the status of core processes regarding each scan. There are two ways to access this:

* The recommended method (if you have installed the `orange3 fork <https://github.com/payno/orange3>`) is through the embedded "object supervisor" window. This is accessible from the Orange canvas interface: `View -> Object Supervisor`.
* Alternatively, you can create an "advancement" widget from the control panel.

.. image:: img/process_supervision.png
   :width: 600 px
   :align: center

Each process will notify the `ProcessManager` when it processes a scan and specify the state of the scan regarding the process.

States can be:

* ON_GOING
* SUCCEED
* FAILED
* PENDING
* SKIPPED

Updates and synchronization are managed by Tomwer using the `SuperviseProcess` class. Additionally, you can filter scans and processes by their names.

.. image:: img/process_supervision_widget.png
   :width: 600 px
   :align: center
