User Input(s) Formatting and Keywords
"""""""""""""""""""""""""""""""""""""

Since version 1.0, users can use string formatting for several values such as output folders.

This is the case for example for the following output values:

* NXtomo output directory for the NXtomomill widgets (`h52nx` and `edf2nx`)
* NXtomo output directory for the data listener
* Volume cast output directory

.. image:: img/output_formatting.png
   :width: 500 px
   :align: center
   :alt: Example of user input formatting

In the upper screenshot, for example, users redefine the output folder using text formatting.
The following keywords can be used for formatting:

* Formatting relative to a scan:
    * `scan_dir_name`: Calls the `TomwerScanBase.scan_dir_name()` function. If a scan is located at '/data/visitor/exp/beamline/date/raw/sample/dataset.h5', the function will return 'sample'.
    * `scan_basename`: Calls the `TomwerScanBase.scan_basename()` function. If a scan is located at '/data/visitor/exp/beamline/date/raw/sample/dataset.h5', the function will return '/data/visitor/exp/beamline/date/raw/sample/'.
    * `scan_parent_dir_basename`: Calls the `TomwerScanBase.scan_parent_dir_basename()` function. If a scan is located at '/data/visitor/exp/beamline/date/raw/sample/dataset.h5', the function will return '/data/visitor/exp/beamline/date/raw/'.

* Formatting relative to a volume:
    * `volume_data_parent_folder`: Calls the `TomwerVolumeBase.volume_data_parent_folder()` function. If a volume is located at '/path/to/my/volume.h5', the function will return '/path/to/my/volume'.

.. note:: If you feel that other keywords could be useful, please let us know.
