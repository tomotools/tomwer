User Guide
==========

.. toctree::
   :maxdepth: 1
   :hidden:

   
   install.rst
   language.rst
   tomotools_training.rst
   slurm.rst
   troubleshooting.rst
   python_scripts/index.rst
   data_portal/index.rst
   process_supervision.rst
   input_text_formatting_and_keywords.rst
   multi_entry_file.rst
   list_of_scan.rst

.. |Python img| image:: python_scripts/img/python.svg
  :width: 20

.. |slurm img| image:: img/icons/slurm.svg
  :width: 20

.. |process supervision img| image:: img/icons/process_supervision.svg
  :width: 20

.. |list of scans img| image:: img/icons/list_of_scan.svg
  :width: 20

.. grid:: 2

   .. grid-item-card::
      :octicon:`tools`
      :doc:`install`

   .. grid-item-card::
      :octicon:`book`
      :doc:`language`

   .. grid-item-card::
      :octicon:`mortar-board`
      :doc:`tomotools_training`

   .. grid-item-card::
      |slurm img|
      :doc:`slurm`

   .. grid-item-card::
      :octicon:`bug`
      :doc:`troubleshooting`

Advanced
^^^^^^^^

.. |input_text_formatting_and_keywords img| image:: img/icons/input_text_formatting_and_keywords.svg
  :width: 20

.. |multi_entry_file img| image:: img/icons/multi_entry_file.svg
  :width: 20

.. grid:: 2

   .. grid-item-card::
      |Python img|
      :doc:`python_scripts/index`

   .. grid-item-card::
      :octicon:`database`
      :doc:`data_portal/index`

   .. grid-item-card::
      |process supervision img|
      :doc:`process_supervision`

   .. grid-item-card::
      |input_text_formatting_and_keywords img|
      :doc:`input_text_formatting_and_keywords`

      `Input Text Formatting and Keywords`

   .. grid-item-card::
      |multi_entry_file img|
      :doc:`multi_entry_file`

      `Multi-Entry File`

   .. grid-item-card::
      |list of scans img|
      :doc:`list_of_scan`