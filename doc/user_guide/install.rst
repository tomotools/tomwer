Installation
::::::::::::

Tomwer relies on `nabu <https://gitlab.esrf.fr/tomotools/nabu>`_ to process the tomographic reconstruction.

As of today, the software is only compatible with Linux. 

.. image:: img/Tux.svg
   :width: 100 px
   :align: center

Step 1 - Install Tomwer
'''''''''''''''''''''''

To install Tomwer with all features, use:

.. code-block:: bash

    pip install tomwer[full]

Alternatively, you can install the main branch directly from GitLab:

.. code-block:: bash

    pip install git+https://gitlab.esrf.fr/tomotools/tomwer/#egg=tomwer[full]

Step 2 - Update Forks for Orange-Canvas-Core and Orange-Widget-Base
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

To access processing wheels and the reprocess action, you might want to install forks of `orange-canvas-core` and `orange-widget-base`. This is optional; the project works with native Orange projects as well.

.. code-block:: bash

    pip install git+https://github.com/payno/orange-canvas-core --no-deps --upgrade
    pip install git+https://github.com/payno/orange-widget-base --no-deps --upgrade

Launching Applications
::::::::::::::::::::::

After installation, Tomwer includes several applications that can be launched with:

.. code-block:: bash

   tomwer appName {options}

.. note:: If you call `tomwer` without any arguments, the man page will be displayed.

.. note:: You can access help for each application using:

    .. code-block:: bash

       tomwer appName --help

Tomwer Canvas
'''''''''''''

You can launch the canvas to create workflows from the different 'bricks':

.. code-block:: bash

   tomwer canvas

.. note:: `tomwer canvas` launches 'orange-canvas' with additional processing (such as setting log levels for some libraries). It is recommended to use `tomwer canvas` instead of `orange-canvas`.

Documentation
::::::::::::::

To build the documentation, use:

.. code-block:: bash

    sphinx-build doc build/html

The documentation will be built in `build/html`, and the entry point is `index.html`:

.. code-block:: bash

   firefox build/html/index.html

.. note:: Building the documentation requires Sphinx. If it is not installed, you may need to install it separately.

To generate documentation accessible from the Orange GUI (by pressing F1), use:

.. code-block:: bash

   sphinx-build doc build/html -b htmlhelp

Tomwer with BLISS Bliss-Tomo
::::::::::::::::::::::::::::

Tomwer and `Bliss-Tomo <https://gitlab.esrf.fr/tomo/bliss-tomo/>`_ (a tomography framework for BLISS) can share information.

+ With the :ref:`data listener` using RPC commands. When a scan is started and finished, it will send a command for Tomwer to process it. This requires the `Bliss-Tomo` framework to be activated at the beamline. Please check with the beamline supervisor.

Tomwer Installation at ESRF
:::::::::::::::::::::::::::

Installation is managed using `automatix <https://gitlab.esrf.fr/tomotools/automatix>`_.

On the SLURM Cluster
''''''''''''''''''''

Users can activate Python virtual environments within the Tomotools suite by sourcing the script `/scisoft/tomotools/activate {version}` with the desired version. This script handles the node architecture and special version names such as `dev` or `stable`.

.. code-block:: bash

   module load tomotools  # or module load tomotools/{version==dev|stable|1.x} for a specific version

At Beamlines
''''''''''''

On beamlines, installations are typically done in `/nobackup` partitions. Locations can be found in `https://gitlab.esrf.fr/tomotools/automatix/-/blob/master/deployment/cfg_esrf_envs.py`.

To simplify activation and launching of the Orange Canvas, a `tomwer-{version}` alias is usually created:

.. code-block:: bash

   tomwer-dev canvas [options]
