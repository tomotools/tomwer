Tomotools Trainings
"""""""""""""""""""

Each year, ESRF beamlines are provided with training on Tomotools. The training covers all interfaces (Python, CLI) and the various components of the software (Tomwer, Nabu, Nxtomomill, etc.).

You can find the latest training materials here: https://tomotools.gitlab-pages.esrf.fr/training/

The hosting GitLab project is available at: https://gitlab.esrf.fr/tomotools/training
