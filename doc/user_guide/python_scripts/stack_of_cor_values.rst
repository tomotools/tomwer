Create a Text File with CoR Values
**********************************

In the following script, the Python script will collect 'CoR' (Center of Rotation) values and store them in a dedicated file, then pass the `in_data` to the next widget.

This script must be used with a widget that computes the Center of Rotation (CoR) upstream, such as:

.. image:: img/stack_of_cor_values/workflow.png
   :align: center

Script
------

.. code-block:: python

    import os
    from silx.io.dictdump import dicttoini, load as load_ini

    # in_data can be None if several data are provided to the widget and/or if the link is removed.
    if in_data is not None:
        cor_relative = in_data.axis_params.relative_cor_value
        cor_abs = in_data.axis_params.absolute_cor_value

        # Output file that will store the CoR.
        # If in_data is a scan contained in /data/visitor/exp/beamline/date/raw/sample/dataset.h5
        # The output file will be /data/visitor/exp/beamline/date/output_cor.ini
        output_cor_file = os.path.join(os.path.dirname(in_data.path), "output_cor.ini")

        if os.path.exists(output_cor_file):
            # If a file already exists, load the CoR values
            existing_cors = load_ini(output_cor_file, "ini")
        else:
            # Else, initialize the dictionary
            existing_cors = {"cors": {}}

        # Update CoR values. In this case, per dataset we will store the tuple (absolute CoR, relative CoR)
        existing_cors["cors"][os.path.basename(in_data.path)] = (cor_abs, cor_relative)

        # Update the output CoR file
        dicttoini(existing_cors, output_cor_file)

.. note:: If you have any Python script use cases of your own, please let us know, and we can add them to this page or create a dedicated widget if this is of common interest.
