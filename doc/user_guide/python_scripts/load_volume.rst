Load a Volume as a Python NumPy Array
*************************************

In the following script, we expect a link sharing the 'volume' information to exist between a Tomwer widget (`cast volume`, `nabu volume reconstruction`, etc.) and a `Python script` widget.

.. code-block:: python

    # What is passed through the link is an instance of a volume.
    volume = in_volume
    # The documentation for the tomoscan volume (and API) is available here: https://tomotools.gitlab-pages.esrf.fr/tomoscan/tutorials/volume.html
    # The volume data can be obtained as a NumPy array from volume.data
    # print(volume.data)
    # The volume metadata can be obtained as a dictionary from volume.metadata
    # print(volume.metadata)
    # However, most of the time, the data is still on disk and not loaded in memory.
    # To load it into memory, we must call:
    volume.load()
    print("Data shape is", volume.data.shape)
    print("Metadata are", volume.metadata)

    # Note: If you want to browse the data without loading it all into memory, 
    # there are utility functions in the volume API, like `browse_slices`, that could help you.

Some useful links:

* `Volume documentation <https://tomoscan-esrf.readthedocs.io/en/latest/tutorials/volume.html>`_
* `Volume tutorial <https://tomoscan-esrf.readthedocs.io/en/latest/tutorials/volume.html>`_
* `Volume API <https://tomoscan-esrf.readthedocs.io/en/latest/_generated/tomoscan.volumebase.VolumeBase.html#tomoscan.volumebase.VolumeBase>`_
