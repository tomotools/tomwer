Define Default Directory for Qt (Input) Dialog
**********************************************

Sometimes it can be inconvenient to browse paths manually when selecting files or datasets (such as in the scan selector). A solution is to use the `TOMWER_DEFAULT_INPUT_DIR` environment variable to define the default location for these dialogs. 

You can set this environment variable before launching Tomwer from the terminal:

.. code-block:: bash

    export TOMWER_DEFAULT_INPUT_DIR={location}

Alternatively, you can set it within a 'Python' widget in Tomwer:

.. code-block:: python

    import os
    os.environ["TOMWER_DEFAULT_INPUT_DIR"] = "my/new/default/path"

.. warning:: Do not forget to execute (run) the script to activate the new environment variable.
