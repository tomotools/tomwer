Overwrite rotation angles
=========================

In the case you want to edit rotation angles of an NXtomo you can force those values from a python script.

Usually the best would be to overwrite them just after the conversion to an NXtomo like:


.. image:: img/overwrite_angles/workflow_overwrite.png

The following is an example on how to overwrite rotation angles (thanks to marta):

.. code-block:: python

    import numpy as np
    import h5py
    from nxtomo.application.nxtomo import NXtomo
    from tomwer.core.scan.nxtomoscan import NXtomoScan

    scan = in_data

    # the scan contains some dark and flat. angles.txt only contains the projection angles
    angles = [0]*41
    angles.extend(
        np.loadtxt("/data/visitor/ih.../angles.txt")
    )

    new_entry=f"{scan.entry}_edited"

    nx_tomo = NXtomo().load(scan.master_file,scan.entry)
    nx_tomo.sample.rotation_angle=angles
    nx_tomo.save(scan.master_file, new_entry)

    out_data = NXtomoScan(scan=scan.master_file,entry=new_entry)

