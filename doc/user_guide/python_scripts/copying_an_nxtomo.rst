Copying a NXtomo
*****************

With Recent Software Versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Usually, the NXtomo contains relative links, so you cannot simply copy and paste it.

Starting from Nxtomomill 1.0, you can access the `nxtomomill nx-copy function <https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/copynx.html>`_.
You can also use the `nxtomo <https://gitlab.esrf.fr/tomotools/nxtomo>`_ Python API of the `NXtomo` object. The `copy_nxtomo_file` function is used as follows:

.. code-block:: python

    from nxtomo.application.nxtomo import copy_nxtomo_file
    copy_nxtomo_file(
        input_file="/path/to/my_nx.nx",
        output_file="/new/path/new_nx.nx",
        entries=None,  # or a tuple of existing NXtomo entries
    )

With Older Software Versions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The simplest way to copy such a file is to use the `nxtomomill.nexus` module.
You can load it with the default parameters (`detector_data_as="as_data_url"`) and save it to a new location.

.. code-block:: python

    from nxtomo.application.nxtomo import NXtomo
    from tomwer.core.scan.nxtomoscan import NXtomoScan

    if in_data is not None:
        scan = in_data
        nx_tomo = NXtomo().load(scan.master_file, scan.entry)  # load data
        new_location = "/my/path/to/nexus.nx"
        new_entry = scan.entry  # or any data path in the HDF5 file you like
        nx_tomo.save(new_location, new_entry)
        print(f"Created new NXtomo to {new_entry}@{new_location}")
        # provide the new scan in output if some processing must be done on this new NXtomo
        out_data = NXtomoScan(scan=new_location, entry=new_entry)

.. warning:: Be careful! The saved file will still contain relative links to the original data. You can avoid this by setting the `load` function option `detector_data_as` to `as_numpy_array`.
             However, this will load all the data into memory.

.. warning:: Only the frames are shared; other datasets (rotation angle, energy, etc.) will be decoupled. So if you edit one dataset in the NXtomo, it won't affect the other one.
