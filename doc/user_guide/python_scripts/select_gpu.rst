Select a GPU
************

Currently, Nabu works on a single GPU. If you have multiple processes that require a GPU, it can be useful to select a specific one to avoid conflicts.

You can do this either from a terminal before launching Tomwer or from a 'Python script' widget. In this example, we have more than one GPU (n >= 0) and we want to use the first one.

From a `Python script` widget
-----------------------------

.. code-block:: python

    import os
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"

From a `Terminal`
-----------------

.. code-block:: bash

    # Define the GPU to be used
    export CUDA_VISIBLE_DEVICES=0
    # Start Tomwer canvas
    tomwer canvas

.. hint::

    You can list accessible GPU devices on a computer or slurm node by using `nvidia-smi` from a terminal.

    .. image:: img/select_gpu/nvidia_smi.png
        :width: 600 px
        :align: center
