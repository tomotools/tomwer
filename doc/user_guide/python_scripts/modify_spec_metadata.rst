Modify Some EDF Metadata
************************

If a value is incorrect in a `.info` file and you need it for running a reconstruction, you will need to modify the `.info` file to share this information with Nabu.
In this example:

* Copy the original file to a `*_raw.info` file (to be safer).
* Modify the "Distance" metadata.
* Reset the Tomwer object's "distance" attribute to ensure coherence.
* Set the output scan to continue processing.

.. code-block:: python

    import os
    import shutil
    from tomwer.core.scan.edfscan import EDFTomoScan
    from silx.io.dictdump import dicttoini, load as load_ini

    def get_key(line):
        """Return the key name of a .info file"""
        return line.split("=")[0].replace(" ", "")

    def get_raw_dict(file_path):
        """Return the .info file metadata as a dictionary with metadata name as key and raw line as value"""
        metadata = {}
        with open(file_path, mode="r") as f:
            line = f.readline()
            while line:
                key = get_key(line)
                metadata[key] = line
                line = f.readline()
        return metadata

    scan = in_data

    if isinstance(scan, EDFTomoScan):
        # Copy the .info file
        info_file = scan.get_info_file(scan.path)
        raw_info_file = info_file[:-5] + "_raw.info"
        if not os.path.exists(raw_info_file):
            # Copy the original file only the first time. Otherwise, we will overwrite it with an updated file.
            shutil.copyfile(
                info_file,
                raw_info_file,
            )
        metadata = get_raw_dict(info_file)
        # Overwrite the "Distance" key
        new_distance = 0.02598
        metadata["Distance"] = "Distance= \t\t" + str(new_distance) + "\n"
        with open(info_file, mode="w") as f:
            for _, line in metadata.items():
                f.write(line)
        # Reset the distance key for coherence
        scan._distance = None
    out_data = scan
