Filter Scans with Failed Processes
**********************************

Here is an example of how to retrieve the processing states of a dataset over its lifetime.
This could be used as pre-processing for 'email notifications,' for example, so that you only receive a notification when a scan has experienced a failure during processing.

.. code-block:: python

    from processview.core.manager import ProcessManager, DatasetState
    scan = in_data

    def get_processes_with_state(state_search, tomo_obj):
        states = {}
        for process in ProcessManager().get_processes():
            state = ProcessManager().get_dataset_state(
                dataset_id=tomo_obj.get_identifier(),
                process=process,
            )
            if state is state_search:
                states[process] = state
        return states

    failed_processes = get_processes_with_state(DatasetState.FAILED, tomo_obj=scan)
    if len(failed_processes) > 0:
        out_data = scan
    else:
        out_data = None
