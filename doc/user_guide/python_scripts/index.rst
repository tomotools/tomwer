Python Scripts
**************

Users can add their own processing using a Python widget to the workflow, as presented in this video: [`Python Widget Example`](https://www.youtube.com/watch?v=VGv628AIgn8).

The Python widget defines a series of inputs and outputs.
Inputs are variables provided when the widget is triggered.
Outputs must be defined in the Python widget to provide inputs to the next widget.

Currently, this widget can have the following inputs:

* `in_data`, `in_datas`: Correspond to the `data` object in Tomwer (also known as `scan`). Warning: if several 'data' inputs are provided, then 'in_data' will be set to None, and `in_datas` will collect all the `in_data` inputs provided.
* `in_volume`, `in_volumes`: Correspond to the `volume` object in Tomwer. Handling of `in_volumes` vs `in_volume` is the same as `in_data` and `in_datas`.
* `in_future_object`, `in_future_objects`: Correspond to `future` objects in Tomwer.
* `in_configuration`, `in_configurations`: Correspond to `configuration` objects in Tomwer (Python dict expected).
* `in_cluster_config`, `in_cluster_configs`: Python dictionary corresponding to a cluster configuration.
* `in_serie`, `in_series`: Correspond to `serie` objects in Tomwer (series of `scan` or `volume`).

Existing outputs are:

* `out_data`
* `out_volume`
* `out_future_tomo_obj`
* `out_object`
* `out_configuration`
* `out_cluster_config`
* `out_serie`

Here are a few examples of such scripts:

.. toctree::
   :maxdepth: 1
   :hidden:

   copying_an_nxtomo.rst
   search_nx_tomo_modify_and_resume_processing.rst
   filtering_scans.rst
   holo_tomo_normalize_reconstructed_slices.rst
   load_volume.rst
   modify_spec_metadata.rst
   overwrite_angles.rst
   remove_volume.rst
   saving_reconstruction_to_another_place.rst
   select_gpu.rst
   stack_of_cor_values.rst
   working_directory.rst

.. grid:: 4

   .. grid-item-card::
      :img-top: img/nxtomo_copy.svg

      :doc:`copying_an_nxtomo`

   .. grid-item-card::
      :img-top: img/search_nxtomo.svg

      :doc:`search_nx_tomo_modify_and_resume_processing`

   .. grid-item-card::
      :img-top: img/modify_latest_reconstruction/workflow.png

      :doc:`saving_reconstruction_to_another_place`

   .. grid-item-card::
      :img-top: img/python.svg

      :doc:`filtering_scans`

   .. grid-item-card::
      :img-top: img/python.svg

      :doc:`holo_tomo_normalize_reconstructed_slices`

   .. grid-item-card::
      :img-top: img/load_volume.svg

      :doc:`load_volume`

   .. grid-item-card::
      :img-top: img/modify_spec_metadata.svg

      :doc:`modify_spec_metadata`

   .. grid-item-card::
      :img-top: img/stack_of_cor_values/workflow.png

      :doc:`stack_of_cor_values`

   .. grid-item-card::
      :img-top: img/working_directory.svg

      :doc:`working_directory`

   .. grid-item-card::
      :img-top: img/select_gpu/icon.svg

      :doc:`select_gpu`

   .. grid-item-card::
      :img-top: img/remove_volume/remove_volume.svg

      :doc:`remove_volume`

   .. grid-item-card::
      :img-top: img/overwrite_angles/overwrite_angles.svg

      :doc:`overwrite_angles`

.. warning:: GUI Freeze

    The Python script will be executed in the main Qt thread.
    As a consequence, during its execution, the GUI will not be responsive.
    If your processing is heavy, you might experience some GUI freezing.

    If the Python script is triggered before the processing finishes, Orange will fail to process anything.
    As a result, the GUI may freeze indefinitely. For advanced processing, one workaround could be to add a `timer` widget before.
