Providing / Searching a List of Scans
"""""""""""""""""""""""""""""""""""""

Currently, there are several options for searching scans:

* **Data Listener**: Used when a Bliss scan is acquired.
* **Scan Discovery**: Allows searching for scans (Bliss - NXtomo - EDF) with possible filtering options.
* **Scan Watcher**: Similar to Scan Discovery, but it re-triggers the discovery process at regular intervals until stopped by the user.

However, if these search options are limited for your use cases, here are two possible workarounds:

Get Input as a List of Files
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
One method is to obtain a list of files (or folders, in the case of EDF) as text, separated by newline (`\n`) or semicolon (`;`).

For example, you can generate this list by executing:

.. code-block:: bash

    # For example, list all .nx (NXtomo) files for a specific proposal
    ls -d /data/visitor/es1303/id19/20230713/PROCESSED_DATA/*/*/*.nx

The output will look like this:

.. code-block:: text

    /data/visitor/es1303/id19/20230713/PROCESSED_DATA/test_TUBS-M/test_TUBS-M_nominal_700_degree/test_TUBS-M_nominal_700_degree_0000.nx
    /data/visitor/es1303/id19/20230713/PROCESSED_DATA/test_TUBS-M/test_TUBS-M_nominal_700_degree/test_TUBS-M_nominal_700_degree_0001.nx
    /data/visitor/es1303/id19/20230713/PROCESSED_DATA/test_TUBS-M/test_TUBS-M_nominal_700_degree/test_TUBS-M_nominal_700_degree_0002.nx
    /data/visitor/es1303/id19/20230713/PROCESSED_DATA/test_TUBS-M/test_TUBS-M_nominal_700_degree/test_TUBS-M_nominal_700_degree_0003.nx
    ...

You can then copy this list to the clipboard and paste it into one of the 'list' widgets via a right-click 'paste' (e.g., scan selector, NXtomomill converters) — since version 1.2.

.. image:: img/paste_clipboard.png

.. image:: img/paste_clipboard_result.png

Get It from a Python Script and Use the 'Hub' Widget
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Alternatively, you can create a list of `TomwerScanBase` instances (typically `NXtomoScan`) and pass it to the workflow.

Here’s an example Python script that searches for files and creates scan objects:

.. code-block:: python

    from glob import glob
    from tomwer.core.scan.scanfactory import ScanFactory

    scans = []
    # Create Tomwer scans by browsing input files
    for file_path in glob("/data/visitor/es1303/id19/20230713/PROCESSED_DATA/*/*/*.nx"):
        scans.extend(ScanFactory.create_scan_objects(file_path))
    
    # This will trigger a list of scans
    out_tomo_objs = scans

.. note:: In the following examples, we are producing a list of scans (as `tomo_objs`). Most widgets expect a single scan or tomo object as input. To split the list, you can use the 'Tomo Objs Hub' in the 'Other' section, like this:

.. image:: img/tomo_objs_hub_example.png
