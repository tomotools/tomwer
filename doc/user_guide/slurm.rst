Recommended Way to Use (ESRF) SLURM Cluster
"""""""""""""""""""""""""""""""""""""""""""

There are several ways to use the SLURM cluster at ESRF. Given the limited resources (especially GPUs), here is the recommended approach to use the SLURM cluster with Tomwer:

1. **Connect to the SLURM cluster front-end:**

.. code-block:: bash

    ssh -XC cluster-access

2. **Connect to the 'interactive' partition in 'interactive' mode:**

.. code-block:: bash

    salloc --partition interactive --x11 srun --pty bash -l

3. **On the node, activate the version of Tomwer that you want to use and start the `canvas` application:**

.. code-block:: bash

    module load tomotools
    tomwer canvas

You should now see the 'canvas' interface.

.. image:: img/canvas_interface.png
    :width: 600 px
    :align: center

.. hint:: 

    You can specify a version of Tomotools when you load the module (like dev or stable):

    .. code-block:: bash

        module load tomotools/{dev|stable|1.x}

4. **Trigger all jobs requiring a GPU (e.g., Nabu reconstructions) remotely.**

Here are two examples:

* Reconstructing one slice over SLURM:

.. image:: img/example_slice_reconstruciton_over_slurm.png
    :width: 600 px
    :align: center

* Reconstructing one slice and a full volume over SLURM:

.. image:: img/example_slice_and_volume_over_slurm.png
    :width: 600 px
    :align: center

Remember that some examples of workflows using SLURM are available in `examples/remote processing with SLURM`, and a video demonstrating this use case is available:

* https://youtu.be/HYufV7Ya9v8

.. warning::

    Please remember that all 'Nabu' tasks will require a GPU, so you will not be able to run them locally on the 'interactive' partition or any node without a dedicated GPU.
