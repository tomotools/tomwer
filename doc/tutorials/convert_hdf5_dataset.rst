How to Convert a .hdf5 Dataset to a Nexus Compliant Dataset
-----------------------------------------------------------

The tool used to convert a Bliss .hdf5 dataset to a Nexus compliant dataset is `nxtomomill <https://gitlab.esrf.fr/tomotools/nxtomomill>`_.
You can either use the `nxtomomill _tomoh52nx <https://tomotools.gitlab-pages.esrf.fr/nxtomomill/tutorials/tomoh52nx.html>`_ tool directly, or use the
'NXTomomill widget' from tomwer.

.. note:: Tomwer only accepts Nexus compliant (NXtomo) datasets. That is why conversion is necessary.
