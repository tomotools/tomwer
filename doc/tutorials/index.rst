Tutorials
=========

Video Tutorials
---------------

.. grid:: 3

   .. grid-item-card::
      :img-top: img/tuto_icons/first_steps.svg

      `First Steps <https://www.youtube.com/watch?v=oQtOeDmLYj0&list=PLddRXwP6Z6F8bX5lh_fPSOtdGjIIDVebq>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/cor_icon.svg

      `Determine Center of Rotation <https://www.youtube.com/watch?v=G9G55lGuDeA&list=PLddRXwP6Z6F-pflMDtI-SyiI4MLQnEAfd>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/reduced_dark_flat.png

      `Reduce Darks and Flats <https://www.youtube.com/watch?v=aBLTWmuMTcQ&list=PLddRXwP6Z6F-sms76AibVaPv9AS5VwuB0>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/edit_nxtomo.svg

      `Edit a NXtomo <https://www.youtube.com/watch?v=4yrEBrAmwtY&list=PLddRXwP6Z6F8dL0fcRM7g_TOOezuh8Cvy>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/volumes_icon.svg

      `Volume <https://youtu.be/gQRWCsyIm7U>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/all_videos.svg

      `All Videos <https://www.youtube.com/watch?v=oQtOeDmLYj0&list=PLddRXwP6Z6F8bX5lh_fPSOtdGjIIDVebq>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/stitching_icon.svg

      `Stitching <https://www.youtube.com/playlist?list=PLddRXwP6Z6F-OX0WfBnrpf8T7_tuG13Rq>`_

   .. grid-item-card::
      :img-top: img/tuto_icons/remote_icon.svg

      `Accessing Tomotools from visa.esrf.fr <https://youtu.be/nERMMj3y64M?list=PLddRXwP6Z6F-sRQxBqord8ufli9Z8e3k0>`_

All existing videos are available at: https://www.youtube.com/@tomotools

Beamlines Tutorials
-------------------

.. card:: ID19

   `ID19 Remote Reconstruction Tutorial <https://confluence.esrf.fr/x/LetQB>`_ thanks to L. Broche.

.. card:: ID11

   `ID11 Running Tomwer Canvas with Scheduled Jobs on P9 <https://confluence.esrf.fr/x/FwzEAg>`_ thanks to P.O. Autran.

Text Tutorials
--------------

Tomwer Canvas
-------------

.. toctree::
   :maxdepth: 1
   :hidden:

   convert_hdf5_dataset.rst
   launchorange.rst
   notebooks/canvas/part_0_overview
   notebooks/canvas/part_1_basic_reconstruction
   notebooks/canvas/part_2_cor_search
   notebooks/canvas/part_3_dark_flat
   notebooks/canvas/part_5_volume_casting
   notebooks/canvas/part_6_edit_metadata
   notebooks/canvas/part_7_submit_job_to_slurm

.. grid:: 3

   .. grid-item-card::
      :img-top: img/tuto_icons/bash_icon.svg

      :doc:`launchorange`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_0_overview`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_1_basic_reconstruction`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_2_cor_search`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_3_dark_flat`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_5_volume_casting`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_6_edit_metadata`

   .. grid-item-card::
      :img-top: img/tuto_icons/jupyter_notebook.svg

      :doc:`notebooks/canvas/part_7_submit_job_to_slurm`

   .. grid-item-card::
      :img-top: img/tuto_icons/bash_icon.svg

      :doc:`convert_hdf5_dataset`

Tomwer Standalone App
---------------------

.. toctree::
   :maxdepth: 1
   :hidden:

   notebooks/standalone_apps/z_stitching

.. grid:: 3

   .. grid-item-card::
      :img-top: img/tuto_icons/stitching_icon.svg

      :doc:`notebooks/standalone_apps/z_stitching`

Ewoks Tutorials
---------------

.. toctree::
   :maxdepth: 1
   :hidden:

   trigger_workflow_from_command_line
   batch_processing.rst

.. grid:: 3

   .. grid-item-card::
      :img-top: img/tuto_icons/bash_icon.svg

      :doc:`trigger_workflow_from_command_line`

   .. grid-item-card::
      :img-top: img/tuto_icons/bash_icon.svg

      :doc:`batch_processing`
