Launch Orange
-------------

If Orange is installed using a Python virtual environment, first activate the Python virtual environment. For example:

.. code-block:: bash

    source /orange3venv/bin/activate

To launch Orange:

.. code-block:: bash

    orange-canvas

The following canvas should appear:

.. image:: img/orangeStart.png

On the left, you can see the different add-ons. Each contains several widgets. This section is also called the 'toolbox.'
The Tomwer add-on is divided into three categories:

- **Control**: Data control, providing input or listening to a Bliss acquisition.
- **Reconstruction**: Data reconstruction of slices using Nabu.
- **Visualization**: Displaying information about data.

The canvas is in the 'main window' (top right), where you can define a workflow to process.

At the bottom, there is a log window to trace the process flow.
