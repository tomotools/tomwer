{
    "cells": [
     {
      "cell_type": "markdown",
      "id": "382ba534-9991-45e5-9af4-c4c24a96a304",
      "metadata": {
       "slideshow": {
        "slide_type": "slide"
       }
      },
      "source": [
       "# Reduced Dark and Flat\n",
       "\n",
       "In order to perform the flat field correction (optional in nabu), an acquisition must contain `reduced` dark and flats.\n",
       "\n",
       "These `reduced` darks and flats come from raw frames: 'darks' and 'flats'.\n",
       "In general, we expect these frames to be part of the NXtomo and call the `dark and flat field construction` widget to generate the reduced ones."
      ]
     },
     {
      "cell_type": "markdown",
      "id": "fcd09b4d-7cbb-47a2-869a-374ae0694b46",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "## Reduced Dark and Flat Field Widget ![dark flat](img/icons/darkflatwidget_icon.png)\n",
       "\n",
       "This is usually the first processing to run. This way, the flat field correction can be done and is useful and/or required by many processes.\n",
       "\n",
       "By default, the `reduced` dark(s) are obtained by computing the `mean` of raw dark frames and the `reduced` flat(s) are obtained by computing the `median` of the flat(s)."
      ]
     },
     {
      "cell_type": "code",
      "execution_count": null,
      "id": "45733342-2822-41dd-95b5-e27d26dca146",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "outputs": [],
      "source": [
       "from IPython.display import Video\n",
       "\n",
       "Video(\"video/reduced_darks_flats_widget.mp4\", embed=True, height=500)"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "2707b3c8-2174-48aa-8694-41fb23c5efc1",
      "metadata": {
       "slideshow": {
        "slide_type": "notes"
       }
      },
      "source": [
       "* Since *tomoscan==1.0* (and *nabu==2022.2*, *tomwer==1.0*), reduced darks and flats are saved under `{dataset_prefix}_darks.hdf5` and `{dataset_prefix}_flats.hdf5` files.\n",
       "* For spec acquisition, the reduced darks and flats will be duplicated (to dark.edf and refHSTXXXX.edf to ensure backward compatibility)."
      ]
     },
     {
      "cell_type": "markdown",
      "id": "db5d9e29-9f34-4421-80e7-dbb239b6e1ac",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "Here is a screenshot of a reduced flats file containing a single series of flats at the beginning.\n",
       "\n",
       "![screenshot reduced darks](img/reduced_darks_screenshot.png)"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "7400cd8c-3bce-4ff6-a7a7-cc1f19d8fff4",
      "metadata": {
       "slideshow": {
        "slide_type": "slide"
       }
      },
      "source": [
       "## Copying Dark(s) and Flat(s)\n"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "85a3fadf-098e-425f-a5ac-cb5a030bd03b",
      "metadata": {
       "slideshow": {
        "slide_type": "fragment"
       }
      },
      "source": [
       "In some cases, it might happen that users need to reuse reduced darks or flats.\n",
       "\n",
       "If the reduced darks and flats already exist, you can simply use the 'copy' function from the `reduced dark and flat widget`."
      ]
     },
     {
      "cell_type": "markdown",
      "id": "e98e11c7-cd2e-4460-a1d1-097f70fea46d",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "!!! The copy option is activated by default !!!\n",
       "\n",
       "* `auto` mode: Each time it meets a dataset with reduced dark/flat, it will keep them in cache. When it meets a dataset with dark/flat missing, it will copy them to it.\n",
       "\n",
       "![dark_flat_copy](img/dark_flat_copy.png)\n"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "79846061",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "* `manual` mode: User can provide a URL to well-formed reduced darks and reduced flats HDF5 dataset.\n",
       "\n",
       "![dark_flat_copy copy interface](img/dark_flat_copy_manual.png)"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "251f6b75",
      "metadata": {
       "slideshow": {
        "slide_type": "notes"
       }
      },
      "source": [
       "Note: The darks and flats cache file is provided at the bottom of the widget. This can be useful to check if the registration goes as expected."
      ]
     },
     {
      "cell_type": "markdown",
      "id": "a435f7f6-9bea-4256-a54c-748f5cd231b9",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "### From a Python Script"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "a5bd34a9-669d-499d-8343-f99de29e0a45",
      "metadata": {
       "slideshow": {
        "slide_type": "fragment"
       }
      },
      "source": [
       "You can also create these following the Tomoscan Python API, like:\n",
       "\n",
       "* Provide reduced darks and flats as a dictionary (key is the index and value is a 2D numpy array).\n",
       "\n",
       "```python\n",
       "from tomwer.core.scan.nxtomoscan import NXtomoScan\n",
       "# from tomwer.core.scan.edfscan import EDFTomoScan\n",
       "# same API for EDF or HDF5\n",
       "\n",
       "scan = NXtomoScan(file_path, data_path)\n",
       "darks = {\n",
       "    0: numpy.array(...),  # darks at start\n",
       "}\n",
       "flats = {\n",
       "    0: numpy.array(...),  # flats at start\n",
       "    3000: numpy.array(...),  # flats at end\n",
       "}\n",
       "\n",
       "scan.save_reduced_darks(darks)\n",
       "scan.save_reduced_flats(flats)\n",
       "\n",
       "```\n",
       "\n",
       "* Provide reduced darks and flats from an already existing HDF5 dataset.\n",
       "\n",
       "```python\n",
       "from tomoscan.esrf.scan.utils import copy_darks_to, copy_flats_to\n",
       "\n",
       "# Create darks and flats as numpy arrays\n",
       "darks = {\n",
       "    0: numpy.ones((100, 100), dtype=numpy.float32),\n",
       "}\n",
       "flats = {\n",
       "    1: numpy.ones((100, 100), dtype=numpy.float32) * 2.0,\n",
       "    100: numpy.ones((100, 100), dtype=numpy.float32) * 2.0,\n",
       "}\n",
       "\n",
       "original_dark_flat_file = os.path.join(tmp_path, \"originals.hdf5\")\n",
       "\n",
       "dicttoh5(darks, h5file=original_dark_flat_file, h5path=\"darks\", mode=\"a\")\n",
       "dicttoh5(flats, h5file=original_dark_flat_file, h5path=\"flats\", mode=\"a\")\n",
       "\n",
       "# Create darks and flats URL\n",
       "darks_url = DataUrl(\n",
       "    file_path=original_dark_flat_file,\n",
       "    data_path=\"/darks\",\n",
       "    scheme=\"silx\",\n",
       ")\n",
       "flats_url = DataUrl(\n",
       "    file_path=original_dark_flat_file,\n",
       "    data_path=\"/flats\",\n",
       "    scheme=\"silx\",\n",
       ")\n",
       "\n",
       "# Apply the copy\n",
       "scan = NXtomoScan(...)\n",
       "copy_flats_to(scan=scan, flats_url=flats_url, save=True)\n",
       "copy_flats_to(scan=scan, flats_url=flats_url, save=True)\n",
       "```"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "314c5e9a-bbcc-483d-a07d-40885406eee7",
      "metadata": {
       "slideshow": {
        "slide_type": "skip"
       }
      },
      "source": [
       "---"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "82316ac9-55b5-4bda-bb91-e766848730e2",
      "metadata": {
       "slideshow": {
        "slide_type": "slide"
       }
      },
      "source": [
       "## Computing Reduced Flats from Projection\n",
       "\n",
       "It can happen that sometimes you have a dataset containing projections that must be used as flats to compute the reduced flats (dataset 1).\n",
       "And that these reduced flats must be used by other datasets (datasets 2*).\n",
       "\n",
       "In this case, you can do the following actions:\n",
       "\n",
       "* Preprocessing (computing reduced flats from dataset 1)\n",
       "   * Convert dataset 1 NXtomo image_key projections to flats (using `image-key-editor` or `image-key-upgrader` widget).\n",
       "   * Then compute reduced flat from those raw flats (using `reduced dark and flat` widget).\n",
       "   * Copy these reduced flats to datasets 2 (by providing URL to the reduced flats or setting them directly from the `reduced flats` input as shown in the video).\n",
       "* Processing (reconstructing datasets 2*)\n",
       "   * Compute reduced darks (from raw) and copy reduced flats from dataset 1 using the `reduced dark and flat` widget (`reduced flat` has been set during preprocessing).\n",
       "   * Then create the workflow you want to process (`default center of rotation`, `nabu slice`, and `data viewer` on the video)."
      ]
     },
     {
      "cell_type": "code",
      "execution_count": null,
      "id": "cba867f9-6936-4b3c-b0f0-ae69c8df8582",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "outputs": [],
      "source": [
       "from IPython.display import YouTubeVideo\n",
       "\n",
       "YouTubeVideo(\"vJOo0rHHUYk\", height=500, width=800)"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "2897e31b-6136-4567-8c42-0af3a4563f49",
      "metadata": {
       "slideshow": {
        "slide_type": "notes"
       }
      },
      "source": [
       "Note: If the processing is done before the flat copy is done or if it fails, then the flat field will fail. You might encounter the following error:\n",
       "```\n",
       "2023-03-06 16:04:45,967 [ERROR] cannot make flat field correction, flat not found [tomwer.core.scan.scanbase](scanbase.py:177)\n",
       "2023-03-06 16:04:45,967:ERROR:tomwer.core.scan.scanbase: cannot make flat field correction, flat not found\n",
       "```"
      ]
     },
     {
      "cell_type": "markdown",
      "id": "34edf4fd-73d0-4426-8943-e47dda824ecb",
      "metadata": {
       "slideshow": {
        "slide_type": "subslide"
       }
      },
      "source": [
       "## Hands-on - Exercise A\n",
       "\n",
       "The `/scisoft/tomo_training/part3_flatfield/WGN_01_0000_P_110_8128_D_129/` contains three NXtomo.\n",
       "\n",
       "Use the first one (WGN_01_0000_P_110_8128_D_129_0000.nx) projections as flats to compute reduced flats.\n",
       "\n",
       "Then provide these reduced flats to compute one of the two other datasets (`WGN_01_0000_P_110_8128_D_129_0001.nx` or `WGN_01_0000_P_110_8128_D_129_0002.nx`).\n",
       "\n",
       "*Note:* This dataset is provided as a `proof of concept`. Please don't be overly 'attentive' to the slice reconstruction."
      ]
     }
    ],
    "metadata": {
     "celltoolbar": "Slideshow",
     "kernelspec": {
      "display_name": "Python 3 (ipykernel)",
      "language": "python",
      "name": "python3"
     },
     "language_info": {
      "codemirror_mode": {
       "name": "ipython",
       "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.11.2"
     }
    },
    "nbformat": 4,
    "nbformat_minor": 5
   }
   