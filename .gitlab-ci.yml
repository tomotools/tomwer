stages:
  - style
  - linting
  - test
  - doc
  - deploy


variables:
  ORANGE_WEB_LOG: 'False'
  QT_QPA_PLATFORM: offscreen


.build_template:
  stage: test
  before_script:
    - arch
    - which python
    - python --version
    - python -m pip install pip setuptools wheel packaging --upgrade
    - python -m pip install fabio --upgrade --pre
    - rm -rf artifacts
    - mkdir artifacts
  tags:
    - linux


# style
black:
  stage: style
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11_glx:latest
  tags:
    - linux
  before_script:
    - pip install black
  script:
    - LC_ALL=C.UTF-8 black --check --safe .


flake8:
  stage: style
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11_glx:latest
  tags:
    - linux
  before_script:
    - pip install flake8
  script:
    - flake8 --config=.flake8


# linting
pylint:
  stage: linting
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11_glx:latest
  before_script:
    - mkdir -p artifacts/linting/
    - pip install pylint
    - python -m pip install git+https://gitlab.esrf.fr/tomotools/nabu
    - pip install .[full_no_cuda] --pre
  script:
    - pylint --errors-only --exit-zero --output-format=text --rcfile=ci/pylintrc tomwer orangecontrib | tee pylint.txt
    - N_ERRS=`wc -l < pylint.txt`
    - echo "Found $N_ERRS linting errors"
    # exit 1 must be on the "script" section. If in "after_script" for example the exit 1 will not make the test failed
    - if [ $N_ERRS != "0" ]; then exit 1; fi
  after_script:
     - mv pylint.txt artifacts/linting/
  artifacts:
    paths:
      - artifacts/linting/
    when: on_failure
    expire_in: 5h


# doc
doc:
  stage: doc
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.8_doc:latest
  extends: .build_template
  before_script:
    - rm -rf artifacts
    - mkdir artifacts
    - python -m pip install .[doc] --pre
    - pip list
  script:
    - sphinx-build -j 4 -v doc html
    - ls -lh .
  after_script:
    - ls html
    - mv html artifacts/doc
  artifacts:
    paths:
      - artifacts/doc/
    when: on_success
    expire_in: 1h
  only:
    - main
    - fix_doc


# test
.pytest:
  stage: test
  extends: .build_template
  script:
    - python -m pip install pytest-cov pytest --pre
    - python -m pip install PyQt5
    - python -m pip install git+https://gitlab.esrf.fr/tomotools/nabu
    - pip install -e .[full_no_cuda,test] -- pre
    - python -m tomwer --help
    - python -m pytest ${PYTEST_OPTIONS} src/tomwer src/orangecontrib
  variables:
    PYTEST_OPTIONS: "--import-mode=importlib --basetemp=pytest_test_dir --cov-report term-missing --cov-report html:code_coverage_infos --cov=./"
  except:
    - fix_doc

test-3.9:
  extends: .pytest
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.9_glx:latest
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

test-3.9-with-coverage-raw:
  extends: test-3.9
  after_script:
    - mv code_coverage_infos artifacts/code_coverage
  artifacts:
    paths:
      - artifacts/code_coverage/
    expire_in: 2h
  when: manual

test-3.11:
  extends: .pytest
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.11_glx:latest
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'

test-3.12:
  extends: .pytest
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.12_glx:latest

# deploy
pages:
  stage: deploy
  tags:
    - linux
  image: gitlab-registry.esrf.fr/dau/ci/pyci/python_3.9_glx:latest
  script:
    - rm -rf public
    # doc
    - mv artifacts/doc public
  after_script:
    - ls -Rl public
  artifacts:
    paths:
      - public
    expire_in: 1h
  only:
    - main
